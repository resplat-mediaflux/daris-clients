```
USAGE:
  daris-subject-update [OPTIONS] NAME

PARAMETERS:
      NAME                The name of the subject.

OPTIONS:
      --mf.config <mflux.cfg>
                          Mediaflux/DaRIS server configuration file. Defaults
                            to $HOME/.Arcitecta/mflux.cfg
  -i, --id <subject-id>   The identifier of the subject.
  -d, --description <description>
                          Description about the subject.
```

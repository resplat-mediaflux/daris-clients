```
USAGE:
  daris-download [OPTIONS] ID...

PARAMETERS:
      ID...              The identifiers of the DaRIS objects to download.

OPTIONS:
      --mf.config <mflux.cfg>
                         Mediaflux/DaRIS server configuration file. Defaults to
                           $HOME/.Arcitecta/mflux.cfg
      --output-dir <output-dir>
                         The output directory. If not specified, defaults to
                           current working directory.
      --unarchive <aar|all|none>
                         Unpack archive files. aar: unpack only archive in AAR
                           format; all: unpack all supported archive formats;
                           none: do not unpack. Defaults to aar.
      --parts <content|metadata|all>
                         Specifies which parts of the assets to download.
                           Defaults to content.
      --transcode FROM:TO[,FROM:TO...]...
                         Transcode
      --nb-workers <n>   Number of worker threads.
      --step <n>         Query result page size.
      --dataset-only     Download datasets only.
      --no-attachments   Exclude attachments.
      --overwrite        Overwrite if exists.
  -h, --help             output usage information
```

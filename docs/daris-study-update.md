```
USAGE:
  daris-study-update [OPTIONS] NAME

PARAMETERS:
      NAME              The name of the study.

OPTIONS:
      --mf.config <mflux.cfg>
                        Mediaflux/DaRIS server configuration file. Defaults to
                          $HOME/.Arcitecta/mflux.cfg
  -i, --id <study-id>   The identifier of the study.
  -d, --description <description>
                        Description about the study.
```

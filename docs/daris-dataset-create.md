```
USAGE:
  daris-dataset-create [OPTIONS] FILES_OR_DIRS...

PARAMETERS:
      FILES_OR_DIRS...     The source/input files or directories.

OPTIONS:
      --mf.config <mflux.cfg>
                           Mediaflux/DaRIS server configuration file. Defaults
                             to $HOME/.Arcitecta/mflux.cfg
  -s, --study <study-id>   The identifier of the parent study.
  -n, --name <name>        The name of the dataset. If not specified, try using
                             the file/directory name.
  -d, --description <description>
                           Description about the dataset.
  -p, --primary            Creates primary dataset. Otherwise, derived dataset.
  -t, --type <mime-type>   The MIME type of the dataset.
  -a, --archive-type <archive-type>
                           Archive type of the dataset content. zip or aar?
      -gz, --gzip          Compress the content using gzip. Ignored if the
                             content is packed as archive.
      --error-if-exists    Stop and report error if dataset with the specified
                             name already exists.
      --input-cid [<cid>...]
                           The cids of the input datasets.
```

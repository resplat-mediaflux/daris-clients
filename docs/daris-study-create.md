```
USAGE:
  daris-study-create [OPTIONS] NAME

PARAMETERS:
      NAME                 The name of the study.

OPTIONS:
      --mf.config <mflux.cfg>
                           Mediaflux/DaRIS server configuration file. Defaults
                             to $HOME/.Arcitecta/mflux.cfg
  -p, --parent-id <pid>    The identifier of the parent subject or ex-method.
  -s, --step <step>        The identifier of the step.
  -d, --description <description>
                           Description about the study.
      --ignore-if-exists   Ignore if study with the specified name already
                             exists.
```

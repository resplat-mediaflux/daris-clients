```
USAGE:
  daris-subject-create [OPTIONS] NAME

PARAMETERS:
      NAME                 The name of the subject.

OPTIONS:
      --mf.config <mflux.cfg>
                           Mediaflux/DaRIS server configuration file. Defaults
                             to $HOME/.Arcitecta/mflux.cfg
  -p, --project <pid>      The identifier of the parent project.
  -m, --method <mid>       The identifier of the method.
  -d, --description <description>
                           Description about the subject.
      --ignore-if-exists   Ignore if subject with the specified name already
                             exists.
```

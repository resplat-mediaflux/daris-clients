package unimelb.mf.client.session;

import java.io.Console;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.ExecutorService;

import arc.mf.client.AuthenticationDetails;
import arc.mf.client.ConnectionDetails;
import arc.mf.client.ProxyDetails;
import arc.mf.client.RemoteServer;
import arc.mf.client.RequestOptions;
import arc.mf.client.ServerClient;
import arc.mf.client.ServerClient.Input;
import arc.mf.client.ServerRoute;
import arc.utils.AbortableOperationHandler;
import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;

public class MFSession {

    public static final int DEFAULT_NB_RETRIES = 2;
    public static final int DEFAULT_RETRY_WAIT_TIME = 100;

    public static interface Listener {
        void connected(MFSession session);

        void disconnected(MFSession session);
    }

    private MFConfig _cfg;
    private AuthenticationDetails _ad;
    private RemoteServer _rs;
    private String _sessionKey;

    private Timer _timer;

    private List<Listener> _ls;
    private Map<String, List<SamlProvider>> _samlProviders;

    protected MFSession(MFConfig cfg, RemoteServer rs, String sessionKey) {
        _cfg = cfg;
        _ad = cfg == null ? null : cfg.authenticationDetails();
        _rs = rs;
        _sessionKey = sessionKey != null ? sessionKey : (cfg == null ? null : cfg.sessionKey());
    }

    public synchronized boolean addListener(Listener l) {
        if (l != null) {
            if (_ls == null) {
                _ls = new Vector<>();
            }
            return _ls.add(l);
        }
        return false;
    }

    public synchronized boolean removeListener(Listener l) {
        if (l != null) {
            if (_ls != null) {
                return _ls.remove(l);
            }
        }
        return false;
    }

    public void authenticate(AuthenticationDetails authenticationDetails) throws Throwable {
        ServerClient.Connection cxn = null;
        try {
            cxn = connect(_rs.connectionDetails(), authenticationDetails, _rs.proxy());
        } finally {
            if (cxn != null) {
                cxn.close(!_rs.usingConnectionPooling());
            }
        }
    }

    public ServerClient.Connection connect() throws Throwable {
        return connect(connectionDetails(), authenticationDetails(), proxyDetails());
    }

    public synchronized ConnectionDetails connectionDetails() {
        if (_rs != null) {
            // state: connected
            return _rs.connectionDetails();
        } else if (_cfg != null) {
            // state: not connected but config is available
            return _cfg.connectionDetails();
        } else {
            return null;
        }
    }

    public synchronized AuthenticationDetails authenticationDetails() {
        if (_ad != null) {
            // state: connected
            return _ad;
        } else if (_cfg != null) {
            // state: not connected but config is available
            return _cfg.authenticationDetails();
        } else {
            return _ad;
        }
    }

    public synchronized ProxyDetails proxyDetails() {
        if (_rs != null) {
            // state: connected
            return _rs.proxy();
        } else if (_cfg != null) {
            // state: not connected but config is avaiable
            return _cfg.proxy();
        } else {
            return null;
        }
    }

    public ServerClient.Connection connect(ConnectionDetails connectionDetails,
            AuthenticationDetails authenticationDetails, ProxyDetails proxyDetails) throws Throwable {
        return connect(connectionDetails, authenticationDetails, proxyDetails, false);
    }

    public synchronized ServerClient.Connection connect(ConnectionDetails connectionDetails,
            AuthenticationDetails authenticationDetails, ProxyDetails proxyDetails, boolean reconnect)
            throws Throwable {
        if (!connectionDetailsEquals(_rs.connectionDetails(), connectionDetails)) {
            boolean connectionPooling = _rs.usingConnectionPooling();
            _rs.discard();

            _rs = new RemoteServer(connectionDetails);
            _rs.setConnectionPooling(connectionPooling);
            if (_cfg != null) {
                _rs.setMaxNumberOfRetryAttempts(_cfg.maxNumberOfRetries());
                _rs.setRetryWaitTime(_cfg.retryWaitTime());
                if (_cfg.useClusterIO()) {
                    _rs.enableClusteredIo();
                } else {
                    _rs.disableClusteredIo();
                }
            }
        }

        _rs.setProxy(proxyDetails);
        // TODO update when cluster io nodes support proxy in the future
        if (_rs.proxy() != null) {
            // proxy not working with cluster io nodes
            _rs.disableClusteredIo();
        }

        ServerClient.Connection cxn = _rs.open();
        if (!reconnect && _sessionKey != null && authenticationDetailsEquals(_ad, authenticationDetails)) {
            cxn.reconnect(_sessionKey);
        } else {
            _sessionKey = cxn.connect(authenticationDetails);
            _ad = authenticationDetails;
        }
        updateConfig(_rs.connectionDetails(), cxn.authenticationDetails(), _rs.proxy());
        if (_ls != null) {
            // notify of connect
            for (Listener cl : _ls) {
                cl.connected(this);
            }
        }
        return cxn;
    }

    private void updateConfig(ConnectionDetails connectionDetails, AuthenticationDetails authenticationDetails,
            ProxyDetails proxy) {
        if (_cfg == null) {
            _cfg = new MFConfig(connectionDetails, authenticationDetails, true, true, ServerClient.DEFAULT_NB_RETRIES,
                    ServerClient.DEFAULT_RETRY_WAIT_TIME, _sessionKey, proxy);
        } else {
            _cfg = new MFConfig(connectionDetails, authenticationDetails, _cfg.connectionPooling(), _cfg.useClusterIO(),
                    _cfg.maxNumberOfRetries(), _cfg.retryWaitTime(), _sessionKey, proxy);
        }
    }

    private static boolean connectionDetailsEquals(ConnectionDetails cd1, ConnectionDetails cd2) throws Throwable {
        if (cd1 == cd2) {
            return true;
        }
        if (cd1 == null || cd2 == null) {
            return false;
        }
        if (cd1.hostAddress() != null && !cd1.hostAddress().equalsIgnoreCase(cd2.hostAddress())) {
            return false;
        }
        if (cd1.hostName() != null && !cd1.hostName().equalsIgnoreCase(cd2.hostName())) {
            return false;
        }
        if (cd1.port() != cd2.port()) {
            return false;
        }
        if (cd1.useHttp() != cd2.useHttp()) {
            return false;
        }
        if (cd1.encrypt() != cd2.encrypt()) {
            return false;
        }
        if (cd1.allowUntrustedServer() != cd2.allowUntrustedServer()) {
            return false;
        }
        if (cd1.ipVersion() != cd2.ipVersion()) {
            return false;
        }
        return cd1.httpCookie() == null || !cd1.httpCookie().equals(cd2.httpCookie());
    }

    private static boolean authenticationDetailsEquals(AuthenticationDetails ad1, AuthenticationDetails ad2) {
        if (ad1 == ad2) {
            return true;
        }
        if (ad1 == null || ad2 == null) {
            return false;
        }

        return Objects.equals(ad1.application(), ad2.application()) && Objects.equals(ad1.token(), ad2.token())
                && Objects.equals(ad1.domain(), ad2.domain()) && Objects.equals(ad1.userName(), ad2.userName())
                && Objects.equals(ad1.userPassword(), ad2.userPassword())
                && Objects.equals(ad1.loginIdentity(), ad2.loginIdentity());
    }

    public synchronized void disconnect() throws Throwable {
        if (_sessionKey != null) {
            ServerClient.Connection cxn = _rs.open();
            try {
                cxn.logoff();
                _sessionKey = null;
                if (_ls != null) {
                    // notify of disconnect
                    for (Listener cl : _ls) {
                        cl.disconnected(this);
                    }
                }
            } finally {
                cxn.close(!_rs.usingConnectionPooling());
            }
        }
    }

    public void disconnect(boolean discard) throws Throwable {
        try {
            disconnect();
        } finally {
            if (discard) {
                discard();
            }
        }
    }

    public XmlDoc.Element execute(ServerRoute serverRoute, String service, String args, Input input,
            ServerClient.Output output, RequestOptions options) throws Throwable {
        return execute(serverRoute, service, args, input == null ? null : Collections.singletonList(input), output,
                options);
    }

    public XmlDoc.Element execute(ServerRoute serverRoute, String service, String args, Input input,
            ServerClient.Output output, AbortableOperationHandler abortHandler) throws Throwable {
        return execute(serverRoute, service, args, input == null ? null : Collections.singletonList(input), output,
                abortHandler);
    }

    public XmlDoc.Element execute(ServerRoute serverRoute, String service, String args, List<Input> inputs,
            ServerClient.Output output, AbortableOperationHandler abortHandler) throws Throwable {
        RequestOptions options = null;
        if (abortHandler != null) {
            options = new RequestOptions();
            options.setAbortHandler(abortHandler);
        }
        return execute(MFRequest.create(serverRoute, service, args, inputs, output, options));
    }

    public XmlDoc.Element execute(ServerRoute serverRoute, String service, String args, List<Input> inputs,
            ServerClient.Output output, RequestOptions options) throws Throwable {
        return execute(MFRequest.create(serverRoute, service, args, inputs, output, options));
    }

    public XmlDoc.Element execute(MFRequest request) throws Throwable {
        return execute(request, _cfg.maxNumberOfRetries());
    }

    public XmlDoc.Element execute(MFRequest request, int nbRetries) throws Throwable {
        ServerClient.Connection cxn = connect();
        try {
            return cxn.executeMultiInput(request.serverRoute(), request.service(), request.args(), request.inputs(),
                    request.output(), request.options());
        } catch (Throwable t) {
            if (this._sessionKey != null && nbRetries > 0) {
                if (_ad != null && t instanceof ServerClient.ExSessionInvalid) {
                    _sessionKey = cxn.connect(_ad);
                }
                if ((request.hasInput() || request.hasOutput()) && request.canBeReExecuted()) {
                    // only retry when there is input or output...
                    if (_cfg.retryWaitTime() > 0) {
                        Thread.sleep(_cfg.retryWaitTime());
                    }
                    System.out.println("Retrying " + request.service() + "(" + nbRetries + ")...");
                    return execute(request, nbRetries - 1);
                }
            }
            throw t;
        } finally {
            // If connection pooling is enabled, do not release resources so
            // that the connection is kept in the pool to be reused.
            cxn.close(!_rs.usingConnectionPooling());
        }
    }

    public XmlDoc.Element execute(ServerRoute route, String service, String args, List<Input> inputs,
            ServerClient.Output output) throws Throwable {
        return execute(route, service, args, inputs, output, (RequestOptions) null);
    }

    public XmlDoc.Element execute(String service, String args, List<Input> inputs) throws Throwable {
        return execute(null, service, args, inputs, null, (RequestOptions) null);
    }

    public XmlDoc.Element execute(String service, String args, Input input, ServerClient.Output output)
            throws Throwable {
        return execute(null, service, args, input, output, (RequestOptions) null);
    }

    public XmlDoc.Element execute(String service, String args, Input input) throws Throwable {
        return execute(null, service, args, input, null, (RequestOptions) null);
    }

    public XmlDoc.Element execute(String service, String args, ServerClient.Output output) throws Throwable {
        return execute(null, service, args, (List<Input>) null, output, (RequestOptions) null);
    }

    public XmlDoc.Element execute(String service, String args) throws Throwable {
        return execute(null, service, args, (List<Input>) null, null, (RequestOptions) null);
    }

    public XmlDoc.Element execute(String service) throws Throwable {
        return execute(null, service, null, (List<Input>) null, null, (RequestOptions) null);
    }

    public XmlDoc.Element execute(String service, String args, List<Input> inputs, ServerClient.Output output,
            AbortableOperationHandler abortHandler) throws Throwable {
        return execute(null, service, args, inputs, output, abortHandler);
    }

    public XmlDoc.Element execute(String service, String args, Input input, AbortableOperationHandler abortHandler)
            throws Throwable {
        return execute(null, service, args, input == null ? null : Collections.singletonList(input), null,
                abortHandler);
    }

    public XmlDoc.Element execute(String service, String args, ServerClient.Output output,
            AbortableOperationHandler abortHandler) throws Throwable {
        return execute(null, service, args, (List<Input>) null, output, abortHandler);
    }

    public XmlDoc.Element execute(String service, String args, MFSlicedFile file, String store,
            ExecutorService executor) throws Throwable {
        String jobTicket = new MFSlicedFileUpload(this, file, store, executor).call();
        try {
            XmlStringWriter w = new XmlStringWriter();
            w.add("input-ticket", jobTicket);
            w.push("service", new String[] { "name", service });
            w.add(new XmlDoc().parse("<args>" + args + "</args>"), false);
            w.pop();
            return execute("service.execute", w.document()).element("reply[@service='" + service + "']/response");
        } catch (Throwable t) {
            MFSlicedFileUpload.discardServerIOJob(this, jobTicket);
            throw t;
        }
    }

    public void discard() {
        if (_rs != null) {
            _rs.discard();
        }
        stopPingServerPeriodically();
    }

    public boolean useClusterIO() {
        return this._cfg.useClusterIO();
    }

    /**
     * @param period time in milliseconds.
     */
    public void startPingServerPeriodically(int period) {
        if (_timer == null) {
            _timer = new Timer();
            _timer.scheduleAtFixedRate(new TimerTask() {

                @Override
                public void run() {
                    try {
                        execute("server.ping");
                    } catch (Throwable e) {
                        String msg = e.getMessage();
                        if (msg != null && msg.contains("Connection has been discarded")) {
                            System.err.println("Server connection has been closed.");
                            if (_timer != null) {
                                _timer.cancel();
                            }
                        } else {
                            e.printStackTrace(System.err);
                        }
                    }
                }
            }, 0, period);
        }
    }

    public void stopPingServerPeriodically() {
        if (_timer != null) {
            _timer.cancel();
            _timer = null;
        }
    }

    public synchronized void setUseClusterIO(boolean useClusterIO) {
        if (_rs != null) {
            if (useClusterIO) {
                _rs.enableClusteredIo();
            } else {
                _rs.disableClusteredIo();
            }
        }
    }

    public synchronized List<SamlProvider> getSamlProviders(ConnectionDetails connectionDetails, String domain,
            boolean refresh) throws Throwable {
        if (_samlProviders != null && !refresh) {
            if (_samlProviders.containsKey(domain)) {
                return _samlProviders.get(domain);
            }
        }
        if (_rs == null || !Objects.equals(_rs.connectionDetails(), connectionDetails)) {
            if (_rs != null) {
                _rs.discard();
            }
            _rs = new RemoteServer(connectionDetails);
            if (_cfg != null) {
                _rs.setConnectionPooling(_cfg.connectionPooling());
            }
        }

        ServerClient.Connection cxn = _rs.open();
        try {
            List<SamlProvider> providers = SamlProvider.getAll(cxn, domain);
            if (_samlProviders == null) {
                _samlProviders = new LinkedHashMap<>();
            }
            _samlProviders.put(domain, providers);
            return providers;
        } finally {
            cxn.close(!_rs.usingConnectionPooling());
        }
    }

    public List<SamlProvider> getSamlProviders(String domain) throws Throwable {
        if (_rs != null) {
            return getSamlProviders(_rs.connectionDetails(), domain, false);
        }
        return null;
    }

    public SamlProvider getSamlProvider(String domain, String shortName) throws Throwable {
        if (_rs != null) {
            return getSamlProvider(_rs.connectionDetails(), domain, shortName);
        }
        return null;
    }

    public SamlProvider getSamlProvider(ConnectionDetails connectionDetails, String domain, String shortName)
            throws Throwable {
        List<SamlProvider> providers = getSamlProviders(connectionDetails, domain, false);
        if (providers != null) {
            for (SamlProvider provider : providers) {
                if (provider.shortName().equalsIgnoreCase(shortName)) {
                    return provider;
                }
            }
        }
        return null;
    }

    public static MFSession create(MFConfigBuilder config) throws Throwable {
        while (config.hasMissingArgument() && config.consoleLogon()) {
            Console console = System.console();
            if (console == null) {
                throw new UnsupportedOperationException("Failed to open system console");
            }
            if (config.host() == null) {
                config.readServerHostFromConsole(console);
            } else {
                console.printf("Host: %s%n", config.host());
            }
            if (config.port() <= 0) {
                config.readServerPortFromConsole(console);
            } else {
                console.printf("Port: %d%n", config.port());
            }
            if (config.transport() == null) {
                config.readServerTransportFromConsole(console);
            } else {
                console.printf("Transport: %s%n", config.transport());
            }
            if (config.domain() == null) {
                config.readDomainFromConsole(console);
            } else {
                console.printf("Domain: %s%n", config.domain());
            }
            if (config.user() == null) {
                config.readUserFromConsole(console);
            } else {
                console.printf("User: %s%n", config.user());
            }
            config.readPasswordFromConsole(console);
        }
        if (config.hasMissingArgument()) {
            throw new IllegalArgumentException("Missing Mediaflux server or user details.");
        }
        return create(config.build());
    }

    public static MFSession create(MFConfig config) throws Throwable {
        final boolean connectionPooling = config.connectionPooling();
        RemoteServer rs = new RemoteServer(config.connectionDetails());
        rs.setConnectionPooling(connectionPooling);
        if (config.useClusterIO()) {
            rs.enableClusteredIo();
        } else {
            rs.disableClusteredIo();
        }
        rs.setProxy(config.proxy());
        ServerClient.Connection c = rs.open();
        try {
            String sessionKey = c.connect(config.authenticationDetails());
            return new MFSession(config, rs, sessionKey);
        } finally {
            c.close(true);
        }
    }
}

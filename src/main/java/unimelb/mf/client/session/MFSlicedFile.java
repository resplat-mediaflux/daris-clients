package unimelb.mf.client.session;

import java.io.File;
import java.io.IOException;

import unimelb.utils.PathUtils;

public class MFSlicedFile {

    public static final int MAX_NUMBER_OF_SLICES = 16;

    private File _file;
    private long[][] _slices;

    public MFSlicedFile(File file, int nbSlices) throws IOException {
        _file = file;
        _slices = getSlices(file, nbSlices);
    }

    public File file() {
        return _file;
    }

    public long fileLength() {
        return _file.length();
    }

    public String fileExtension() {
        return PathUtils.getFileExtension(_file.getName());
    }

    public int nbSlices() {
        int nbSlices = _slices.length;
        return nbSlices;
    }

    public long[] getSlice(int i) {
        return _slices[i];
    }

    public MFFileSliceInput[] getSliceInputs() throws Throwable {
        MFFileSliceInput[] inputs = new MFFileSliceInput[nbSlices()];
        for (int i = 0; i < inputs.length; i++) {
            inputs[i] = new MFFileSliceInput(_file, _slices[i][0], _slices[i][1]);
        }
        return inputs;
    }

    private static long[][] getSlices(File file, int nbSlices) throws IOException {
        if (nbSlices <= 1 || nbSlices > MAX_NUMBER_OF_SLICES) {
            throw new IOException("Invalid number of slices: " + nbSlices);
        }
        long fileLength = file.length();
        long sliceSize = fileLength / nbSlices;
        long lastSliceSize = sliceSize + (fileLength % nbSlices);

        long[][] slices = new long[nbSlices][2];
        for (int i = 0; i < nbSlices; i++) {
            long offset = i * sliceSize;
            slices[i][0] = offset;
            slices[i][1] = i < nbSlices - 1 ? sliceSize : lastSliceSize;
        }
        return slices;
    }
}

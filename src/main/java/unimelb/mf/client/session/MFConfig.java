package unimelb.mf.client.session;

import java.io.File;

import arc.mf.client.AuthenticationDetails;
import arc.mf.client.ConnectionDetails;
import arc.mf.client.ProxyDetails;

/**
 * See src/main/config/mf-sync-properties.sample.xml
 *
 * @author wliu5
 */
public class MFConfig {

    public static final String DEFAULT_MFLUX_CFG_FILE = System.getProperty("user.home") + File.separator + ".Arcitecta"
            + File.separator + "mflux.cfg";
    public static final String ENV_MFLUX_CFG = "MFLUX_CFG";
    public static final String PROPERTY_MF_CONFIG = "mf.cfg";
    public static final String PROPERTY_MF_HOST = "mf.host";
    public static final String PROPERTY_MF_PORT = "mf.port";
    public static final String PROPERTY_MF_TRANSPORT = "mf.transport";
    public static final String PROPERTY_MF_DOMAIN = "mf.domain";
    public static final String PROPERTY_MF_USER = "mf.user";
    public static final String PROPERTY_MF_PASSWORD = "mf.password";
    public static final String PROPERTY_MF_TOKEN = "mf.token";

    private final ConnectionDetails _connectionDetails;
    private final AuthenticationDetails _authenticationDetails;
    private final String _sessionKey;

    private final boolean _connectionPooling;
    private final boolean _useClusterIO;

    private final int _maxNumberOfRetries;
    private final long _retryWaitTime;

    private final ProxyDetails _proxy;

    public MFConfig(ConnectionDetails connectionDetails, AuthenticationDetails authenticationDetails,
            boolean connectionPooling, boolean useClusterIO, int maxNumberOfRetries, long retryWaitTime,
            String sessionKey, ProxyDetails proxy) {
        _connectionDetails = connectionDetails;
        _authenticationDetails = authenticationDetails;
        _connectionPooling = connectionPooling;
        _useClusterIO = useClusterIO;
        _maxNumberOfRetries = maxNumberOfRetries;
        _retryWaitTime = retryWaitTime;
        _sessionKey = sessionKey;
        _proxy = proxy;
    }

    public MFConfig(ConnectionDetails connectionDetails, AuthenticationDetails authenticationDetails,
            ProxyDetails proxy) {
        this(connectionDetails, authenticationDetails, true, true, MFSession.DEFAULT_NB_RETRIES,
                MFSession.DEFAULT_RETRY_WAIT_TIME, null, proxy);
    }

    public ConnectionDetails connectionDetails() {
        return _connectionDetails;
    }

    public AuthenticationDetails authenticationDetails() {
        return _authenticationDetails;
    }

    public String sessionKey() {
        return _sessionKey;
    }

    public int maxNumberOfRetries() {
        return _maxNumberOfRetries;
    }

    public long retryWaitTime() {
        return _retryWaitTime;
    }

    public boolean connectionPooling() {
        return _connectionPooling;
    }

    public boolean useClusterIO() {
        return _useClusterIO;
    }

    public ProxyDetails proxy() {
        return _proxy;
    }

}

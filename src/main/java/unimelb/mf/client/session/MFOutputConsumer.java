package unimelb.mf.client.session;

import arc.mf.client.ServerClient;
import arc.streams.LongInputStream;
import arc.streams.NonCloseInputStream;
import arc.streams.SizedInputStream;
import arc.streams.UnsizedInputStream;
import arc.xml.XmlDoc.Element;
import unimelb.io.CountedInputStream;

public class MFOutputConsumer extends ServerClient.OutputConsumer implements MFOutput {

    private CountedInputStream _cin;

    @Override
    protected final synchronized void consume(Element re, LongInputStream in) throws Throwable {
        _cin = (in instanceof NonCloseInputStream) ? new CountedInputStream(in)
                : new CountedInputStream(new NonCloseInputStream(in));
        try (LongInputStream lin = in.length() >= 0 ? new SizedInputStream(_cin, in.length())
                : new UnsizedInputStream(_cin)) {
            consume(lin, re);
        }
    }

    protected void consume(LongInputStream in, Element re) throws Throwable {

    }

    @Override
    public synchronized boolean hasBeenRead() {
        return _cin != null && _cin.bytesCount() > 0;
    }

    @Override
    public boolean isUnused() {
        return !this.hasBeenRead();
    }

}
package unimelb.mf.client.session;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.Reader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import arc.mf.client.*;
import arc.mf.client.Configuration.Transport;
import arc.xml.XmlDoc;

public class MFConfigBuilder {

	private String _hostName = null;
	private int _port;
	private boolean _useHttp = true;
	private boolean _encrypt = true;
	private boolean _allowUntrustedServer = true;

	private String _domain = null;
	private String _userName = null;
	private String _userPassword = null;

	private String _token = null;
	private String _app = null;

	private String _sessionKey = null;

	private boolean _connectionPooling = true;

	private int _maxNumberOfRetries = ServerClient.DEFAULT_NB_RETRIES;
	private long _retryWaitTime = ServerClient.DEFAULT_RETRY_WAIT_TIME;

	private boolean _useClusterIO = true;

	private ProxyDetails _proxy;

	private boolean _consoleLogon = true;

	public MFConfigBuilder() {
	}

	public MFConfigBuilder(File xmlFile) throws Throwable {
		if (xmlFile != null && xmlFile.exists()) {
			loadFromXmlFile(xmlFile);
		}
	}

	public MFConfigBuilder(Path xmlFile) throws Throwable {
		this(xmlFile == null ? null : xmlFile.toFile());
	}

	public MFConfigBuilder(XmlDoc.Element pe) throws Throwable {
		if (pe != null) {
			loadFromXml(pe);
		}
	}

	public boolean allowUntrustedServer() {
		return _allowUntrustedServer;
	}

	public String app() {
		return _app;
	}

	public AuthenticationDetails authenticationDetails() {
		if (_token == null && (_domain == null || _userName == null || _userPassword == null)) {
			return null;
		} else {
			if (_token == null) {
				return new AuthenticationDetails(_app, _domain, _userName, _userPassword);
			} else {
				return new AuthenticationDetails(_app, _token);
			}
		}
	}

	public MFConfig build() {
		checkMissingArguments();
		return new MFConfig(connectionDetails(), authenticationDetails(), connectionPooling(), useClusterIO(),
				maxNumberOfRetries(), retryWaitTime(), sessionKey(), proxy());
	}

	public void checkMissingArguments() throws IllegalArgumentException {
		if (_hostName == null) {
			throw new IllegalArgumentException("Missing mf.host");
		}
		if (_port <= 0) {
			throw new IllegalArgumentException("Missing mf.port");
		}
		if (transport() == null) {
			throw new IllegalArgumentException("Missing mf.transport");
		}
		if (_token == null && (_domain == null || _userName == null || _userPassword == null) && _sessionKey == null) {
			throw new IllegalArgumentException("Missing/Incomplete mf.token or mf.auth.");
		}
	}

	public ConnectionDetails connectionDetails() {
		if (hasConnectionDetails()) {
			return new ConnectionSpec().setHostName(_hostName).setPort(_port).setUseHttp(_useHttp).setEncrypt(_encrypt)
					.setAllowUntrustedServer(_allowUntrustedServer).build();
		} else {
			return null;
		}
	}

	public boolean connectionPooling() {
		return _connectionPooling;
	}

	public boolean consoleLogon() {
		return _consoleLogon;
	}

	public String domain() {
		return _domain;
	}

	public boolean encrypt() {
		return _encrypt;
	}

	// @formatter:off

    /**
     * Try finding mflux.cfg file in the following order:
     * 1. try system environment variable: MFLUX_CFG
     * 2. try system property: mf.cfg
     * 3. try default location: $HOME/.Arcitecta/mflux.cfg
     *
     * @return the absolute path of the configuration file. if not found, return null.
     * @throws Throwable Exception
     */
    // @formatter:on
	public String findAndLoadFromConfigFile() throws Throwable {

		/*
		 * try system environment variable: MFLUX_CFG
		 */
		String cfgFile = System.getenv(MFConfig.ENV_MFLUX_CFG);
		if (cfgFile != null) {
			File f = new File(cfgFile);
			if (f.exists()) {
				loadFromConfigFile(f);
				return f.getAbsolutePath();
			}
		}

		/*
		 * try system property: mf.cfg
		 */
		cfgFile = System.getProperty(MFConfig.PROPERTY_MF_CONFIG);
		if (cfgFile != null) {
			File f = new File(cfgFile);
			if (f.exists()) {
				loadFromConfigFile(f);
				return f.getAbsolutePath();
			}
		}

		/*
		 * try default location: $HOME/.Arcitecta/mflux.cfg
		 */
		cfgFile = MFConfig.DEFAULT_MFLUX_CFG_FILE;
		File f = new File(cfgFile);
		if (f.exists()) {
			loadFromConfigFile(f);
			return f.getAbsolutePath();
		}
		return null;
	}

	public void loadFromSystemEnv() {

		String host = MFSystemEnv.getMFHost();
		if (host != null) {
			this.setHost(host);
		}

		Integer port = MFSystemEnv.getMFPort();
		if (port != null) {
			this.setPort(port);
		}

		String transport = MFSystemEnv.getMFTransport();
		if (transport != null) {
			this.setTransport(transport);
		}

		String domain = MFSystemEnv.getMFDomain();
		if (domain != null) {
			this.setDomain(domain);
		}

		String user = MFSystemEnv.getMFUser();
		if (user != null) {
			this.setUser(user);
		}

		String password = MFSystemEnv.getMFPassword();
		if (password != null) {
			this.setPassword(password);
		}

		String token = MFSystemEnv.getMFToken();
		if (token != null) {
			this.setToken(token);
		}

		String app = MFSystemEnv.getMFApp();
		if (app != null) {
			this.setApp(app);
		}
	}

	public void loadFromSystemProperties() {

		String host = System.getProperty(MFConfig.PROPERTY_MF_HOST);
		if (host != null) {
			this.setHost(host);
		}

		int port = Integer.parseInt(System.getProperty(MFConfig.PROPERTY_MF_PORT, "0"));
		if (port > 0) {
			this.setPort(port);
		}

		String transport = System.getProperty(MFConfig.PROPERTY_MF_TRANSPORT);
		if (transport != null) {
			this.setTransport(transport);
		}

		String domain = System.getProperty(MFConfig.PROPERTY_MF_DOMAIN);
		if (domain != null) {
			this.setDomain(domain);
		}

		String user = System.getProperty(MFConfig.PROPERTY_MF_USER);
		if (user != null) {
			this.setUser(user);
		}

		String password = System.getProperty(MFConfig.PROPERTY_MF_PASSWORD);
		if (password != null) {
			this.setPassword(password);
		}

		String token = System.getProperty(MFConfig.PROPERTY_MF_TOKEN);
		if (token != null) {
			this.setToken(token);
		}
	}

	public boolean hasAuthenticationDetails() {
		return hasToken() || hasUserCredentials();
	}

	public boolean hasConnectionDetails() {
		return _hostName != null && _port > 0;
	}

	public boolean hasMissingArgument() {
		if (_hostName == null) {
			return true;
		}
		if (_port <= 0) {
			return true;
		}
		return _token == null && (_domain == null || _userName == null || _userPassword == null) && _sessionKey == null;
	}

	public boolean hasSessionKey() {
		return _sessionKey != null;
	}

	public boolean hasToken() {
		return _token != null;
	}

	public boolean hasUserCredentials() {
		return _domain != null && _userName != null && _userPassword != null;
	}

	public String host() {
		return _hostName;
	}

	public MFConfigBuilder loadFromConfigFile(File configFile) throws Exception {
		return loadFromConfigFile(configFile.toPath());
	}

	public MFConfigBuilder loadFromConfigFile(Path configFile) throws Exception {
		if (configFile != null) {
			Properties props = new Properties();
			try (InputStream in = new BufferedInputStream(Files.newInputStream(configFile))) {
				props.load(in);
				if (props.containsKey("host")) {
					String host = props.getProperty("host");
					if (host != null) {
						host = host.trim();
						if (!host.isEmpty()) {
							setHost(host);
						}
					}
				}
				if (props.containsKey("port")) {
					String portStr = props.getProperty("port");
					if (portStr != null) {
						portStr = portStr.trim();
						if (!portStr.isEmpty()) {
							int port = Integer.parseInt(portStr);
							setPort(port);
						}
					}
				}
				if (props.containsKey("transport")) {
					String transport = props.getProperty("transport");
					if (transport != null) {
						transport = transport.trim();
						if (!transport.isEmpty()) {
							setTransport(transport);
						}
					}
				}
				if (props.containsKey("domain")) {
					String domain = props.getProperty("domain");
					if (domain != null) {
						domain = domain.trim();
						if (!domain.isEmpty()) {
							setDomain(domain);
						}
					}
				}
				if (props.containsKey("user")) {
					String user = props.getProperty("user");
					if (user != null) {
						user = user.trim();
						if (!user.isEmpty()) {
							setUser(user);
						}
					}
				}
				if (props.containsKey("password")) {
					String password = props.getProperty("password");
					if (password != null) {
						password = password.trim();
						if (!password.isEmpty()) {
							setPassword(password);
						}
					}
				}
				if (props.containsKey("token")) {
					String token = props.getProperty("token");
					if (token != null) {
						token = token.trim();
						if (!token.isEmpty()) {
							setToken(token);
						}
					}
				}
				if (props.containsKey("token.app")) {
					String tokenApp = props.getProperty("token.app");
					if (tokenApp != null) {
						tokenApp = tokenApp.trim();
						if (!tokenApp.isEmpty()) {
							setApp(tokenApp);
						}
					}
				}
				if (props.containsKey("proxy")) {
					String proxyAddress = props.getProperty("proxy");
					if (proxyAddress != null) {
						proxyAddress = proxyAddress.trim();
						if (!proxyAddress.isEmpty()) {
							parseProxy(proxyAddress);
						}
					}
				}
			}
		}
		return this;
	}

	public MFConfigBuilder loadFromConfigFile(String configFile) throws Exception {
		return loadFromConfigFile(Paths.get(configFile));
	}

	/**
	 * Load the specified Mediaflux configuration file. If the specified file is
	 * null or the file is not found. Try finding and loading the file specified in
	 * 1) system property; 2) system environment variable 3) default location:
	 * $HOME/.Arcitecta/mflux.cfg
	 *
	 * @param configFile file path
	 * @throws Throwable exception
	 */
	public String loadFromConfigFileOrFind(String configFile) throws Throwable {
		if (configFile != null) {
			File cfgFile = new File(configFile);
			if (cfgFile.exists()) {
				loadFromConfigFile(cfgFile);
				return cfgFile.getAbsolutePath();
			}
		}
		return findAndLoadFromConfigFile();
	}

	private void loadFromXml(XmlDoc.Element pe) throws Throwable {

		// @formatter:off
        _hostName = pe.value("server/host");
        _port = pe.intValue("server/port", 0);

        String transport;
        if (pe.elementExists("server/transport")) {
            transport = pe.stringValue("server/transport", "HTTPS");
        } else {
            // back compatible with old element name: protocol
            transport = pe.stringValue("server/protocol", "HTTPS");
        }

        if ("HTTP".equalsIgnoreCase(transport)) {
            _useHttp = true;
            _encrypt = false;
            if (_port == 0) {
                _port = 80;
            }
        }
        if ("HTTPS".equalsIgnoreCase(transport)) {
            _useHttp = true;
            _encrypt = true;
            if (_port == 0) {
                _port = 443;
            }
        }
        if (transport.toLowerCase().startsWith("tcp")) {
            _useHttp = false;
            _encrypt = false;
            if (_port == 0) {
                _port = 1967;
            }
        }

        if(pe.elementExists("server/session/useClusterIO")){
            _useClusterIO = pe.booleanValue("server/session/useClusterIO");
        }
        _maxNumberOfRetries = pe.intValue("server/session/maxNumberOfRetries", ServerClient.DEFAULT_NB_RETRIES);
        _retryWaitTime = pe.longValue("server/session/retryWaitTime", ServerClient.DEFAULT_RETRY_WAIT_TIME);


        _app = pe.value("credential/app");
        _domain = pe.value("credential/domain");
        _userName = pe.value("credential/user");
        _userPassword = pe.value("credential/password");
        _token = pe.value("credential/token");
        _sessionKey = pe.value("credential/sessionKey");
        String proxyHostName = pe.value("proxy/host");
        int proxyPort = pe.intValue("proxy/host", -1);
        if (proxyHostName != null && proxyPort > 0 && proxyPort < 65535) {
            String proxyUserName = pe.value("proxy/userName");
            String proxyUserPassword = pe.value("proxy/userPassword");
            _proxy = new ProxyDetails(proxyHostName, proxyPort, proxyUserName,
                    proxyUserName == null ? null : proxyUserPassword);
        }
        // @formatter:on

	}

	public void loadFromXmlFile(File xmlFile) throws Throwable {
		try (Reader reader = new BufferedReader(new FileReader(xmlFile))) {
			XmlDoc.Element pe = new XmlDoc().parse(reader);
			loadFromXml(pe);
		}
	}

	public int maxNumberOfRetries() {
		return _maxNumberOfRetries;
	}

	public List<String> parseArgs(String[] args) throws Throwable {
		return parseArgs(args, 0, args.length);
	}

	public List<String> parseArgs(String[] args, int offset, int length) {

		List<String> remainArgs = new ArrayList<>();
		for (int i = offset; i < offset + length;) {
			if ("--mf.config".equalsIgnoreCase(args[i])) {
				try {
					loadFromConfigFile(args[i + 1]);
				} catch (Throwable e) {
					throw new IllegalArgumentException("Invalid --mf.config: " + args[i + 1], e);
				}
				i += 2;
			} else if ("--mf.host".equalsIgnoreCase(args[i])) {
				setHost(args[i + 1]);
				i += 2;
			} else if ("--mf.port".equalsIgnoreCase(args[i])) {
				setPort(Integer.parseInt(args[i + 1]));
				i += 2;
			} else if ("--mf.transport".equalsIgnoreCase(args[i])) {
				setTransport(args[i + 1]);
				i += 2;
			} else if ("--mf.auth".equalsIgnoreCase(args[i])) {
				String auth = args[i + 1];
				String[] parts = auth.split(",");
				if (parts.length != 3) {
					throw new IllegalArgumentException("Invalid mf.auth: " + auth);
				}
				setUserCredentials(parts[0], parts[1], parts[2]);
				i += 2;
			} else if ("--mf.token".equalsIgnoreCase(args[i])) {
				setToken(args[i + 1]);
				i += 2;
			} else if ("--no-cluster-io".equalsIgnoreCase(args[i])) {
				setUseClusterIO(false);
				i++;
			} else {
				remainArgs.add(args[i]);
				i++;
			}
		}

		return remainArgs;
	}

	private void parseTransport(String proto) {
		if (proto != null) {
			proto = proto.trim();
			if (!proto.isEmpty()) {
				if ("HTTP".equalsIgnoreCase(proto)) {
					_useHttp = true;
					_encrypt = false;
				} else if ("HTTPS".equalsIgnoreCase(proto)) {
					_useHttp = true;
					_encrypt = true;
				} else if (proto.toLowerCase().startsWith("tcp")) {
					_useHttp = false;
					_encrypt = false;
				} else {
					throw new IllegalArgumentException("Invalid transport protocol: " + proto);
				}
			}
		}
	}

	public String password() {
		return _userPassword;
	}

	public int port() {
		return _port;
	}

	public MFConfigBuilder readDomainFromConsole(Console console) {
		String domain;
		do {
			domain = _domain == null ? console.readLine("Domain: ") : console.readLine("Domain[%s]: ", _domain);
			if (_domain != null && domain != null && domain.trim().isEmpty()) {
				// use existing value, no change
				return this;
			}
		} while (domain == null || domain.trim().isEmpty());

		_domain = domain.trim();
		return this;
	}

	public MFConfigBuilder readPasswordFromConsole(Console console) {
		String password = null;
		do {
			char[] pwd = console.readPassword("Password: ");
			if (pwd != null && pwd.length > 0) {
				password = new String(pwd);
			}
		} while (password == null || password.trim().isEmpty());

		_userPassword = password.trim();
		return this;
	}

	public MFConfigBuilder readServerHostFromConsole(Console console) {
		String host;
		do {
			host = _hostName == null ? console.readLine("Host: ") : console.readLine("Host[%s]: ", _hostName);
			if (_hostName != null && host != null && host.trim().isEmpty()) {
				// use existing value, no change
				return this;
			}
		} while (host == null || host.trim().isEmpty());

		_hostName = host.trim();
		return this;
	}

	public MFConfigBuilder readServerPortFromConsole(Console console) {
		int port = -1;
		do {
			String p = _port <= 0 ? console.readLine("Port: ") : console.readLine("Port[%d]: ", _port);
			if (_port > 0 && p != null && p.trim().isEmpty()) {
				// use existing value, no change
				return this;
			}
			if (p != null && !p.trim().isEmpty()) {
				try {
					port = Integer.parseInt(p.trim());
				} catch (NumberFormatException e) {
					console.printf("%n");
					console.printf("Invalid port: %s Expects a number in between 1 and 65535.", p.trim());
					console.printf("%n");
					port = -1;
				}
			}
		} while (port <= 0 || port > 65535);

		_port = port;
		return this;
	}

	public MFConfigBuilder readServerTransportFromConsole(Console console) {
		while (true) {
			String transport = console.readLine("Transport(HTTP/HTTPS/TCP_IP)[%s]: ", transport());
			if (transport != null) {
				if (transport.trim().isEmpty()) {
					// use existing value, no change
					return this;
				} else {
					transport = transport.trim();
					if (!transport.equalsIgnoreCase("HTTP") && !transport.equalsIgnoreCase("HTTPS")
							&& !transport.toLowerCase().startsWith("tcp")) {
						// invalid value
						console.printf("%n");
						console.printf("Invalid transport: %s. Expects HTTP, HTTPS or TCP_IP%n", transport);
						console.printf("%n");
					} else {
						parseTransport(transport);
						return this;
					}
				}
			}
		}
	}

	public MFConfigBuilder readUserFromConsole(Console console) {
		String user;
		do {
			user = _userName == null ? console.readLine("User: ") : console.readLine("User[%s]: ", _userName);
			if (_userName != null && user != null && user.trim().isEmpty()) {
				// use existing value, no change
				return this;
			}
		} while (user == null || user.trim().isEmpty());

		_userName = user.trim();
		return this;
	}

	public long retryWaitTime() {
		return _retryWaitTime;
	}

	public String sessionKey() {
		return _sessionKey;
	}

	public MFConfigBuilder setAllowUntrustedServer(boolean allowUntrustedServer) {
		_allowUntrustedServer = allowUntrustedServer;
		return this;
	}

	public MFConfigBuilder setApp(String app) {
		_app = app;
		return this;
	}

	public MFConfigBuilder setAuthenticationDetails(AuthenticationDetails authenticationDetails) {
		_domain = authenticationDetails.domain();
		_userName = authenticationDetails.userName();
		_userPassword = authenticationDetails.userPassword();
		_token = authenticationDetails.token();
		_app = authenticationDetails.application();
		return this;
	}

	public MFConfigBuilder setConnectionDetails(ConnectionDetails connectionDetails) {
		_hostName = connectionDetails.hostName();
		if (_hostName == null) {
			_hostName = connectionDetails.hostAddressOrName();
		}
		_port = connectionDetails.port();
		_useHttp = connectionDetails.useHttp();
		_encrypt = connectionDetails.encrypt();
		_allowUntrustedServer = connectionDetails.allowUntrustedServer();
		return this;
	}

	public void setConnectionPooling(boolean connectionPooling) {
		_connectionPooling = connectionPooling;
	}

	public MFConfigBuilder setConsoleLogon(boolean consoleLogon) {
		_consoleLogon = consoleLogon;
		return this;
	}

	public MFConfigBuilder setDomain(String domain) {
		_domain = domain;
		return this;
	}

	public MFConfigBuilder setHost(String host) {
		_hostName = host;
		return this;
	}

	public MFConfigBuilder setMaxNumberOfRetries(int nbRetries) {
		_maxNumberOfRetries = nbRetries;
		return this;
	}

	public MFConfigBuilder setPassword(String password) {
		_userPassword = password;
		return this;
	}

	public MFConfigBuilder setPort(int port) {
		_port = port;
		return this;
	}

	public MFConfigBuilder setRetryWaitTime(long millisecs) {
		_retryWaitTime = millisecs;
		return this;
	}

	public MFConfigBuilder setServer(String host, int port, boolean useHttp, boolean encrypt) {
		_hostName = host;
		_port = port;
		_useHttp = useHttp;
		_encrypt = encrypt;
		return this;
	}

	public MFConfigBuilder setServer(String host, int port, String transport) {
		_hostName = host;
		_port = port;
		setTransport(transport);
		return this;
	}

	public MFConfigBuilder setSessionKey(String sessionKey) {
		_sessionKey = sessionKey;
		return this;
	}

	public MFConfigBuilder setToken(String token) {
		_token = token;
		return this;
	}

	public MFConfigBuilder setTransport(String transport) {
		parseTransport(transport);
		return this;
	}

	public MFConfigBuilder setTransport(Transport transport) {
		if (transport != null) {
			switch (transport) {
			case HTTP:
				_useHttp = true;
				_encrypt = false;
				_allowUntrustedServer = false;
				break;
			case HTTPS_TRUSTED:
				_useHttp = true;
				_encrypt = true;
				_allowUntrustedServer = false;
				break;
			case HTTPS_UNTRUSTED:
				_useHttp = true;
				_encrypt = true;
				_allowUntrustedServer = true;
				break;
			case TCPIP:
				_useHttp = false;
				_encrypt = false;
				_allowUntrustedServer = false;
				break;
			default:
				break;
			}
		}
		return this;
	}

	public MFConfigBuilder setUseClusterIO(boolean useClusterIO) {
		_useClusterIO = useClusterIO;
		return this;
	}

	public MFConfigBuilder setUser(String user) {
		_userName = user;
		return this;
	}

	public MFConfigBuilder setUserCredentials(String domain, String user, String password) {
		_domain = domain;
		_userName = user;
		_userPassword = password;
		return this;
	}

	public MFConfigBuilder setProxy(String proxyHostName, int proxyPort, String proxyUserName,
			String proxyUserPassword) {
		if (proxyHostName == null) {
			throw new IllegalArgumentException("Invalid proxy host: " + null);
		}
		if (proxyPort <= 0 || proxyPort > 65535) {
			throw new IllegalArgumentException("Invalid proxy port: " + proxyPort);
		}
		_proxy = new ProxyDetails(proxyHostName, proxyPort, proxyUserName, proxyUserPassword);
		return this;
	}

	public ProxyDetails proxy() {
		return _proxy;
	}

	public MFConfigBuilder parseProxy(String proxyAddress) {
		if (proxyAddress != null) {
			URI uri = URI.create(!proxyAddress.contains("://") ? "http://" + proxyAddress : proxyAddress);
			String hostName = uri.getHost();
			if (hostName == null) {
				throw new IllegalArgumentException("Failed to parse proxy host from: " + proxyAddress);
			}

			int port = uri.getPort();
			if (port < 0 || port > 65535) {
				throw new IllegalArgumentException("Failed to parse proxy port from: " + proxyAddress);
			}

			String userInfo = uri.getUserInfo();
			String userName = null;
			String userPassword = null;
			if (userInfo != null) {
				if (userInfo.indexOf(':') == -1) {
					userName = userInfo;
				} else {
					String[] parts = userInfo.split(":");
					userName = parts[0];
					if (parts.length > 1) {
						userPassword = parts[1];
					}
				}
				if (userName == null || userName.isEmpty()) {
					throw new IllegalArgumentException("Failed to parse proxy user name from: " + proxyAddress);
				}
			}
			setProxy(hostName, port, userName, userPassword);
			return this;
		} else {
			throw new IllegalArgumentException("Invalid proxy address: " + null);
		}
	}

	public MFConfigBuilder setProxy(String proxyHostName, int proxyPort) {
		return setProxy(proxyHostName, proxyPort, null, null);
	}

	public String token() {
		return _token;
	}

	public String transport() {
		if (_useHttp) {
			return _encrypt ? "HTTPS" : "HTTP";
		} else {
			return "TCP_IP";
		}
	}

	public boolean useClusterIO() {
		return _useClusterIO;
	}

	public boolean useHttp() {
		return _useHttp;
	}

	public String user() {
		return _userName;
	}

}

package unimelb.mf.client.session;

import java.util.concurrent.Callable;

import arc.xml.XmlStringWriter;

public class MFFileSliceUploadTask implements Callable<Void> {

    private MFSession _session;
    private String _jobTicket;
    private MFFileSliceInput _input;

    MFFileSliceUploadTask(MFSession session, String jobTicket, MFFileSliceInput input) {
        _session = session;
        _jobTicket = jobTicket;
        _input = input;
    }

    @Override
    public Void call() throws Exception {
        try {
            XmlStringWriter w = new XmlStringWriter();
            w.add("complete", false);
            if (_input.store() != null) {
                w.add("store", _input.store());
            }
            w.add("ticket", _jobTicket);
            w.add("offset", _input.offset());
            // TODO remove debug
            // System.out.println("DEBUG: " + Thread.currentThread().getId() + ": " + DateTime.string(new Date())
            //         + " - start uploading file slice (offset=" + _input.offset() + ", length=" + _input.length()
            //         + ") using server.io.write");
            _session.execute("server.io.write", w.document(), _input);
            // TODO remove debug
            // System.out.println("DEBUG: " + Thread.currentThread().getId() + ": " + DateTime.string(new Date())
            //         + " - complete uploading file slice using server.io.write");
            return null;
        } catch (Throwable t) {
            if (t instanceof Exception) {
                throw (Exception) t;
            } else {
                throw new Exception(t);
            }
        }
    }

}

package unimelb.mf.client.session;

import java.util.ArrayList;
import java.util.List;

import arc.mf.client.RequestOptions;
import arc.mf.client.ServerClient;
import arc.mf.client.ServerRoute;
import arc.mf.client.ServiceSequenceListener;
import arc.mf.client.ServiceStatusHandler;
import arc.utils.AbortableOperationHandler;

public class MFRequestBuilder {

    private ServerRoute _route;
    private String _service;
    private String _args;
    private List<ServerClient.Input> _inputs;
    private ServerClient.Output _output;
    private RequestOptions _options;

    public MFRequestBuilder() {
        _options = new RequestOptions();
    }

    public MFRequestBuilder setRoute(ServerRoute route) {
        _route = route;
        return this;
    }

    public MFRequestBuilder setService(String service) {
        _service = service;
        return this;
    }

    public MFRequestBuilder setArgs(String args) {
        _args = args;
        return this;
    }

    public MFRequestBuilder setInputs(List<ServerClient.Input> inputs) {
        if (inputs == null || inputs.isEmpty()) {
            _inputs = null;
        } else {
            _inputs = new ArrayList<>(inputs);
        }
        return this;
    }

    public MFRequestBuilder addInput(ServerClient.Input input) {
        if (_inputs == null) {
            _inputs = new ArrayList<>();
        }
        _inputs.add(input);
        return this;
    }

    public MFRequestBuilder setOutput(ServerClient.Output output) {
        _output = output;
        return this;
    }

    public MFRequestBuilder setOptions(RequestOptions options) {
        _options = options == null ? new RequestOptions() : options;
        return this;
    }

    public MFRequestBuilder setAbortHandler(AbortableOperationHandler ah) {
        _options.setAbortHandler(ah);
        return this;
    }

    public MFRequestBuilder setBackground(boolean background) {
        _options.setBackground(background);
        return this;
    }

    public MFRequestBuilder setDescription(String description) {
        _options.setDescription(description);
        return this;
    }

    public MFRequestBuilder setKey(String key) {
        _options.setKey(key);
        return this;
    }

    public MFRequestBuilder setRetainHours(int retainHours) {
        _options.setRetainHours(retainHours);
        return this;
    }

    public MFRequestBuilder setSequenceListener(ServiceSequenceListener sequenceListener) {
        _options.setSequenceListener(sequenceListener);
        return this;
    }

    public MFRequestBuilder setStatusHandler(ServiceStatusHandler statusHandler, int waitTime) {
        _options.setStatusHandler(statusHandler, waitTime);
        return this;
    }

    public MFRequest build() {
        return new MFRequest(_route, _service, _args, _inputs, _output, _options);
    }

    public MFRequest build(ServerRoute route, String service, String args, List<ServerClient.Input> inputs,
                           ServerClient.Output output, RequestOptions options) {
        return setRoute(route).setService(service).setArgs(args).setInputs(inputs).setOutput(output).setOptions(options)
                .build();
    }

}

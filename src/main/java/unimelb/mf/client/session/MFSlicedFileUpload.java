package unimelb.mf.client.session;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import arc.xml.XmlStringWriter;

public class MFSlicedFileUpload implements Callable<String> {

    private MFSession _session;
    private MFSlicedFile _file;
    private String _store;
    private ExecutorService _executor;

    public MFSlicedFileUpload(MFSession session, MFSlicedFile file, String store, ExecutorService executor) {
        _session = session;
        _file = file;
        _store = (store == null || store.startsWith("asset:")) ? store : ("asset:" + store);
        _executor = executor;
    }

    @Override
    public String call() throws Exception {
        try {
            return execute();
        } catch (Throwable t) {
            if (t instanceof Exception) {
                throw (Exception) t;
            } else {
                throw new Exception(t);
            }
        }
    }

    private String execute() throws Throwable {
        /**
         * create server io job
         */
        XmlStringWriter w = new XmlStringWriter();
        w.add("size", _file.fileLength());
        w.add("ext", _file.fileExtension());
        if (_store != null) {
            w.add("store", _store);
        }
        // TODO
//        w.add("cluster", _session.useClusterIO());
        w.add("cluster", false);

        String jobTicket = _session.execute("server.io.job.create", w.document()).value("ticket");

        try {
            /**
             * upload slices in parallel
             */
            MFFileSliceInput[] inputs = _file.getSliceInputs();
            List<Future<Void>> futures = new ArrayList<>();
            for (MFFileSliceInput input : inputs) {
                Future<Void> future = _executor.submit(new MFFileSliceUploadTask(_session, jobTicket, input));
                futures.add(future);
            }
            
            // join
            for (Future<Void> future : futures) {
                future.get();
            }

            // finish the io job
            w = new XmlStringWriter();
            w.add("size", _file.fileLength());
            w.add("ticket", jobTicket);
            _session.execute("server.io.write.finish", w.document());

            return jobTicket;
        } catch (Throwable e) {
            discardServerIOJob(_session, jobTicket);
            throw e;
        }
    }

    public static void discardServerIOJob(MFSession session, String jobTicket) {
        try {
            XmlStringWriter w = new XmlStringWriter();
            w.add("ticket", jobTicket);
            session.execute("server.io.job.discard", w.document());
        } catch (Throwable e) {

        }
    }

}

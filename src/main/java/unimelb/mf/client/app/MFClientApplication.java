package unimelb.mf.client.app;

import unimelb.mf.client.task.MFClientTask;
import unimelb.mf.client.session.MFSession;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public interface MFClientApplication {

    String applicationName();

    MFSession session();

    ExecutorService executor();

    default <T> void execute(MFClientTask<T> task, boolean blocking) throws Throwable {
        if (executor() != null) {
            Future<T> future = executor().submit(task);
            if (blocking) {
                future.get();
            }
        } else {
            task.execute();
        }
    }

    default void shutdown(boolean wait) throws Throwable {
        if (executor() != null) {
            if (!executor().isShutdown()) {
                executor().shutdown();
            }
            if (wait) {
                // wait until all tasks are processed by workers.
                executor().awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            }
        }
    }
}

package unimelb.mf.client.util;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;

public class AssetUtils {

    public static XmlDoc.Element getAssetMetaById(MFSession session, String assetId) throws Throwable {
        return getAssetMeta(session, assetId, null, null, false);
    }

    public static XmlDoc.Element getAssetMetaByCid(MFSession session, String cid) throws Throwable {
        return getAssetMeta(session, null, cid, null, false);
    }

    public static XmlDoc.Element getAssetMetaByPath(MFSession session, String assetPath) throws Throwable {
        return getAssetMetaByPath(session, assetPath, null, false);
    }

    public static XmlDoc.Element getAssetMetaById(MFSession session, String assetId, Integer version) throws Throwable {
        return getAssetMeta(session, assetId, null, version, false);
    }

    public static XmlDoc.Element getAssetMetaByCid(MFSession session, String cid, Integer version) throws Throwable {
        return getAssetMeta(session, null, cid, version, false);
    }

    public static XmlDoc.Element getAssetMetaByPath(MFSession session, String assetPath, Integer version)
            throws Throwable {
        return getAssetMetaByPath(session, assetPath, version, false);
    }

    public static XmlDoc.Element getAssetMetaById(MFSession session, String assetId, Integer version, boolean lock)
            throws Throwable {
        return getAssetMeta(session, assetId, null, version, lock);
    }

    public static XmlDoc.Element getAssetMetaByCid(MFSession session, String cid, Integer version, boolean lock)
            throws Throwable {
        return getAssetMeta(session, null, cid, version, lock);
    }

    public static XmlDoc.Element getAssetMetaByPath(MFSession session, String assetPath, Integer version, boolean lock)
            throws Throwable {
        return getAssetMeta(session, "path=" + assetPath, null, version, lock);
    }

    public static XmlDoc.Element getAssetMeta(MFSession session, String assetId, String cid, Integer version,
            boolean lock) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        if (assetId != null && cid != null) {
            throw new IllegalArgumentException("Expects asset id or cid. Found both.");
        }
        if (assetId == null && cid == null) {
            throw new IllegalArgumentException("Missing asset identifier. Expects asset id or cid.");
        }
        if (assetId != null) {
            w.add("id", new String[] { "version", version == null ? null : Integer.toString(version) }, assetId);
        } else {
            w.add("cid", new String[] { "version", version == null ? null : Integer.toString(version) }, cid);
        }
        if (lock) {
            w.add("lock", true);
        }
        return session.execute("asset.get", w.document()).element("asset");
    }

    public static String assetIdFromCid(MFSession session, String cid) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("cid", cid);
        return session.execute("asset.identifier.get", w.document()).value("id");
    }

    public static String assetIdFromPath(MFSession session, String assetPath) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("path", assetPath);
        return session.execute("asset.identifier.get", w.document()).value("id");
    }

    public static String assetCidFromId(MFSession session, String assetId) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", assetId);
        return session.execute("asset.identifier.get", w.document()).value("id/@cid");
    }

    public static boolean assetExistsById(MFSession session, String assetId) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", assetId);
        return session.execute("asset.exists", w.document()).booleanValue("exists");
    }

    public static boolean assetExistsByCid(MFSession session, String cid) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("cid", cid);
        return session.execute("asset.exists", w.document()).booleanValue("exists");
    }

    public static boolean assetExistsByPath(MFSession session, String assetPath) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", "path=" + assetPath);
        return session.execute("asset.exists", w.document()).booleanValue("exists");
    }

    public static String getAssetStoreByCid(MFSession session, String cid) throws Throwable {
        XmlDoc.Element ae = getAssetMetaByCid(session, cid);
        String assetNamespace = ae.value("namespace");
        return AssetNamespaceUtils.getStore(session, assetNamespace);
    }

    public static String getAssetVidByCid(MFSession session, String cid) throws Throwable {
        XmlDoc.Element ae = getAssetMetaByCid(session, cid);
        return ae.value("@vid");
    }

    public static Long getAssetContentCsumByCid(MFSession session, String cid) throws Throwable {
        XmlDoc.Element ae = getAssetMetaByCid(session, cid);
        return ae.longValue("content/csum[@base='10']", null);
    }
}

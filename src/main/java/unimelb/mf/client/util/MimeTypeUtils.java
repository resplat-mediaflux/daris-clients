package unimelb.mf.client.util;

import java.util.Collection;

import unimelb.mf.client.session.MFSession;

public class MimeTypeUtils {

    public static Collection<String> typesFromExt(MFSession session, String ext) throws Throwable {
        return session.execute("type.ext.types", "<extension>" + ext + "</extension>").values("extension/type");
    }

    public static String typeFromExt(MFSession session, String ext) throws Throwable {
        return session.execute("type.ext.types", "<extension>" + ext + "</extension>").value("extension/type");
    }

}

package unimelb.mf.client.util;

import java.util.logging.Logger;

import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;
import unimelb.utils.PathUtils;

public class AssetNamespaceUtils {

    public static void createAssetNamespace(MFSession session, String ns, boolean parents, Logger logger)
            throws Throwable {
        if (!assetNamespaceExists(session, ns)) {
            if (logger != null) {
                logger.info("Creating asset namespace: '" + ns + "'");
            }
            XmlStringWriter w = new XmlStringWriter();
            w.add("namespace", new String[] { "all", Boolean.toString(parents) }, ns);
            session.execute("asset.namespace.create", w.document());
        }
    }

    public static void createAssetNamespace(MFSession session, String ns, boolean parents) throws Throwable {
        createAssetNamespace(session, ns, parents, (Logger) null);
    }

    public static boolean assetNamespaceExists(MFSession session, String ns) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", ns);
        return session.execute("asset.namespace.exists", w.document()).booleanValue("exists");
    }

    public static String getStore(MFSession session, String namespace) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("namespace", namespace);
        return session.execute("asset.namespace.store.name", w.document()).value("store");
    }

    public static String getParentAssetNamespace(String assetNamespace) {
        if (assetNamespace == null || assetNamespace.isEmpty() || "/".equals(assetNamespace)) {
            return null;
        }
        return PathUtils.getParentPath(assetNamespace);
    }
}

package unimelb.mf.client.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.List;

import arc.xml.XmlDoc;
import unimelb.utils.StringUtils;

public class XmlUtils {
    public static void print(PrintStream ps, XmlDoc.Element e) {
        print(ps, e, 0, 4);
    }

    public static void print(PrintStream ps, XmlDoc.Element e, int indent, int tabSize) {
        ps.print(StringUtils.repeat(' ', indent));
        ps.print(":" + e.name());
        List<XmlDoc.Attribute> attrs = e.attributes();
        if (attrs != null) {
            for (XmlDoc.Attribute attr : attrs) {
                ps.print(String.format(" -%s \"%s\"", attr.name(), attr.value()));
            }
        }
        if (e.value() != null) {
            ps.print(" \"" + e.value() + "\"");
        }
        ps.println();

        List<XmlDoc.Element> ses = e.elements();
        if (ses != null) {
            for (XmlDoc.Element se : ses) {
                print(ps, se, indent + tabSize, tabSize);
            }
        }
    }

    public static void saveToFile(XmlDoc.Element e, File f) throws Throwable {
        OutputStream os = new BufferedOutputStream(new FileOutputStream(f));
        try {
            os.write(e.toString().getBytes("UTF-8"));
            os.flush();
        } finally {
            os.close();
        }
    }

    public static void saveToFile(XmlDoc.Element e, Path f) throws Throwable {
        saveToFile(e, f.toFile());
    }
}

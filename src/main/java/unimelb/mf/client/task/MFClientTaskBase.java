package unimelb.mf.client.task;

import org.slf4j.Logger;

import unimelb.mf.client.session.MFSession;

public abstract class MFClientTaskBase<T> implements MFClientTask<T> {

    private final MFSession session;

    protected MFClientTaskBase(MFSession session) {
        this.session = session;
    }

    public abstract Logger logger();

    @Override
    public final MFSession session() {
        return this.session;
    }

    @Override
    public final void logError(String message, Throwable e) {
        logger().error(message, e);
    }

    @Override
    public final void logInfo(String message) {
        logger().info(message);
    }

    @Override
    public final void logWarning(String message) {
        logger().warn(message);
    }

}

package unimelb.mf.client.task;

import java.util.concurrent.Callable;

import unimelb.logging.Loggable;
import unimelb.mf.client.session.MFSession;

public interface MFClientTask<T> extends Callable<T>, Loggable {

    MFSession session();

    T execute() throws Throwable;

    default T call() throws Exception {
        try {
            return execute();
        } catch (Throwable e) {
            logError(e.getMessage(), e);
            if (e instanceof Exception) {
                if (e instanceof InterruptedException) {
                    Thread.currentThread().interrupt();
                }
                throw (Exception) e;
            } else {
                throw new Exception(e);
            }
        }
    }

}

package unimelb.mf.client.gui.object;

import java.util.ArrayList;
import java.util.List;

import arc.mf.client.ServerClient;
import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import arc.xml.XmlWriter;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Worker.State;
import unimelb.mf.client.gui.app.Application;
import unimelb.mf.client.gui.app.Service;
import unimelb.mf.client.session.MFRequest;
import unimelb.mf.client.session.MFRequestBuilder;
import unimelb.utils.Action;

public abstract class ObjectMessage<T> {

    private Application _app;

    private SimpleBooleanProperty _executing;
    private SimpleBooleanProperty _succeeded;
    private SimpleBooleanProperty _executed;

    private List<ServerClient.Input> _inputs;
    private ServerClient.Output _output;

    private SimpleObjectProperty<T> _result;

    protected ObjectMessage(Application app) {
        _app = app;
        _executing = new SimpleBooleanProperty(false);
        _succeeded = new SimpleBooleanProperty(false);
        _executed = new SimpleBooleanProperty(false);
        _result = new SimpleObjectProperty<T>(null);
    }

    private MFRequest buildRequest() {
        MFRequest request = null;
        MFRequestBuilder rb = new MFRequestBuilder();
        rb.setService(serviceName());
        XmlStringWriter w = new XmlStringWriter();
        updateServiceArgs(w);
        rb.setInputs(_inputs);
        rb.setOutput(_output);
        try {
            rb.setArgs(w.document());
            request = rb.build();
        } catch (Throwable e) {
            _app.displayError("constructing request for service: " + serviceName(), e);
        }
        return request;
    }

    protected abstract String serviceName();

    protected abstract void updateServiceArgs(XmlWriter w);

    protected abstract T instantiate(XmlDoc.Element re);

    protected void setInput(ServerClient.Input... inputs) {
        if (inputs == null || inputs.length == 0) {
            _inputs = null;
        } else {
            if (_inputs == null) {
                _inputs = new ArrayList<ServerClient.Input>(inputs.length);
            }
            for (ServerClient.Input input : inputs) {
                _inputs.add(input);
            }
        }
    }

    protected void setOutput(ServerClient.Output output) {
        _output = output;
    }

    public void send(Action postAction) {
        Service<T> service = new Service<T>(_app, null) {
            protected T instantiate(XmlDoc.Element re) {
                return ObjectMessage.this.instantiate(re);
            }
        };
        service.setRequest(buildRequest());
        service.setOnSucceeded(e -> {
            _succeeded.set(true);
            if (postAction != null) {
                postAction.execute();
            }
        });
        _result.bind(service.valueProperty());
        service.stateProperty().addListener((obs, ov, nv) -> {
            _executing.set(nv == State.SCHEDULED || nv == State.RUNNING);
            _executed.set(nv == State.CANCELLED || nv == State.FAILED || nv == State.SUCCEEDED);
        });
        service.start();
    }

    public boolean executing() {
        return _executing.get();
    }

    public boolean executed() {
        return _executed.get();
    }

    public boolean succeeded() {
        return _succeeded.get();
    }

    public T result() {
        return _result.get();
    }

    public ReadOnlyObjectProperty<T> resultProperty() {
        return _result;
    }

}

package unimelb.mf.client.gui.object;

import java.util.ArrayList;
import java.util.List;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import arc.xml.XmlWriter;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Worker.State;
import unimelb.mf.client.gui.app.ApplicationConfiguration;
import unimelb.mf.client.gui.app.Application;
import unimelb.mf.client.gui.app.Service;
import unimelb.mf.client.session.MFRequest;
import unimelb.mf.client.session.MFRequestBuilder;
import unimelb.utils.Action;

public abstract class ObjectCollectionRef<T> {

    private Application _app;
    private SimpleLongProperty _idx;
    private SimpleIntegerProperty _pageSize;
    private SimpleLongProperty _remaining;
    private SimpleLongProperty _total;
    private SimpleLongProperty _from;
    private SimpleLongProperty _to;
    private SimpleBooleanProperty _resolved;
    private SimpleBooleanProperty _resolving;
    private SimpleObjectProperty<List<T>> _collection;

    protected ObjectCollectionRef(Application app) {
        _app = app;
        _idx = new SimpleLongProperty(1);
        _idx.addListener((obs, ov, nv) -> {
            reset();
        });
        _pageSize = new SimpleIntegerProperty(_app.configuration().pageSize());
        _pageSize.addListener((obs, ov, nv) -> {
            reset();
        });
        _remaining = new SimpleLongProperty(Long.MAX_VALUE);
        _total = new SimpleLongProperty(-1);
        _from = new SimpleLongProperty(1);
        _to = new SimpleLongProperty(_pageSize.get());
        _resolved = new SimpleBooleanProperty(false);
        _resolving = new SimpleBooleanProperty(false);
        _collection = new SimpleObjectProperty<List<T>>(null);
    }

    private MFRequest buildRequest() {
        MFRequest request = null;
        MFRequestBuilder rb = new MFRequestBuilder();
        rb.setService(serviceName());
        try {
            XmlStringWriter w = new XmlStringWriter();
            updateServiceArgs(_idx.get(), _pageSize.get(), w);
            rb.setArgs(w.document());
            request = rb.build();
        } catch (Throwable e) {
            _app.displayError("constructing request for service: " + serviceName(), e);
        }
        return request;
    }

    protected void setSize(int size) {
        if (size <= 0) {
            _pageSize.set(ApplicationConfiguration.DEFAULT_PAGE_SIZE);
        } else if (size > ApplicationConfiguration.MAX_PAGE_SIZE) {
            _pageSize.set(ApplicationConfiguration.MAX_PAGE_SIZE);
        } else {
            _pageSize.set(size);
        }
    }

    public void setIdx(long idx) {
        if (idx < 1) {
            _idx.set(1);
        } else {
            _idx.set(idx);
        }
    }

    protected abstract String entityName();

    protected abstract String serviceName();

    protected abstract void updateServiceArgs(long idx, int size, XmlWriter w) throws Throwable;

    protected abstract T instantiateMember(XmlDoc.Element me) throws Throwable;

    protected List<T> instantiate(XmlDoc.Element re) {
        try {
            List<XmlDoc.Element> mes = re.elements(entityName());
            XmlDoc.Element ce = re.element("cursor");
            if (ce == null) {
                throw new Exception("Missing cursor element in the result of service: " + serviceName());
            }
            updateCursor(ce);
            if (mes != null && !mes.isEmpty()) {
                List<T> ms = new ArrayList<T>();
                for (XmlDoc.Element me : mes) {
                    T m = instantiateMember(me);
                    ms.add(m);
                }
                return ms;
            }
        } catch (Throwable e) {
            _app.displayError("Instantiating result of service: " + serviceName(), e);
        }
        return null;
    }

    protected void updateCursor(XmlDoc.Element ce) throws Throwable {
        _total.set(ce.longValue("total"));
        _remaining.set(ce.longValue("remaining"));
        _from.set(ce.longValue("from"));
        _to.set(ce.longValue("to"));
        // set idx to the first record of the page.
        _idx.set(_from.get());
    }

    public void resolve(Action postAction) {
        if (resolved()) {
            if (postAction != null) {
                postAction.execute();
            }
        } else {
            MFRequest request = buildRequest();
            Service<List<T>> service = new Service<List<T>>(_app, null) {
                protected List<T> instantiate(XmlDoc.Element re) {
                    return ObjectCollectionRef.this.instantiate(re);
                }
            };
            service.setRequest(request);
            service.setOnSucceeded(e -> {
                _resolved.set(true);
                if (postAction != null) {
                    service.setOnSucceeded(null);
                    postAction.execute();
                }
            });
            _collection.bind(service.valueProperty());
            service.stateProperty().addListener((obs, ov, nv) -> {
                _resolving.set(nv == State.SCHEDULED || nv == State.RUNNING);
            });
            service.start();
        }
    }

    public void resolve(long idx, Action postAction) {
        setIdx(idx);
        resolve(postAction);
    }

    public boolean resolved() {
        return _resolved.get();
    }
    
    public boolean resolving() {
        return _resolving.get();
    }

    public void reset() {
        _resolved.set(false);
    }

    public List<T> collection() {
        return _collection.get();
    }

    public ReadOnlyObjectProperty<List<T>> collectionProperty() {
        return _collection;
    }

    public ReadOnlyLongProperty totalProperty() {
        return _total;
    }

    public long total() {
        return _total.get();
    }

    public void setTotal(long total) {
        _total.set(total);
    }

    public ReadOnlyLongProperty remainingProperty() {
        return _remaining;
    }

    public ReadOnlyLongProperty fromProperty() {
        return _from;
    }

    public ReadOnlyLongProperty toProperty() {
        return _to;
    }

    public ReadOnlyLongProperty idxProperty() {
        return _idx;
    }

    public ReadOnlyIntegerProperty pageSizeProperty() {
        return _pageSize;
    }

    public int pageSize() {
        return _pageSize.get();
    }

}

package unimelb.mf.client.gui.object;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import arc.xml.XmlWriter;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Worker.State;
import unimelb.daris.client.model.object.DObject;
import unimelb.mf.client.gui.app.Application;
import unimelb.mf.client.gui.app.Service;
import unimelb.mf.client.session.MFRequest;
import unimelb.mf.client.session.MFRequestBuilder;
import unimelb.utils.Action;

public abstract class ObjectRef<T> {

    private Application _app;
    private SimpleBooleanProperty _resolved;
    private SimpleBooleanProperty _resolving;
    private SimpleObjectProperty<T> _referent;

    protected ObjectRef(Application app) {
        _app = app;
        _resolved = new SimpleBooleanProperty(false);
        _resolving = new SimpleBooleanProperty(false);
        _referent = new SimpleObjectProperty<T>(null);
    }

    private MFRequest buildRequest() {
        MFRequest request = null;
        MFRequestBuilder rb = new MFRequestBuilder();
        rb.setService(serviceName());
        XmlStringWriter w = new XmlStringWriter();
        try {
            updateServiceArgs(w);
            rb.setArgs(w.document());
            request = rb.build();
        } catch (Throwable e) {
            _app.displayError("constructing request for service: " + serviceName(), e);
        }
        return request;
    }

    protected abstract String serviceName();

    protected abstract void updateServiceArgs(XmlWriter w) throws Throwable;

    protected abstract T instantiate(XmlDoc.Element re) throws Throwable;

    public void resolve(Action postAction) {
        Service<T> service = new Service<T>(_app, null) {
            protected T instantiate(XmlDoc.Element re) throws Throwable {
                return ObjectRef.this.instantiate(re);
            }
        };
        service.setRequest(buildRequest());
        service.setOnSucceeded(e -> {
            _resolved.set(true);
            if (postAction != null) {
                postAction.execute();
            }
        });
        _referent.bind(service.valueProperty());
        service.stateProperty().addListener((obs, ov, nv) -> {
            _resolving.set(nv == State.SCHEDULED || nv == State.RUNNING);
        });
        service.start();
    }

    public boolean resolved() {
        return _resolved.get();
    }

    public boolean resolving() {
        return _resolving.get();
    }

    public void reset() {
        if (resolved()) {
            _resolved.set(false);
        }
    }

    public T referent() {
        return _referent.get();
    }

    public ReadOnlyObjectProperty<T> referentProperty() {
        return _referent;
    }

}
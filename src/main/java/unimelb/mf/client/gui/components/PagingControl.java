package unimelb.mf.client.gui.components;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class PagingControl implements InterfaceComponent {

	public static final int DEFAULT_PAGE_SIZE = 100;
	public static final int MIN_PAGE_SIZE = 1;

	private MenuButton _mb;

	private SimpleLongProperty _total;
	private SimpleLongProperty _from;
	private SimpleLongProperty _to;
	private SimpleIntegerProperty _pageSize;

	private SimpleLongProperty _pageIndex;
	private SimpleLongProperty _pageCount;

	private SimpleLongProperty _goto;
	private HBox _gotoHBox;

	public PagingControl() {
		_mb = new MenuButton();
		_mb.setFont(Font.font(11));
		_mb.setTextFill(Color.BLUE);
		_total = new SimpleLongProperty(0);
		_pageSize = new SimpleIntegerProperty(DEFAULT_PAGE_SIZE);
		_from = new SimpleLongProperty(0);
		_to = new SimpleLongProperty(0);
		_pageIndex = new SimpleLongProperty(0);
		_pageCount = new SimpleLongProperty(0);

		_goto = new SimpleLongProperty(0);

		updateMenuButtonText(_from.get(), _to.get(), _total.get(), _pageSize.get());
		_gotoHBox = new HBox();
		_mb.getItems().add(new CustomMenuItem(_gotoHBox));
		_mb.showingProperty().addListener((obsShowing, oldShowing, showing) -> {
			if (showing) {
				long pageIndex = pageIndex();
				long pageCount = pageCount();
				_gotoHBox.getChildren().clear();
				_gotoHBox.getChildren().add(new Label("Go to: "));
				Slider gotoSlider = new Slider(0, 0, 0);
				gotoSlider.setShowTickLabels(true);
				gotoSlider.setShowTickMarks(true);
				gotoSlider.setOrientation(Orientation.HORIZONTAL);
				gotoSlider.setMin(pageCount <= 0 ? 0 : 1);
				gotoSlider.setMax(pageCount <= 0 ? 0 : pageCount);
				double blockIncrement = pageCount <= 20 ? 1 : pageCount / 20;
				gotoSlider.setBlockIncrement(blockIncrement);
				double majorTickUnit = pageCount <= 20 ? 1 : pageCount / 20;
				gotoSlider.setMajorTickUnit(majorTickUnit);
				gotoSlider.setValue(pageIndex);
				_gotoHBox.getChildren().add(gotoSlider);
				gotoSlider.valueProperty().addListener((obs, ov, nv) -> {
					long newPageIdx = Math.round(nv.doubleValue());
					long newIdx = (newPageIdx - 1) * 100 + 1;
					_goto.set(newIdx);
				});
			}
		});

		_from.addListener((obs, ov, nv) -> {
			long from = (Long) nv;
			Platform.runLater(() -> {
				updateMenuButtonText(from, to(), total(), pageSize());
				updatePageInfo(from, to(), total(), pageSize());
			});
		});

		_to.addListener((obs, ov, nv) -> {
			long to = (Long) nv;
			Platform.runLater(() -> {
				updateMenuButtonText(from(), to, total(), pageSize());
			});
		});

		_total.addListener((obs, ov, nv) -> {
			long total = (Long) nv;
			Platform.runLater(() -> {
				updateMenuButtonText(from(), to(), total, pageSize());
				updatePageInfo(from(), to(), total, pageSize());
			});
		});

		_pageSize.addListener((obs, ov, nv) -> {
			int pageSize = (Integer) nv;
			Platform.runLater(() -> {
				updateMenuButtonText(from(), to(), total(), pageSize);
				updatePageInfo(from(), to(), total(), pageSize);
			});
		});

	}

	public int pageSize() {
		return _pageSize.get();
	}

	public void setPageSize(int pageSize) {
		if (pageSize < MIN_PAGE_SIZE) {
			_pageSize.set(MIN_PAGE_SIZE);
		} else {
			_pageSize.set(pageSize);
		}
	}

	public IntegerProperty pageSizeProperty() {
		return _pageSize;
	}

	public LongProperty totalProperty() {
		return _total;
	}

	public long total() {
		return _total.get();
	}

	public LongProperty fromProperty() {
		return _from;
	}

	public long from() {
		return _from.get();
	}

	public LongProperty toProperty() {
		return _to;
	}

	public long to() {
		return _to.get();
	}

	private void updatePageInfo(long from, long to, long total, int pageSize) {
		// update page index
		_pageIndex.set(from / (long) pageSize + 1L);

		// update page count
		if (total < 0) {
			// unknown;
			_pageCount.set(-1L);
		} else if (total == 0) {
			_pageCount.set(0);
		} else {
			_pageCount.set(total / (long) pageSize + 1L);
		}
	}

	public long pageIndex() {
		return _pageIndex.get();
	}

	public long pageCount() {
		return _pageCount.get();
	}

	public ReadOnlyLongProperty gotoProperty() {
		return _goto;
	}

	protected void updateMenuButtonText(long from, long to, long total, int pageSize) {
		StringBuilder sb = new StringBuilder();
		sb.append(from);
		sb.append("..");
		sb.append(to);
		if (total > 0) {
			sb.append("/");
			sb.append(total);
		}
		_mb.setText(sb.toString());
	}

	@Override
	public Node gui() {
		return _mb;
	}

}

package unimelb.mf.client.gui.components;

import javafx.beans.property.ReadOnlyBooleanProperty;

public interface CanChange {

	ReadOnlyBooleanProperty changedProperty();

	default boolean changed() {
		return changedProperty().get();
	}

}

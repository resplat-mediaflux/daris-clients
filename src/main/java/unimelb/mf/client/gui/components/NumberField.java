package unimelb.mf.client.gui.components;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

public class NumberField<T extends Number> implements InterfaceComponent {

    private TextField _textField;

    private final T _initialValue;
    private final ObjectProperty<T> _value;
    private final ObjectProperty<T> _maxValue;
    private final ObjectProperty<T> _minValue;
    private final String _validChars;

    public T value() {
        return _value.getValue();
    }

    public void setValue(T newValue) {
        _value.setValue(newValue);
    }

    public ObjectProperty<T> valueProperty() {
        return _value;
    }

    public T maxValue() {
        return _maxValue.get();
    }

    public void setMaxValue(T maxValue) {
        _maxValue.set(maxValue);
    }

    public ObjectProperty<T> maxValueProperty() {
        return _maxValue;
    }

    public T minValue() {
        return _minValue.get();
    }

    public void setMinValue(T minValue) {
        _minValue.set(minValue);
    }

    public ObjectProperty<T> minValueProperty() {
        return _minValue;
    }

    public NumberField(T minValue, T maxValue, T initialValue) {
        _initialValue = initialValue;
        if (minValue.doubleValue() > maxValue.doubleValue()) {
            throw new IllegalArgumentException(
                    "LongField min value " + minValue + " greater than max value " + maxValue);
        }
        if (maxValue.doubleValue() < minValue.doubleValue()) {
            throw new IllegalArgumentException("IntField max value " + minValue + " less than min value " + maxValue);
        }
        if (!((minValue.doubleValue() <= initialValue.doubleValue())
                && (initialValue.doubleValue() <= maxValue.doubleValue()))) {
            throw new IllegalArgumentException(
                    "LongField initial value " + initialValue + " not between " + minValue + " and " + maxValue);
        }
        if (!(minValue instanceof Integer || minValue instanceof Long || minValue instanceof Float
                || minValue instanceof Double)) {
            throw new UnsupportedOperationException("Only Integer, Long, Float and Double are supported.");
        }

        _minValue = new SimpleObjectProperty<T>(minValue);
        _maxValue = new SimpleObjectProperty<T>(maxValue);
        _value = new SimpleObjectProperty<T>(initialValue);

        _textField = new TextField();
        _textField.setText(String.valueOf(initialValue));

        //
        StringBuilder sb = new StringBuilder("0123456789");
        if (minValue.doubleValue() < 0) {
            sb.append("-");
        }
        if (minValue instanceof Double || minValue instanceof Float) {
            sb.append(".");
        }
        _validChars = sb.toString();

        // make sure the value property is clamped to the required range
        // and update the field's text to be in sync with the value.
        _value.addListener((obs, ov, nv) -> {
            if (nv == null) {
                _textField.setText("");
            } else {
                if (nv.doubleValue() < minValue().doubleValue()) {
                    setValue(minValue());
                    return;
                }

                if (nv.doubleValue() > maxValue().doubleValue()) {
                    setValue(maxValue());
                    return;
                }

                if (nv.doubleValue() == 0
                        && (_textField.textProperty().get() == null || "".equals(_textField.textProperty().get()))) {
                    // no action required, text property is already blank,
                    // we don't need to set it to 0.
                } else {
                    _textField.setText(nv.toString());
                }
            }
        });

        // restrict key input to numerals.
        _textField.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (!_validChars.contains(keyEvent.getCharacter())) {
                    keyEvent.consume();
                }
            }
        });

        // ensure any entered values lie inside the required range.
        _textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                if (newValue == null || "".equals(newValue)) {
                    _value.setValue(_initialValue);
                    return;
                }

                String regex = minValue().doubleValue() >= 0 ? "^\\ *\\d+\\ *$" : "^\\ *-?\\d+\\ *$";
                if (!newValue.matches(regex)) {
                    _textField.textProperty().setValue(oldValue);
                } else {
                    T number = parseNumber(newValue);
                    if (minValue().doubleValue() > number.doubleValue()
                            || number.doubleValue() > maxValue().doubleValue()) {
                        _textField.textProperty().setValue(oldValue);
                    }
                }
                _value.set(parseNumber(_textField.textProperty().get()));
            }
        });
    }

    @SuppressWarnings("unchecked")
    private T parseNumber(String str) {
        if (_initialValue instanceof Integer) {
            return (T) Integer.valueOf(Integer.parseInt(str));
        } else if (_initialValue instanceof Long) {
            return (T) Long.valueOf(Long.parseLong(str));
        } else if (_initialValue instanceof Float) {
            return (T) Float.valueOf(Float.parseFloat(str));
        } else {
            return (T) Double.valueOf(Double.parseDouble(str));
        }
    }

    @Override
    public Node gui() {
        return _textField;
    }

    public void setPrefWidth(double value) {
        _textField.setPrefWidth(value);
    }

    public void setPromptText(String value) {
        _textField.setPromptText(value);
    }

    public void setOnAction(EventHandler<ActionEvent> value) {
        _textField.setOnAction(value);
    }

}
package unimelb.mf.client.gui.components;

import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyStringProperty;

public interface MustBeValid extends CanChange {

	ReadOnlyBooleanProperty validProperty();

	ReadOnlyStringProperty issueProperty();

	default boolean valid() {
		return validProperty().get();
	}

	default String issue() {
		return issueProperty().get();
	}

}

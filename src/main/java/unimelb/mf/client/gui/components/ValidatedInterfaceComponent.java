package unimelb.mf.client.gui.components;

import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.scene.Node;

public class ValidatedInterfaceComponent implements InterfaceComponent, MustBeValid {

	private SimpleBooleanProperty _changed;
	private SimpleBooleanProperty _valid;
	private SimpleStringProperty _issue;

	private SimpleListProperty<MustBeValid> _mbvs;

	protected ValidatedInterfaceComponent() {
		_changed = new SimpleBooleanProperty(false);
		_valid = new SimpleBooleanProperty(true);
		_issue = new SimpleStringProperty(null);
		_mbvs = new SimpleListProperty<MustBeValid>(FXCollections.observableArrayList());
		
		// TODO wiring/binding
	}

	@Override
	public ReadOnlyBooleanProperty changedProperty() {
		return _changed;
	}

	protected void setChanged(boolean changed) {
		_changed.setValue(changed);
	}

	@Override
	public ReadOnlyBooleanProperty validProperty() {
		return _valid;
	}

	protected void setValid(boolean valid) {
		_valid.setValue(valid);
	}

	@Override
	public ReadOnlyStringProperty issueProperty() {
		return _issue;
	}

	protected void setIssue(String issue) {
		_issue.setValue(issue);
	}

	protected void addMustBeValid(MustBeValid mbv) {
		_mbvs.get().add(mbv);
	}

	protected void removeMustBeValid(MustBeValid mbv) {
		_mbvs.get().remove(mbv);
	}

	@Override
	public Node gui() {
		// TODO Auto-generated method stub
		return null;
	}

	private void validate() {
		if (!_mbvs.isEmpty()) {
			for (MustBeValid mbv : _mbvs) {
				if (!mbv.valid()) {
					_valid.setValue(false);
					_issue.setValue(mbv.issue());
					return;
				}
			}
		}
		_valid.setValue(true);
		_issue.setValue(null);
	}

}

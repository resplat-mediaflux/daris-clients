package unimelb.mf.client.gui.components;

import javafx.scene.Node;

public interface InterfaceComponent {

	Node gui();

}

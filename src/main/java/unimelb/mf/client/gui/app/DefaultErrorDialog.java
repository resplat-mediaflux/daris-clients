package unimelb.mf.client.gui.app;

import arc.exception.ThrowableUtil;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class DefaultErrorDialog implements ErrorDialog {

    public static final double DEFAULT_WIDTH = 600;
    public static final double DEFAULT_HEIGHT = 360;

    private Stage _stage;
    private Scene _scene;
    private Accordion _accordion;
    private TitledPane _messagePane;
    private TitledPane _contextPane;
    private TitledPane _stackTracePane;
    private TextArea _message;
    private TextArea _context;
    private TextArea _stackTrace;

    private double _width;
    private double _height;

    public DefaultErrorDialog() {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public DefaultErrorDialog(double width, double height) {
        _width = width;
        _height = height;

        _stage = new Stage(StageStyle.UTILITY);
        _stage.initModality(Modality.APPLICATION_MODAL);

        _accordion = new Accordion();

        _messagePane = new TitledPane();
        _accordion.getPanes().add(_messagePane);
        _messagePane.setText("Error:");

        _message = new TextArea();
        _message.setEditable(false);
        _message.setWrapText(true);
        _messagePane.setContent(_message);

        _contextPane = new TitledPane();
        _accordion.getPanes().add(_contextPane);
        _contextPane.setText("Context:");

        _context = new TextArea();
        _context.setEditable(false);
        _contextPane.setContent(_context);

        _stackTracePane = new TitledPane();
        _accordion.getPanes().add(_stackTracePane);
        _stackTracePane.setText("Stack Trace:");

        _stackTrace = new TextArea();
        _stackTrace.setEditable(false);
        _stackTracePane.setContent(_stackTrace);

        _accordion.setExpandedPane(_messagePane);

        _scene = new Scene(_accordion, _width, _height);

        _stage.setScene(_scene);

    }

    @Override
    public void display(String context, Throwable e, String message) {
        _message.setText(message == null ? (e == null ? null : e.getMessage()) : message);
        _context.setText(context);
        _stackTrace.setText(e == null ? null : ThrowableUtil.stackTrace(e));
        _stage.setTitle("Error");
        _stage.show();
    }

}

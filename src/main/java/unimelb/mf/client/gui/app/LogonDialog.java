package unimelb.mf.client.gui.app;

import unimelb.utils.Action;

public interface LogonDialog {
    void display(Application app, Action postAction);
}

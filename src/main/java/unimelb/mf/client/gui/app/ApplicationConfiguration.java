package unimelb.mf.client.gui.app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import arc.mf.client.ConnectionDetails;
import arc.mf.client.ConnectionSpec;
import arc.mf.client.TransportType;
import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import arc.xml.XmlWriter;

public class ApplicationConfiguration {

    public static final int DEFAULT_SERVICE_THREAD_POOL_SIZE = 1;
    public static final int MAX_SERVICE_THREAD_POOL_SIZE = 4;
    public static final int DEFAULT_TRANSFER_THREAD_POOL_SIZE = 1;
    public static final int MAX_TRANSFER_THREAD_POOL_SIZE = 8;
    public static final int DEFAULT_PAGE_SIZE = 100;
    public static final int MAX_PAGE_SIZE = 10000;

    private File _configFile;
    private List<ConnectionDetails> _servers;
    private int _serviceThreadPoolSize = DEFAULT_SERVICE_THREAD_POOL_SIZE;
    private int _transferThreadPoolSize = DEFAULT_TRANSFER_THREAD_POOL_SIZE;
    private int _pageSize = DEFAULT_PAGE_SIZE;

    public ApplicationConfiguration(File configFile) {

        _configFile = configFile;
        _servers = new ArrayList<ConnectionDetails>();
        load();
    }

    public ApplicationConfiguration(Path configFile) {
        this(configFile.toFile());
    }

    protected void loadXml(XmlDoc.Element pe) throws Throwable {
        _servers.clear();
        if (pe != null) {
            List<XmlDoc.Element> ses = pe.elements("login/server");
            if (ses != null) {
                for (XmlDoc.Element se : ses) {
                    ConnectionSpec spec = new ConnectionSpec();
                    spec.setHostName(se.value("address"));
                    spec.setAllowUntrustedServer(se.booleanValue("alwaysTrustServer", true));
                    String transport = se.value("transport");
                    if (transport != null) {
                        spec.setTransportType(transport.equalsIgnoreCase("HTTPS") ? TransportType.HTTPS
                                : (transport.equalsIgnoreCase("HTTP") ? TransportType.HTTP : TransportType.TCPIP));
                    } else {
                        spec.setTransportType(null);
                    }
                    spec.setPort(se.intValue("port"));
                    ConnectionDetails server = spec.build();
                    _servers.add(server);
                }
            }
            setServiceThreadPoolSize(pe.intValue("serviceThreadPoolSize", DEFAULT_SERVICE_THREAD_POOL_SIZE));
            setTransferThreadPoolSize(pe.intValue("transferThreadPoolSize", DEFAULT_TRANSFER_THREAD_POOL_SIZE));
            setPageSize(pe.intValue("pageSize", DEFAULT_PAGE_SIZE));
        }
    }

    void setServiceThreadPoolSize(int threadPoolSize) {
        if (threadPoolSize <= 0) {
            _serviceThreadPoolSize = DEFAULT_SERVICE_THREAD_POOL_SIZE;
        } else if (threadPoolSize > MAX_SERVICE_THREAD_POOL_SIZE) {
            _serviceThreadPoolSize = MAX_SERVICE_THREAD_POOL_SIZE;
        } else {
            _serviceThreadPoolSize = threadPoolSize;
        }
    }

    void setTransferThreadPoolSize(int threadPoolSize) {
        if (threadPoolSize <= 0) {
            _transferThreadPoolSize = DEFAULT_TRANSFER_THREAD_POOL_SIZE;
        } else if (threadPoolSize > MAX_TRANSFER_THREAD_POOL_SIZE) {
            _transferThreadPoolSize = MAX_TRANSFER_THREAD_POOL_SIZE;
        } else {
            _transferThreadPoolSize = threadPoolSize;
        }
    }

    void setPageSize(int pageSize) {
        if (pageSize <= 0) {
            _pageSize = DEFAULT_PAGE_SIZE;
        } else if (pageSize > MAX_PAGE_SIZE) {
            _pageSize = MAX_PAGE_SIZE;
        } else {
            _pageSize = pageSize;
        }
    }

    protected void saveXml(XmlWriter w) throws Throwable {
        w.push("properties");
        w.push("login");
        for (ConnectionDetails server : _servers) {
            w.push("server");
            w.add("address", server.hostName());
            w.add("transport", server.useHttp() ? (server.encrypt() ? "HTTPS" : "HTTP") : "TCP/IP");
            w.add("port", server.port());
            w.add("alwaysTrustServer", server.allowUntrustedServer());
            w.pop();
        }
        w.pop();
        w.add("serviceThreadPoolSize", serviceThreadPoolSize());
        w.add("transferThreadPoolSize", transferThreadPoolSize());
        w.add("pageSize", pageSize());
        w.pop();
    }

    protected void loadXmlFile(File f) throws Throwable {
        if (f != null && f.exists()) {
            try (Reader r = new BufferedReader(new FileReader(f))) {
                XmlDoc.Element pe = new XmlDoc().parse(r);
                loadXml(pe);
            }
        }
    }

    protected void saveXmlFile(File f) throws Throwable {
        try (Writer w = new BufferedWriter(new FileWriter(f))) {
            XmlStringWriter xsw = new XmlStringWriter();
            saveXml(xsw);
            w.write(xsw.document());
        }
    }

    protected void load() {
        try {
            if (_configFile != null) {
                loadXmlFile(_configFile);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            saveXmlFile(_configFile);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public List<ConnectionDetails> servers() {
        return Collections.unmodifiableList(_servers);
    }

    public ConnectionDetails getServer(String host) {
        if (host != null) {
            for (ConnectionDetails server : _servers) {
                if (host.equalsIgnoreCase(server.hostName())) {
                    return server;
                }
            }
        }
        return null;
    }

    public void addServer(ConnectionDetails server) {
        int idx = -1;
        for (int i = 0; i < _servers.size(); i++) {
            ConnectionDetails s = _servers.get(i);
            if (Objects.equals(s.hostName(), server.hostName())) {
                idx = i;
                break;
            }
        }
        if (idx >= 0) {
            _servers.remove(idx);
        }
        _servers.add(0, server);
    }

    public List<String> hosts() {
        List<String> hosts = new ArrayList<String>();
        for (ConnectionDetails server : _servers) {
            String host = server.hostName();
            if (!hosts.contains(host)) {
                hosts.add(host);
            }
        }
        return hosts;
    }

    public int transferThreadPoolSize() {
        return _transferThreadPoolSize;
    }

    public int serviceThreadPoolSize() {
        return _serviceThreadPoolSize;
    }

    public int pageSize() {
        return _pageSize;
    }

}

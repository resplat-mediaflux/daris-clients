package unimelb.mf.client.gui.app;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import arc.mf.client.AuthenticationDetails;
import arc.mf.client.ConnectionDetails;
import arc.mf.client.ConnectionSpec;
import arc.mf.client.TransportType;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import unimelb.mf.client.gui.components.NumberField;
import unimelb.mf.client.session.SamlProvider;
import unimelb.utils.Action;

public class DefaultLogonDialog implements LogonDialog {

    public static final int DEFAULT_WIDTH = 800;
    public static final int DEFAULT_HEIGHT = 600;

    private Application _app;
    private Action _postAction;

    private BooleanProperty _validity;

    private int _width;
    private int _height;

    private Stage _stage;
    private Scene _scene;
    private GridPane _grid;

    private Label _serverLabel;
    private HBox _serverHB;
    private ComboBox<String> _transportCombo;
    private ComboBox<String> _hostCombo;
    private NumberField<Integer> _portField;

    private Label _domainLabel;
    private TextField _domainField;

    private Label _providerLabel;
    private ComboBox<SamlProvider> _providerCombo;

    private Label _userLabel;
    private TextField _userField;

    private Label _passwordLabel;
    private PasswordField _passwordField;

    private Label _statusLabel;
    private Button _signInButton;

    public DefaultLogonDialog() {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public DefaultLogonDialog(int width, int height) {
        _validity = new SimpleBooleanProperty(false);
        _width = width;
        _height = height;

        _stage = new Stage(StageStyle.UTILITY);
        _stage.initModality(Modality.APPLICATION_MODAL);
        _stage.setOnCloseRequest(e -> {
            _app.close();
        });

        _grid = new GridPane();
        _grid.setAlignment(Pos.CENTER);
        _grid.setHgap(10);
        _grid.setVgap(10);
        _grid.setPadding(new Insets(25, 25, 25, 25));
        _grid.setBorder(new Border(new BorderStroke[] {
                new BorderStroke(Color.BEIGE, BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT) }));

        ColumnConstraints ccl = new ColumnConstraints();
        ccl.setHalignment(HPos.RIGHT);
        _grid.getColumnConstraints().add(ccl);
        ColumnConstraints ccr = new ColumnConstraints();
        ccr.setHalignment(HPos.LEFT);
        _grid.getColumnConstraints().add(ccr);

        _serverLabel = createLabel("Server:");
        _serverHB = new HBox();
        _transportCombo = new ComboBox<String>();
        _transportCombo.setPrefWidth(90);
        _transportCombo.getItems().addAll(Arrays.asList("HTTPS", "HTTP", "TCP/IP"));
        _transportCombo.setValue("HTTPS");
        _transportCombo.valueProperty().addListener((ob, ov, nv) -> {
            if ("HTTPS".equalsIgnoreCase(nv)) {
                _portField.setValue(443);
            } else if ("HTTP".equalsIgnoreCase(nv)) {
                _portField.setValue(80);
            } else if ("TCP/IP".equalsIgnoreCase(nv)) {
                _portField.setValue(1967);
            }
            updateValidity();
        });

        _hostCombo = new ComboBox<String>();
        _hostCombo.setEditable(true);
        _hostCombo.setPrefWidth(300);
        _hostCombo.setPromptText("host name or address");
        _hostCombo.valueProperty().addListener((ob, ov, nv) -> {
            ConnectionDetails server = _app.configuration().getServer(nv);
            if (server != null) {
                _transportCombo.setValue(server.useHttp() ? (server.encrypt() ? "HTTPS" : "HTTP") : "TCP/IP");
                _portField.setValue(server.port());
            }
            updateValidity();
        });

        _portField = new NumberField<Integer>(0, 65535, 443);
        _portField.setPrefWidth(60);
        _portField.setPromptText("port");
        _portField.valueProperty().addListener((ob, ov, nv) -> {
            updateValidity();
        });
        _portField.setOnAction(e -> {
            if (_validity.get()) {
                _signInButton.requestFocus();
            } else {
                _domainField.requestFocus();
            }
        });
        _serverHB.getChildren().addAll(_transportCombo, _hostCombo, _portField.gui());

        _domainLabel = createLabel("Domain:");
        _domainField = new TextField();
        _domainField.textProperty().addListener((obs, ov, nv) -> {
            updateValidity();
            updateProvider();
        });

        _domainField.setOnAction(e -> {
            if (_validity.get()) {
                _signInButton.requestFocus();
            } else {
                if (_providerCombo.getItems().isEmpty()) {
                    _userField.requestFocus();
                } else {
                    _providerCombo.requestFocus();
                }
            }
        });

        _providerLabel = createLabel("Provider:");
        _providerCombo = new ComboBox<SamlProvider>();
        _providerCombo.setPrefWidth(500);
        _providerCombo.valueProperty().addListener((obs, ov, nv) -> {
            updateValidity();
        });

        _userLabel = createLabel("User:");
        _userField = new TextField();
        _userField.textProperty().addListener((obs, ov, nv) -> {
            updateValidity();
        });

        _userField.setOnAction(e -> {
            if (_validity.get()) {
                _signInButton.requestFocus();
            } else {
                _passwordField.requestFocus();
            }
        });

        _passwordLabel = createLabel("Password:");
        _passwordField = new PasswordField();
        _passwordField.textProperty().addListener((obs, ov, nv) -> {
            updateValidity();
        });

        _passwordField.setOnAction(e -> {
            if (_validity.get()) {
                _signInButton.requestFocus();
            }
        });

        _statusLabel = new Label();
        _statusLabel.setTextFill(Color.RED);
        _statusLabel.setWrapText(true);
        _statusLabel.setTextAlignment(TextAlignment.JUSTIFY);

        _signInButton = new Button("Sign In");
        _signInButton.disableProperty().bind(_validity.not());
        _signInButton.setDefaultButton(true);

        _signInButton.setOnAction((e) -> {
            signIn();
        });

        addGridRows();

        _scene = new Scene(_grid, _width, _height);

        _stage.setScene(_scene);
    }

    @Override
    public void display(Application app, Action postAction) {
        _app = app;
        _hostCombo.getItems().clear();
        _hostCombo.getItems().addAll(_app.configuration().hosts());
        _postAction = postAction;

        ConnectionDetails connectionDetails = _app.session().connectionDetails();
        if (connectionDetails == null) {
            List<ConnectionDetails> servers = _app.configuration().servers();
            if (_hostCombo.getValue() == null && !servers.isEmpty()) {
                connectionDetails = servers.get(0);
            }
        }
        AuthenticationDetails authenticationDetails = _app.session().authenticationDetails();

        updateGUI(connectionDetails, authenticationDetails);

        _stage.show();
    }

    private void updateGridRows() {
        _grid.getChildren().clear();
        addGridRows();
    }

    private void addGridRows() {
        int i = 0;
        _grid.addRow(i++, _serverLabel, _serverHB);
        _grid.addRow(i++, _domainLabel, _domainField);
        if (!_providerCombo.getItems().isEmpty()) {
            _grid.addRow(i++, _providerLabel, _providerCombo);
        }
        _grid.addRow(i++, _userLabel, _userField);
        _grid.addRow(i++, _passwordLabel, _passwordField);
        _grid.add(_statusLabel, 0, i++, 2, 1);
        _grid.add(_signInButton, 0, i++, 2, 1);
    }

    private void updateValidity() {
        _validity.set(false);
        _statusLabel.setText(null);
        String transport = _transportCombo.getValue() == null ? null : _transportCombo.getValue().trim();
        String host = _hostCombo.getValue() == null ? null : _hostCombo.getValue().trim();
        int port = _portField.value();
        String domain = _domainField.getText() == null ? null : _domainField.getText().trim();
        SamlProvider provider = _providerCombo.getValue();
        String user = _userField.getText() == null ? null : _userField.getText().trim();
        String password = _passwordField.getText() == null ? null : _passwordField.getText().trim();

        if (transport == null || transport.trim().isEmpty()) {
            _statusLabel.setText("Transport protocol is not set.");
            return;
        }

        if (host == null || host.trim().isEmpty()) {
            _statusLabel.setText("Host name or address is missing.");
            return;
        }

        if (port <= 0) {
            _statusLabel.setText("Server port is not set.");
            return;
        }

        if (domain == null || domain.trim().isEmpty()) {
            _statusLabel.setText("Domain is missing.");
            return;
        }

        if (_grid.getChildren().contains(_providerCombo) && provider == null) {
            _statusLabel.setText("Provider is not selected.");
            return;
        }

        if (user == null || user.trim().isEmpty()) {
            _statusLabel.setText("User is missing.");
            return;
        }

        if (password == null || password.trim().isEmpty()) {
            _statusLabel.setText("Password is missing.");
            return;
        }
        _validity.set(true);
    }

    private void updateProvider() {
        String transport = _transportCombo.getValue() == null ? null : _transportCombo.getValue().trim();
        String host = _hostCombo.getValue() == null ? null : _hostCombo.getValue().trim();
        int port = _portField.value();
        String domain = _domainField.getText() == null ? null : _domainField.getText().trim();
        if (transport == null || transport.isEmpty() || host == null || host.isEmpty() || port <= 0 || domain == null
                || domain.isEmpty()) {
            updateProvider(null, null);
        } else {
            SamlProvider provider = _providerCombo.getValue();
            String shortName = provider == null ? null : provider.shortName();
            updateProvider(connectionDetails(), domain, shortName);
        }
    }

    private void updateProvider(ConnectionDetails server, String domain, String shortName) {
        _app.executeServiceTask(() -> {
            List<SamlProvider> providers = null;
            SamlProvider provider = null;
            try {
                providers = _app.session().getSamlProviders(server, domain, false);
                provider = _app.session().getSamlProvider(server, domain, shortName);
            } catch (Throwable e) {
                e.printStackTrace();
            } finally {
                updateProvider(provider, providers);
            }
        });
    }

    private void updateProvider(SamlProvider provider, Collection<SamlProvider> providers) {
        Platform.runLater(() -> {
            if (providers != null && !providers.isEmpty()) {
                _providerCombo.getItems().setAll(providers);
                _providerCombo.setValue(provider);
                if (!_grid.getChildren().contains(_providerCombo)) {
                    updateGridRows();
                }
                updateValidity();
            } else {
                _providerCombo.getItems().clear();
                _providerCombo.setValue(null);
                if (_grid.getChildren().contains(_providerCombo)) {
                    updateGridRows();
                }
                updateValidity();
            }
        });
    }

    private static Label createLabel(String text) {
        Label label = new Label(text);
        label.setFont(Font.font("Arial Black", FontWeight.BOLD, 16));
        return label;
    }

    private void updateGUI(ConnectionDetails connectionDetails, AuthenticationDetails authenticationDetails) {
        if (connectionDetails != null) {
            String transport = connectionDetails.useHttp() ? (connectionDetails.encrypt() ? "HTTPS" : "HTTP")
                    : "TCP/IP";
            _transportCombo.setValue(transport);
            _hostCombo.setValue(connectionDetails.hostName());
            _portField.setValue(connectionDetails.port());
        }
        if (authenticationDetails != null) {
            _domainField.setText(authenticationDetails.domain());
            String username = authenticationDetails.userName();
            if (username != null) {
                int idx = username.indexOf(':');
                String providerShortName = idx > 0 ? username.substring(0, idx) : null;
                if (idx > 0) {
                    username = username.substring(idx + 1);
                }
                if (connectionDetails != null && providerShortName != null) {
                    updateProvider(connectionDetails, authenticationDetails.domain(), providerShortName);
                }
                _userField.setText(username);
            }
        }
    }

    private ConnectionDetails connectionDetails() {
        ConnectionSpec spec = new ConnectionSpec();
        String host = _hostCombo.getValue();
        if (host == null) {
            return null;
        }
        spec.setHostName(host);
        int port = _portField.value();
        spec.setPort(port);
        String transport = _transportCombo.getValue();
        if (transport == null) {
            return null;
        }
        spec.setTransportType(transport.equalsIgnoreCase("HTTPS") ? TransportType.HTTPS
                : (transport.equalsIgnoreCase("HTTP") ? TransportType.HTTP : TransportType.TCPIP));
        return spec.build();
    }

    private AuthenticationDetails authenticationDetails() {
        String domain = _domainField.getText().trim();
        if (domain == null) {
            return null;
        }
        SamlProvider provider = _providerCombo.getValue();
        String user = _userField.getText().trim();
        String username = provider == null ? user : (provider.shortName() + ":" + user);
        if (username == null) {
            return null;
        }
        String password = _passwordField.getText().trim();
        if (password == null) {
            return null;
        }
        return new AuthenticationDetails(_app.name(), domain, username, password);
    }

    private void signIn() {
        ConnectionDetails cd = connectionDetails();
        AuthenticationDetails ad = authenticationDetails();
        try {
            // TODO proxy settings...
            _app.session().connect(cd, ad, null, true);
            _statusLabel.setText(null);
            _passwordField.setText(null);
            _stage.hide();
            if (_postAction != null) {
                _postAction.execute();
            }
        } catch (Throwable e) {
            e.printStackTrace();
            _statusLabel.setText(e.getMessage() == null ? "Authentication failure" : e.getMessage());
        }
    }

}

package unimelb.mf.client.gui.app;

public interface ErrorDialog {

    void display(String context, Throwable e, String message);

    default void display(String context, Throwable e) {
        display(context, e, e != null ? e.getMessage() : null);
    }

    default void display(Throwable e) {
        display(null, e, e == null ? null : e.getMessage());
    }

    default void display(String context, String message) {
        display(context, null, message);
    }

    default void display(String message) {
        display(null, null, message);
    }

}

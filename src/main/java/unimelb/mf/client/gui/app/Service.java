package unimelb.mf.client.gui.app;

import arc.mf.client.ServerClient.ExSessionInvalid;
import arc.xml.XmlDoc;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import unimelb.mf.client.session.MFRequest;
import unimelb.mf.client.session.MFSession;
import unimelb.utils.Action;

public abstract class Service<T> extends javafx.concurrent.Service<T> {

    private Application _app;
    private SimpleObjectProperty<MFRequest> _request;

    protected Service(Application app, MFRequest request) {
        _app = app;
        _request = new SimpleObjectProperty<MFRequest>(request);
        setOnFailed(event -> {
            Throwable e = getException();
            if (e instanceof ExSessionInvalid) {
                if (!_app.isLoggingOn()) {
                    _app.logon(new Action() {
                        @Override
                        public void execute() {
                            restart();
                        }
                    });
                }
            } else {
                _app.displayError("Executing service: " + request().service(), e);
            }
        });
        setOnCancelled(event -> {
            try {
                _request.get().abort();
            } catch (Throwable e) {
                _app.displayError("aborting service: " + _request.get().service(), e, e.getMessage());
            }
        });
        if (_request.get() != null && (_request.get().hasInput() || _request.get().hasOutput())) {
            setExecutor(_app.transferExecutor());
        } else {
            setExecutor(_app.serviceExecutor());
        }
    }

    public final MFRequest request() {
        return _request.get();
    }

    public void setRequest(MFRequest request) {
        if (getState() != State.READY) {
            throw new IllegalStateException("Cannot set request while the worker state is " + getState());
        }
        _request.set(request);
        if (_request.get() != null && (_request.get().hasInput() || _request.get().hasOutput())) {
            setExecutor(_app.transferExecutor());
        } else {
            setExecutor(_app.serviceExecutor());
        }
    }

    public final MFSession session() {
        return _app.session();
    }

    @Override
    protected Task<T> createTask() {
        Task<T> task = new Task<T>() {

            @Override
            protected T call() throws Exception {
                try {
                    XmlDoc.Element re = execute(session(), request());
                    return instantiate(re);
                } catch (Throwable e) {
                    if (e instanceof Exception) {
                        throw (Exception) e;
                    } else {
                        throw new Exception(e);
                    }
                }
            }
        };
        return task;
    }

    protected XmlDoc.Element execute(MFSession session, MFRequest request) throws Throwable {
        return session.execute(request);
    }

    protected abstract T instantiate(XmlDoc.Element re) throws Throwable;

}

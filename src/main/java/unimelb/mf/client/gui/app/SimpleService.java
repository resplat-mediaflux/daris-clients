package unimelb.mf.client.gui.app;

import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import unimelb.mf.client.session.MFRequest;

public class SimpleService extends Service<XmlDoc.Element> {

    public SimpleService(Application app, MFRequest request) {
        super(app, request);
    }

    @Override
    protected Element instantiate(Element re) {
        return re;
    }

}

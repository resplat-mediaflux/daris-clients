package unimelb.mf.client.gui.app;

import java.nio.file.Paths;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import arc.mf.client.AuthenticationDetails;
import arc.mf.client.ConnectionDetails;
import arc.mf.client.ProxyDetails;
import arc.mf.client.ServerClient;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import unimelb.mf.client.session.MFSession;
import unimelb.utils.Action;

public abstract class Application extends javafx.application.Application implements AutoCloseable {

    public static final String CONFIG_DIR = Paths.get(System.getProperty("user.home"), ".Arcitecta").toString();
    public static final String CONFIG_FILE_SUFFIX = "-properties.xml";

    private final String _name;
    private String _displayName;
    private ApplicationConfiguration _config;
    private MFSession _session;
    private ThreadPoolExecutor _serviceExecutor;
    private ThreadPoolExecutor _transferExecutor;
    private LogonDialog _logonDialog;
    private ErrorDialog _errorDialog;

    private SimpleBooleanProperty _loggingOn;

    private static AuthenticationDetails setApp(AuthenticationDetails ad, String app) {
        if (Objects.equals(ad.application(), app)) {
            return ad;
        } else {
            if (ad.token() != null) {
                return new AuthenticationDetails(app, ad.token());
            } else {
                return new AuthenticationDetails(app, ad.domain(), ad.userName(), ad.userPassword());
            }
        }
    }

    protected Application(String name, String displayName, LogonDialog logonDlg, ErrorDialog errorDlg) {
        _name = name;
        _config = new ApplicationConfiguration(Paths.get(CONFIG_DIR, _name + CONFIG_FILE_SUFFIX));
        _session = new MFSession(null, null, null) {
            public ServerClient.Connection connect(ConnectionDetails connectionDetails,
                                                   AuthenticationDetails authenticationDetails,
                                                   ProxyDetails proxyDetails) throws Throwable {
                return super.connect(connectionDetails,
                        setApp(authenticationDetails, _name), proxyDetails);
            }
        };
        _session.addListener(new MFSession.Listener() {

            @Override
            public void disconnected(MFSession session) {

            }

            @Override
            public void connected(MFSession session) {
                _config.addServer(session.connectionDetails());
                _config.save(); // Maybe when application exists.
            }
        });
        _loggingOn = new SimpleBooleanProperty(false);
        // TODO bound blocking queue
        _serviceExecutor = new ThreadPoolExecutor(_config.serviceThreadPoolSize(), _config.serviceThreadPoolSize(), 0,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "service");
            }
        }, new ThreadPoolExecutor.CallerRunsPolicy());
        // TODO bound blocking queue
        _transferExecutor = new ThreadPoolExecutor(_config.serviceThreadPoolSize(), _config.serviceThreadPoolSize(), 0,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "transfer");
            }
        }, new ThreadPoolExecutor.CallerRunsPolicy());
        _logonDialog = logonDlg != null ? logonDlg : new DefaultLogonDialog();
        _errorDialog = errorDlg != null ? errorDlg : new DefaultErrorDialog();
    }

    public final String name() {
        return _name;
    }

    public String displayName() {
        if (_displayName != null) {
            return _displayName;
        } else {
            return _name;
        }
    }

    public ApplicationConfiguration configuration() {
        return _config;
    }

    public MFSession session() {
        return _session;
    }

    public LogonDialog logonDialog() {
        return _logonDialog;
    }

    public ErrorDialog errorDialog() {
        return _errorDialog;
    }

    public final ThreadPoolExecutor serviceExecutor() {
        return _serviceExecutor;
    }

    public final ThreadPoolExecutor transferExecutor() {
        return _transferExecutor;
    }

    public void displayError(String context, Throwable e, String message) {
        errorDialog().display(context, e, message);
    }

    public void displayError(String context, Throwable e) {
        errorDialog().display(context, e);
    }

    public void displayError(Throwable e) {
        errorDialog().display(e);
    }

    public void displayError(String context, String message) {
        errorDialog().display(context, message);
    }

    public void displayError(String message) {
        errorDialog().display(message);
    }

    public void logon(Action postAction) {
        _loggingOn.set(true);
        logonDialog().display(this, () -> {
            _loggingOn.set(false);
            if (postAction != null) {
                postAction.execute();
            }
        });
    }

    public boolean isLoggingOn() {
        return _loggingOn.get();
    }

    public void close() {
        try {
            session().discard();
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            serviceExecutor().shutdown();
            transferExecutor().shutdown();
            try {
                serviceExecutor().awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
                transferExecutor().awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            } catch (Throwable e) {
                e.printStackTrace();
            }
            Platform.exit();
            System.exit(0);
        }
    }

    public void executeServiceTask(Runnable command) {
        serviceExecutor().execute(command);
    }

    public <T> Future<T> submitServiceTask(Callable<T> task) {
        return serviceExecutor().submit(task);
    }

    public void executeTransferTask(Runnable command) {
        serviceExecutor().execute(command);
    }

    public <T> Future<T> submitTransferTask(Callable<T> task) {
        return serviceExecutor().submit(task);
    }
}

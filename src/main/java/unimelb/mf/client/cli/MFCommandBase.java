package unimelb.mf.client.cli;

import java.nio.file.Path;

import org.slf4j.Logger;

import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;

public abstract class MFCommandBase implements MFCommand {

    public abstract String appName();

    protected abstract Path mfluxCfgFile();

    @Override
    public final MFSession authenticate() throws Throwable {
        MFConfigBuilder mfcb = new MFConfigBuilder();
        mfcb.setApp(this.appName());
        if (this.mfluxCfgFile() != null) {
            mfcb.loadFromConfigFile(this.mfluxCfgFile().toFile());
        } else {
            mfcb.findAndLoadFromConfigFile();
        }
        mfcb.loadFromSystemEnv();
        mfcb.loadFromSystemProperties();
        mfcb.setConsoleLogon(true);
        return MFSession.create(mfcb);
    }

    public abstract Logger logger();

    @Override
    public final void logError(String message, Throwable e) {
        logger().error(message, e);
    }

    @Override
    public final void logInfo(String message) {
        logger().info(message);
    }

    @Override
    public final void logWarning(String message) {
        logger().warn(message);
    }

}

package unimelb.mf.client.cli;

import java.util.concurrent.Callable;

import unimelb.logging.Loggable;
import unimelb.mf.client.session.MFSession;

public interface MFCommand extends Callable<Integer>, Loggable {

	MFSession authenticate() throws Throwable;

	void execute(MFSession session) throws Throwable;

	@Override
	default Integer call() throws Exception {
		try {
			MFSession session = authenticate();
			try {
				execute(session);
				return 0;
			} finally {
				session.discard();
			}
		} catch (Throwable e) {
			logError(e.getMessage(), e);
			if (e instanceof Exception) {
				throw (Exception) e;
			} else {
				throw new Exception(e);
			}
		}
	}

}

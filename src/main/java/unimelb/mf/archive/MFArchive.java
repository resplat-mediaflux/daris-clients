package unimelb.mf.archive;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import arc.archive.ArchiveInput;
import arc.archive.ArchiveOutput;
import arc.archive.ArchiveRegistry;
import arc.mime.MimeType;
import arc.mime.NamedMimeType;
import arc.streams.LongInputStream;
import arc.streams.ProgressMonitoredInputStream;
import arc.streams.SizedInputStream;
import arc.streams.UnsizedInputStream;
import arc.utils.ProgressMonitor;
import unimelb.io.AbortCheck;
import unimelb.io.CollectionTransferProgressListener;
import unimelb.io.StreamCopy;
import unimelb.io.TransferProgressListener;
import unimelb.utils.AbortedException;
import unimelb.utils.PathUtils;

public class MFArchive {

	public static final Logger logger = LoggerFactory.getLogger(MFArchive.class);

	public static final String TYPE_AAR = "application/arc-archive";
	public static final String TYPE_ZIP = "application/zip";
	public static final String TYPE_JAR = "application/java-archive";
	public static final String TYPE_TAR = "application/x-tar";
	public static final String TYPE_GZIPPED_TAR = "application/x-gtar";

	public static final int DEFAULT_COMPRESSION_LEVEL = 6;

	public static enum Type {
		// @formatter:off
		AAR(TYPE_AAR, "aar"), ZIP(TYPE_ZIP, "zip"), JAR(TYPE_JAR, "jar"), TAR(TYPE_TAR, "tar"),
		GZIPPED_TAR(TYPE_GZIPPED_TAR, "tar.gz");
		// @formatter:on

		public final String mimeType;
		public final String extension;

		Type(String mimeType, String extension) {
			this.mimeType = mimeType;
			this.extension = extension;
		}

		public static Type fromFileExtension(String ext) {
			if (ext != null) {
				Type[] vs = values();
				for (Type v : vs) {
					if ("tgz".equalsIgnoreCase(ext) || "tar.gz".equalsIgnoreCase(ext)) {
						return GZIPPED_TAR;
					} else if (v.extension.equalsIgnoreCase(ext)) {
						return v;
					}
				}
			}
			return null;
		}

		public static Type fromMimeType(String mimeType) {
			if (mimeType != null) {
				Type[] vs = values();
				for (Type v : vs) {
					if (v.mimeType.equalsIgnoreCase(mimeType)) {
						return v;
					}
				}
			}
			return null;
		}

		public static Type fromFileName(String fileName) {
			if (fileName != null) {
				int idx = fileName.lastIndexOf('.');
				if (idx > 0) {
					String ext = fileName.substring(idx + 1);
					if (ext.indexOf('/') == -1 && ext.indexOf('\\') == -1) {
						return fromFileExtension(ext);
					}
				}
			}
			return null;
		}

		public static boolean isArchiveType(String mimeType) {
			return fromMimeType(mimeType) != null;
		}
	}

	public static void extract(Path inFile, Path outputDir, boolean overwrite, CollectionTransferProgressListener pl,
			AbortCheck ac) throws Throwable {
		extract(inFile.toFile(), outputDir, overwrite, pl, ac);
	}

	public static void extract(File inFile, Path outputDir, boolean overwrite, CollectionTransferProgressListener pl,
			AbortCheck ac) throws Throwable {
		extract(inFile, null, outputDir, overwrite, pl, ac);
	}

	public static void extract(File inFile, String mimeType, Path outputDir, boolean overwrite,
			CollectionTransferProgressListener pl, AbortCheck ac) throws Throwable {
		ArchiveInput ai = mimeType == null ? ArchiveRegistry.createInput(inFile)
				: ArchiveRegistry.createInput(inFile, new NamedMimeType(mimeType));
		try {
			extract(ai, outputDir, overwrite, pl, ac);
		} finally {
			ai.close();
		}
	}

	public static void extract(InputStream in, long length, String mimeType, Path outputDir, boolean overwrite,
			CollectionTransferProgressListener pl, AbortCheck ac) throws Throwable {
		try (LongInputStream li = length >= 0 ? new SizedInputStream(in, length) : new UnsizedInputStream(in)) {
			extract(li, mimeType, outputDir, overwrite, pl, ac);
		}
	}

	public static void extract(LongInputStream in, String mimeType, Path outputDir, boolean overwrite,
			CollectionTransferProgressListener pl, AbortCheck ac) throws Throwable {
		ArchiveInput ai = ArchiveRegistry.createInput(in, mimeType == null ? null : new NamedMimeType(mimeType));
		try {
			extract(ai, outputDir, overwrite, pl, ac);
		} finally {
			ai.close();
		}
	}

	public static void extract(ArchiveInput ai, Path outputDir, boolean overwrite,
			CollectionTransferProgressListener pl, AbortCheck ac) throws Throwable {
		try {
			ArchiveInput.Entry entry = null;
			while ((entry = ai.next()) != null) {
				if (ac != null && ac.aborted()) {
					throw new AbortedException();
				}
				String ename = entry.name();
				while (ename.startsWith("/")) {
					ename = ename.substring(1);
				}
				Path output = Paths.get(outputDir.toString(), ename);
				if (entry.isDirectory()) {
					String activity = "Creating directory: '" + output.toString() + "'...";
					logger.info(activity);
					if (pl != null) {
						pl.inform(activity);
					}
					Files.createDirectories(output);
				} else {
					String activity = "Extracting file: '" + output.toString() + "'...";
					logger.info(activity);
					if (pl != null) {
						pl.inform(activity);
					}
				}
				boolean exists = Files.exists(output);
				if (exists) {
					if (overwrite) {
						String activity = "File: '" + output.toString() + "' already exists. Overwriting.";
						logger.info(activity);
						if (pl != null) {
							pl.inform(activity);
						}
						StreamCopy.copy(entry.stream(), output, pl, ac);
						if (pl != null) {
							pl.incTransferredFiles(1);
						}
					} else {
						String activity = "File: '" + output.toString() + "' already exists. Skipped.";
						logger.info(activity);
						if (pl != null) {
							pl.inform(activity);
						}
					}
				} else {
					Path parentDir = output.getParent();
					if (parentDir != null) {
						Files.createDirectories(parentDir);
					}
					StreamCopy.copy(entry.stream(), output, pl, ac);
					if (pl != null) {
						pl.incTransferredFiles(1);
					}
				}
				ai.closeEntry();
			}
		} finally {
			ai.close();
		}
	}

	public static void create(Path of, CollectionTransferProgressListener pl, AbortCheck ac, Path... inputs)
			throws Throwable {
		create(of, mimeTypeStringOf(of), DEFAULT_COMPRESSION_LEVEL, pl, ac, inputs);
	}

	public static void create(Path of, String mimeType, int compressionLevel, CollectionTransferProgressListener pl,
			AbortCheck ac, Path... inputs) throws Throwable {
		mimeType = mimeType == null ? mimeTypeStringOf(of) : mimeType;
		if ("application/x-tar".equals(mimeType)) {
			compressionLevel = 0;
		}
		try (OutputStream os = new BufferedOutputStream(Files.newOutputStream(of))) {
			ArchiveOutput ao = ArchiveRegistry.createOutput(os, mimeType, compressionLevel, null);
			try {
				if (inputs != null) {
					for (Path input : inputs) {
						add(ao, input.getParent(), input, pl, ac);
					}
				}
			} finally {
				ao.close();
			}
		}
	}

	public static void create(Path of, int compressionLevel, CollectionTransferProgressListener pl, AbortCheck ac,
			Collection<Path> inputs) throws Throwable {
		create(of, mimeTypeStringOf(of), compressionLevel, pl, ac, inputs);
	}

	public static void create(Path of, String mimeType, int compressionLevel, CollectionTransferProgressListener pl,
			AbortCheck ac, Collection<Path> inputs) throws Throwable {
		mimeType = mimeType == null ? mimeTypeStringOf(of) : mimeType;
		if ("application/x-tar".equals(mimeType)) {
			compressionLevel = 0;
		}
		try (OutputStream os = new BufferedOutputStream(Files.newOutputStream(of))) {
			ArchiveOutput ao = ArchiveRegistry.createOutput(os, mimeType, compressionLevel, null);
			try {
				if (inputs != null) {
					for (Path input : inputs) {
						add(ao, input.getParent(), input, pl, ac);
					}
				}
			} finally {
				ao.close();
			}
		}
	}

	public static void create(OutputStream os, String mimeType, int compressionLevel,
			CollectionTransferProgressListener pl, AbortCheck ac, Path... inputs) throws Throwable {
		ArchiveOutput ao = ArchiveRegistry.createOutput(os, mimeType, compressionLevel, null);
		try {
			if (inputs != null) {
				for (Path input : inputs) {
					add(ao, input.getParent(), input, pl, ac);
				}
			}
		} finally {
			ao.close();
		}
	}

	public static void add(ArchiveOutput ao, Path baseDir, Path input, CollectionTransferProgressListener pl,
			AbortCheck ac) throws Throwable {
		String entryName = entryNameFor(baseDir, input);
		if (entryName == null) {
			throw new Exception("Failed to resolve entry name from file: '" + input + "'");
		}
		if (Files.isDirectory(input)) {
			String activity = "Adding directory: '" + entryName + "'";
			logger.info(activity);
			if (pl != null) {
				pl.inform(activity);
			}
			// ao.addDirectory(entryName);
			Files.walkFileTree(input, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					try {
						add(ao, baseDir, file, pl, ac);
					} catch (Throwable e) {
						if (e instanceof InterruptedException) {
							Thread.currentThread().interrupt();
							return FileVisitResult.TERMINATE;
						}
						logger.error(e.getMessage(), e);
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException ioe) {
					logger.error("Failed to access file: " + file, ioe);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException ioe) {
					if (ioe != null) {
						logger.error(ioe.getMessage(), ioe);
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					return super.preVisitDirectory(dir, attrs);
				}
			});
		} else {
			if (ac != null && ac.aborted()) {
				throw new AbortedException();
			}
			String activity = "Adding file: '" + entryName + "'";
			logger.info(activity);
			if (pl != null) {
				pl.inform(activity);
			}
			addFile(ao, entryName, input, pl, ac);
			if (pl != null) {
				pl.incTransferredFiles(1);
			}
		}
	}

	private static void addFile(ArchiveOutput ao, String name, Path f, TransferProgressListener pl, AbortCheck ac)
			throws Throwable {
		try (InputStream is = Files.newInputStream(f);
				BufferedInputStream bis = new BufferedInputStream(is);
				SizedInputStream sis = new SizedInputStream(bis, Files.size(f));
				ProgressMonitoredInputStream pmis = new ProgressMonitoredInputStream(new ProgressMonitor() {

					@Override
					public boolean abort() {
						if (ac != null) {
							return ac.aborted();
						}
						return false;
					}

					@Override
					public void begin(int arg0, long arg1) {

					}

					@Override
					public void beginMultiPart(int arg0, long arg1) {

					}

					@Override
					public void end(int arg0) {

					}

					@Override
					public void endMultiPart(int arg0) {

					}

					@Override
					public void update(long nbBytes) throws Throwable {
						if (pl != null) {
							pl.incTransferredBytes(nbBytes);
						}
					}
				}, sis, true)) {
			ao.add(null, name, pmis);
		}
	}

	public static String entryNameFor(Path baseDir, Path input) {
		if (baseDir == null) {
			return PathUtils.toSystemIndependentPath(input);
		} else {
			Path b = baseDir.toAbsolutePath();
			Path i = input.toAbsolutePath();
			if (i.equals(b)) {
				return null;
			}
			if (i.startsWith(b)) {
				return PathUtils.toSystemIndependentPath(b.relativize(i));
			} else {
				return PathUtils.toSystemIndependentPath(input);
			}
		}
	}

	public static MimeType mimeTypeFromExtension(String ext) {
		if ("aar".equalsIgnoreCase(ext)) {
			return new NamedMimeType(TYPE_AAR);
		} else if ("jar".equalsIgnoreCase(ext)) {
			return new NamedMimeType(TYPE_JAR);
		} else if ("tar".equalsIgnoreCase(ext)) {
			return new NamedMimeType(TYPE_TAR);
		} else if ("tgz".equalsIgnoreCase(ext) || "tar.gz".equalsIgnoreCase(ext)) {
			return new NamedMimeType(TYPE_GZIPPED_TAR);
		} else if ("zip".equalsIgnoreCase(ext)) {
			return new NamedMimeType(TYPE_ZIP);
		} else {
			return null;
		}
	}

	public static MimeType mimeTypeFromName(String name) {
		if (name.toLowerCase().endsWith(".aar")) {
			return new NamedMimeType(TYPE_AAR);
		} else if (name.toLowerCase().endsWith(".jar")) {
			return new NamedMimeType(TYPE_JAR);
		} else if (name.toLowerCase().endsWith(".tar")) {
			return new NamedMimeType(TYPE_TAR);
		} else if (name.toLowerCase().endsWith(".tar.gz") || name.toLowerCase().endsWith(".tgz")) {
			return new NamedMimeType(TYPE_GZIPPED_TAR);
		} else if (name.toLowerCase().endsWith(".zip")) {
			return new NamedMimeType(TYPE_ZIP);
		} else {
			return null;
		}
	}

	public static String mimeTypeStringOf(File f) {
		MimeType mt = mimeTypeOf(f);
		if (mt == null) {
			return null;
		}
		return mt.name();
	}

	public static MimeType mimeTypeOf(File f) {
		return mimeTypeFromName(f.getName());
	}

	public static String mimeTypeStringOf(Path f) {
		MimeType mt = mimeTypeOf(f);
		if (mt == null) {
			return null;
		}
		return mt.name();
	}

	public static MimeType mimeTypeOf(Path f) {
		Path name = f.getFileName();
		if (name != null) {
			return mimeTypeFromName(name.toString());
		}
		return null;
	}

	public static boolean isArchive(File archiveFile) {
		return mimeTypeOf(archiveFile) != null;
	}

	public static boolean isArchive(Path archiveFile) {
		return mimeTypeOf(archiveFile) != null;
	}
}

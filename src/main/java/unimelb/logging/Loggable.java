package unimelb.logging;

public interface Loggable {

    default void logError(String message, Throwable e) {

    }

    default void logInfo(String message) {

    }

    default void logWarning(String message) {

    }

}

package unimelb.logging;

import java.nio.file.Path;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;

public class Logback {

    public static void setLogLevel(String name, ch.qos.logback.classic.Level level) {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(name);
        root.setLevel(level);
    }

    public static void setRootLogLevel(ch.qos.logback.classic.Level level) {
        setLogLevel(org.slf4j.Logger.ROOT_LOGGER_NAME, level);
    }

    public static org.slf4j.Logger getLogger(String loggerName, Path logFile, org.slf4j.event.Level level,
            Boolean additive) {

        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(loggerName);

        if (logFile != null) {
            LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();

            PatternLayoutEncoder ple = new PatternLayoutEncoder();
            ple.setPattern("%date %level [%thread] %logger{10} [%file:%line] %msg%n");
            ple.setContext(lc);
            ple.start();

            FileAppender<ILoggingEvent> fileAppender = new FileAppender<ILoggingEvent>();
            fileAppender.setFile(logFile.toString());
            fileAppender.setEncoder(ple);
            fileAppender.setContext(lc);
            fileAppender.start();

            logger.addAppender(fileAppender);
        }

        if (level != null) {
            logger.setLevel(toLogbackLevel(level));
        }

        if (additive != null) {
            logger.setAdditive(additive);
        }

        return logger;
    }

    public static void initLogger(String loggerName, Path logFile, org.slf4j.event.Level level, Boolean additive) {
        getLogger(loggerName, logFile, level, additive);
    }

    public static org.slf4j.Logger getRootLogger(Path logFile, org.slf4j.event.Level level, boolean additive) {
        return getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME, logFile, level, additive);
    }

    public static void initRootLogger(Path logFile, org.slf4j.event.Level level, boolean additive) {
        getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME, logFile, level, additive);
    }

    public static org.slf4j.Logger getRootLogger(Path logFile, boolean verbose) {
        return getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME, logFile,
                verbose ? org.slf4j.event.Level.DEBUG : org.slf4j.event.Level.WARN, true);
    }

    public static void initRootLogger(Path logFile, boolean verbose) {
        getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME, logFile,
                verbose ? org.slf4j.event.Level.DEBUG : org.slf4j.event.Level.WARN, true);
    }

    public static void initRootLogger(boolean verbose) {
        getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME, null,
                verbose ? org.slf4j.event.Level.DEBUG : org.slf4j.event.Level.WARN, true);
    }

    private static ch.qos.logback.classic.Level toLogbackLevel(org.slf4j.event.Level level) {
        ch.qos.logback.classic.Level logbackLevel = ch.qos.logback.classic.Level.WARN;
        if (level != null) {
            switch (level) {
            case ERROR:
                logbackLevel = ch.qos.logback.classic.Level.ERROR;
                break;
            case WARN:
                logbackLevel = ch.qos.logback.classic.Level.WARN;
                break;
            case INFO:
                logbackLevel = ch.qos.logback.classic.Level.INFO;
                break;
            case DEBUG:
                logbackLevel = ch.qos.logback.classic.Level.DEBUG;
                break;
            case TRACE:
                logbackLevel = ch.qos.logback.classic.Level.ALL;
                break;
            default:
                break;
            }
        }
        return logbackLevel;
    }
}

package unimelb.daris.client.siemens.upload.cli;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import arc.utils.DateTime;
import arc.xml.XmlStringWriter;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IExecutionExceptionHandler;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParseResult;
import picocli.CommandLine.Spec;
import unimelb.daris.client.model.CiteableIdUtils;
import unimelb.daris.client.siemens.upload.FileUploadListener;
import unimelb.daris.client.siemens.upload.SiemensRawMRFileInfo;
import unimelb.daris.client.siemens.upload.SiemensRawMRFileUploadTask;
import unimelb.daris.client.siemens.upload.UploadOptions;
import unimelb.daris.siemens.raw.mr.SiemensRawMRFile;
import unimelb.logging.Logback;
import unimelb.mf.client.cli.MFCommandBase;
import unimelb.mf.client.session.MFSession;
import unimelb.picocli.converters.PathConverter;

@Command(name = SiemensRawMRUploadCommand.APP_NAME, abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class SiemensRawMRUploadCommand extends MFCommandBase implements FileUploadListener {

	public static final String APP_NAME = "siemens-raw-mr-upload";

	public static final String LOG_FILENAME = APP_NAME + ".log";

	@Option(names = { "-c",
			"--config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux/DaRIS server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
	private Path mfCfgFile;

	@Option(names = { "-i",
			"--id" }, paramLabel = "<id>", description = "The identifier of the parent subject or project.", required = true, arity = "1")
	private String parentCID;

	@Option(names = {
			"--patient-id" }, paramLabel = "<patient_id>", description = "The DICOM patient id (0010,0010) of the parent subject. If specified, it must be consistent with the parent id argument(--id).", required = false, arity = "1")
	private String patientID;

	@Option(names = {
			"--checksum" }, negatable = true, description = "Calculate and compare CRC32 checksums between the local file and the uploaded dataset content. Enabled by default.")
	private Boolean checksum;

	@Option(names = {
			"--delete" }, negatable = true, description = "Delete the source file after uploading successfully. Disabled by default.")
	private Boolean delete;

	@Option(names = { "--expire" }, negatable = true, description = "Set the expire-date. Disabled by default.")
	private Boolean expire;

	@Option(names = {
			"--follow-symlinks" }, negatable = false, description = "Follow symbolic links. Disabled by default.")
	private Boolean followSymlinks;

	@Option(names = {
			"--log-dir" }, paramLabel = "<directory>", description = "The log directory path. If not specified, logging is disabled.", required = false, arity = "1")
	private Path logDir;

	@Option(names = {
			"--nb-workers" }, paramLabel = "<n>", description = "Number of concurrent worker threads. Defaults to 1.", required = false, arity = "1")
	private int nbWorkers = 1;

	@Option(names = { "--verbose" }, negatable = false, description = "Print progress information verbosely.")
	private boolean verbose = false;

	@Option(names = { "-h", "--help" }, usageHelp = true, description = "Display a help message.")
	private boolean help = false;

	@Parameters(description = "The raw MR files or the directories contain raw MR files.", arity = "1..", index = "0", paramLabel = "RAW_MR_FILE_OR_DIR")
	private Path[] inputs;

	@Spec
	private CommandSpec spec;

	private long _startTimeMillis = 0;
	private long _finishTimeMillis = 0;
	private AtomicLong totalFiles = new AtomicLong(0);
	private AtomicLong uploadedFiles = new AtomicLong(0);
	private AtomicLong skippedFiles = new AtomicLong(0);
	private AtomicLong failedFiles = new AtomicLong(0);
	private AtomicLong deletedFiles = new AtomicLong(0);
	private AtomicLong totalBytes = new AtomicLong(0);
	private AtomicLong progressedBytes = new AtomicLong(0);
	private AtomicLong uploadedBytes = new AtomicLong(0);
	private ConcurrentHashMap<Path, String> uploadedDatasets = new ConcurrentHashMap<>();

	private ExecutorService workers;

	private Logger logger;

	@Override
	public final Logger logger() {
		if (logger == null) {
			logger = LoggerFactory.getLogger(SiemensRawPETCTUploadCommand.class);
		}
		return logger;
	}

	@Override
	public String appName() {
		return APP_NAME;
	}

	@Override
	protected Path mfluxCfgFile() {
		return this.mfCfgFile;
	}

	@Override
	public void execute(MFSession session) throws Throwable {

		// set default option values
		this.checksum = this.checksum == null ? true : this.checksum;
		this.expire = this.expire == null ? false : this.expire;
		this.delete = this.delete == null ? false : this.delete;
		this.followSymlinks = this.followSymlinks == null ? false : this.followSymlinks;

		// init logging
		initLogging();
		
		// validate parentCID
		boolean parentIsSubject = false;
		if (CiteableIdUtils.isProjectCID(this.parentCID)) {
			parentIsSubject = false;
		} else if (CiteableIdUtils.isSubjectCID(this.parentCID)) {
			parentIsSubject = true;
		} else {
			throw new IllegalArgumentException(
					"Invalid parent id. Expects a project id or a subject id. Found: " + this.parentCID);
		}

		// if patientID is specified, find the corresponding parent subject cid
		if (this.patientID != null) {
			String subjectCID = findSubjectByPatientID(session, this.parentCID, this.patientID);
			if (subjectCID == null) {
				if (parentIsSubject) {
					throw new IllegalArgumentException(
							"The PatientID: '" + this.patientID + "' does not match subject: " + this.parentCID);
				} else {
					throw new IllegalArgumentException("Could not find the subject with PatientID: '" + this.patientID
							+ "' in project: " + this.parentCID);
				}
			} else {
				if (parentIsSubject && !subjectCID.equals(this.parentCID)) {
					throw new IllegalArgumentException(
							"The PatientID: '" + this.patientID + "' does not match subject: " + this.parentCID);
				}
				this.parentCID = subjectCID;
			}
		}

		_startTimeMillis = System.currentTimeMillis();
		UploadOptions options = new UploadOptions.Builder().setParentId(this.parentCID).setChecksum(this.checksum)
				.setExpire(this.expire).setDelete(this.delete).setFollowSymlinks(this.followSymlinks).build();
		if (this.verbose) {
			System.out.println(options);
		}

		// scan for raw pet/ct files
		List<SiemensRawMRFileInfo> rawFiles = new ArrayList<SiemensRawMRFileInfo>();
		for (Path input : inputs) {
			if (!Files.exists(input)) {
				throw new IllegalArgumentException("Input file: '" + input + "' does not exist.");
			}
			addRawFiles(input, rawFiles);
		}

		// init thread pool
		this.workers = new ThreadPoolExecutor(nbWorkers, nbWorkers, 0, TimeUnit.MILLISECONDS,
				new ArrayBlockingQueue<>(100), r -> new Thread(r, APP_NAME + ".worker"),
				new ThreadPoolExecutor.CallerRunsPolicy());

		// process raw files...
		for (SiemensRawMRFileInfo rawFile : rawFiles) {
			// find or create parents
			try {
				rawFile.findOrCreateParents(session, this.parentCID);
			} catch (Throwable e) {
				logError("Failed to upload file: '" + rawFile.file() + "' - could not identify its parent.", e);
				// count this file as failed
				this.totalFiles.getAndIncrement();
				this.totalBytes.getAndAdd(Files.size(rawFile.file()));
				this.failedFiles.getAndDecrement();
			}
			// submit to thread pool to start uploading/verifying
			if (rawFile.studyCID() != null || rawFile.datasetCID() != null) {
				submit(session, rawFile, options);
			}
		}
		// shutdown thread pool
		this.workers.shutdown();
		this.workers.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		_finishTimeMillis = System.currentTimeMillis();

		String summary = getSummary();
		logInfo(summary);
		if (!this.verbose) {
			System.out.println(summary);
		}

	}

	private void initLogging() throws Throwable {
		if (this.logDir != null) {
			if (!Files.exists(logDir)) {
				throw new IllegalArgumentException("'" + logDir + "' does not exist.");
			}
			if (!Files.isDirectory(logDir)) {
				throw new IllegalArgumentException("'" + logDir + "' is not a directory.");
			}
		}
		Path logFile = this.logDir == null ? null : Paths.get(logDir.toString(), LOG_FILENAME);
		Logback.initRootLogger(logFile, this.verbose);
		this.logger = LoggerFactory.getLogger(SiemensRawPETCTUploadCommand.class);
	}

	private void addRawFiles(Path input, List<SiemensRawMRFileInfo> rawFiles) throws Throwable {
		if (!Files.isDirectory(input)) {
			String fileName = input.getFileName().toString();
			if (!fileName.toLowerCase().endsWith(".dat")) {
				logWarning("File '" + input + "' is not a Siemens raw MR file. Expects file name ends with .dat");
			} else {
				if (!SiemensRawMRFile.isFileNameValid(input)) {
					logWarning("File '" + input + "' is not a Siemens raw MR file. Invalid file name.");
				}
				try {
					SiemensRawMRFileInfo rawFile = SiemensRawMRFileInfo.parse(input);
					rawFiles.add(rawFile);
				} catch (Throwable e) {
					logError("Failed to parse raw PET/CT file: '" + input + "'", e);
				}
			}
		} else {
			Files.walkFileTree(input, this.followSymlinks ? EnumSet.of(FileVisitOption.FOLLOW_LINKS)
					: EnumSet.noneOf(FileVisitOption.class), Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
						@Override
						public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
							try {
								addRawFiles(file, rawFiles);
							} catch (Throwable e) {
								logError(e.getMessage(), e);
							}
							return FileVisitResult.CONTINUE;
						}

						@Override
						public FileVisitResult visitFileFailed(Path file, IOException ioe) {
							if (ioe != null) {
								logError(ioe.getMessage(), ioe);
							}
							return FileVisitResult.CONTINUE;
						}

						@Override
						public FileVisitResult postVisitDirectory(Path dir, IOException ioe) {
							if (ioe != null) {
								logError(ioe.getMessage(), ioe);
							}
							return FileVisitResult.CONTINUE;
						}

						@Override
						public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
								throws IOException {
							return super.preVisitDirectory(dir, attrs);
						}
					});
		}
	}

	private void submit(MFSession session, SiemensRawMRFileInfo rawFile, UploadOptions options) throws Throwable {
		this.workers.submit(new SiemensRawMRFileUploadTask(session, rawFile, options, this));
	}

	@Override
	public void progressed(Path file, long progressed) {
		this.progressedBytes.getAndAdd(progressed);
	}

	@Override
	public void started(Path file, long fileLength) {
		System.out.println("Processing file: '" + file + "' (size=" + fileLength + ")");
		this.totalFiles.getAndIncrement();
		this.totalBytes.getAndAdd(fileLength);
	}

	@Override
	public void skipped(Path file, String datasetCID) {
		System.out.println("Skipped file: '" + file + "' - dataset " + datasetCID + " already exists.");
		this.skippedFiles.getAndIncrement();
	}

	@Override
	public void failed(Path file) {
		System.err.println("Failed to upload file: '" + file + "'");
		this.failedFiles.getAndIncrement();
	}

	@Override
	public void uploaded(Path file, long fileLength, String datasetCID) {
		System.err.println("Uploaded file: '" + file + "' to dataset: " + datasetCID + " (" + fileLength + " bytes)");
		this.uploadedFiles.getAndIncrement();
		this.uploadedBytes.getAndAdd(fileLength);
		this.uploadedDatasets.put(file, datasetCID);
	}

	@Override
	public void deleted(Path file) {
		this.deletedFiles.getAndIncrement();
		System.out.println("Deleted file: '" + file + "'");
	}

	private void printSummary(PrintStream ps) {
		ps.println();
		ps.printf(" Start time: %s%n", DateTime.string(new Date(_startTimeMillis)));
		ps.printf("Finish time: %s%n", DateTime.string(new Date(_finishTimeMillis)));
		long durationMillis = _finishTimeMillis - _startTimeMillis;
		Duration duration = Duration.ofMillis(durationMillis);
		long durationSeconds = duration.getSeconds();
		ps.printf("   Duration: %d:%02d:%02d.%03d%n", durationSeconds / 3600, (durationSeconds % 3600) / 60,
				durationSeconds % 60, durationMillis % 1000);
		ps.println();
		ps.printf("   Total files: %8d%n", this.totalFiles.get());
		ps.printf("Uploaded files: %8d%n", this.uploadedFiles.get());
		if (this.skippedFiles.get() > 0) {
			ps.printf(" Skipped files: %8d%n", this.skippedFiles.get());
		}
		if (this.failedFiles.get() > 0) {
			ps.printf("  Failed files: %8d%n", this.failedFiles.get());
		}
		if (this.delete || this.deletedFiles.get() > 0) {
			ps.printf(" Deleted files: %8d%n", this.deletedFiles.get());
		}
		ps.println();
		ps.printf("   Total bytes: %16d%n", this.totalBytes.get());
		ps.printf("Uploaded bytes: %16d%n", this.uploadedBytes.get());
		if (this.uploadedBytes.get() > 0) {
			ps.printf("  Upload speed:        %9.2f MB/s%n",
					((double) this.uploadedBytes.get() / ((double) durationMillis / 1000.0) / 1000000.0));
		}
		ps.println();
	}

	private String getSummary() throws IOException {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			try (PrintStream ps = new PrintStream(baos)) {
				printSummary(ps);
			}
			return new String(baos.toByteArray());
		}
	}

	private static String findSubjectByPatientID(MFSession session, String parentCID, String patientID)
			throws Throwable {
		StringBuilder query = new StringBuilder();
		if (parentCID != null) {
			query.append("(cid starts with '" + parentCID + "' or cid='" + parentCID + "') and ");
		}
		query.append("(xpath(mf-dicom-patient/id)='" + patientID + "' or xpath(mf-dicom-patient-encrypted/id)='"
				+ patientID + "')");
		XmlStringWriter w = new XmlStringWriter();
		w.add("where", query.toString());
		w.add("action", "get-cid");
		w.add("size", 2);
		Collection<String> cids = session.execute("asset.query", w.document()).values("cid");
		if (cids == null || cids.isEmpty()) {
			return null;
		}
		if (cids.size() > 1) {
			throw new IllegalArgumentException("Found multiple subjects match the PatientID: " + patientID
					+ ". Please specify --id argument using the id of one of the matching subjects");
		}
		return cids.iterator().next();
	}

	public static void main(String[] args) {
		CommandLine cl = new CommandLine(new SiemensRawMRUploadCommand());
		cl.setExecutionExceptionHandler(new IExecutionExceptionHandler() {
			@Override
			public int handleExecutionException(Exception ex, CommandLine commandLine, ParseResult parseResult)
					throws Exception {
				if (ex instanceof IllegalArgumentException) {
					ex.printStackTrace();
					cl.usage(System.err);
					return 1;
				} else {
					throw ex;
				}
			}
		});
		System.exit(cl.execute(args));
	}
}

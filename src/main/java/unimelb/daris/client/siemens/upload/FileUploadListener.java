package unimelb.daris.client.siemens.upload;

import java.nio.file.Path;

public interface FileUploadListener {

    void progressed(Path file, long progressed);

    void started(Path file, long fileLength);

    void skipped(Path file, String datasetCID);

    void failed(Path file);

    void deleted(Path file);

    void uploaded(Path file, long fileLength, String datasetCID);

}

package unimelb.daris.client.siemens.upload;

import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.PersonName;
import org.dcm4che3.data.PersonName.Component;
import org.dcm4che3.data.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import unimelb.daris.client.model.CiteableIdUtils;
import unimelb.daris.client.model.dataset.Dataset;
import unimelb.daris.client.model.project.Project;
import unimelb.daris.client.model.study.Study;
import unimelb.daris.client.model.subject.Subject;
import unimelb.daris.siemens.raw.pet.RawPETCTFile;
import unimelb.mf.client.session.MFSession;

public class SiemensRawPETCTFileInfo {

    private static final Logger logger = LoggerFactory.getLogger(SiemensRawPETCTFileInfo.class);

    private final Path _file;
    private final long _offset;
    private final Attributes _attrs;
    private PersonName _patientName;
    private String _patientID;
    private Date _patientBirthDate;
    private String _patientSex;
    private String _referringPhysicianName;

    private Date _studyDate;
    private String _studyInstanceUID;
    private String _studyDescription;
    private String _accessionNumber;

    private String _seriesInstanceUID;
    private String _modality;
    private String _seriesNumber;
    private String _protocolName;
    private String _seriesDescription;

    private Date _acquisitionDate;
    private String[] _imageType;

    private String _instanceNumber;

    private String _petctType;
    private Date _exportDate;

    private String _subjectCID;
    private String _studyCID;
    private String _datasetCID;

    SiemensRawPETCTFileInfo(Path file, long offset, Attributes attrs) throws Throwable {
        _file = file;
        final String fileName = file.getFileName().toString();
        _offset = offset;
        _attrs = attrs;
        if (attrs != null) {
            _patientID = _attrs.getString(Tag.PatientID);
            String patientName = _attrs.getString(Tag.PatientName, null);
            _patientName = patientName == null ? null : new PersonName(patientName);
            _patientBirthDate = _attrs.getDate(Tag.PatientBirthDate);
            _patientSex = _attrs.getString(Tag.PatientSex);
            _referringPhysicianName = _attrs.getString(Tag.ReferringPhysicianName);

            _studyDate = _attrs.getDate(Tag.StudyDateAndTime);
            _studyInstanceUID = _attrs.getString(Tag.StudyInstanceUID);
            _studyDescription = _attrs.getString(Tag.StudyDescription);
            _accessionNumber = _attrs.getString(Tag.AccessionNumber);

            _seriesInstanceUID = _attrs.getString(Tag.SeriesInstanceUID);
            _modality = _attrs.getString(Tag.Modality);
            _seriesNumber = _attrs.getString(Tag.SeriesNumber);
            _protocolName = _attrs.getString(Tag.ProtocolName);
            _seriesDescription = _attrs.getString(Tag.SeriesDescription);

            _acquisitionDate = _attrs.getDate(Tag.AcquisitionDateAndTime);
            _imageType = _attrs.getStrings(Tag.ImageType);

            _instanceNumber = _attrs.getString(Tag.InstanceNumber);
        } else {
            parseFromFileName(fileName);
        }
        _exportDate = parseExportDate(fileName);
    }

    private void parseFromFileName(String fileName) throws Throwable {
        if (fileName.toLowerCase().endsWith(".ptr")) {
            // Phantom_Hoffman.CT.PET_autonomy_mk6240_90_120_(Adult).601.RAW.20210720.150640.332972.2021.08.17.13.08.23.717000.2851347.ptr
            String[] parts = fileName.split("\\.");
            if (parts.length != 17) {
                throw new IOException("Failed to parse meta data from file: " + fileName);
            }
            // 0010,0010 PatientName
            _patientName = new PersonName(parts[0].replace('_', '^'));
            // 0008,0060 Modality
            _modality = parts[1];
            // 0008,1030 StudyDescription
            _studyDescription = parts[2].replaceFirst("_", "^").replace('_', ' ');
            // 0020,0011 SeriesNumber
            _seriesNumber = parts[3];
            //
            _petctType = parts[4];
            // 0008, 0020 StudyDate - parts[5]
            // 0008, 0030 StudyTime - parts[6] parts[7]
            _studyDate = new SimpleDateFormat("yyyyMMdd.HHmmss").parse(parts[5] + "." + parts[6]);
            long millisecs = TimeUnit.MICROSECONDS.toMillis(Long.parseLong(parts[7]));
            _studyDate = new Date(_studyDate.getTime() + millisecs);
        } else if (fileName.toLowerCase().endsWith(".ptd")) {
            // Phantom_Hoffman.PT.PET_adnettsm_mk6240_90_110_(Adult).602.PET_CALIBRATION.2021.08.17.16.11.21.274000.2.0.6649513.ptd
            String[] parts = fileName.split("\\.");
            if (parts.length != 16) {
                throw new IOException("Failed to parse meta data from file: " + fileName);
            }
            // 0010,0010 PatientName
            _patientName = new PersonName(parts[0].replace('_', '^'));
            // 0008,0060 Modality
            _modality = parts[1];
            // 0008,1030 StudyDescription
            _studyDescription = parts[2].replaceFirst("_", "^").replace('_', ' ');
            // 0020,0011 SeriesNumber
            _seriesNumber = parts[3];
            //
            _petctType = parts[4];
        } else {
            throw new Exception("Failed to parse meta data from file name: " + fileName);
        }
    }

    public Date exportDate() {
        return _exportDate;
    }

    public static final Pattern EXPORT_DATE_PATTERN = Pattern
            .compile("\\.(\\d{4}\\.\\d{2}\\.\\d{2}\\.\\d{2}\\.\\d{2}\\.\\d{2}\\.\\d{6})\\.");
    public static final SimpleDateFormat EXPORT_DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSS");

    private static Date parseExportDate(String fileName) {
        Date ed = null;
        Matcher m = EXPORT_DATE_PATTERN.matcher(fileName);
        if (m.find()) {
            try {
                String s = m.group(1);
                ed = EXPORT_DATE_FORMAT.parse(s.substring(0, s.length() - 3));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return ed;
    }

    public String seriesDescription() {
        return _seriesDescription;
    }

    public String protocolName() {
        return _protocolName;
    }

    public String seriesInstanceUID() {
        return _seriesInstanceUID;
    }

    public String seriesNumber() {
        return _seriesNumber;
    }

    public Date acquisitionDate() {
        return _acquisitionDate;
    }

    public String[] imageType() {
        return _imageType;
    }

    public String instanceNumber() {
        return _instanceNumber;
    }

    public String modality() {
        return _modality;
    }

    public Date studyDate() {
        return _studyDate;
    }

    public String studyInstanceUID() {
        return _studyInstanceUID;
    }

    public String accessionNumber() {
        return _accessionNumber;
    }

    public final Path file() {
        return _file;
    }

    public final long offset() {
        return _offset;
    }

    public final Attributes attributes() {
        return _attrs;
    }

    public final PersonName patientName() {
        return _patientName;
    }

    public final String patientFirstName() {
        if (_patientName != null) {
            return _patientName.get(Component.GivenName);
        }
        return null;
    }

    public final String patientLastName() {
        if (_patientName != null) {
            return _patientName.get(Component.FamilyName);
        }
        return null;
    }

    public final String patientID() {
        return _patientID;
    }

    public final Date patientBirthDate() {
        return _patientBirthDate;
    }

    public final String patientSexRaw() {
        return _patientSex;
    }

    public final String patientSex() {
        if (_patientSex != null) {
            if ("M".equalsIgnoreCase(_patientSex)) {
                return "male";
            } else if ("F".equalsIgnoreCase(_patientSex)) {
                return "female";
            } else {
                return "other";
            }
        }
        return null;
    }

    public String referringPhysicianName() {
        return _referringPhysicianName;
    }

    public String studyDescription() {
        return _studyDescription;
    }

    public String petctType() {
        return this._petctType;
    }

    public String subjectCID() {
        return _subjectCID;
    }

    public String studyCID() {
        return _studyCID;
    }

    public String datasetCID() {
        return _datasetCID;
    }

    public void findOrCreateParents(MFSession session, String parentCID, boolean ignoreRPN) throws Throwable {
        if (!ignoreRPN && _referringPhysicianName != null && CiteableIdUtils.isCID(_referringPhysicianName)) {
            if (parentCID == null || CiteableIdUtils.isAncestor(parentCID, _referringPhysicianName)) {
                parentCID = _referringPhysicianName;
            } else {
                if (!parentCID.equals(_referringPhysicianName)
                        && !CiteableIdUtils.isAncestor(_referringPhysicianName, parentCID)) {
                    throw new Exception("RerferringPhysicianName: " + _referringPhysicianName
                            + " conflicts with the specified parent id: " + parentCID);
                }
            }
        }
        _datasetCID = Dataset.findSiemensRawPETCTSeries(session, parentCID, this.file());
        if (_datasetCID != null) {
            _studyCID = CiteableIdUtils.getParentCID(_datasetCID);
            _subjectCID = CiteableIdUtils.getParentCID(_studyCID, 2);
            return;
        }
        _subjectCID = findOrCreateSubject(session, parentCID);
        _studyCID = findOrCreateStudy(session, _subjectCID);
    }

    private String findOrCreateSubject(MFSession session, String parentCID) throws Throwable {
        String subjectCID = null;
        final String fileName = _file.getFileName().toString();
        if (CiteableIdUtils.isSubjectCID(parentCID)) {
            subjectCID = parentCID;
            if (!Subject.exists(session, parentCID)) {
                throw new IllegalArgumentException("Subject " + subjectCID + " does not exist.");
            }
        } else {
            int cidDepth = CiteableIdUtils.getDepth(parentCID);
            if (cidDepth > CiteableIdUtils.PROJECT_ID_DEPTH) {
                throw new IllegalArgumentException("Invalid parent citeable id: " + parentCID + ".");
            } else if (cidDepth == CiteableIdUtils.PROJECT_ID_DEPTH) {
                if (!Project.exists(session, parentCID)) {
                    throw new IllegalArgumentException("Project " + parentCID + " does not exist.");
                }
            }
            logger.info("Searching for parent subject for file: '" + fileName + "'");
            subjectCID = Subject.find(session, parentCID, patientID(), patientFirstName(), patientLastName(),
                    patientBirthDate(), patientSex());
        }
        if (subjectCID == null) {
            // TODO create subject
            throw new IllegalArgumentException("Could not find parent subject for file: '" + fileName + "'");
        }
        logger.info("Found parent subject " + subjectCID + " for file: '" + fileName + "'");
        return subjectCID;
    }

    private String findOrCreateStudy(MFSession session, String parentCID) throws Throwable {
        String studyCID = Study.findSiemensRawPETCTStudy(session, parentCID, this);
        if (studyCID == null) {
            studyCID = Study.createSiemensRawPETCTStudy(session, parentCID, this);
        }
        return studyCID;
    }

    public static SiemensRawPETCTFileInfo parse(Path f) throws Throwable {
        Entry<Long, Attributes> e = RawPETCTFile.readDicomAttributes(f);
        if (e != null) {
            long offset = e.getKey();
            Attributes attributes = e.getValue();
            if (offset >= 0 && attributes != null && !attributes.isEmpty()) {
                return new SiemensRawPETCTFileInfo(f, offset, attributes);
            }
        }
        return new SiemensRawPETCTFileInfo(f, -1, null);
    }

    // public static void main(String[] args) throws Throwable {
    // SiemensRawPETCTFileInfo info = parse(Paths.get(
    // "/tmp/Physics2017_Medical.CT.Head_catphan2016_(Adult).601.RAW.20170731.143437.614976.2017.08.07.11.45.53.472000.19894018.ptr"));
    // System.out.println(info.studyDate());
    // System.out.println(info.patientName());
    // System.out.println(info.modality());
    // System.out.println(info.petctType());
    // System.out.println(info.studyDescription());
    // System.out.println(info.seriesNumber());
    // }
}

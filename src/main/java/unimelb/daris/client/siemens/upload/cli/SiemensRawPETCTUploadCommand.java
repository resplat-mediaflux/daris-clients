package unimelb.daris.client.siemens.upload.cli;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import arc.utils.DateTime;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IExecutionExceptionHandler;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParseResult;
import picocli.CommandLine.Spec;
import unimelb.daris.client.siemens.upload.FileUploadListener;
import unimelb.daris.client.siemens.upload.SiemensRawPETCTFileInfo;
import unimelb.daris.client.siemens.upload.SiemensRawPETCTFileUploadTask;
import unimelb.daris.client.siemens.upload.UploadOptions;
import unimelb.daris.siemens.raw.pet.RawPETCTFile;
import unimelb.logging.Logback;
import unimelb.mf.client.cli.MFCommandBase;
import unimelb.mf.client.session.MFSession;
import unimelb.picocli.converters.PathConverter;

@Command(name = SiemensRawPETCTUploadCommand.APP_NAME, abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class SiemensRawPETCTUploadCommand extends MFCommandBase implements FileUploadListener {

    public static final String APP_NAME = "siemens-raw-petct-upload";

    public static final String LOG_FILENAME = APP_NAME + ".log";

    public static final String PETCT_PROJECT_ID_ROOT = "1.7";

    public static final String DOT_DS_STORE = ".DS_Store";
    public static final String DOT_UNDERSCORE = "._";
    public static final long DOT_UNDERSCORE_FILE_SIZE = 4096;

    @Option(names = { "-c",
            "--config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux/DaRIS server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCfgFile;

    @Option(names = { "-i",
            "--id" }, paramLabel = "<id>", description = "The identifier of the parent subject or project.", required = false, defaultValue = PETCT_PROJECT_ID_ROOT, arity = "1")
    private String parentCID;

    @Option(names = {
            "--ignore-rpn" }, negatable = false, required = false, description = "Ignore the id in ReferringPhysicianName [0008,0090] element.")
    private Boolean ignoreRPN;

    @Option(names = {
            "--checksum" }, negatable = true, description = "Calculate and compare CRC32 checksums between the local file and the uploaded dataset content. Enabled by default.")
    private Boolean checksum;

    @Option(names = {
            "--delete" }, negatable = true, description = "Delete the source file after uploading successfully. Disabled by default.")
    private Boolean delete;

    @Option(names = { "--expire" }, negatable = true, description = "Set the expire-date. Disabled by default.")
    private Boolean expire;

    @Option(names = {
            "--follow-symlinks" }, negatable = false, description = "Follow symbolic links. Disabled by default.")
    private Boolean followSymlinks;

    @Option(names = {
            "--log-dir" }, paramLabel = "<directory>", description = "The log directory path. If not specified, logging is disabled.", required = false, arity = "1")
    private Path logDir;

    @Option(names = {
            "--nb-workers" }, paramLabel = "<n>", description = "Number of concurrent worker threads. Defaults to 1.", required = false, arity = "1")
    private int nbWorkers = 1;

    @Option(names = { "--verbose" }, negatable = false, description = "Print progress information verbosely.")
    private boolean verbose = false;

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "Display a help message.")
    private boolean help = false;

    @Parameters(description = "The raw PET/CT files or the directories contain raw PET/CT files.", arity = "1..", index = "0", paramLabel = "RAW_PETCT_FILE_OR_DIR")
    private Set<Path> inputs;

    @Spec
    private CommandSpec spec;

    private long startTimeMillis = 0;
    private long finishTimeMillis = 0;
    private AtomicLong totalFiles = new AtomicLong(0);
    private AtomicLong uploadedFiles = new AtomicLong(0);
    private AtomicLong skippedFiles = new AtomicLong(0);
    private AtomicLong failedFiles = new AtomicLong(0);
    private AtomicLong deletedFiles = new AtomicLong(0);
    private AtomicLong totalBytes = new AtomicLong(0);
    private AtomicLong progressedBytes = new AtomicLong(0);
    private AtomicLong uploadedBytes = new AtomicLong(0);
    private ConcurrentHashMap<Path, String> uploadedDatasets = new ConcurrentHashMap<>();

    private ExecutorService workers;

    private Logger logger;

    private Set<Path> parentDirectories = Collections.synchronizedSet(new LinkedHashSet<>());

    @Override
    public final Logger logger() {
        if (logger == null) {
            logger = LoggerFactory.getLogger(SiemensRawPETCTUploadCommand.class);
        }
        return logger;
    }

    @Override
    public String appName() {
        return APP_NAME;
    }

    @Override
    protected Path mfluxCfgFile() {
        return this.mfCfgFile;
    }

    @Override
    public void execute(MFSession session) throws Throwable {

        // convert inputs to absolute paths
        this.inputs = this.inputs.stream().map(Path::toAbsolutePath).collect(Collectors.toSet());

        // set default option values
        this.ignoreRPN = this.ignoreRPN == null ? false : this.ignoreRPN;
        this.checksum = this.checksum == null ? true : this.checksum;
        this.expire = this.expire == null ? false : this.expire;
        this.delete = this.delete == null ? false : this.delete;
        this.followSymlinks = this.followSymlinks == null ? false : this.followSymlinks;

        this.startTimeMillis = System.currentTimeMillis();
        UploadOptions options = new UploadOptions.Builder().setParentId(this.parentCID).setChecksum(this.checksum)
                .setExpire(this.expire).setDelete(this.delete).setFollowSymlinks(this.followSymlinks).build();
        if (this.verbose) {
            System.out.println(options);
        }

        // init logging
        initLogging();

        // scan for raw pet/ct files
        List<SiemensRawPETCTFileInfo> rawFiles = new ArrayList<SiemensRawPETCTFileInfo>();
        for (Path input : inputs) {
            if (!Files.exists(input)) {
                throw new IllegalArgumentException("Input file: '" + input + "' does not exist.");
            }
            addRawFiles(input, rawFiles);
        }

        // init thread pool
        this.workers = new ThreadPoolExecutor(nbWorkers, nbWorkers, 0, TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<>(100), r -> new Thread(r, APP_NAME + ".worker"),
                new ThreadPoolExecutor.CallerRunsPolicy());

        // process raw files...
        for (SiemensRawPETCTFileInfo rawFile : rawFiles) {
            // find or create parents
            try {
                rawFile.findOrCreateParents(session, this.parentCID, this.ignoreRPN);
            } catch (Throwable e) {
                logError("Failed to upload file: '" + rawFile.file() + "'", e);
                // count this file as failed
                this.totalFiles.getAndIncrement();
                this.totalBytes.getAndAdd(Files.size(rawFile.file()));
                this.failedFiles.getAndDecrement();
            }
            // submit to thread pool to start uploading/verifying
            if (rawFile.studyCID() != null || rawFile.datasetCID() != null) {
                submit(session, rawFile, options);
            }
        }

        // shutdown thread pool
        this.workers.shutdown();
        this.workers.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        this.finishTimeMillis = System.currentTimeMillis();

        // delete empty directories
        if (this.delete && !this.parentDirectories.isEmpty()) {
            for (Path dir : this.parentDirectories) {
                if (Files.isDirectory(dir)) {
                    try {
                        // delete directory if empty
                        if (!Files.list(dir).findFirst().isPresent()) {
                            this.logger.info("Deleting empty directory: '" + dir + "'");
                            Files.deleteIfExists(dir);
                        }
                    } catch (Throwable e) {
                        this.logger.warn("Failed to delete directory: '" + dir + "'", e);
                    }
                }
            }
        }

        String summary = getSummary();
        logInfo(summary);
        if (!this.verbose) {
            System.out.println(summary);
        }
    }

    private void initLogging() throws Throwable {
        if (this.logDir != null) {
            if (!Files.exists(logDir)) {
                throw new IllegalArgumentException("'" + logDir + "' does not exist.");
            }
            if (!Files.isDirectory(logDir)) {
                throw new IllegalArgumentException("'" + logDir + "' is not a directory.");
            }
        }
        Path logFile = this.logDir == null ? null : Paths.get(logDir.toString(), LOG_FILENAME);
        Logback.initRootLogger(logFile, this.verbose);
        this.logger = LoggerFactory.getLogger(SiemensRawPETCTUploadCommand.class);
    }

    private void addRawFiles(Path input, List<SiemensRawPETCTFileInfo> rawFiles) throws Throwable {
        if (!Files.isDirectory(input)) {
            if (!RawPETCTFile.hasValidFileExtension(input.getFileName().toString())) {
                logWarning("File '" + input
                        + "' is not a Siemens raw PET/CT file. Expects file name ends with .ptd or .ptr");
            } else {
                try {
                    SiemensRawPETCTFileInfo rawFile = SiemensRawPETCTFileInfo.parse(input);
                    rawFiles.add(rawFile);
                } catch (Throwable e) {
                    logError("Failed to parse raw PET/CT file: '" + input + "'", e);
                }
            }
        } else {
            Files.walkFileTree(input, this.followSymlinks ? EnumSet.of(FileVisitOption.FOLLOW_LINKS)
                    : EnumSet.noneOf(FileVisitOption.class), Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                            try {
                                if (SiemensRawPETCTUploadCommand.this.delete == true) {
                                    // delete .DS_Store file, and ._* file if size is 4096
                                    String fileName = file.getFileName().toString();
                                    if (fileName.equals(DOT_DS_STORE) || (fileName.startsWith(DOT_UNDERSCORE)
                                            && Files.size(file) == DOT_UNDERSCORE_FILE_SIZE)) {
                                        Files.deleteIfExists(file);
                                        return FileVisitResult.CONTINUE;
                                    }
                                }
                                addRawFiles(file, rawFiles);
                            } catch (Throwable e) {
                                logError(e.getMessage(), e);
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult visitFileFailed(Path file, IOException ioe) {
                            if (ioe != null) {
                                logError(ioe.getMessage(), ioe);
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult postVisitDirectory(Path dir, IOException ioe) {
                            if (ioe != null) {
                                logError(ioe.getMessage(), ioe);
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                                throws IOException {
                            return super.preVisitDirectory(dir, attrs);
                        }
                    });
        }
    }

    private void submit(MFSession session, SiemensRawPETCTFileInfo rawFile, UploadOptions options) throws Throwable {
        this.workers.submit(new SiemensRawPETCTFileUploadTask(session, rawFile, options, this));
    }

    @Override
    public void progressed(Path file, long progressed) {
        this.progressedBytes.getAndAdd(progressed);
    }

    @Override
    public void started(Path file, long fileLength) {
        System.out.println("Processing file: '" + file + "' (size=" + fileLength + ")");
        this.totalFiles.getAndIncrement();
        this.totalBytes.getAndAdd(fileLength);
    }

    @Override
    public void skipped(Path file, String datasetCID) {
        System.out.println("Skipped file: '" + file + "' - dataset " + datasetCID + " already exists.");
        this.skippedFiles.getAndIncrement();
    }

    @Override
    public void failed(Path file) {
        System.err.println("Failed to upload file: '" + file + "'");
        this.failedFiles.getAndIncrement();
    }

    @Override
    public void uploaded(Path file, long fileLength, String datasetCID) {
        System.err.println("Uploaded file: '" + file + "' to dataset: " + datasetCID + " (" + fileLength + " bytes)");
        this.uploadedFiles.getAndIncrement();
        this.uploadedBytes.getAndAdd(fileLength);
        this.uploadedDatasets.put(file, datasetCID);
    }

    @Override
    public void deleted(Path file) {
        this.deletedFiles.getAndIncrement();
        System.out.println("Deleted file: '" + file + "'");
        Path dir = file.getParent();
        if (!this.inputs.contains(dir)) { // do not delete root/input dir...
            this.parentDirectories.add(dir);
        }
    }

    private void printSummary(PrintStream ps) {
        ps.println();
        ps.printf(" Start time: %s%n", DateTime.string(new Date(this.startTimeMillis)));
        ps.printf("Finish time: %s%n", DateTime.string(new Date(this.finishTimeMillis)));
        long durationMillis = this.finishTimeMillis - this.startTimeMillis;
        Duration duration = Duration.ofMillis(durationMillis);
        long durationSeconds = duration.getSeconds();
        ps.printf("   Duration: %d:%02d:%02d.%03d%n", durationSeconds / 3600, (durationSeconds % 3600) / 60,
                durationSeconds % 60, durationMillis % 1000);
        ps.println();
        ps.printf("   Total files: %8d%n", this.totalFiles.get());
        ps.printf("Uploaded files: %8d%n", this.uploadedFiles.get());
        if (this.skippedFiles.get() > 0) {
            ps.printf(" Skipped files: %8d%n", this.skippedFiles.get());
        }
        if (this.failedFiles.get() > 0) {
            ps.printf("  Failed files: %8d%n", this.failedFiles.get());
        }
        if (this.delete || this.deletedFiles.get() > 0) {
            ps.printf(" Deleted files: %8d%n", this.deletedFiles.get());
        }
        ps.println();
        ps.printf("   Total bytes: %16d%n", this.totalBytes.get());
        ps.printf("Uploaded bytes: %16d%n", this.uploadedBytes.get());
        if (this.uploadedBytes.get() > 0) {
            ps.printf("  Upload speed:        %9.2f MB/s%n",
                    ((double) this.uploadedBytes.get() / ((double) durationMillis / 1000.0) / 1000000.0));
        }
        ps.println();
    }

    private String getSummary() throws IOException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            try (PrintStream ps = new PrintStream(baos)) {
                printSummary(ps);
            }
            return new String(baos.toByteArray());
        }
    }

    public static void main(String[] args) {
        CommandLine cl = new CommandLine(new SiemensRawPETCTUploadCommand());
        cl.setExecutionExceptionHandler(new IExecutionExceptionHandler() {
            @Override
            public int handleExecutionException(Exception ex, CommandLine commandLine, ParseResult parseResult)
                    throws Exception {
                if (ex instanceof IllegalArgumentException) {
                    ex.printStackTrace();
                    cl.usage(System.err);
                    return 1;
                } else {
                    throw ex;
                }
            }
        });
        System.exit(cl.execute(args));
    }

}

package unimelb.daris.client.siemens.upload;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import arc.xml.XmlDoc;
import unimelb.daris.client.model.dataset.Dataset;
import unimelb.io.TransferProgressListener;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.MFClientTaskBase;
import unimelb.mf.client.util.AssetUtils;
import unimelb.utils.ChecksumUtils;

public class SiemensRawPETCTFileUploadTask extends MFClientTaskBase<String> {

    public static final int BUFFER_SIZE = 8192;

    private static final Logger logger = LoggerFactory.getLogger(SiemensRawPETCTFileUploadTask.class);

    private SiemensRawPETCTFileInfo _rawFile;
    private final UploadOptions _options;
    private final FileUploadListener _ul;

    public SiemensRawPETCTFileUploadTask(MFSession session, SiemensRawPETCTFileInfo rawFile, UploadOptions options,
            FileUploadListener ul) {
        super(session);
        _rawFile = rawFile;
        _options = options;
        _ul = ul;
    }

    @Override
    public final Logger logger() {
        return logger;
    }

    @Override
    public final String execute() throws Throwable {
        try {
            return upload();
        } catch (Throwable e) {
            _ul.failed(_rawFile.file());
            throw e;
        }
    }

    private String upload() throws Throwable {

        final Path file = _rawFile.file();
        final long fileLength = Files.size(file);
        final String fileName = file.getFileName().toString();
        _ul.started(file, fileLength);

        Date expireDate = null;
        if (_options.expire) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(_rawFile.studyDate());
            cal.add(Calendar.YEAR, 1);
            expireDate = cal.getTime();
        }

        TransferProgressListener tpl = nbBytes -> {
            _ul.progressed(file, nbBytes);
        };

        /*
         * find dataset by name
         */
        String datasetCID = _rawFile.datasetCID();
        if (datasetCID != null) {
            logInfo("Found dataset " + datasetCID + " matches file: '" + fileName + "'");
            // get dataset asset meta
            XmlDoc.Element ae = AssetUtils.getAssetMetaByCid(session(), datasetCID);
            long assetCSize = ae.longValue("content/size", 0);
            if (fileLength == assetCSize) {
                logInfo("File '" + fileName + "' sizes match. (size= " + fileLength + ")");
                if (_options.checksum) {
                    long assetCSum = ae.longValue("content/csum[@base='10']", 0);
                    logInfo("Calculating CRC32 checksum for file: '" + fileName + "'");
                    long fileCSum = ChecksumUtils.getCRC32Value(file);
                    if (assetCSum == fileCSum) {
                        logInfo("File '" + fileName + "' checksums match. (checksum=" + Long.toHexString(fileCSum)
                                + ")");
                        _ul.skipped(file, datasetCID);
                    } else {
                        logWarning("File '" + fileName + "' checksum=" + Long.toHexString(fileCSum)
                                + " does not match asset " + datasetCID + " content checksum="
                                + Long.toHexString(fileCSum));
                        logInfo("Updating dataset: " + datasetCID + " with file: '" + file + "'");
                        Dataset.updateSiemensRawPETCTSeries(session(), datasetCID, _rawFile, expireDate,
                                _options.checksum, tpl);
                        _ul.uploaded(file, fileLength, datasetCID);
                    }
                } else {
                    _ul.skipped(file, datasetCID);
                }
            } else {
                logWarning("File '" + fileName + "' size=" + fileLength + " does not match asset " + datasetCID
                        + " content size=" + assetCSize);
                logInfo("Updating dataset: " + datasetCID + " with file: '" + file + "'");
                Dataset.updateSiemensRawPETCTSeries(session(), datasetCID, _rawFile, expireDate, _options.checksum,
                        tpl);
                _ul.uploaded(file, fileLength, datasetCID);
            }
        } else {
            String studyCID = _rawFile.studyCID();
            /*
             * create new raw pet/ct dataset
             */
            logInfo("Uploading file: '" + fileName + "' to study " + studyCID);
            datasetCID = Dataset.createSiemensRawPETCTSeries(session(), studyCID, _rawFile, expireDate,
                    _options.checksum, tpl);
            _ul.uploaded(file, fileLength, datasetCID);
        }

        // delete source file.
        if (_options.delete) {
            XmlDoc.Element ae = AssetUtils.getAssetMetaByCid(session(), datasetCID);
            long assetContentSize = ae.longValue("content/size", 0);
            if (assetContentSize > 0 && assetContentSize == fileLength) {
                logInfo("Deleting file: '" + file + "'");
                Files.delete(file);
                _ul.deleted(file);
            }
        }
        return datasetCID;
    }
}

package unimelb.daris.client.siemens.upload;

import unimelb.daris.client.model.CiteableIdUtils;

public class UploadOptions {

    public final String parentId;
    public final boolean checksum;
    public final boolean expire;
    public final boolean delete;
    public final boolean followSymlinks;

    private UploadOptions(String parentId, boolean checksum, boolean expire, boolean delete, boolean followSymlinks) {
        this.parentId = parentId;
        this.checksum = checksum;
        this.expire = expire;
        this.delete = delete;
        this.followSymlinks = followSymlinks;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n>>> Options:\n");
        if (this.parentId != null && CiteableIdUtils.getDepth(this.parentId) >= CiteableIdUtils.PROJECT_ID_DEPTH) {
            sb.append(String.format("    ParentID: %s%n", this.parentId));
        }
        sb.append(String.format("    Checksum: %s%n", this.checksum));
        sb.append(String.format("      Expire: %s%n", this.expire));
        sb.append(String.format("      Delete: %s%n", this.delete));
        if (this.followSymlinks) {
            sb.append(String.format("    FollowSymlinks: %s%n", this.followSymlinks));
        }
        sb.append("\n");
        return sb.toString();
    }

    public static class Builder {
        private String parentId = null;
        private boolean checksum = false;
        private boolean expire = false;
        private boolean delete = false;
        private boolean followSymlinks = false;

        public Builder setParentId(String parentId) {
            this.parentId = parentId;
            return this;
        }

        public Builder setChecksum(boolean checksum) {
            this.checksum = checksum;
            return this;
        }

        public Builder setExpire(boolean expire) {
            this.expire = expire;
            return this;
        }

        public Builder setDelete(boolean delete) {
            this.delete = delete;
            return this;
        }

        public Builder setFollowSymlinks(boolean followSymlinks) {
            this.followSymlinks = followSymlinks;
            return this;
        }

        public UploadOptions build() {
            return new UploadOptions(parentId, checksum, expire, delete, followSymlinks);
        }
    }

}

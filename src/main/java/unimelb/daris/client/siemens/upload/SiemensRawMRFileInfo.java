package unimelb.daris.client.siemens.upload;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import unimelb.daris.client.model.CiteableIdUtils;
import unimelb.daris.client.model.dataset.Dataset;
import unimelb.daris.client.model.project.Project;
import unimelb.daris.client.model.study.Study;
import unimelb.daris.client.model.subject.Subject;
import unimelb.daris.siemens.raw.mr.SiemensRawMRFile;
import unimelb.daris.siemens.raw.mr.SiemensRawMRFile.Version;
import unimelb.daris.siemens.raw.mr.SiemensRawMRFileMetaData;
import unimelb.io.StreamUtils;
import unimelb.mf.client.session.MFSession;

public class SiemensRawMRFileInfo {

    private static final Logger logger = LoggerFactory.getLogger(SiemensRawMRFileInfo.class);

    private final Path _file;
    private final String _fileName;
    private Version _version;
    private int _mid;
    private int _fid;
    private String _protocolName;
    private String _patientName;
    private String _patientID;
    private String _patientBirthDateRaw;
    private String _patientSexRaw;
    private String _patientPosition;
    private String _studyDescription;
    private String _frameOfReference;
    private String _softwareVersions;
    private String _manufacturer;
    private String _manufacturerModelName;
    private String _deviceSeriesNumber;
    private String _institutionName;
    private String _institutionAddress;
    private String _modality;
    private String _magneticFieldStrength;
    private String _transmitCoilName;
    private String _flipAngle;

    private String _datasetCID;
    private String _studyCID;
    private String _subjectCID;

    private SiemensRawMRFileInfo(Path file) {
        this._file = file;
        this._fileName = file.getFileName().toString();
    }

    public final Path file() {
        return _file;
    }

    public final String fileName() {
        return _fileName;
    }

    public final Version version() {
        return _version;
    }

    public final int mid() {
        return _mid;
    }

    public final int fid() {
        return _fid;
    }

    public final String protocolName() {
        return _protocolName;
    }

    public final String patientName() {
        return _patientName;
    }

    public final String patientFirstName() {
        if (this._patientName != null) {
            String[] parts = this._patientName.split("\\^");
            if (parts.length > 1) {
                String firstName = parts[1];
                if (firstName != null && !firstName.isEmpty()) {
                    return firstName.trim();
                }
            }
        }
        return null;
    }

    public final String patientLastName() {
        if (this._patientName != null) {
            String[] parts = this._patientName.split("\\^");
            String lastName = parts[0];
            if (lastName != null && !lastName.isEmpty()) {
                return lastName.trim();
            }
        }
        return null;
    }

    public final String patientMiddleName() {
        if (this._patientName != null) {
            String[] parts = this._patientName.split("\\^");
            if (parts.length > 2) {
                String middleName = parts[2];
                if (middleName != null && !middleName.isEmpty()) {
                    return middleName.trim();
                }
            }
        }
        return null;
    }

    public final Date scanDate() throws ParseException {
        if (this._frameOfReference != null) {
            // 1.3.12.2.1107.5.2.34.18978.1.20140718161933906.0.0.0
            String[] parts = _frameOfReference.split("\\.");
            if (parts.length == 14 && parts[10].length() == 17) {
                return new SimpleDateFormat("yyyyMMddHHmmssSSS").parse(parts[10].substring(0, 17));
            }
        }
        return null;
    }

    public final String patientSexRaw() {
        return this._patientSexRaw;
    }

    public final Integer getPatientSexInt() {
        if (this._patientSexRaw != null) {
            return Integer.parseInt(this._patientSexRaw);
        }
        return null;
    }

    public final String patientSex() {
        if (this._patientSexRaw != null) {
            if (this._patientSexRaw.equals("1")) {
                return "female";
            } else if (this._patientSexRaw.equals("2")) {
                return "male";
            } else if (this._patientSexRaw.equals("3")) {
                return "other";
            }
        }
        return null;
    }

    public final String patientID() {
        return _patientID;
    }

    public final String patientBirthDateRaw() {
        return _patientBirthDateRaw;
    }

    public final Date patientBirthDate() {
        if (this._patientBirthDateRaw != null) {
            try {
                return new SimpleDateFormat("yyyyMMdd").parse(this._patientBirthDateRaw);
            } catch (Throwable e) {
                logger.warn("Failed to parse PatientBirthDay: " + this._patientBirthDateRaw);
            }
        }
        return null;
    }

    public final String patientPosition() {
        return _patientPosition;
    }

    public final String studyDescription() {
        return _studyDescription;
    }

    public final String frameOfReference() {
        return _frameOfReference;
    }

    public final String softwareVersions() {
        return _softwareVersions;
    }

    public final String manufacturer() {
        return _manufacturer;
    }

    public final String manufacturerModelName() {
        return _manufacturerModelName;
    }

    public final String deviceSeriesNumber() {
        return _deviceSeriesNumber;
    }

    public final String institutionName() {
        return _institutionName;
    }

    public final String institutionAddress() {
        return _institutionAddress;
    }

    public final String modality() {
        return _modality;
    }

    public final String magneticFieldStrength() {
        return _magneticFieldStrength;
    }

    public final String transmitCoilName() {
        return _transmitCoilName;
    }

    public final String flipAngle() {
        return _flipAngle;
    }

    public final String datasetCID() {
        return _datasetCID;
    }

    public final String subjectCID() {
        return _subjectCID;
    }

    public final String studyCID() {
        return _studyCID;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FileName: ").append(this.fileName()).append(System.lineSeparator());
        sb.append("FileVersion: ").append(this._version).append(System.lineSeparator());
        sb.append("MID: ").append(this._mid).append(System.lineSeparator());
        sb.append("FID: ").append(this._fid).append(System.lineSeparator());
        if (this._protocolName != null) {
            sb.append("ProtocolName: ").append(this._protocolName).append(System.lineSeparator());
        }
        if (this._modality != null) {
            sb.append("Modality: ").append(this._modality).append(System.lineSeparator());
        }
        if (this._patientName != null) {
            sb.append("PatientName: ").append(this._patientName).append(System.lineSeparator());
        }
        if (this._patientID != null) {
            sb.append("PatientID: ").append(this._patientID).append(System.lineSeparator());
        }
        if (this._patientSexRaw != null) {
            sb.append("PatientSex: ").append(this._patientSexRaw).append(System.lineSeparator());
        }
        if (this._patientBirthDateRaw != null) {
            sb.append("PatientBirthDate: ").append(this._patientBirthDateRaw).append(System.lineSeparator());
        }
        if (this._studyDescription != null) {
            sb.append("StudyDescription: ").append(this._studyDescription).append(System.lineSeparator());
        }
        if (this._patientPosition != null) {
            sb.append("PatientPosition: ").append(this._patientPosition).append(System.lineSeparator());
        }
        if (this._frameOfReference != null) {
            sb.append("FrameOfReference: ").append(this._frameOfReference).append(System.lineSeparator());
        }
        if (this._manufacturer != null) {
            sb.append("Manufacturer: ").append(this._manufacturer).append(System.lineSeparator());
        }
        if (this._manufacturerModelName != null) {
            sb.append("ManufacturerModelName: ").append(this._manufacturerModelName).append(System.lineSeparator());
        }
        if (this._deviceSeriesNumber != null) {
            sb.append("DeviceSeriesNumber: ").append(this._deviceSeriesNumber).append(System.lineSeparator());
        }
        if (this._institutionName != null) {
            sb.append("InstitutionName: ").append(this._institutionName).append(System.lineSeparator());
        }
        if (this._institutionAddress != null) {
            sb.append("InstitutionAddress: ").append(this._institutionAddress).append(System.lineSeparator());
        }
        if (this._magneticFieldStrength != null) {
            sb.append("MagneticFieldStrength: ").append(this._magneticFieldStrength).append(System.lineSeparator());
        }
        if (this._transmitCoilName != null) {
            sb.append("TransmitCoilName: ").append(this._transmitCoilName).append(System.lineSeparator());
        }
        if (this._flipAngle != null) {
            sb.append("FlipAngle: ").append(this._flipAngle).append(System.lineSeparator());
        }
        return sb.toString();
    }

    public static SiemensRawMRFileInfo parse(Path f) throws Throwable {
        try {

            SiemensRawMRFileInfo info = new SiemensRawMRFileInfo(f);
            try (InputStream in = new BufferedInputStream(Files.newInputStream(f))) {
                parse(in, info);
                return info;
            }
        } catch (Throwable e) {
            throw new IOException("Failed to parse file: " + f, e);
        }
    }

    private static void parse(InputStream in, SiemensRawMRFileInfo info) throws Throwable {
        long vb15HeaderOffset = 0;
        byte[] buf = new byte[32];
        in.mark(SiemensRawMRFile.VB19_EXT_HEADER_LENGTH);
        StreamUtils.readFully(in, buf);
        try {
            int mid = ByteBuffer.wrap(buf, 8, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
            int fid = ByteBuffer.wrap(buf, 12, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
            long offset = ByteBuffer.wrap(buf, 16, 8).order(ByteOrder.LITTLE_ENDIAN).getLong();
            if (fid >= 0 && mid >= 0 && offset == SiemensRawMRFile.VB19_EXT_HEADER_LENGTH) {
                // is VB19
                info._version = Version.VB19;
                info._mid = mid;
                info._fid = fid;
                // offset: 0x20 (32)
                StreamUtils.skip(in, 64);
                // offset: 0x60 (96)
                info._protocolName = StreamUtils.readString(in);
                vb15HeaderOffset = offset;
            } else if (buf[8] == (byte) 'C' && buf[9] == (byte) 'o' && buf[10] == (byte) 'n' && buf[11] == (byte) 'f'
                    && buf[12] == (byte) 'i' && buf[13] == (byte) 'g') {
                // is VB15
                info._version = Version.VB15;
                info._mid = SiemensRawMRFile.getMIDFromFileName(info._fileName);
                info._fid = SiemensRawMRFile.getFIDFromFileName(info._fileName);
                info._protocolName = SiemensRawMRFile.getSequenceNameFromFileName(info._fileName);
            }
        } finally {
            in.reset();
        }

        if (info._version == null) {
            throw new IOException("Failed to parse meas VB version from file: " + info._fileName);
        }

        SiemensRawMRFileMetaData.parse(in, vb15HeaderOffset, (group, element) -> {
            if (SiemensRawMRFile.GROUP_CONFIG.equalsIgnoreCase(group)) {
                if ("tPatientName".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._patientName = element.value();
                } else if ("PatientID".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._patientID = element.value();
                } else if ("PatientBirthDay".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._patientBirthDateRaw = element.value();
                } else if ("PatientSex".equals(element.name()) && "ParamLong".equals(element.type())) {
                    info._patientSexRaw = element.value();
                } else if ("tStudyDescription".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._studyDescription = element.value();
                } else if ("PatientPosition".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._patientPosition = element.value();
                } else if ("FrameOfReference".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._frameOfReference = element.value();
                }

            } else if (SiemensRawMRFile.GROUP_DICOM.equalsIgnoreCase(group)) {
                if ("SoftwareVersions".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._softwareVersions = element.value();
                } else if ("Manufacturer".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._manufacturer = element.value();
                } else if ("ManufacturersModelName".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._manufacturerModelName = element.value();
                } else if ("InstitutionAddress".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._institutionAddress = element.value();
                } else if ("DeviceSerialNumber".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._deviceSeriesNumber = element.value();
                } else if ("InstitutionName".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._institutionName = element.value();
                } else if ("Modality".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._modality = element.value();
                } else if ("tProtocolName".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._protocolName = element.value();
                } else if ("flMagneticFieldStrength".equals(element.name()) && "ParamDouble".equals(element.type())) {
                    info._magneticFieldStrength = element.value();
                } else if ("TransmittingCoil".equals(element.name()) && "ParamString".equals(element.type())) {
                    info._transmitCoilName = element.value();
                } else if ("adFlipAngleDegree".equals(element.name()) && "ParamDouble".equals(element.type())) {
                    info._flipAngle = element.value();
                }
            }
        }, SiemensRawMRFile.GROUP_CONFIG, SiemensRawMRFile.GROUP_DICOM);
    }

    public void findOrCreateParents(MFSession session, String parentCID) throws Throwable {
        _datasetCID = Dataset.findSiemensRawMRSeries(session, parentCID, this.file());
        if (_datasetCID != null) {
            _studyCID = CiteableIdUtils.getParentCID(_datasetCID);
            _subjectCID = CiteableIdUtils.getParentCID(_studyCID, 2);
            return;
        }
        _subjectCID = findOrCreateSubject(session, parentCID);
        _studyCID = findOrCreateStudy(session, _subjectCID);
    }

    private String findOrCreateSubject(MFSession session, String parentCID) throws Throwable {
        String subjectCID;
        if (CiteableIdUtils.isSubjectCID(parentCID)) {
            subjectCID = parentCID;
            if (!Subject.exists(session, parentCID)) {
                throw new IllegalArgumentException("Subject " + subjectCID + " does not exist.");
            }
        } else {
            int cidDepth = CiteableIdUtils.getDepth(parentCID);
            if (cidDepth > CiteableIdUtils.PROJECT_ID_DEPTH) {
                throw new IllegalArgumentException("Invalid parent citeable id: " + parentCID + ".");
            } else if (cidDepth == CiteableIdUtils.PROJECT_ID_DEPTH) {
                if (!Project.exists(session, parentCID)) {
                    throw new IllegalArgumentException("Project " + parentCID + " does not exist.");
                }
            }
            String fileName = _file.getFileName().toString();
            logger.info("Searching for parent subject for file: '" + fileName + "'");
            subjectCID = Subject.find(session, parentCID, patientID(), patientFirstName(), patientLastName(),
                    patientBirthDate(), patientSex());
            if (subjectCID == null) {
                // TODO create subject
                throw new IllegalArgumentException("Could not find parent subject for file: '" + fileName + "'");
            }
        }
        return subjectCID;
    }

    private String findOrCreateStudy(MFSession session, String subjectCID) throws Throwable {
        String studyCID = Study.findSiemensRawMRStudy(session, subjectCID, this.scanDate());
        if (studyCID == null) {
            studyCID = Study.createSiemensRawMRStudy(session, subjectCID, this.frameOfReference(), this.scanDate());
        }
        return studyCID;
    }

    public static void main(String[] args) throws Throwable {

        // @formatter:off
//        SiemensRawMRFileInfo fi = parse(Paths.get("/Users/wliu5/Downloads/Samples/meas_MID00014_FID00893_AdjDico.dat"));
//        System.out.println(fi);
        // @formatter:on

    }

}

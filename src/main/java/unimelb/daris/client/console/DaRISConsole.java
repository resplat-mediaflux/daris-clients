package unimelb.daris.client.console;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.jline.builtins.Completers.TreeCompleter;
import org.jline.keymap.KeyMap;
import org.jline.reader.Binding;
import org.jline.reader.Completer;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.Macro;
import org.jline.reader.MaskingCallback;
import org.jline.reader.ParsedLine;
import org.jline.reader.Parser;
import org.jline.reader.Reference;
import org.jline.reader.UserInterruptException;
import org.jline.reader.impl.LineReaderImpl;
import org.jline.terminal.Cursor;
import org.jline.terminal.MouseEvent;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.jline.utils.AttributedString;
import org.jline.utils.InfoCmp.Capability;

import unimelb.mf.client.task.MFClientTask;
import unimelb.mf.client.session.MFSession;

public class DaRISConsole implements MFClientTask<Void> {

    private MFSession _session;
    private DaRISConsoleSettings _settings;

    public DaRISConsole(MFSession session, DaRISConsoleSettings settings) {
        _session = session;
        _settings = settings;
    }

    @Override
    public MFSession session() {
        return _session;
    }

    @Override
    public Void execute() throws Throwable {

//        String prompt = "DaRIS> ";
        String rightPrompt = null;
        Character mask = null;
        String trigger = null;
        boolean color = false;
        boolean timer = false;

        TerminalBuilder builder = TerminalBuilder.builder();
        int mouse = 0;
        // @formatter:off
        Completer completer = new TreeCompleter(
                TreeCompleter.node("create", 
                        TreeCompleter.node("project"),
                        TreeCompleter.node("subject"),
                        TreeCompleter.node("study"),
                        TreeCompleter.node("dataset")),
                TreeCompleter.node("describe"),
                TreeCompleter.node("get"),
                TreeCompleter.node("list"),
                TreeCompleter.node("update",
                        TreeCompleter.node("project"),
                        TreeCompleter.node("subject"),
                        TreeCompleter.node("study"),
                        TreeCompleter.node("dataset"))
                );
        // @formatter:on
        Parser parser = null;

        builder.system(true);
        Terminal terminal = builder.build();

        LineReader reader = LineReaderBuilder.builder().terminal(terminal).completer(completer).parser(parser).build();

        if (timer) {
            Executors.newScheduledThreadPool(1).scheduleAtFixedRate(() -> {
                reader.callWidget(LineReader.CLEAR);
                reader.getTerminal().writer().println("Hello world!");
                reader.callWidget(LineReader.REDRAW_LINE);
                reader.callWidget(LineReader.REDISPLAY);
                reader.getTerminal().writer().flush();
            }, 1, 1, TimeUnit.SECONDS);
        }
        if (mouse != 0) {
            reader.setOpt(LineReader.Option.MOUSE);
            if (mouse == 2) {
                reader.getWidgets().put(LineReader.CALLBACK_INIT, () -> {
                    terminal.trackMouse(Terminal.MouseTracking.Any);
                    return true;
                });
                reader.getWidgets().put(LineReader.MOUSE, () -> {
                    MouseEvent event = reader.readMouseEvent();
                    StringBuilder tsb = new StringBuilder();
                    Cursor cursor = terminal.getCursorPosition(c -> tsb.append((char) c));
                    reader.runMacro(tsb.toString());
                    String msg = "          " + event.toString();
                    int w = terminal.getWidth();
                    terminal.puts(Capability.cursor_address, 0, Math.max(0, w - msg.length()));
                    terminal.writer().append(msg);
                    terminal.puts(Capability.cursor_address, cursor.getY(), cursor.getX());
                    terminal.flush();
                    return true;
                });
            }
        }

        while (true) {
            String line = null;
            try {
                line = reader.readLine(_settings.prompt(), rightPrompt, (MaskingCallback) null, null);
            } catch (UserInterruptException e) {
                // Ignore
            } catch (EndOfFileException e) {
                return null;
            }
            if (line == null) {
                continue;
            }

            line = line.trim();

            if (color) {
                terminal.writer().println(
                        AttributedString.fromAnsi("\u001B[33m======>\u001B[0m\"" + line + "\"").toAnsi(terminal));

            } else {
                terminal.writer().println("======>\"" + line + "\"");
            }
            terminal.flush();

            // If we input the special word then we will mask
            // the next line.
            if ((trigger != null) && (line.compareTo(trigger) == 0)) {
                line = reader.readLine("password> ", mask);
            }
            if (line.equalsIgnoreCase("quit") || line.equalsIgnoreCase("exit")) {
                break;
            }
            ParsedLine pl = reader.getParser().parse(line, 0);
            if ("set".equals(pl.word())) {
                if (pl.words().size() == 3) {
                    reader.setVariable(pl.words().get(1), pl.words().get(2));
                }
            } else if ("tput".equals(pl.word())) {
                if (pl.words().size() == 2) {
                    Capability vcap = Capability.byName(pl.words().get(1));
                    if (vcap != null) {
                        terminal.puts(vcap);
                    } else {
                        terminal.writer().println("Unknown capability");
                    }
                }
            } else if ("testkey".equals(pl.word())) {
                terminal.writer().write("Input the key event(Enter to complete): ");
                terminal.writer().flush();
                StringBuilder sb = new StringBuilder();
                while (true) {
                    int c = ((LineReaderImpl) reader).readCharacter();
                    if (c == 10 || c == 13)
                        break;
                    sb.append(new String(Character.toChars(c)));
                }
                terminal.writer().println(KeyMap.display(sb.toString()));
                terminal.writer().flush();
            } else if ("bindkey".equals(pl.word())) {
                if (pl.words().size() == 1) {
                    StringBuilder sb = new StringBuilder();
                    Map<String, Binding> bound = reader.getKeys().getBoundKeys();
                    for (Map.Entry<String, Binding> entry : bound.entrySet()) {
                        sb.append("\"");
                        entry.getKey().chars().forEachOrdered(c -> {
                            if (c < 32) {
                                sb.append('^');
                                sb.append((char) (c + 'A' - 1));
                            } else {
                                sb.append((char) c);
                            }
                        });
                        sb.append("\" ");
                        if (entry.getValue() instanceof Macro) {
                            sb.append("\"");
                            ((Macro) entry.getValue()).getSequence().chars().forEachOrdered(c -> {
                                if (c < 32) {
                                    sb.append('^');
                                    sb.append((char) (c + 'A' - 1));
                                } else {
                                    sb.append((char) c);
                                }
                            });
                            sb.append("\"");
                        } else if (entry.getValue() instanceof Reference) {
                            sb.append(((Reference) entry.getValue()).name().toLowerCase().replace('_', '-'));
                        } else {
                            sb.append(entry.getValue().toString());
                        }
                        sb.append("\n");
                    }
                    terminal.writer().print(sb.toString());
                    terminal.flush();
                } else if (pl.words().size() == 3) {
                    reader.getKeys().bind(new Reference(pl.words().get(2)), KeyMap.translate(pl.words().get(1)));
                }
            } else if ("cls".equals(pl.word())) {
                terminal.puts(Capability.clear_screen);
                terminal.flush();
            } else if ("sleep".equals(pl.word())) {
                Thread.sleep(3000);
            }
        }
        return null;
    }

}

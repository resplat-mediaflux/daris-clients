package unimelb.daris.client.console;

public class DaRISConsoleSettings {

    public static final String DEFAULT_PROMPT = "DaRIS> ";

    private String _prompt = DEFAULT_PROMPT;

    public String prompt() {
        return _prompt;
    }

}

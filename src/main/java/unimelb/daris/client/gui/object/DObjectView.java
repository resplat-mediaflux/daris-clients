package unimelb.daris.client.gui.object;

import java.util.Objects;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.StackPane;
import unimelb.daris.client.model.object.DObject;
import unimelb.daris.client.model.object.DObjectRef;
import unimelb.mf.client.gui.components.InterfaceComponent;
import unimelb.mf.client.gui.xml.XmlTreeTableView;

public class DObjectView<T extends DObject> implements InterfaceComponent {

    protected DObjectRef object;

    private StackPane _stackPane;

    public DObjectView(DObjectRef o) {
        _stackPane = new StackPane();
        setObject(o);
    }

    public void setObject(DObjectRef o) {
        if (!Objects.equals(this.object, o)) {
            this.object = o;
        }
        updateGUI();
    }

    private void updateGUI() {
        _stackPane.getChildren().clear();
        if (this.object == null) {
            return;
        }
        _stackPane.getChildren().add(new Label("loading " + this.object.typeAndId() + "..."));
        this.object.resolve(() -> {
            T o = (T) this.object.referent();
            Platform.runLater(() -> {
                updateGUI(o);
            });
        });
    }

    private void updateGUI(T o) {
        TabPane tabs = new TabPane();
        tabs.getTabs().add(new Tab(o.typeAndId(), new XmlTreeTableView(o.xml())));
        _stackPane.getChildren().add(tabs);
    }

    @Override
    public Node gui() {
        return _stackPane;
    }

}

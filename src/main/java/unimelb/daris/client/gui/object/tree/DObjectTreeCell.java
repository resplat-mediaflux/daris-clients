package unimelb.daris.client.gui.object.tree;

import javafx.scene.Node;
import javafx.scene.control.TreeCell;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import unimelb.daris.client.model.dataset.Dataset;
import unimelb.daris.client.model.exmethod.ExMethod;
import unimelb.daris.client.model.object.DObject;
import unimelb.daris.client.model.object.DObjectRef;
import unimelb.daris.client.model.project.Project;
import unimelb.daris.client.model.study.Study;
import unimelb.daris.client.model.subject.Subject;

public class DObjectTreeCell extends TreeCell<DObjectRef> {
    public static final String STYLE_CLASS_DRAG_ENTERED = "tree-cell-graphic-drag-entered";

    DObjectTreeCell() {
        super();
        setOnDragOver(event -> {
            if (event.getGestureSource() != DObjectTreeCell.this && canDrop(event, object())) {
                event.acceptTransferModes(TransferMode.ANY);
            }
            event.consume();
        });
        setOnDragEntered(event -> {
            DObjectRef o = object();
            if (canDrop(event, o)) {
                o.resolve(() -> {
                    if (canDrop(o.referent())) {
                        DObjectTreeCell.this.getStyleClass().add(STYLE_CLASS_DRAG_ENTERED);
                    }
                });
            }
            event.consume();
        });
        setOnDragExited(event -> {
            if (canDrop(event, object())) {
                object().resolve(() -> {
                    if (canDrop(object().referent())) {
                        DObjectTreeCell.this.getStyleClass().remove(STYLE_CLASS_DRAG_ENTERED);
                    }
                });
            }
            event.consume();
        });
        setOnDragDropped(event -> {
            Dragboard db = event.getDragboard();
            if (db.hasFiles()) {
                // TODO
                // @formatter:off

//                final List<File> files = event.getDragboard().getFiles();
//                object().resolve(() -> {
//                    Platform.runLater(() -> {
//                        if (canDrop(object().referent())) {
//                            new UploadMenu(object(), files).show(DObjectTreeCell.this,
//                                    event.getScreenX(), event.getSceneY());
//                        }
//                    });
//                });
                // @formatter:on
            }
            event.setDropCompleted(db.hasFiles());
            event.consume();
        });
    }

    private DObjectRef object() {
        if (getTreeItem() == null) {
            return null;
        }
        return getTreeItem().getValue();
    }

    private static boolean canDrop(DragEvent event, DObjectRef o) {
        if (event == null || o == null) {
            return false;
        }
        if (!event.getDragboard().hasFiles()) {
            return false;
        }
        if (o.resolved() && o.isProject() && ((Project) o.referent()).numberOfMethods() != 1) {
            return false;
        }
        return o.isProject() || o.isSubject() || o.isExMethod() || o.isStudy() || o.isDataSet();
    }

    private static boolean canDrop(DObject o) {
        if (o == null) {
            return false;
        }
        if (o instanceof Project) {
            if (((Project) o).numberOfMethods() == 1) {
                return true;
            } else {
                return false;
            }
        } else if (o instanceof Subject) {
            return true;
        } else if (o instanceof ExMethod) {
            return true;
        } else if (o instanceof Study) {
            return true;
        } else if (o instanceof Dataset) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void updateItem(DObjectRef o, final boolean empty) {
        super.updateItem(o, empty);
        setText(null);
        if (empty) {
            setGraphic((Node) null);
        } else {
            setGraphic(getTreeItem().getGraphic());
        }
    }
}

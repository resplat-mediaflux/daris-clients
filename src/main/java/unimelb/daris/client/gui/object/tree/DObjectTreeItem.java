package unimelb.daris.client.gui.object.tree;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import unimelb.daris.client.model.CiteableIdUtils;
import unimelb.daris.client.model.object.DObjectChildrenRef;
import unimelb.daris.client.model.object.DObjectRef;
import unimelb.mf.client.gui.components.PagingControl;

public class DObjectTreeItem extends TreeItem<DObjectRef> {

    public static String ICON_FOLDER_OPEN = "/images/16folder_blue_open.png";
    public static String ICON_FOLDER = "/images/16folder_blue.png";
    public static String ICON_DATASET = "/images/16dataset_primary.png";
    public static String ICON_LOADING = "/images/16loading.gif";

    private DObjectChildrenRef _children;
    private HBox _graphic;
    private HBox _icon;
    private Label _label;
    private HBox _adornments;

    public DObjectTreeItem(DObjectRef o) {
        super(o);
        _children = new DObjectChildrenRef(o);
        _graphic = new HBox();
        _icon = new HBox(0.0);
        _icon.setPrefWidth(16.0);
        _icon.setPrefHeight(16.0);
        _graphic.getChildren().add(_icon);
        _label = new Label(labelTextFor(o));
        _graphic.getChildren().add(_label);
        _adornments = new HBox(0.0);
        _adornments.setPadding(new Insets(0, 0, 0, 3));
        updateAdornments();
        _graphic.getChildren().add(_adornments);
        setIcon(false);
        setGraphic(_graphic);
        expandedProperty().addListener((obs, ov, nv) -> {
            if (!nv.equals(ov)) {
                setIcon(nv);
            }
        });
        _children.totalProperty().addListener((obs, ov, nv) -> {
            Platform.runLater(() -> {
                updateAdornments();
            });
        });
        _children.idxProperty().addListener((obs, ov, nv) -> {

        });
    }

    private Label createAdornmentLabel(long nbc) {
        Label label = new Label();
        if (nbc > 0) {
            label.setText("[" + nbc + "]");
        }
        label.setFont(Font.font(11));
        label.setTextFill(Color.BLUE);
        return label;
    }

    private PagingControl createAdornmentPagingControl() {
        PagingControl pc = new PagingControl();
        pc.pageSizeProperty().bind(_children.pageSizeProperty());
        pc.fromProperty().bind(_children.fromProperty());
        pc.toProperty().bind(_children.toProperty());
        pc.totalProperty().bind(_children.totalProperty());
        return pc;
    }

    private void updateAdornments() {
        long total = _children.total();
        int pageSize = _children.pageSize();
        _adornments.getChildren().clear();
        if (total >= 0 && total <= pageSize) {
            if (total > 0) {
                _adornments.getChildren().add(createAdornmentLabel(total));
            }
        } else {
            PagingControl pc = createAdornmentPagingControl();
            pc.gotoProperty().addListener((obs, ov, nv) -> {
                _children.setIdx(nv.longValue());
                setExpanded(true);
                refresh(isExpanded());
            });
            _adornments.getChildren().add(pc.gui());
        }
    }

    @Override
    public ObservableList<TreeItem<DObjectRef>> getChildren() {
        if (!_children.resolved() && !_children.resolving()) {
            // @formatter:off
//            DObjectRef o = getValue();
//            System.out.println(o == null ? null : o.cid());
            // @formatter:on

            setIcon(ICON_LOADING);
            _children.resolve(() -> {
                List<DObjectRef> cos = _children.collection();
                List<TreeItem<DObjectRef>> items = new ArrayList<TreeItem<DObjectRef>>(cos == null ? 0 : cos.size());
                if (cos != null) {
                    for (DObjectRef co : cos) {
                        items.add(new DObjectTreeItem(co));
                    }
                }
                Platform.runLater(() -> {
                    super.getChildren().setAll(items);
                    setIcon(isExpanded());

                });
            });
        }
        return super.getChildren();
    }

    @Override
    public boolean isLeaf() {
        DObjectRef o = getValue();
        if (o == null) {
            return false;
        }
        return !o.hasChildren();
    }

    private void setIcon(boolean open) {
        if (open) {
            setIcon(openIconPathFor(getValue()));
        } else {
            setIcon(iconPathFor(getValue()));
        }
    }

    private void setIcon(String path) {
        if (path != null) {
            _icon.getChildren().clear();
            ImageView i = new ImageView(new Image(getClass().getResourceAsStream(path), 16.0, 16.0, false, false));
            i.setFitWidth(16.0);
            i.setFitHeight(16.0);
            _icon.getChildren().add(i);
        }
    }

    private static String iconPathFor(DObjectRef o) {
        if (o != null) {
            String cid = o.cid();
            if (CiteableIdUtils.isDataSetCID(cid)) {
                return ICON_DATASET;
            } else {
                return ICON_FOLDER;
            }
        } else {
            // root node
            return ICON_FOLDER;
        }
    }

    private static String openIconPathFor(DObjectRef o) {
        if (o != null) {
            String cid = o.cid();
            if (CiteableIdUtils.isDataSetCID(cid)) {
                return ICON_DATASET;
            } else {
                return ICON_FOLDER_OPEN;
            }
        } else {
            // root node
            return ICON_FOLDER_OPEN;
        }
    }

    private static String labelTextFor(DObjectRef object) {
        if (object == null) {
            return "DaRIS";
        }
        if (object.isProject()) {
            return object.cid() + (object.name() == null ? "" : (": " + object.name()));
        } else {
            return CiteableIdUtils.getLastPart(object.cid()) + (object.name() == null ? "" : (": " + object.name()));
        }
    }

    public void refresh(boolean refreshChildren) {
        DObjectRef o = getValue();
        if (o != null) {
            o.reset();
            o.resolve(() -> {
                Platform.runLater(() -> {
                    _label.setText(labelTextFor(o));
                });
            });
        }
        if (refreshChildren) {
            boolean expanded = isExpanded();
            if (expanded) {
                setExpanded(false);
            }
            _children.reset();
            if (expanded) {
                setExpanded(true);
            }
        }
    }

}

package unimelb.daris.client.gui.object.tree;

import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import unimelb.daris.client.model.object.DObjectRef;

public class DObjectTreeView extends TreeView<DObjectRef> {

    private static class RootItem extends DObjectTreeItem {

        private RootItem() {
            super(null);
            setExpanded(true);
        }

    }

    private ContextMenu _contextMenu;

    public DObjectTreeView() {
        super(new RootItem());
        setShowRoot(true);
        setCellFactory(tree -> {
            return new DObjectTreeCell();
        });
        getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
            if (nv != null) {
                DObjectRef o = nv.getValue();
                if ((o != null && !o.resolved())) {
                    ((DObjectTreeItem) nv).refresh(false);
                }
            }
        });
        getSelectionModel().select(getRoot());
        _contextMenu = new ContextMenu(new MenuItem());
        _contextMenu.setOnShowing(e -> {
            // TODO @formatter:off
//            DObjectRef o = DObjectTreeView.this.getSelectionModel().getSelectedItem().getValue();
//            _contextMenu.getItems().clear();
//            MenuItem refreshMenuItem = new MenuItem(DObjectMenu.menuItemTextFor("Refresh", o));
//            refreshMenuItem.setOnAction(event -> {
//                DObjectTreeItem item = findTreeItem(o);
//                if (item != null) {
//                    item.refresh(true);
//                }
//            });
//            _contextMenu.getItems().add(refreshMenuItem);
//            DObjectMenu.updateMenuItems(_contextMenu.getItems(), MenuUpdateAction.PREPEND, o);
            // @formatter:on
        });
        _contextMenu.setOnHidden(e -> {
            _contextMenu.getItems().setAll(new MenuItem());
        });
        setContextMenu(_contextMenu);
    }

    // TODO: use or remove
    DObjectTreeItem findTreeItem(DObjectRef o) {
        DObjectTreeItem root = (DObjectTreeItem) getRoot();
        if (root == null) {
            return null;
        }
        return findTreeItem(root, o);
    }

    private static DObjectTreeItem findTreeItem(DObjectTreeItem root, DObjectRef o) {
        DObjectRef rootObj = root.getValue();
        String rootCid = rootObj == null ? null : rootObj.cid();

        String cid = o.cid();
        if (cid == null) {
            if (rootCid == null) {
                return root;
            } else {
                return null;
            }
        }
        if (cid.equals(rootCid)) {
            return root;
        }
        if (rootCid == null || cid.startsWith(rootCid + ".")) {
            ObservableList<TreeItem<DObjectRef>> children = root.getChildren();
            if (!children.isEmpty()) {
                for (TreeItem<DObjectRef> child : children) {
                    DObjectTreeItem item = findTreeItem((DObjectTreeItem) child, o);
                    if (item != null) {
                        return item;
                    }
                }
            }
        }
        return null;
    }

}

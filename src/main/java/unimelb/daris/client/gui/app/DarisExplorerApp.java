package unimelb.daris.client.gui.app;

import javafx.scene.Scene;
import javafx.stage.Stage;
import unimelb.mf.client.gui.app.Application;

public class DarisExplorerApp extends Application {

    private static DarisExplorerApp _instance;

    public static DarisExplorerApp get() {
        if (_instance == null) {
            throw new IllegalStateException("App instance has not been set.");
        }
        return _instance;
    }

    public static final String NAME = "daris-navigator";
    public static final String DISPLAY_NAME = "DaRIS Navigator";

    private final String _css;

    public DarisExplorerApp() {
        super(NAME, DISPLAY_NAME, null, null);
        _css = this.getClass().getResource("/css/App.css").toExternalForm();
        if (_instance != null) {
            throw new IllegalStateException("App instance has already been set.");
        }
        _instance = this;
    }

    public String css() {
        return _css;
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        primaryStage.setOnCloseRequest(event -> {
            close();
        });
        primaryStage.setMaximized(true);
        primaryStage.setTitle(displayName());

        // Sign in first
        logon(() -> {
            // Display main window after logged in.
            MainWindow main = new MainWindow();
            Scene scene = main.scene();
            scene.getStylesheets().add(css());
            primaryStage.setScene(scene);
            primaryStage.show();
        });
    }

}

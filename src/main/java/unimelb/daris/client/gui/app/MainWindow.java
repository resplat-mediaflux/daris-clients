package unimelb.daris.client.gui.app;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import unimelb.daris.client.gui.object.DObjectView;
import unimelb.daris.client.gui.object.tree.DObjectTreeView;
import unimelb.utils.OSUtils;

public class MainWindow {

    private Scene _scene;
    private BorderPane _layout;
    private MenuBar _menuBar;
    private DObjectTreeView _treeNav;
    private DObjectView _dv;
    private Menu _darisMenu;

    public MainWindow() {

        _layout = new BorderPane();

        _menuBar = new MenuBar();
        if (OSUtils.IS_MAC_OS) {
            _menuBar.useSystemMenuBarProperty().set(true);
        }
        _layout.setTop(_menuBar);

        // menu
        initMenus(_menuBar);

        // tree view
        StackPane navStackPane = new StackPane();
        _treeNav = new DObjectTreeView();
        navStackPane.getChildren().add(_treeNav);

        // detail view
        _dv = new DObjectView(null);

        _treeNav.getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
            _dv.setObject(nv.getValue());
        });

        SplitPane splitPane = new SplitPane();
        splitPane.setOrientation(Orientation.HORIZONTAL);
        splitPane.setDividerPositions(0.3f);
        splitPane.getItems().setAll(navStackPane, _dv.gui());

        _layout.setCenter(splitPane);

        _scene = new Scene(_layout, 1280.0, 800.0, Color.WHITE);
    }

    private void initMenus(MenuBar menuBar) {
        _darisMenu = new Menu("DaRIS");
        menuBar.getMenus().add(_darisMenu);
        initDarisMenu(_darisMenu);
    }

    private void initDarisMenu(Menu menu) {
        // DaRIS -> About
        MenuItem aboutItem = new MenuItem("About DaRIS");
        aboutItem.setOnAction(event -> {
            // TODO
            // new AboutDialog().show();
        });
        menu.getItems().add(aboutItem);

        // DaRIS -> ---
        menu.getItems().add(new SeparatorMenuItem());

        // DaRIS -> Preferences
        MenuItem preferencesItem = new MenuItem("Preferences");
        preferencesItem.setOnAction(event -> {
            // TODO
        });
        menu.getItems().add(preferencesItem);

        // DaRIS -> ---
        menu.getItems().add(new SeparatorMenuItem());

        // DaRIS -> Exit
        MenuItem exitItem = new MenuItem("Exit");
        exitItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });
        if (OSUtils.isMacOS()) {
            exitItem.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.META_DOWN));
        } else {
            exitItem.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN));
        }
        menu.getItems().add(exitItem);
    }

    public Scene scene() {
        return _scene;
    }

}

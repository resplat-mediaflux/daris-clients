package unimelb.daris.client.dicom;

import java.io.OutputStream;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import arc.archive.ArchiveOutput;
import arc.archive.ArchiveRegistry;
import arc.mf.client.ServerClient;
import arc.mf.client.archive.Archive;
import arc.streams.StreamCopy.AbortCheck;
import arc.xml.XmlStringWriter;
import unimelb.daris.dicom.data.DicomFileInfo;
import unimelb.daris.dicom.data.DicomFiles;
import unimelb.io.CollectionTransferProgressListener;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.MFClientTask;

public class DicomIngestTask implements MFClientTask<Void> {

    public static final Logger logger = LoggerFactory.getLogger(DicomIngestTask.class);

    private MFSession _session;
    private DicomIngestSettings _settings;
    private CollectionTransferProgressListener _pl;
    private String _cid;
    private Path[] _inputs;

    public DicomIngestTask(MFSession session, DicomIngestSettings settings, CollectionTransferProgressListener pl,
            String cid, Path... inputs) {
        _session = session;
        _settings = settings;
        _pl = pl;
        _cid = cid;
        _inputs = inputs;
    }

    @Override
    public MFSession session() {
        return _session;
    }

    @Override
    public Void execute() throws Throwable {

        if (_inputs == null || _inputs.length == 0) {
            return null;
        }

        XmlStringWriter w = new XmlStringWriter();
        if (_cid != null) {
            _settings.setCID(_cid);
        }
        _settings.save(w);
        Archive.declareSupportForAllTypes();
        DicomFiles dcmFiles = DicomFiles.scan(_inputs);
        if (dcmFiles.isEmpty()) {
            return null;
        }
        if (_pl != null) {
            _pl.beginCollectionTransfer(dcmFiles.size(), -1);
        }
        ServerClient.Input sci = new ServerClient.GeneratedInput(_settings.archiveFileMimeType(),
                _settings.archiveFileExtension(), _inputs[0].toAbsolutePath().toString(), -1, null) {
            @Override
            protected void copyTo(OutputStream os, AbortCheck ac) throws Throwable {
                ArchiveOutput ao = ArchiveRegistry.createOutput(os, _settings.archiveFileMimeType(), 6, null);
                try {
                    int i = 0;
                    int total = dcmFiles.size();
                    for (DicomFileInfo dcmFile : dcmFiles) {
                        String activity = String.format("Sending %d/%d file: %s", i + 1, total,
                                dcmFile.path.toString());
                        logger.info(activity);
                        if (_pl != null) {
                            _pl.inform(activity);
                        }
                        ao.add("application/dicom", String.format("%08d_%s.dcm", i + 1, dcmFile.sopInstanceUID),
                                dcmFile.path.toFile());
                        if (_pl != null) {
                            _pl.incTransferredFiles(1);
                        }
                        i++;
                    }
                    if (_pl != null) {
                        _pl.endCollectionTransfer();
                    }
                } finally {
                    ao.close();
                }
            }
        };
        _session.execute("dicom.ingest", w.document(), sci);
        return null;
    }

}

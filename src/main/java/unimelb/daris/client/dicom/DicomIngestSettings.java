package unimelb.daris.client.dicom;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import arc.xml.XmlWriter;
import unimelb.mf.archive.MFArchive;

public class DicomIngestSettings {

    public static final String DEFAULT_ENGINE = "nig.dicom";
    public static final String DEFAULT_ID_BY = "study.id,patient.name.first,patient.name,referring.physician.name";
    public static final String DEFAULT_SUBJECT_NAME_FROM = "patient.id";
    public static final String DEFAULT_SUBJECT_FIND_METHOD = "id";
    public static final String ARG_CID = "nig.dicom.id.citable";
    public static final String ARG_ID_BY = "nig.dicom.id.by";
    public static final String ARG_SUBJECT_NAME_FROM = "nig.dicom.subject.name.from";
    public static final String ARG_SUBJECT_FIND_METHOD = "nig.dicom.subject.find.method";
    public static final String ARG_SUBJECT_CREATE = "nig.dicom.subject.create";
    public static final String ARG_IGNORE_NON_DIGITS = "nig.dicom.id.ignore-non-digits";
    public static final String ARG_WRITE_DICOM_PATIENT = "nig.dicom.write.mf-dicom-patient";
    public static final String ARG_ENCRYPT_DICOM_PATIENT = "nig.dicom.use.encrypted.patient";
    public static final String DEFAULT_ARCHIVE_MIME_TYPE = MFArchive.TYPE_AAR;

    public static class Builder {
        private String _engine = DEFAULT_ENGINE;
        private boolean _anonymize = false;
        private Map<String, String> _args;
        private boolean _wait = true;
        private String _archiveMimeType = DEFAULT_ARCHIVE_MIME_TYPE;

        public Builder() {
            _args = new LinkedHashMap<String, String>();

            setIdBy(DEFAULT_ID_BY);
            setSubjectNameFrom(DEFAULT_SUBJECT_NAME_FROM);
            setSubjectFindMethod(DEFAULT_SUBJECT_FIND_METHOD);
            setSubjectCreate(true);
            setIgnoreNonDigits(true);
            setWriteDicomPatient(true);
        }

        public Builder setSubjectFindMethod(String subjectFindMethod) {
            _args.put(ARG_SUBJECT_FIND_METHOD, subjectFindMethod);
            return this;
        }

        public Builder setSubjectNameFrom(String subjectNameFrom) {
            _args.put(ARG_SUBJECT_NAME_FROM, subjectNameFrom);
            return this;
        }

        public Builder setIdBy(String idBy) {
            _args.put(ARG_ID_BY, idBy);
            return this;
        }

        public Builder setEngine(String engine) {
            _engine = engine;
            return this;
        }

        public Builder setAnonymize(boolean anonymize) {
            _anonymize = anonymize;
            return this;
        }

        public Builder setArg(String name, String value) {
            _args.put(name, value);
            return this;
        }

        public Builder setWait(boolean wait) {
            _wait = wait;
            return this;
        }

        public Builder setArchiveFileMimeType(String mimeType) {
            _archiveMimeType = mimeType;
            return this;
        }

        public Builder setArchiveFormat(String format) {
            MFArchive.Type t = MFArchive.Type.fromFileExtension(format);
            if (t == null) {
                throw new IllegalArgumentException("Unknown archive format: " + format);
            }
            setArchiveFileMimeType(t.mimeType);
            return this;
        }

        public Builder setIgnoreNonDigits(boolean ignoreNonDigits) {
            setArg(ARG_IGNORE_NON_DIGITS, Boolean.toString(ignoreNonDigits));
            return this;
        }

        public Builder setSubjectCreate(boolean subjectCreate) {
            setArg(ARG_SUBJECT_CREATE, Boolean.toString(subjectCreate));
            return this;
        }

        public Builder setWriteDicomPatient(boolean writeDicomPatient) {
            setArg(ARG_WRITE_DICOM_PATIENT, Boolean.toString(writeDicomPatient));
            return this;
        }

        public Builder setEncryptDicomPatient(boolean encryptDicomPatient) {
            setArg(ARG_ENCRYPT_DICOM_PATIENT, Boolean.toString(encryptDicomPatient));
            return this;
        }

        public Builder setCID(String cid) {
            setArg(ARG_CID, cid);
            return this;
        }

        public DicomIngestSettings build() {
            return new DicomIngestSettings(_engine, _anonymize, _args, _wait, _archiveMimeType);
        }

    }

    private final String _engine;
    private final boolean _anonymize;
    private final Map<String, String> _args;
    private final boolean _wait;
    private final String _mimeType;

    public DicomIngestSettings(String engine, boolean anonymize, Map<String, String> args, boolean wait,
            String mimeType) {
        _engine = engine;
        _anonymize = anonymize;
        _args = args == null ? new LinkedHashMap<String, String>() : new LinkedHashMap<String, String>(args);
        _wait = wait;
        _mimeType = mimeType;
    }

    public final String engine() {
        return _engine;
    }

    public final boolean anonymize() {
        return _anonymize;
    }

    public final Map<String, String> args() {
        return Collections.unmodifiableMap(_args);
    }

    public final String cid() {
        return _args.get(ARG_CID);
    }

    public final void setCID(String cid) {
        _args.put(ARG_CID, cid);
    }

    public final boolean await() {
        return _wait;
    }

    public final String archiveFileMimeType() {
        return _mimeType;
    }

    public final String archiveFileExtension() {
        if (archiveFileMimeType() != null) {
            MFArchive.Type atype = MFArchive.Type.fromMimeType(archiveFileMimeType());
            if (atype != null) {
                return atype.extension;
            }
        }
        return null;
    }

    public void save(String cid, XmlWriter w) throws Throwable {
        setCID(cid);
        save(w);
    }

    public void save(XmlWriter w) throws Throwable {
        w.add("engine", "nig.dicom");
        w.add("anonymize", _anonymize);
        Set<String> argNames = _args.keySet();
        for (String argName : argNames) {
            String argValue = _args.get(argName);
            w.add("arg", new String[] { "name", argName }, argValue);
        }
        w.add("wait", _wait);
        w.add("type", _mimeType);
    }

}

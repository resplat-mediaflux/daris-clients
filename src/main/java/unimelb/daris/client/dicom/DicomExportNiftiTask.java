package unimelb.daris.client.dicom;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import arc.archive.ArchiveInput;
import arc.archive.ArchiveRegistry;
import arc.mf.client.ServerClient.OutputConsumer;
import arc.mf.client.archive.Archive;
import arc.mime.NamedMimeType;
import arc.streams.LongInputStream;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlStringWriter;
import unimelb.daris.client.model.CiteableIdUtils;
import unimelb.daris.client.model.dataset.DicomDataset;
import unimelb.io.StreamCopy;
import unimelb.mf.archive.MFArchive;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.MFClientTask;
import unimelb.mf.client.util.AssetUtils;

public class DicomExportNiftiTask implements MFClientTask<Void> {

    public static final Logger logger = LoggerFactory.getLogger(DicomExportNiftiTask.class);

    public static class Settings {

        private String _filterQuery;
        private Path _outputDir;
        private String _namePattern;
        private boolean _gzip;
        private boolean _includeJson;
        private boolean _overwrite;

        public final boolean gzip() {
            return _gzip;
        }

        public final void setGzip(boolean gzip) {
            this._gzip = gzip;
        }

        public final Path outputDirectory() {
            return _outputDir;
        }

        public final void setOutputDiretory(Path outputDir) {
            this._outputDir = outputDir;
        }

        public final String namePattern() {
            return _namePattern;
        }

        public final void setNamePattern(String namePattern) {
            this._namePattern = namePattern;
        }

        public final String filterQuery() {
            return _filterQuery;
        }

        public final void setFilterQuery(String filterQuery) {
            _filterQuery = filterQuery;
        }

        public final boolean includeJson() {
            return _includeJson;
        }

        public final void setIncludeJson(boolean includeJson) {
            _includeJson = includeJson;
        }

        public final boolean overwrite() {
            return _overwrite;
        }

        public final void setOverwrite(boolean overwrite) {
            _overwrite = overwrite;
        }
    }

    private MFSession _session;
    private Set<String> _cids;
    private Settings _settings;

    public DicomExportNiftiTask(MFSession session, Collection<String> cids, Settings settings) {
        _session = session;
        _cids = new LinkedHashSet<>(cids);
        _settings = settings;
    }

    @Override
    public MFSession session() {
        return _session;
    }

    @Override
    public Void execute() throws Throwable {
        export(_session, _cids, _settings);
        return null;
    }

    private static void export(MFSession session, Collection<String> cids, Settings settings) throws Throwable {
        Archive.declareSupportForAllTypes();
        for (String cid : cids) {
            logger.info("processing " + CiteableIdUtils.getTypeFromCID(cid) + " " + cid + "...");
            export(session, cid, settings);
        }
    }

    private static void export(MFSession session, String cid, Settings settings) throws Throwable {
        StringBuilder sb = new StringBuilder();
        sb.append("cid='" + cid + "' or cid starts with '" + cid + "' and type='dicom/series'");
        if (settings.filterQuery() != null) {
            sb.append(" and (").append(settings.filterQuery()).append(")");
        }
        XmlStringWriter w = new XmlStringWriter();
        w.add("where", sb.toString());
        w.add("where", "asset has content");
        w.add("as", "iterator");
        w.add("action", "get-meta");
        long iteratorId = session.execute("asset.query", w.document()).longValue("iterator");
        try {
            boolean complete = false;
            while (!complete) {
                XmlDoc.Element re = session.execute("asset.query.iterate", "<id>" + iteratorId + "</id>");
                List<XmlDoc.Element> aes = re.elements("asset");
                if (aes != null) {
                    for (XmlDoc.Element ae : aes) {
                        exportDicomDataset(session, ae, settings);
                    }
                }
                complete = re.booleanValue("iterated/@complete");
            }
        } finally {
            session.execute("asset.query.iterator.destroy",
                    "<id>" + iteratorId + "</id><ignore-missing>true</ignore-missing>");
        }
    }

    private static void exportDicomDataset(MFSession session, XmlDoc.Element ae, Settings settings) throws Throwable {
        String assetId = ae.value("@id");
        String cid = ae.value("cid");
        logger.info("exporting dataset " + cid + "...");
        String fileName = generateOutputFileName(session, ae, settings.namePattern());
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", assetId);
        w.push("transcode");
        w.add("from", "dicom/series");
        w.add("to", "nifti/series");
        w.pop();
        session.execute("asset.transcode", w.document(), null, new OutputConsumer() {

            @Override
            protected void consume(Element e, LongInputStream in) throws Throwable {
                ArchiveInput ai = ArchiveRegistry.createInput(in, new NamedMimeType(MFArchive.TYPE_ZIP));
                try {
                    int nbJson = 0;
                    int nbNii = 0;
                    ArchiveInput.Entry entry = null;
                    while ((entry = ai.next()) != null) {

                        logger.info("processing entry: " + entry.name());
                        if (entry.isDirectory()) {
                            // ignore directories
                            continue;
                        }

                        String ename = entry.name().toLowerCase();
                        if (settings.includeJson()) {
                            if (!ename.endsWith(".nii") && !ename.endsWith(".nii.gz") && !ename.endsWith(".json")) {
                                continue;
                            }
                        } else {
                            if (!ename.endsWith(".nii") && !ename.endsWith(".nii.gz")) {
                                continue;
                            }
                        }

                        int idx = ename.lastIndexOf('/');
                        if (idx >= 0) {
                            // ignore parents
                            ename = ename.substring(idx + 1);
                        }

                        Path outputPath = null;
                        if (ename.endsWith(".json")) {
                            if (nbJson == 0) {
                                outputPath = Paths.get(settings.outputDirectory().toString(), fileName + ".json");
                            } else {
                                outputPath = Paths.get(settings.outputDirectory().toString(),
                                        fileName + "_" + nbJson + ".json");
                            }
                            if (!Files.exists(outputPath) || settings.overwrite()) {
                                if (!Files.exists(outputPath.getParent())) {
                                    Files.createDirectories(outputPath.getParent());
                                }
                                System.out.printf("Writing to file: '%s'%n", outputPath);
                                StreamCopy.copy(entry.stream(), outputPath, null, null);
                            } else {
                                System.out.printf("File: '%s' already exists. Skipped!%n", outputPath);
                            }
                            nbJson += 1;
                        } else {
                            String ext = settings.gzip() ? ".nii.gz" : ".nii";
                            if (nbNii == 0) {
                                outputPath = Paths.get(settings.outputDirectory().toString(), fileName + ext);
                            } else {
                                outputPath = Paths.get(settings.outputDirectory().toString(),
                                        fileName + "_" + nbNii + ext);
                            }
                            if (!Files.exists(outputPath) || settings.overwrite()) {
                                if (!Files.exists(outputPath.getParent())) {
                                    Files.createDirectories(outputPath.getParent());
                                }

                                if (ename.endsWith(".nii")) {
                                    if (settings.gzip()) {
                                        try (GZIPOutputStream gos = new GZIPOutputStream(
                                                Files.newOutputStream(outputPath))) {
                                            System.out.printf("Writing to file: '%s'%n", outputPath);
                                            StreamCopy.copy(entry.stream(), gos, null, null);
                                        }
                                    } else {
                                        System.out.printf("Writing to file: '%s'%n", outputPath);
                                        StreamCopy.copy(entry.stream(), outputPath, null, null);
                                    }
                                } else {
                                    // ename.endsWith(".nii.gz")
                                    if (settings.gzip()) {
                                        System.out.printf("Writing to file: '%s'%n", outputPath);
                                        StreamCopy.copy(entry.stream(), outputPath, null, null);
                                    } else {
                                        System.out.printf("Writing to file: '%s'%n'", outputPath);
                                        StreamCopy.copy(new GZIPInputStream(entry.stream()), outputPath, null, null);
                                    }
                                }
                            } else {
                                System.out.printf("File: '%s' already exists. Skipped!", outputPath);
                            }
                            nbNii += 1;
                        }
                    }
                } finally {
                    ai.close();
                }
            }
        });
    }

    private static String generateOutputFileName(MFSession session, XmlDoc.Element ae, String namePattern)
            throws Throwable {
        String assetId = ae.value("@id");
        String cid = ae.value("cid");
        if (namePattern == null) {
            String objName = ae.value("meta/daris:pssd-object/name");
            if (cid == null) {
                return "asset_" + assetId;
            }
            if (objName == null || objName.trim().isEmpty()) {
                return cid;
            }
            return cid + "_" + objName.replace('/', '_').replaceAll("\\ +", "_");
        } else {
            String name = NamePattern.eval(namePattern, session, ae);
            if (name != null && !name.isEmpty()) {
                if (name.toLowerCase().endsWith(".nii")) {
                    name = name.substring(0, name.length() - 4);
                } else if (name.toLowerCase().endsWith(".nii.gz")) {
                    name = name.substring(0, name.length() - 7);
                }
                logger.info("Generated output file name: " + name);
                return name;
            }
            logger.warn("Failed to resolve name pattern: '" + namePattern + "'. Fall back to cid: " + cid);
            return cid;
        }
    }

    /**
     * 
     * 1. Asset meta data value: {XPATH,PARENT_LEVELS}
     * 
     * Examples:
     * 
     * {cid}
     * 
     * {meta/daris:pssd-object/name}
     * 
     * {meta/mf-dicom-study/uid, 1}
     * 
     * 2. DICOM element value: [GGGG,EEEE]
     * 
     * Examples:
     * 
     * [0010,0020]
     * 
     */

    static class NamePattern {

        public static final String REGEX_XPATH_VALUE = "\\{([\\w-:.@]+(\\/[\\w-:.@]+)*){1}(\\ *,\\ *([1234]{1}))?\\}";
        public static final String REGEX_DICOM_VALUE = "\\[(\\d{4})\\ *,\\ *(\\d{4})\\]";

        public static String eval(String s, MFSession session, XmlDoc.Element ae) throws Throwable {
            if (s.matches(".*" + REGEX_XPATH_VALUE + ".*")) {
                s = evalXPathValues(s, session, ae);
            }
            if (s.matches(".*" + REGEX_DICOM_VALUE + ".*")) {
                s = evalDicomValues(s, session, ae);
            }
            return s;
        }

        private static String evalXPathValues(String s, MFSession session, XmlDoc.Element datasetAE) throws Throwable {
            String datasetCID = datasetAE.value("cid");
            Map<String, XmlDoc.Element> aes = new LinkedHashMap<>();
            if (datasetCID != null) {
                aes.put(datasetCID, datasetAE);
            }
            Pattern pattern = Pattern.compile(REGEX_XPATH_VALUE);
            Matcher matcher = pattern.matcher(s);
            Map<String, String> matchValues = new LinkedHashMap<>();
            while (matcher.find()) {
                String match = matcher.group();
                if (!matchValues.containsKey(match)) {
                    String xpath = matcher.group(1);
                    String level = matcher.group(4);
                    String cid = level == null ? datasetCID
                            : CiteableIdUtils.getParentCID(datasetCID, Integer.parseInt(level));
                    if (cid == null) {
                        matchValues.put(match, "");
                    } else if (cid.equals(datasetCID)) {
                        matchValues.put(match, datasetAE.stringValue(xpath, ""));
                    } else {
                        XmlDoc.Element ae = null;
                        if (aes.containsKey(cid)) {
                            ae = aes.get(cid);
                        } else {
                            ae = AssetUtils.getAssetMetaByCid(session, cid);
                            aes.put(cid, ae);
                        }
                        matchValues.put(match, ae.stringValue(xpath, ""));
                    }
                }
            }

            Set<String> matches = matchValues.keySet();
            for (String match : matches) {
                String value = matchValues.get(match);
                s = s.replace(match, safeValue(value));
            }
            return s;
        }

        private static String evalDicomValues(String s, MFSession session, XmlDoc.Element ae) throws Throwable {
            String assetId = ae.value("@id");
            XmlDoc.Element dicomMetaData = DicomDataset.getDicomMetaData(session, assetId);
            Pattern pattern = Pattern.compile(REGEX_DICOM_VALUE);
            Matcher matcher = pattern.matcher(s);
            Map<String, String> matchValues = new LinkedHashMap<>();
            while (matcher.find()) {
                String match = matcher.group();
                if (!matchValues.containsKey(match)) {
                    String gggg = matcher.group(1);
                    String eeee = matcher.group(2);
                    String tag = gggg + eeee;
                    String value = DicomDataset.getDicomElementValue(dicomMetaData, tag);
                    matchValues.put(match, value == null ? "" : value);
                }
            }
            Set<String> matches = matchValues.keySet();
            for (String match : matches) {
                String value = matchValues.get(match);
                s = s.replace(match, safeValue(value));
            }
            return s;
        }

        private static String safeValue(String value) {
            return value.trim().replaceAll("\\ *,\\ *", ",").replaceAll("\\ *;\\ *", ";")
                    .replaceAll("[\\\\/:*?\"<>| ]", "_").replaceAll("_{2,}", "_");
        }

    }

}

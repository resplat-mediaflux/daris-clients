package unimelb.daris.client.cli;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import ch.qos.logback.classic.Level;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IExecutionExceptionHandler;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParseResult;
import unimelb.daris.client.dicom.DicomExportNiftiTask;
import unimelb.daris.client.model.CiteableIdUtils;
import unimelb.logging.Logback;
import unimelb.mf.client.cli.MFCommand;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.picocli.converters.PathConverter;

@Command(name = "daris-dicom-export-nifti", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class DicomExportNiftiCommand implements MFCommand {

    public static final String APPLICATION_NAME = "daris-dicom-export-nifti";

    @Option(names = { "-c",
            "--mf.config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux/DaRIS server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCFG;

    @Option(names = { "-f",
            "--filter" }, paramLabel = "<query>", description = "A query to select DICOM datasets.", required = false)
    private String filter;

    @Option(names = { "-d",
            "--dir" }, paramLabel = "<output-directory>", description = "The output directory. Defaults to current working directory.", required = false)
    private Path dir;

    @Option(names = { "-n",
            "--name" }, paramLabel = "<output-file-name-pattern>", description = "The output filename pattern. If not specified, defaults to the dataset cid.", required = false)
    private String name;

    @Option(names = { "-o", "--overwrite" }, description = "Overwrite files already exist.", required = false)
    private boolean overwrite = false;

    @Option(names = { "-g", "--gzip" }, description = "GZip the output NIFTI files (to *.nii.gz).", required = false)
    private boolean gzip = false;

    @Option(names = { "-j",
            "--json" }, description = "Include the JSON file describes the NIFTI image.", required = false)
    private boolean json = false;

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "output usage information")
    private boolean printHelp;

    @Parameters(description = "The cid of the DICOM dataset or the parent project/subject/study.", arity = "1..", index = "0", paramLabel = "CID")
    private String[] cids;

    @Override
    public MFSession authenticate() throws Throwable {
        MFConfigBuilder mfcb = new MFConfigBuilder();
        mfcb.setApp(DicomIngestCommand.APPLICATION_NAME);
        if (this.mfCFG != null) {
            mfcb.loadFromConfigFile(mfCFG.toFile());
        } else {
            mfcb.findAndLoadFromConfigFile();
        }
        mfcb.setConsoleLogon(true);
        return MFSession.create(mfcb);
    }

    @Override
    public void execute(MFSession session) throws Throwable {

        DicomExportNiftiTask.Settings settings = new DicomExportNiftiTask.Settings();
        settings.setFilterQuery(this.filter);
        settings.setNamePattern(this.name);
        settings.setOverwrite(this.overwrite);
        settings.setGzip(this.gzip);
        settings.setIncludeJson(this.json);

        if (this.dir == null) {
            this.dir = Paths.get(System.getProperty("user.dir"));
        }
        if (Files.isDirectory(this.dir)) {
            settings.setOutputDiretory(this.dir);
        } else {
            throw new IllegalArgumentException(
                    "Output directory: '" + this.dir + "' does not exist or it is not a directory.");
        }

        for (String cid : cids) {
            if (!CiteableIdUtils.isCID(cid) || CiteableIdUtils.getDepth(cid) < CiteableIdUtils.PROJECT_ID_DEPTH) {
                throw new IllegalArgumentException(cid + " is not a valid id.");
            }
        }

        DicomExportNiftiTask exportNifti = new DicomExportNiftiTask(session, Arrays.asList(cids), settings);
        exportNifti.execute();
    }

    public static void main(String[] args) {

        Logback.setRootLogLevel(Level.WARN);

        CommandLine cl = new CommandLine(new DicomExportNiftiCommand());
        cl.setExecutionExceptionHandler(new IExecutionExceptionHandler() {
            @Override
            public int handleExecutionException(Exception ex, CommandLine commandLine, ParseResult parseResult)
                    throws Exception {
                if (ex instanceof IllegalArgumentException) {
                    System.err.println("Error: " + ex.getMessage());
                    cl.usage(System.err);
                    return 1;
                } else {
                    throw ex;
                }
            }
        });
        System.exit(cl.execute(args));
    }
}
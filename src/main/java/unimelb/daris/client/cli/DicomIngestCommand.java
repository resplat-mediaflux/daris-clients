package unimelb.daris.client.cli;

import java.nio.file.Path;

import ch.qos.logback.classic.Level;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import unimelb.daris.client.dicom.DicomIngestSettings;
import unimelb.daris.client.dicom.DicomIngestTask;
import unimelb.io.AbstractCollectionTransferProgressListener;
import unimelb.logging.Logback;
import unimelb.mf.client.cli.MFCommand;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.picocli.converters.PathConverter;

@Command(name = "daris-dicom-ingest", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class DicomIngestCommand implements MFCommand {

    public static final String APPLICATION_NAME = "daris-dicom-ingest";

    @Option(names = {
            "--mf.config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux/DaRIS server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCFG;

    @Option(names = { "--anonymize" }, description = "Anonymize patient name attribute", required = false)
    private boolean anonymize = false;

    @Option(names = {
            "--cid" }, paramLabel = "<cid>", description = "The destination DaRIS object cid.", required = true)
    private String cid;

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "output usage information")
    private boolean printHelp;

    @Parameters(description = "Input dicom files or directories.", arity = "1..", index = "0", paramLabel = "DICOM_FILES", converter = PathConverter.MustExist.class)
    private Path[] inputs;

    @Override
    public MFSession authenticate() throws Throwable {
        MFConfigBuilder mfcb = new MFConfigBuilder();
        mfcb.setApp(DicomIngestCommand.APPLICATION_NAME);
        if (this.mfCFG != null) {
            mfcb.loadFromConfigFile(mfCFG.toFile());
        } else {
            mfcb.findAndLoadFromConfigFile();
        }
        mfcb.setConsoleLogon(true);
        return MFSession.create(mfcb);
    }

    @Override
    public void execute(MFSession session) throws Throwable {
        DicomIngestSettings.Builder disb = new DicomIngestSettings.Builder();
        disb.setAnonymize(this.anonymize);
        disb.setCID(this.cid);
        DicomIngestSettings settings = disb.build();

        AbstractCollectionTransferProgressListener pl = new AbstractCollectionTransferProgressListener() {

            @Override
            public void beginCollectionTransfer(long totalFiles, long totalBytes) {
                super.beginCollectionTransfer(totalFiles, totalBytes);
                if (totalFiles > 0) {
                    System.out.println("Importing of " + totalFiles + " dicom files...");
                }
            }

            @Override
            public void incTransferredFiles(long nbFiles) {
                super.incTransferredFiles(nbFiles);
                if (totalFiles() > 0 && transferredFiles() > 0) {
                    System.out.println(String.format("Sent %d/%d files.", transferredFiles(), totalFiles()));
                }

            }

            @Override
            public void inform(String message) {
                System.out.println(message);
            }

            @Override
            public void endCollectionTransfer() {
                System.out.println("Completed!");
            }
        };
        DicomIngestTask dicomIngest = new DicomIngestTask(session, settings, pl, this.cid, this.inputs);
        dicomIngest.execute();
    }

    public static void main(String[] args) {

        Logback.setRootLogLevel(Level.WARN);
        
        System.exit(new CommandLine(new DicomIngestCommand()).execute(args));
    }
}

package unimelb.daris.client.cli;

import java.nio.file.Path;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import unimelb.mf.client.cli.MFCommand;
import unimelb.mf.client.session.MFSession;
import unimelb.picocli.converters.PathConverter;

@Command(name = "daris-dataset-search", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class DatasetSearchCommand implements MFCommand {

    @Option(names = {
            "--mf.config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux/DaRIS server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCFG;

    @Override
    public MFSession authenticate() throws Throwable {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void execute(MFSession session) throws Throwable {
        // TODO Auto-generated method stub

    }

}

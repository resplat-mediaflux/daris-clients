package unimelb.daris.client.cli;

import java.nio.file.Path;

import ch.qos.logback.classic.Level;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IExecutionExceptionHandler;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParseResult;
import picocli.CommandLine.Spec;
import unimelb.daris.client.model.subject.Subject;
import unimelb.logging.Logback;
import unimelb.mf.client.cli.MFCommand;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.picocli.converters.PathConverter;

@Command(name = "daris-subject-update", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class SubjectUpdateCommand implements MFCommand {

    @Option(names = {
            "--mf.config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux/DaRIS server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCFG;

    @Option(names = { "-i",
            "--id" }, paramLabel = "<subject-id>", description = "The identifier of the subject.", required = true, arity = "1")
    private String subjectCID;

    @Option(names = { "-d",
            "--description" }, paramLabel = "<description>", description = "Description about the subject.", required = false, arity = "1")
    private String description;

    @Parameters(description = "The name of the subject.", arity = "1", index = "0", paramLabel = "NAME")
    private String name;

    @Spec
    private CommandSpec spec;

    @Override
    public MFSession authenticate() throws Throwable {
        MFConfigBuilder mfcb = new MFConfigBuilder();
        mfcb.setApp(Application.DEFAULT_APPLICATION_NAME);
        if (this.mfCFG != null) {
            mfcb.loadFromConfigFile(mfCFG.toFile());
        } else {
            mfcb.findAndLoadFromConfigFile();
        }
        mfcb.setConsoleLogon(true);
        return MFSession.create(mfcb);
    }

    @Override
    public void execute(MFSession session) throws Throwable {
        validate(session);
        Subject.update(session, this.subjectCID, this.name, this.description);
    }

    private void validate(MFSession session) throws Throwable {
        if (!Subject.exists(session, this.subjectCID)) {
            throw new IllegalArgumentException("Subject " + this.subjectCID + " does not exist.");
        }
        if (!Subject.isSubject(session, this.subjectCID)) {
            throw new IllegalArgumentException("Asset " + this.subjectCID + " is not a valid subject.");
        }
    }

    public static void main(String[] args) {

        Logback.setRootLogLevel(Level.WARN);

        CommandLine cl = new CommandLine(new SubjectUpdateCommand());
        cl.setExecutionExceptionHandler(new IExecutionExceptionHandler() {
            @Override
            public int handleExecutionException(Exception ex, CommandLine commandLine, ParseResult parseResult)
                    throws Exception {
                if (ex instanceof IllegalArgumentException) {
                    ex.printStackTrace();
                    cl.usage(System.err);
                    return 1;
                } else {
                    throw ex;
                }
            }
        });
        System.exit(cl.execute(args));
    }

}

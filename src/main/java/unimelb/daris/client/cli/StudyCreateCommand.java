package unimelb.daris.client.cli;

import java.nio.file.Path;
import java.util.List;

import arc.xml.XmlDoc;
import ch.qos.logback.classic.Level;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IExecutionExceptionHandler;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParseResult;
import picocli.CommandLine.Spec;
import unimelb.daris.client.model.exmethod.ExMethod;
import unimelb.daris.client.model.object.DObject;
import unimelb.daris.client.model.study.Study;
import unimelb.daris.client.model.subject.Subject;
import unimelb.logging.Logback;
import unimelb.mf.client.cli.MFCommand;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetUtils;
import unimelb.picocli.converters.PathConverter;

@Command(name = "daris-study-create", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class StudyCreateCommand implements MFCommand {

    @Option(names = {
            "--mf.config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux/DaRIS server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCFG;

    @Option(names = { "-p",
            "--parent-id" }, paramLabel = "<pid>", description = "The identifier of the parent subject or ex-method.", required = true, arity = "1")
    private String parentCID;

    @Option(names = { "-s",
            "--step" }, paramLabel = "<step>", description = "The identifier of the step.", required = false, arity = "1")
    private String step;

    @Option(names = { "-d",
            "--description" }, paramLabel = "<description>", description = "Description about the study.", required = false, arity = "1")
    private String description;

    @Option(names = {
            "--ignore-if-exists" }, description = "Ignore if study with the specified name already exists.", required = false)
    private boolean ignoreIfExists = false;

    @Parameters(description = "The name of the study.", arity = "1", index = "0", paramLabel = "NAME")
    private String name;

    private String studyCID = null;

    @Spec
    private CommandSpec spec;

    @Override
    public MFSession authenticate() throws Throwable {
        MFConfigBuilder mfcb = new MFConfigBuilder();
        mfcb.setApp(Application.DEFAULT_APPLICATION_NAME);
        if (this.mfCFG != null) {
            mfcb.loadFromConfigFile(mfCFG.toFile());
        } else {
            mfcb.findAndLoadFromConfigFile();
        }
        mfcb.setConsoleLogon(true);
        return MFSession.create(mfcb);
    }

    @Override
    public void execute(MFSession session) throws Throwable {
        validate(session);

        if (this.studyCID != null && this.ignoreIfExists) {
            System.out.println("Found existing study: " + this.studyCID);
        } else {
            this.studyCID = Study.create(session, this.parentCID, this.step, this.name, this.description);
            System.out.println("Created study: " + this.studyCID);
        }
    }

    private void validate(MFSession session) throws Throwable {
        if (!DObject.exists(session, this.parentCID)) {
            throw new IllegalArgumentException("Parent object: " + this.parentCID + " not found.");
        }
        XmlDoc.Element parentAE = AssetUtils.getAssetMetaByCid(session, this.parentCID);
        String objectType = parentAE.value("meta/daris:pssd-object/type");
        if (!"subject".equals(objectType) && !"ex-method".equals(objectType)) {
            throw new IllegalArgumentException(
                    "Invalid parent object: " + this.parentCID + ". Unexpected daris:pssd-object/type: " + objectType);
        }
        if ("subject".equals(objectType)) {
            int nbExMethods = Subject.countExMethods(session, this.parentCID);
            if (nbExMethods == 1) {
                this.parentCID = Subject.getFirstExMethod(session, this.parentCID);
            } else {
                String msg;
                if (nbExMethods <= 0) {
                    msg = "No ex-method found in subject: " + this.parentCID;
                } else {
                    msg = "Multiple ex-methods found in subject: " + this.parentCID
                            + ". Must specify ex-method id rather than subject id.";
                }
                throw new IllegalArgumentException(msg);
            }
        }

        List<String> steps = ExMethod.getStudySteps(session, this.parentCID);
        if (steps == null || steps.isEmpty()) {
            throw new Exception("No step found in ex-method: '" + this.parentCID + "'");
        }
        if (steps.size() > 0) {
            if (this.step == null) {
                this.step = steps.get(0);
            } else {
                if (!steps.contains(this.step)) {
                    throw new IllegalArgumentException("Invalid step: " + this.step);
                }
            }
        }

        int nbStudies = Study.count(session, this.parentCID, this.name);
        if (nbStudies == 1) {
            this.studyCID = Study.findFirst(session, this.parentCID, this.name);
        }

        if (!ignoreIfExists) {
            if (nbStudies > 0) {
                throw new IllegalArgumentException("Study already exists. Found " + nbStudies
                        + " existing studies with name: '" + this.name + "'.");
            }
        }

        if (this.name == null) {
            throw new IllegalArgumentException("Study name is null");
        }
        this.name = this.name.trim();
        if (this.name.isEmpty()) {
            throw new IllegalArgumentException("Study name is empty");
        }

    }

    public static void main(String[] args) {

        Logback.setRootLogLevel(Level.WARN);

        CommandLine cl = new CommandLine(new StudyCreateCommand());
        cl.setExecutionExceptionHandler(new IExecutionExceptionHandler() {
            @Override
            public int handleExecutionException(Exception ex, CommandLine commandLine, ParseResult parseResult)
                    throws Exception {
                if (ex instanceof IllegalArgumentException) {
                    ex.printStackTrace();
                    cl.usage(System.err);
                    return 1;
                } else {
                    throw ex;
                }
            }
        });
        System.exit(cl.execute(args));
    }
}

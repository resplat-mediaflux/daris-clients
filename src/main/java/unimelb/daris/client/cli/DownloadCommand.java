package unimelb.daris.client.cli;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

import ch.qos.logback.classic.Level;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Spec;
import unimelb.daris.client.download.DownloadApplication;
import unimelb.daris.client.download.DownloadSettings;
import unimelb.daris.client.download.DownloadSettings.Parts;
import unimelb.daris.client.download.DownloadSettings.Unarchive;
import unimelb.daris.client.model.CiteableIdUtils;
import unimelb.io.AbstractCollectionTransferProgressListener;
import unimelb.io.CollectionTransferProgressListener;
import unimelb.logging.Logback;
import unimelb.mf.client.cli.MFCommand;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.picocli.converters.PathConverter;

@Command(name = "daris-download", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class DownloadCommand implements MFCommand {

    @Option(names = {
            "--mf.config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux/DaRIS server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCFG;

    @Option(names = {
            "--output-dir" }, paramLabel = "<output-dir>", description = "The output directory. If not specified, defaults to current working directory.", required = false, arity = "1")
    private Path outputDir;

    @Option(names = {
            "--unarchive" }, description = "Unpack archive files. aar: unpack only archive in AAR format; all: unpack all supported archive formats; none: do not unpack. Defaults to aar.", paramLabel = "<aar|all|none>", required = false, converter = UnarchiveConverter.class)
    private Unarchive unarchive = Unarchive.AAR;

    @Option(names = {
            "--parts" }, description = "Specifies which parts of the assets to download. Defaults to content.", paramLabel = "<content|metadata|all>", required = false, converter = PartsConverter.class)
    private Parts parts = Parts.CONTENT;

    @Option(names = {
            "--transcode" }, description = "Transcode data set format from one to another.", paramLabel = "FROM:TO[,FROM:TO...]...", required = false, converter = TranscodeConverter.class)
    private Transcodes transcodes;

    @Option(names = {
            "--filter" }, description = "A query to filter/select the datasets.", hidden = false, paramLabel = "<query>", required = false)
    private String filter;

    @Option(names = { "--nb-workers" }, description = "Number of worker threads.", paramLabel = "<n>", required = false)
    private int nbWorkers = DownloadSettings.DEFAULT_NUMBER_OF_WORKERS;

    @Option(names = { "--step" }, description = "Query result page size.", paramLabel = "<n>", required = false)
    private int step = DownloadSettings.DEFAULT_QUERY_PAGE_SIZE;

    @Option(names = { "--dataset-only" }, description = "Download datasets only.", required = false)
    private boolean datasetOnly = false;

    @Option(names = { "--no-attachments" }, description = "Exclude attachments.", required = false)
    private boolean noAttachments = false;

    @Option(names = { "--overwrite" }, description = "Overwrite if exists.", required = false)
    private boolean overwrite = false;

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "output usage information")
    private boolean printHelp;

    @Parameters(description = "The identifiers of the DaRIS objects to download.", arity = "1..", index = "0", paramLabel = "ID")
    private String[] cids;

    @Spec
    private CommandSpec spec;

    @Override
    public MFSession authenticate() throws Throwable {
        MFConfigBuilder mfcb = new MFConfigBuilder();
        mfcb.setApp(DownloadApplication.APPLICATION_NAME);
        if (this.mfCFG != null) {
            mfcb.loadFromConfigFile(this.mfCFG.toFile());
        } else {
            mfcb.findAndLoadFromConfigFile();
        }
        mfcb.setConsoleLogon(true);
        return MFSession.create(mfcb);
    }

    @Override
    public void execute(MFSession session) throws Throwable {
        for (String cid : this.cids) {
            if (!CiteableIdUtils.isCID(cid)) {
                throw new IllegalArgumentException(cid + " is not a valid DaRIS id.");
            }
        }

        // @formatter:off
		DownloadSettings settings = new DownloadSettings.Builder()
			.setUnarchive(this.unarchive)
			.setParts(this.parts)
			.setDatasetOnly(this.datasetOnly)
			.setIncludeAttachments(!this.noAttachments)
			.setTranscodes(this.transcodes==null?null:this.transcodes.get())
			.setOverwrite(this.overwrite)
			.setFilter(this.filter)
			.setNumberOfWorkers(this.nbWorkers)
			.setQueryPageSize(this.step)
			.build();
		// @formatter:on

        if (this.outputDir == null) {
            this.outputDir = Paths.get(System.getProperty("user.dir"));
        }

        DownloadApplication app = new DownloadApplication(session, settings);
        CollectionTransferProgressListener pl = new AbstractCollectionTransferProgressListener() {
            @Override
            public void inform(String message) {
                System.out.println(message);
            }

            @Override
            public void endCollectionTransfer() {

            }
        };
        app.download(this.outputDir, pl, null, this.cids);
        app.shutdown(true);

    }

    static class UnarchiveConverter implements CommandLine.ITypeConverter<Unarchive> {

        @Override
        public Unarchive convert(String value) throws Exception {
            Unarchive unarchive = Unarchive.fromString(value);
            if (unarchive == null) {
                throw new IllegalArgumentException("Invalid --unarchive option value: " + value);
            }
            return unarchive;
        }

    }

    static class PartsConverter implements CommandLine.ITypeConverter<Parts> {

        @Override
        public Parts convert(String value) throws Exception {
            Parts parts = Parts.fromString(value);
            if (parts == null) {
                throw new IllegalArgumentException("Invalid --parts option value: " + value);
            }
            return parts;
        }

    }

    static class Transcodes {
        private Map<String, String> transcodes = new LinkedHashMap<String, String>();

        void add(String from, String to) {
            this.transcodes.put(from, to);
        }

        public Map<String, String> get() {
            return this.transcodes;
        }
    }

    static class TranscodeConverter implements CommandLine.ITypeConverter<Transcodes> {

        @Override
        public Transcodes convert(String value) throws Exception {
            Transcodes transcodes = new Transcodes();
            if (value != null && !value.isEmpty()) {
                String[] tokens = value.split("\\ *,\\ *");
                if (tokens != null && tokens.length > 0) {
                    for (String token : tokens) {
                        String[] pair = token.split("\\ *:\\ *");
                        String from = pair[0];
                        String to = pair[1];
                        if (from != null && !from.trim().isEmpty() && to != null && !to.trim().isEmpty()) {
                            transcodes.add(from, to);
                        }
                    }
                }
            }
            return transcodes;
        }

    }

    public static void main(String[] args) {

        Logback.setRootLogLevel(Level.WARN);

        System.exit(new CommandLine(new DownloadCommand()).execute(args));
    }

}

package unimelb.daris.client.cli;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.Map;

import arc.xml.XmlDoc;
import ch.qos.logback.classic.Level;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IExecutionExceptionHandler;
import picocli.CommandLine.ITypeConverter;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParseResult;
import picocli.CommandLine.Spec;
import unimelb.daris.client.model.dataset.Dataset;
import unimelb.daris.client.model.study.Study;
import unimelb.io.AbstractCollectionTransferProgressListener;
import unimelb.logging.Logback;
import unimelb.mf.archive.MFArchive;
import unimelb.mf.archive.MFArchive.Type;
import unimelb.mf.client.cli.MFCommand;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetUtils;
import unimelb.picocli.converters.PathConverter;
import unimelb.utils.FileNameUtils;

@Command(name = "daris-dataset-create", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class DatasetCreateCommand implements MFCommand {

    @Option(names = {
            "--mf.config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux/DaRIS server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCFG;

    @Option(names = { "-s",
            "--study" }, paramLabel = "<study-id>", description = "The identifier of the parent study.", required = true, arity = "1")
    private String studyCID;

    @Option(names = { "-n",
            "--name" }, paramLabel = "<name>", description = "The name of the dataset. If not specified, try using the file/directory name.", required = false, arity = "1")
    private String name;

    @Option(names = { "-d",
            "--description" }, paramLabel = "<description>", description = "Description about the dataset.", required = false, arity = "1")
    private String description;

    @Option(names = { "-p",
            "--primary" }, description = "Creates primary dataset. Otherwise, derived dataset.", required = false)
    private boolean primary = false;

    @Option(names = { "-t",
            "--type" }, paramLabel = "<mime-type>", description = "The MIME type of the dataset.", required = false, arity = "1")
    private String type;

    @Option(names = { "-a",
            "--archive-type" }, paramLabel = "<archive-type>", description = "Archive type of the dataset content. zip or aar?", required = false, arity = "1", converter = ArchiveTypeConverter.class)
    private MFArchive.Type atype = null;

    @Option(names = { "-gz",
            "--gzip" }, description = "Compress the content using gzip. Ignored if the content is packed as archive.", required = false)
    private boolean gzip = false;

    @Option(names = {
            "--error-if-exists" }, description = "Stop and report error if dataset with the specified name already exists.", required = false)
    private boolean errorIfExists = false;

    @Option(names = {
            "--input-cid" }, paramLabel = "<cid>", description = "The cids of the input datasets.", required = false, arity = "0..")
    private String[] inputCids = null;

    @Parameters(description = "The source/input files or directories.", arity = "1..", index = "0", paramLabel = "FILES_OR_DIRS")
    private Path[] inputs;

    @Spec
    private CommandSpec spec;

    private Map<String, String> inputDatasets = null;

    @Override
    public MFSession authenticate() throws Throwable {
        MFConfigBuilder mfcb = new MFConfigBuilder();
        mfcb.setApp(Application.DEFAULT_APPLICATION_NAME);
        if (this.mfCFG != null) {
            mfcb.loadFromConfigFile(mfCFG.toFile());
        } else {
            mfcb.findAndLoadFromConfigFile();
        }
        mfcb.setConsoleLogon(true);
        return MFSession.create(mfcb);
    }

    @Override
    public void execute(MFSession session) throws Throwable {

        validate(session);

        AbstractCollectionTransferProgressListener pl = new AbstractCollectionTransferProgressListener() {

            @Override
            public void inform(String message) {
                System.out.println(message);
            }

            @Override
            public void endCollectionTransfer() {

            }
        };
        String datasetCID = Dataset.create(session, this.studyCID, this.primary, this.name, this.description, this.type,
                this.atype, this.gzip, pl, null, this.inputDatasets, this.inputs);
        System.out.println("Created dataset: " + datasetCID);

    }

    private void validate(MFSession session) throws Throwable {
        if (!Study.exists(session, this.studyCID)) {
            throw new IllegalArgumentException("Study: " + this.studyCID + " not found.");
        }
        XmlDoc.Element studyAE = AssetUtils.getAssetMetaByCid(session, this.studyCID);
        String objectType = studyAE.value("meta/daris:pssd-object/type");
        if (!"study".equals(objectType)) {
            throw new IllegalArgumentException(
                    "Invalid project: " + this.studyCID + ". Found daris:pssd-object/type: " + objectType);
        }

        if (this.inputs == null || this.inputs.length == 0) {
            throw new IllegalArgumentException("Missing inputs.");
        }

        if (this.name != null) {
            this.name = this.name.trim();
            if (this.name.isEmpty()) {
                throw new IllegalArgumentException("Dataset name is empty");
            }
        }

        if (this.inputCids != null && this.inputCids.length > 0) {
            this.inputDatasets = new LinkedHashMap<String, String>(this.inputCids.length);
            for (String cid : inputCids) {
                String vid = AssetUtils.getAssetVidByCid(session, cid);
                this.inputDatasets.put(cid, vid);
            }
        }

        /*
         * generate file name
         */
        // @formatter:off
		Path file1 = null;
		Path dir1 = null;

		for (Path input : this.inputs) {
			if (!Files.exists(input)) {
				throw new IllegalArgumentException("'" + input + "' does not exist");
			}
			if (Files.isDirectory(input)) {
				if (dir1 == null) {
					dir1 = input;
				}
			} else {
				if (file1 == null) {
					file1 = input;
				}
			}
			if (dir1 != null && file1 != null) {
				break;
			}
		}

		String fileName;

		if (this.inputs.length == 1) {
			// single file or directory
			if (file1 != null) {
				// single file
				if (this.gzip) {
					fileName = file1.toFile().getName() + ".gz";
				} else {
					fileName = file1.toFile().getName();
				}
			} else {
				// single directory
				if (this.atype == null) {
					this.atype = MFArchive.Type.AAR;
				}
				fileName = dir1.toFile().getName() + "." + atype.extension;
			}
		} else {
			// multiple files or directories
			if (this.atype == null) {
				this.atype = MFArchive.Type.AAR;
			}
			if (this.name != null) {
				// use the specified name as file name
				if (!this.name.toLowerCase().endsWith("." + atype.extension)) {
					fileName = this.name + "." + atype.extension;
				} else {
					fileName = this.name;
				}
			} else {
				// use first directory or first file name as file name.
				if (dir1 != null) {
					fileName = dir1.toFile().getName() + "." + atype.extension;
				} else {
					fileName = FileNameUtils.removeFileExtension(file1.toFile().getName()) + "." + atype.extension;
				}
			}
		}

		if(this.name==null) {
			this.name  = fileName;
		}
		// @formatter:on

        int nbDatasets = Dataset.count(session, this.studyCID, this.name, fileName);
        if (nbDatasets > 0 && this.errorIfExists) {
            throw new IllegalArgumentException("Dataset already exists. Found " + nbDatasets
                    + " existing subjects with name: '" + this.name + "'.");
        }
    }

    private static class ArchiveTypeConverter implements ITypeConverter<MFArchive.Type> {

        @Override
        public Type convert(String atype) throws Exception {
            if ("aar".equalsIgnoreCase(atype)) {
                return MFArchive.Type.AAR;
            } else if ("zip".equalsIgnoreCase(atype)) {
                return MFArchive.Type.ZIP;
            }
            throw new IllegalArgumentException("Invalid --archive-type: " + atype);
        }

    }

    public static void main(String[] args) {

        Logback.setRootLogLevel(Level.WARN);

        CommandLine cl = new CommandLine(new DatasetCreateCommand());
        cl.setExecutionExceptionHandler(new IExecutionExceptionHandler() {
            @Override
            public int handleExecutionException(Exception ex, CommandLine commandLine, ParseResult parseResult)
                    throws Exception {
                if (ex instanceof IllegalArgumentException) {
                    ex.printStackTrace();
                    cl.usage(System.err);
                    return 1;
                } else {
                    throw ex;
                }
            }
        });
        System.exit(cl.execute(args));
    }
}

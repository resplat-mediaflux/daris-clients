package unimelb.daris.client.cli;

import java.nio.file.Path;
import java.util.Collection;

import arc.xml.XmlDoc;
import ch.qos.logback.classic.Level;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IExecutionExceptionHandler;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParseResult;
import picocli.CommandLine.Spec;
import unimelb.daris.client.model.DataUse;
import unimelb.daris.client.model.project.Project;
import unimelb.daris.client.model.subject.Subject;
import unimelb.logging.Logback;
import unimelb.mf.client.cli.MFCommand;
import unimelb.mf.client.session.MFConfigBuilder;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetUtils;
import unimelb.picocli.converters.PathConverter;

@Command(name = "daris-subject-create", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", separator = " ", sortOptions = false)
public class SubjectCreateCommand implements MFCommand {

    @Option(names = {
            "--mf.config" }, paramLabel = "<mflux.cfg>", description = "Mediaflux/DaRIS server configuration file. Defaults to $HOME/.Arcitecta/mflux.cfg", required = false, converter = PathConverter.MustBeRegularFile.class)
    private Path mfCFG;

    @Option(names = { "-p",
            "--project" }, paramLabel = "<pid>", description = "The identifier of the parent project.", required = true, arity = "1")
    private String projectCID;

    @Option(names = { "-m",
            "--method" }, paramLabel = "<mid>", description = "The identifier of the method.", required = false, arity = "1")
    private String methodCID;

    @Option(names = { "-d",
            "--description" }, paramLabel = "<description>", description = "Description about the subject.", required = false, arity = "1")
    private String description;

    @Option(names = {
            "--ignore-if-exists" }, description = "Ignore if subject with the specified name already exists.", required = false)
    private boolean ignoreIfExists = false;

    @Parameters(description = "The name of the subject.", arity = "1", index = "0", paramLabel = "NAME")
    private String name;

    private DataUse dataUse = DataUse.specific;

    private String subjectCID = null;

    @Spec
    private CommandSpec spec;

    @Override
    public MFSession authenticate() throws Throwable {
        MFConfigBuilder mfcb = new MFConfigBuilder();
        mfcb.setApp(Application.DEFAULT_APPLICATION_NAME);
        if (this.mfCFG != null) {
            mfcb.loadFromConfigFile(mfCFG.toFile());
        } else {
            mfcb.findAndLoadFromConfigFile();
        }
        mfcb.setConsoleLogon(true);
        return MFSession.create(mfcb);
    }

    @Override
    public void execute(MFSession session) throws Throwable {
        validate(session);

        if (this.subjectCID != null && this.ignoreIfExists) {
            System.out.println("Found existing subject: " + this.subjectCID);
        } else {
            this.subjectCID = Subject.create(session, this.projectCID, this.methodCID, this.dataUse, this.name,
                    this.description, false);
            System.out.println("Created subject: " + this.subjectCID);
        }

    }

    private void validate(MFSession session) throws Throwable {
        if (!Project.exists(session, this.projectCID)) {
            throw new IllegalArgumentException("Project: " + this.projectCID + " not found.");
        }
        XmlDoc.Element projectAE = AssetUtils.getAssetMetaByCid(session, this.projectCID);
        String objectType = projectAE.value("meta/daris:pssd-object/type");
        if (!"project".equals(objectType)) {
            throw new IllegalArgumentException(
                    "Invalid project: " + this.projectCID + ". Found daris:pssd-object/type: " + objectType);
        }
        Collection<String> projectMethods = projectAE.values("meta/daris:pssd-project/method/id");
        if (projectMethods == null || projectMethods.isEmpty()) {
            throw new IllegalArgumentException("No method is set on project: " + this.projectCID + ".");
        }
        if (this.methodCID == null) {
            int nbMethods = projectMethods.size();
            if (nbMethods == 1) {
                this.methodCID = projectMethods.iterator().next();
            } else {
                throw new IllegalArgumentException("Missing --method argument.");
            }
        } else {
            if (!projectMethods.contains(this.methodCID)) {
                throw new IllegalArgumentException("Invalid --method argument: " + this.methodCID
                        + ". It's not registered in project: " + this.projectCID);
            }
        }

        this.dataUse = DataUse
                .fromString(projectAE.stringValue("meta/daris:pssd-project/data-use", DataUse.specific.toString()));

        int nbSubjects = Subject.count(session, this.projectCID, this.name);
        if (nbSubjects == 1) {
            this.subjectCID = Subject.findFirst(session, this.projectCID, this.name);
        }

        if (!ignoreIfExists) {
            if (nbSubjects > 0) {
                throw new IllegalArgumentException("Subject already exists. Found " + nbSubjects
                        + " existing subjects with name: '" + this.name + "'.");
            }
        }

        if (this.name == null) {
            throw new IllegalArgumentException("Subject name is null");
        }
        this.name = this.name.trim();
        if (this.name.isEmpty()) {
            throw new IllegalArgumentException("Subject name is empty");
        }

    }

    public static void main(String[] args) {

        Logback.setRootLogLevel(Level.WARN);

        CommandLine cl = new CommandLine(new SubjectCreateCommand());
        cl.setExecutionExceptionHandler(new IExecutionExceptionHandler() {
            @Override
            public int handleExecutionException(Exception ex, CommandLine commandLine, ParseResult parseResult)
                    throws Exception {
                if (ex instanceof IllegalArgumentException) {
                    ex.printStackTrace();
                    cl.usage(System.err);
                    return 1;
                } else {
                    throw ex;
                }
            }
        });
        System.exit(cl.execute(args));
    }
}

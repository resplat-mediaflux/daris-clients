package unimelb.daris.client.cli;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Callable;

import arc.mf.client.archive.Archive;
import ch.qos.logback.classic.Level;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Spec;
import unimelb.daris.client.cli.ArchiveCommand.ArchiveCreateCommand;
import unimelb.daris.client.cli.ArchiveCommand.ArchiveExtractCommand;
import unimelb.io.AbstractCollectionTransferProgressListener;
import unimelb.logging.Logback;
import unimelb.mf.archive.MFArchive;
import unimelb.picocli.converters.PathConverter;
import unimelb.utils.FileNameUtils;

@Command(name = "daris-archive", subcommands = { ArchiveCreateCommand.class,
        ArchiveExtractCommand.class }, abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", commandListHeading = "\nCOMMANDS:\n", optionListHeading = "\nOPTIONS:\n")
public class ArchiveCommand implements Callable<Integer> {

    public static final String VERSION = "1.0.0";

    public static class SrcArchiveConverter extends PathConverter.MustBeRegularFile {
        @Override
        public Path convert(String value) throws Exception {
            Path path = super.convert(value);
            if (!MFArchive.isArchive(path)) {
                throw new IllegalArgumentException("Unknown archive format: " + FileNameUtils.getFileExtension(value));
            }
            return path;
        }
    }

    public static class DstArchiveConverter extends PathConverter.MustNotExist {
        @Override
        public Path convert(String value) throws Exception {
            Path path = super.convert(value);
            if (!MFArchive.isArchive(path)) {
                throw new IllegalArgumentException("Unknown archive format: " + FileNameUtils.getFileExtension(value));
            }
            return path;
        }
    }

    public static class CompressionLevelConverter implements CommandLine.ITypeConverter<Integer> {

        @Override
        public Integer convert(String value) throws Exception {
            Integer level = Integer.parseInt(value);
            if (level < 0 || level > 9) {
                throw new IllegalArgumentException(
                        "Invalid compression level: " + value + ". Expects integer between 0 and 9.");
            }
            return level;
        }

    }

    @Command(name = "create", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", sortOptions = false, separator = " ")
    public static class ArchiveCreateCommand implements Callable<Integer> {

        @Option(names = { "-l",
                "--compression-level" }, description = "compression level. 0: no compression; 1: fastest compression; 9: slowest compression. Defaults to 6.", arity = "1", required = false, paramLabel = "<level>", defaultValue = "6", converter = CompressionLevelConverter.class)
        private int cLevel = MFArchive.DEFAULT_COMPRESSION_LEVEL;

        @Option(names = { "-h", "--help" }, usageHelp = true, description = "output usage information")
        private boolean printHelp;

        @Parameters(description = "destination archive file.", arity = "1", index = "0", paramLabel = "<dst-archive>", converter = DstArchiveConverter.class)
        private Path dstArchive;

        @Parameters(description = "input files or directories to add to the archive.", arity = "1..", index = "1", paramLabel = "<inputs>", converter = PathConverter.MustExist.class)
        private List<Path> inputs;

        @Spec
        private CommandSpec spec;

        @Override
        public Integer call() throws Exception {
            try {
                Archive.declareSupportForAllTypes();
                MFArchive.create(dstArchive, null, cLevel, new AbstractCollectionTransferProgressListener() {

                    @Override
                    public void inform(String message) {
                        System.out.println(message);
                    }

                    @Override
                    public void endCollectionTransfer() {

                    }
                }, null, inputs);
                return 0;
            } catch (Throwable e) {
                if (e instanceof Exception) {
                    throw (Exception) e;
                } else {
                    throw new Exception(e);
                }
            }
        }

    }

    @Command(name = "extract", abbreviateSynopsis = true, synopsisHeading = "\nUSAGE:\n  ", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", sortOptions = false, separator = " ")
    public static class ArchiveExtractCommand implements Callable<Integer> {

        @Option(names = { "-w", "--overwrite" }, description = "overwrite if the file pre-exists", required = false)
        private boolean overwrite = false;

        @Option(names = { "-h", "--help" }, usageHelp = true, description = "output usage information")
        private boolean printHelp;

        @Parameters(description = "the source archive to extract.", arity = "1", index = "0", paramLabel = "<src-archive>", converter = SrcArchiveConverter.class)
        private Path srcArchive;

        @Parameters(description = "the output directory.", arity = "0..1", index = "1", paramLabel = "output-dir", converter = PathConverter.MustBeDirectory.class)
        private Path outputDir = Paths.get(System.getProperty("user.dir"));

        @Spec
        private CommandSpec spec;

        @Override
        public Integer call() throws Exception {

            try {
                Archive.declareSupportForAllTypes();
                AbstractCollectionTransferProgressListener pl = new AbstractCollectionTransferProgressListener() {

                    @Override
                    public void inform(String message) {
                        System.out.println(message);
                    }

                    @Override
                    public void endCollectionTransfer() {

                    }
                };
                MFArchive.extract(srcArchive, outputDir, overwrite, pl, null);
                System.out.println(
                        String.format("Extracted %d files (%d bytes).", pl.transferredFiles(), pl.transferredBytes()));
                return 0;
            } catch (Throwable e) {
                if (e instanceof Exception) {
                    throw (Exception) e;
                } else {
                    throw new Exception(e);
                }
            }
        }
    }

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "output usage information")
    private boolean printHelp;

    @Spec
    private CommandSpec spec;

    @Override
    public Integer call() throws Exception {
        new CommandLine(this).usage(System.out);
        return 0;
    }

    public static void main(String[] args) {

        Logback.setRootLogLevel(Level.WARN);

        System.exit(new CommandLine(new ArchiveCommand()).execute(args));
    }

}
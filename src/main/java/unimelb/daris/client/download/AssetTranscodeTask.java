package unimelb.daris.client.download;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import arc.archive.ArchiveInput;
import arc.archive.ArchiveRegistry;
import arc.mf.client.ServerClient;
import arc.mf.client.archive.Archive;
import arc.mime.NamedMimeType;
import arc.streams.LongInputStream;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlStringWriter;
import unimelb.io.AbortCheck;
import unimelb.io.CollectionTransferProgressListener;
import unimelb.mf.archive.MFArchive;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.MFClientTask;
import unimelb.mf.client.util.AssetUtils;
import unimelb.utils.FileNameUtils;

public class AssetTranscodeTask implements MFClientTask<Void> {

    private static Logger logger = LoggerFactory.getLogger(AssetTranscodeTask.class);

    private MFSession _session;
    private String _assetId;
    private String _fromType;
    private String _toType;
    private Path _rootDir;
    private boolean _overwrite;
    private CollectionTransferProgressListener _pl;
    private AbortCheck _ac;

    public AssetTranscodeTask(MFSession session, String assetId, String toType, Path rootDir, boolean overwrite,
            CollectionTransferProgressListener pl, AbortCheck ac) throws Throwable {
        this(session, assetId, null, toType, rootDir, overwrite, pl, ac);
    }

    public AssetTranscodeTask(MFSession session, String assetId, String fromType, String toType, Path rootDir,
            boolean overwrite, CollectionTransferProgressListener pl, AbortCheck ac) throws Throwable {
        _session = session;
        _assetId = assetId;
        _fromType = fromType;
        _toType = toType;
        _rootDir = rootDir;
        _overwrite = overwrite;
        _pl = pl;
        _ac = ac;
    }

    @Override
    public Void execute() throws Throwable {
        Archive.declareSupportForAllTypes();
        XmlDoc.Element ae = AssetUtils.getAssetMetaById(session(), _assetId);
        if (_fromType == null) {
            _fromType = ae.value("type");
        }
        String title = AssetGetTask.titleOf(ae);
        Path outputDir = createOutputDirectory(ae);

        XmlStringWriter w = new XmlStringWriter();
        w.add("id", _assetId);
        w.add("atype", "aar");
        w.push("transcode");
        w.add("from", _fromType);
        w.add("to", _toType);
        w.pop();
        ServerClient.OutputConsumer output = new ServerClient.OutputConsumer() {
            @Override
            protected void consume(Element re, LongInputStream is) throws Throwable {
                // Archive.declareSupportForAllTypes();
                try {
                    ArchiveInput ai = ArchiveRegistry.createInput(is, new NamedMimeType(MFArchive.TYPE_AAR));
                    try {
                        MFArchive.extract(ai, outputDir, _overwrite, _pl, _ac);
                    } finally {
                        ai.close();
                    }
                } finally {
                    is.close();
                }
            }
        };
        String activity = "transcoding " + title + " (" + _fromType + " => " + _toType + ")...";
        logger.info(activity);
        if (_pl != null) {
            _pl.inform(activity);
        }
        session().execute("asset.transcode", w.document(), (ServerClient.Input) null, output);
        if (_pl != null) {
            _pl.incTransferredFiles(1);
        }
        return null;
    }

    private Path createOutputDirectory(XmlDoc.Element ae) throws Throwable {
        Path objDir = AssetGetTask.createObjectDirectory(session(), ae, _rootDir);
        Path outputDir = Paths.get(objDir.toString(), FileNameUtils.tidySafeFileName(_toType, '_'));
        Files.createDirectories(outputDir);
        return outputDir;
    }

    @Override
    public MFSession session() {
        return _session;
    }

}

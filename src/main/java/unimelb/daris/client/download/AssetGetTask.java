package unimelb.daris.client.download;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import arc.archive.ArchiveInput;
import arc.archive.ArchiveRegistry;
import arc.mf.client.ServerClient;
import arc.mf.client.ServerClient.OutputConsumer;
import arc.mf.client.archive.Archive;
import arc.mime.NamedMimeType;
import arc.streams.LongInputStream;
import arc.streams.StreamCopy;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlStringWriter;
import unimelb.daris.client.download.DownloadSettings.Parts;
import unimelb.daris.client.download.DownloadSettings.Unarchive;
import unimelb.daris.client.model.CiteableIdUtils;
import unimelb.daris.dicom.util.FileNameUtils;
import unimelb.io.AbortCheck;
import unimelb.io.CollectionTransferProgressListener;
import unimelb.mf.archive.MFArchive;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.task.MFClientTask;
import unimelb.mf.client.util.AssetUtils;
import unimelb.mf.client.util.XmlUtils;

public class AssetGetTask implements MFClientTask<Void> {

    public static final String ASSET_META_DATA_FILE_SUFFIX = "_asset-metadata.xml";
    public static final String DARIS_META_DATA_FILE_SUFFIX = "_daris-metadata.xml";
    public static final String ATTACHMENTS_DIR_SUFFIX = "_attachments";

    private static Logger logger = LoggerFactory.getLogger(AssetGetTask.class);

    private MFSession _session;
    private DownloadSettings _settings;
    private final XmlDoc.Element _ae;
    private final String _assetId;
    private final String _cid;
    private Path _rootDir;
    private CollectionTransferProgressListener _pl;
    private AbortCheck _ac;

    public AssetGetTask(MFSession session, DownloadSettings settings, XmlDoc.Element ae, Path rootDir,
            CollectionTransferProgressListener pl, AbortCheck ac) throws Throwable {
        _session = session;
        _settings = settings;
        _ae = ae;
        _assetId = _ae.value("@id");
        _cid = _ae.value("cid");
        _rootDir = rootDir;
        _pl = pl;
        _ac = ac;
    }

    static Path createObjectDirectory(MFSession session, Element ae, Path rootDir) throws Throwable {
        Path outputDir = null;
        if (ae.elementExists("meta/daris:pssd-object")) {
            String cid = ae.value("cid");
            assert cid != null;
            outputDir = Paths.get(rootDir.toString());
            // TODO include object name
            String projectCID = CiteableIdUtils.getProjectCID(cid);
            outputDir = Paths.get(outputDir.toString(), projectCID);
            if (!CiteableIdUtils.isProjectCID(cid)) {
                String subjectCID = CiteableIdUtils.getSubjectCID(cid);
                outputDir = Paths.get(outputDir.toString(), subjectCID);
                if (!(CiteableIdUtils.isSubjectCID(cid) || CiteableIdUtils.isExMethodCID(cid))) {
                    String studyCID = CiteableIdUtils.getStudyCID(cid);
                    outputDir = Paths.get(outputDir.toString(), studyCID);
                    if (!CiteableIdUtils.isStudyCID(cid)) {
                        outputDir = Paths.get(outputDir.toString(), cid);
                    }
                }
            }
        } else if (ae.elementExists("related[@type='attached-to']/to")) {
            XmlDoc.Element oae = AssetUtils.getAssetMetaById(session, ae.value("related[@type='attached-to']/to"));
            Path objDir = createObjectDirectory(session, oae, rootDir);
            outputDir = Paths.get(objDir.toString(), "attachments");
        } else {
            outputDir = Paths.get(rootDir.toString(), ae.value("namespace"));
        }
        Files.createDirectories(outputDir);
        return outputDir;
    }

    static XmlDoc.Element objectMetadataOf(MFSession session, String cid) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", cid);
        return session.execute("om.pssd.object.describe", w.document()).element("object");
    }

    static String contentFileNameOf(XmlDoc.Element ae) throws Throwable {
        String contentFileName = ae.value("meta/daris:pssd-filename/original");
        if (contentFileName == null) {
            contentFileName = ae.value("meta/daris:pssd-object/name");
            if (contentFileName == null) {
                contentFileName = ae.value("name");
            }
            if (contentFileName == null) {
                contentFileName = ae.value("cid");
            }
            if (contentFileName == null) {
                contentFileName = ae.value("@id");
            }
            contentFileName = FileNameUtils.tidySafeFileName(contentFileName);

            String ext = ae.value("content/type/@ext");
            if (ext != null && !contentFileName.toLowerCase().endsWith(ext)) {
                contentFileName = contentFileName + "." + ext;
            }
        }
        return contentFileName;
    }

    static String titleOf(XmlDoc.Element ae) throws Throwable {
        String cid = ae.value("cid");
        String assetId = ae.value("@id");
        if (cid != null) {
            String objectType = ae.value("meta/daris:pssd-object/type");
            if (objectType != null) {
                return objectType + " " + cid;
            }
        }
        return "asset " + assetId;
    }

    @Override
    public Void execute() throws Throwable {
        if (_settings.unarchive() != Unarchive.NONE) {
            Archive.declareSupportForAllTypes();
        }

        /*
         * save meta data to file
         */
        if (_settings.parts() != Parts.CONTENT) {
            Path objDir = createObjectDirectory(session(), _ae, _rootDir);
            if (_ae.elementExists("meta/daris:pssd-object")) {
                // write to object metadata file

                // NOTE: we do not save asset metadata for daris objects
                // from asset.get service.
                // Because it may contains private subject metadata that the
                // caller does not have access. Instead, we use the output from
                // om.pssd.object.describe service.
                XmlDoc.Element oe = objectMetadataOf(session(), _cid);
                Path mdf = Paths.get(objDir.toString(), _cid + DARIS_META_DATA_FILE_SUFFIX);
                String activity = "Saving object metadata: '" + mdf + "'";
                logger.info(activity);
                if (_pl != null) {
                    _pl.inform(activity);
                }
                XmlUtils.saveToFile(oe, mdf);
            } else {
                // write to asset metadata file
                Path mdf = Paths.get(objDir.toString(), _assetId + ASSET_META_DATA_FILE_SUFFIX);
                String activity = "Saving asset " + _assetId + " metadata to '" + mdf + "'";
                logger.info(activity);
                if (_pl != null) {
                    _pl.inform(activity);
                }
                XmlUtils.saveToFile(_ae, mdf);
            }
        }

        /*
         * download content
         */
        if (_settings.parts() != Parts.META && _ae.elementExists("content")) {
            XmlStringWriter w = new XmlStringWriter();
            w.add("id", _assetId);
            session().execute("asset.get", w.document(), (ServerClient.Input) null,
                    _settings.parts() == Parts.META ? null : new OutputConsumer() {
                        @Override
                        protected void consume(Element re, LongInputStream in) throws Throwable {
                            try {
                                Path objDir = createObjectDirectory(session(), _ae, _rootDir);
                                String mimeType = in.type();
                                if (mimeType == null) {
                                    mimeType = _ae.value("content/type");
                                }
                                if (mimeType != null && ArchiveRegistry.isAnArchive(mimeType)
                                        && (_settings.unarchive() == Unarchive.ALL
                                                || (_settings.unarchive() == Unarchive.AAR
                                                        && mimeType.equals(MFArchive.TYPE_AAR)))) {
                                    ArchiveInput ai = ArchiveRegistry.createInput(in, new NamedMimeType(mimeType));
                                    try {
                                        String assetType = _ae.value("type");
                                        Path outputDir = assetType == null ? objDir
                                                : Paths.get(objDir.toString(), assetType.replace('/', '_'));
                                        MFArchive.extract(ai, outputDir, _settings.overwrite(), _pl, _ac);
                                    } finally {
                                        ai.close();
                                    }
                                } else {
                                    // write to content file
                                    Path outputContentFile = Paths.get(objDir.toString(), contentFileNameOf(_ae));
                                    if (Files.exists(outputContentFile) && !_settings.overwrite()) {
                                        String activity = "File: '" + outputContentFile + "' already exists. Skipped.";
                                        logger.info(activity);
                                        if (_pl != null) {
                                            _pl.inform(activity);
                                        }
                                    } else {
                                        String activity = "Writing file: '" + outputContentFile + "'...";
                                        logger.info(activity);
                                        if (_pl != null) {
                                            _pl.inform(activity);
                                        }
                                        StreamCopy.copy(in, outputContentFile.toFile());
                                    }
                                }
                            } finally {
                                in.close();
                            }
                        }
                    });
        }

        if (_settings.includeAttachments()) {
            Collection<String> attachmentAssetIds = _ae.values("related[@type='attachment']/to[@exists='true']");
            if (attachmentAssetIds != null && !attachmentAssetIds.isEmpty()) {
                Path objDir = createObjectDirectory(session(), _ae, _rootDir);
                Path attachmentsDir = Paths.get(objDir.toString(), _cid + ATTACHMENTS_DIR_SUFFIX);
                Files.createDirectories(attachmentsDir);
                for (String attachmentAssetId : attachmentAssetIds) {
                    String name = AssetUtils.getAssetMetaById(_session, attachmentAssetId).value("name");
                    File of = Paths.get(attachmentsDir.toString(), name).toFile();
                    XmlStringWriter w = new XmlStringWriter();
                    w.add("id", attachmentAssetId);
                    String activity = "Downloading attachment " + attachmentAssetId + " to '" + of.getAbsolutePath()
                            + "'";
                    logger.info(activity);
                    if (_pl != null) {
                        _pl.inform(activity);
                    }
                    session().execute("asset.get", w.document(), null, new ServerClient.FileOutput(of));
                }
            }
        }

        if (_pl != null) {
            _pl.incTransferredFiles(1);
        }
        return null;
    }

    @Override
    public MFSession session() {
        return _session;
    }

}

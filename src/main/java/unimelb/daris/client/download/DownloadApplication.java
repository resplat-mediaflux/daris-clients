package unimelb.daris.client.download;

import java.nio.file.Path;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import unimelb.io.AbortCheck;
import unimelb.io.AbstractCollectionTransferProgressListener;
import unimelb.io.CollectionTransferProgressListener;
import unimelb.mf.client.app.MFClientApplication;
import unimelb.mf.client.session.MFSession;

public class DownloadApplication implements MFClientApplication {

	public static final String APPLICATION_NAME = "daris-download";

	private MFSession _session;
	private DownloadSettings _settings;
	private ThreadPoolExecutor _executor;

	public DownloadApplication(MFSession session, DownloadSettings settings) {
		_session = session;
		_settings = settings;
		_executor = new ThreadPoolExecutor(_settings.numberOfWorkers(), _settings.numberOfWorkers(), 0,
				TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(100), new ThreadFactory() {
					@Override
					public Thread newThread(Runnable r) {
						return new Thread(r, applicationName() + ".worker");
					}
				}, new ThreadPoolExecutor.CallerRunsPolicy());
	}

	@Override
	public String applicationName() {
		return APPLICATION_NAME;
	}

	@Override
	public MFSession session() {
		return _session;
	}

	@Override
	public ExecutorService executor() {
		return _executor;
	}

	public void download(Path dstDir, CollectionTransferProgressListener pl, AbortCheck ac, String... cids)
			throws Throwable {
		this.execute(new DownloadTask(_session, _executor, _settings, dstDir, pl, ac, cids), true);
	}

	public void download(Path dstDir, String... cids) throws Throwable {
		download(dstDir, new AbstractCollectionTransferProgressListener() {
			@Override
			public void inform(String activity) {
				System.out.println(activity);
			}

			@Override
			public void endCollectionTransfer() {

			}
		}, null, cids);
	}
}

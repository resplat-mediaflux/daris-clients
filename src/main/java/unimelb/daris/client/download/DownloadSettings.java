package unimelb.daris.client.download;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DownloadSettings {

    public static final int DEFAULT_NUMBER_OF_WORKERS = 2;
    public static final int MIN_NUMBER_OF_WORKERS = 1;
    public static final int MAX_NUMBER_OF_WORKERS = 8;
    public static final int DEFAULT_QUERY_PAGE_SIZE = 100;
    public static final int MIN_QUERY_RESULT_SIZE = 1;
    public static final int MAX_QUERY_RESULT_SIZE = 10000;

    public static enum Parts {
        META, CONTENT, ALL;

        public static Parts fromString(String s, Parts def) throws IllegalArgumentException {
            if (s == null || s.trim().isEmpty()) {
                return def;
            }
            Parts[] vs = values();
            for (Parts v : vs) {
                if (v.name().equalsIgnoreCase(s)) {
                    return v;
                }
            }
            throw new IllegalArgumentException("Invalid value: " + s);
        }

        public static Parts fromString(String s) {
            return fromString(s, null);
        }
    }

    public static enum Unarchive {
        AAR, ALL, NONE;
        public static Unarchive fromString(String s, Unarchive def) throws IllegalArgumentException {
            if (s == null || s.trim().isEmpty()) {
                return def;
            }
            Unarchive[] vs = values();
            for (Unarchive v : vs) {
                if (v.name().equalsIgnoreCase(s)) {
                    return v;
                }
            }
            throw new IllegalArgumentException("Invalid value: " + s);
        }

        public static Unarchive fromString(String s) {
            return fromString(s, null);
        }
    }

    private final boolean recursive;
    private final boolean includeAttachments;
    private final Unarchive unarchive;
    private final boolean datasetOnly;
    private final Parts parts;
    private final Map<String, String> transcodes;
    private final boolean overwrite;
    private final String filter;
    private final int nbWorkers;
    private final int queryPageSize;

    public DownloadSettings(boolean recursive, boolean includeAttachments, Unarchive unarchive, boolean datasetOnly,
            Parts parts, Map<String, String> transcodes, boolean overwrite, String filter, int nbWorkers,
            int queryResultSize) {
        this.recursive = recursive;
        this.includeAttachments = includeAttachments;
        this.unarchive = unarchive == null ? Unarchive.AAR : unarchive;
        this.datasetOnly = datasetOnly;
        this.parts = parts == null ? Parts.CONTENT : parts;
        this.transcodes = transcodes == null ? null : Collections.unmodifiableMap(transcodes);
        this.overwrite = overwrite;
        this.filter = filter;
        this.nbWorkers = nbWorkers;
        this.queryPageSize = queryResultSize;
    }

    public boolean recursive() {
        return this.recursive;
    }

    public boolean includeAttachments() {
        return this.includeAttachments;
    }

    public Unarchive unarchive() {
        return this.unarchive;
    }

    public boolean datasetOnly() {
        return this.datasetOnly;
    }

    public Parts parts() {
        return this.parts;
    }

    public boolean hasTranscode(String from) {
        return this.transcodes != null && !this.transcodes.isEmpty() && this.transcodes.containsKey(from);
    }

    public Map<String, String> transcodes() {
        return this.transcodes;
    }

    public String transcode(String from) {
        if (this.transcodes == null) {
            return null;
        }
        return transcodes.get(from);
    }

    public boolean hasTranscodes() {
        return this.transcodes != null && !this.transcodes.isEmpty();
    }

    public boolean overwrite() {
        return this.overwrite;
    }

    public String filter() {
        return this.filter;
    }

    public int numberOfWorkers() {
        return this.nbWorkers;
    }

    public int queryPageSize() {
        return this.queryPageSize;
    }

    public static class Builder {

        private boolean _recursive = true;
        private boolean _includeAttachments = true;
        private Unarchive _unarchive = Unarchive.AAR;
        private boolean _datasetOnly = true;
        private Parts _parts = Parts.CONTENT;
        private Map<String, String> _transcodes = null;
        private boolean _overwrite = false;
        private String _filter = null;
        private int _nbWorkers = DEFAULT_NUMBER_OF_WORKERS;
        private int _queryPageSize = DEFAULT_QUERY_PAGE_SIZE;

        public Builder setRecursive(boolean recursive) {
            _recursive = recursive;
            return this;
        }

        public Builder setIncludeAttachments(boolean includeAttachments) {
            _includeAttachments = includeAttachments;
            return this;
        }

        public Builder setUnarchive(Unarchive unarchive) {
            _unarchive = unarchive == null ? Unarchive.AAR : unarchive;
            return this;
        }

        public Builder setDatasetOnly(boolean datasetOnly) {
            _datasetOnly = datasetOnly;
            return this;
        }

        public Builder setParts(Parts parts) {
            _parts = parts == null ? Parts.CONTENT : parts;
            return this;
        }

        public Builder addTranscode(String from, String to) {
            if (_transcodes == null) {
                _transcodes = new HashMap<String, String>();
            }
            _transcodes.put(from, to);
            return this;
        }

        public Builder removeTranscode(String from) {
            if (_transcodes != null) {
                _transcodes.remove(from);
            }
            return this;
        }

        public Builder setFilter(String filter) {
            _filter = filter;
            return this;
        }

        public Builder setNumberOfWorkers(int nbWorkers) {
            if (nbWorkers < MIN_NUMBER_OF_WORKERS || nbWorkers > MAX_NUMBER_OF_WORKERS) {
                throw new IllegalArgumentException("Expects number in the range of " + MIN_NUMBER_OF_WORKERS + ".."
                        + MAX_NUMBER_OF_WORKERS + "(inclusive). Found: " + nbWorkers);
            }
            _nbWorkers = nbWorkers;
            return this;
        }

        public Builder setQueryPageSize(int queryPageSize) {
            if (queryPageSize < MIN_QUERY_RESULT_SIZE || queryPageSize > MAX_QUERY_RESULT_SIZE) {
                throw new IllegalArgumentException("Expects number in the range of " + MIN_QUERY_RESULT_SIZE + ".."
                        + MAX_QUERY_RESULT_SIZE + "(inclusive). Found: " + queryPageSize);
            }
            _queryPageSize = queryPageSize;
            return this;
        }

        public Builder setTranscodes(Map<String, String> transcodes) {
            _transcodes = transcodes;
            return this;
        }

        public Builder setOverwrite(boolean overwrite) {
            _overwrite = overwrite;
            return this;
        }

        public DownloadSettings build() {
            return new DownloadSettings(_recursive, _includeAttachments, _unarchive, _datasetOnly, _parts, _transcodes,
                    _overwrite, _filter, _nbWorkers, _queryPageSize);
        }

    }

}

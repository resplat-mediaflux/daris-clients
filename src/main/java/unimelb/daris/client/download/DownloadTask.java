package unimelb.daris.client.download;

import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ExecutorService;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.daris.client.download.DownloadSettings.Parts;
import unimelb.io.AbortCheck;
import unimelb.io.CollectionTransferProgressListener;
import unimelb.mf.client.task.MFClientTask;
import unimelb.mf.client.session.MFSession;

public class DownloadTask implements MFClientTask<Void> {

    private MFSession _session;
    private DownloadSettings _settings;
    private ExecutorService _workers;
    private Path _dstDir;
    private CollectionTransferProgressListener _pl;
    private AbortCheck _ac;
    private String[] _cids;

    public DownloadTask(MFSession session, ExecutorService workers, DownloadSettings settings, Path dstDir,
            CollectionTransferProgressListener pl, AbortCheck ac, String... cids) {
        _session = session;
        _workers = workers;
        _settings = settings;
        _dstDir = dstDir;
        _pl = pl;
        _ac = ac;
        _cids = cids;
    }

    private String constructQuery(String[] cids) {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        boolean first = true;
        for (String cid : cids) {
            if (!first) {
                sb.append(" or ");
            } else {
                first = false;
            }
            sb.append("(");
            sb.append("cid='").append(cid).append("'");
            if (_settings.recursive()) {
                sb.append(" or cid starts with '").append(cid).append("'");
            }
            sb.append(")");
        }
        sb.append(")");
        if (_settings.filter() != null) {
            sb.append("and (").append(_settings.filter()).append(")");
        }
        if (_settings.datasetOnly()) {
            sb.append("and (model='om.pssd.dataset')");
        }
        if (_settings.parts() == Parts.CONTENT) {
            sb.append("and (asset has content)");
        }
        return sb.toString();
    }

    @Override
    public Void execute() throws Throwable {
        String where = constructQuery(_cids);
        long idx = 1;
        long remaining = Long.MAX_VALUE;
        long total = -1;
        while (remaining > 0) {
            XmlStringWriter w = new XmlStringWriter();
            w.add("where", where);
            w.add("count", true);
            w.add("idx", idx);
            w.add("size", _settings.queryPageSize());
            w.add("action", "get-meta");
            XmlDoc.Element re = _session.execute("asset.query", w.document());
            remaining = re.longValue("cursor/remaining");
            idx += _settings.queryPageSize();
            if (total < 0) {
                total = re.longValue("cursor/total");
                if (_pl != null) {
                    _pl.beginCollectionTransfer(total, -1L);
                }
            }
            List<XmlDoc.Element> aes = re.elements("asset");
            if (aes != null) {
                for (XmlDoc.Element ae : aes) {
                    if (_settings.parts() != DownloadSettings.Parts.META && ae.elementExists("content")) {
                        String type = ae.value("type");
                        if (type != null) {
                            String toType = _settings.transcode(type);
                            if (toType != null) {
                                _workers.submit(new AssetTranscodeTask(_session, ae.value("@id"), type, toType, _dstDir,
                                        _settings.overwrite(), _pl, _ac));
                                continue;
                            }
                        }
                    }
                    _workers.submit(new AssetGetTask(_session, _settings, ae, _dstDir, _pl, _ac));
                }
            }
        }
        return null;
    }

    @Override
    public MFSession session() {
        return _session;
    }

}

package unimelb.daris.client.model.object;

import java.util.List;

import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import unimelb.daris.client.gui.app.DarisExplorerApp;
import unimelb.mf.client.gui.object.ObjectCollectionRef;
import unimelb.utils.Action;

public class DObjectChildrenRef extends ObjectCollectionRef<DObjectRef> {

    private SimpleObjectProperty<DObjectRef> _obj;
    private SimpleStringProperty _seekToCid;
    private SimpleObjectProperty<SortKey> _sortKey;

    public DObjectChildrenRef(DObjectRef o) {
        super(DarisExplorerApp.get());
        setTotal(o == null ? -1 : o.numberOfChildren());
        _obj = new SimpleObjectProperty<DObjectRef>(o);
        _seekToCid = new SimpleStringProperty(null);
        _seekToCid.addListener((obs, ov, nv) -> {
            reset();
        });
        _sortKey = new SimpleObjectProperty<SortKey>(SortKey.citeableId());
        _sortKey.addListener((obs, ov, nv) -> {
            reset();
        });
    }

    public List<DObjectRef> children() {
        return collection();
    }

    @Override
    protected final String entityName() {
        return "object";
    }

    @Override
    protected String serviceName() {
        return "daris.object.children.list";
    }

    @Override
    protected void updateServiceArgs(long idx, int size, XmlWriter w) throws Throwable {
        DObjectRef o = _obj.get();
        if (o != null) {
            w.add("cid", o.cid());
        }
        String seekToCid = _seekToCid.get();
        if (seekToCid == null) {
            w.add("idx", idx);
            w.add("size", size);
        } else {
            w.push("seek-to");
            w.add("cid", seekToCid);
            w.pop();
        }
        SortKey sortKey = _sortKey.get();
        if(sortKey!=null) {
            w.push("sort");
            w.add("key", sortKey.key());
            w.add("order", sortKey.order());
            w.pop();
        }
    }

    @Override
    protected List<DObjectRef> instantiate(XmlDoc.Element re) {
        List<DObjectRef> os = super.instantiate(re);
        _seekToCid.set(null);
        return os;
    }

    public void reset() {
        super.reset();
        _seekToCid.set(null);
    }

    public void seekTo(String seekToCid, Action postAction) {
        setSeekToCid(seekToCid);
        resolve(postAction);
    }

    protected void setSeekToCid(String seekToCid) {
        _seekToCid.set(seekToCid);
    }

    public void setSortKey(SortKey sortKey) {
        _sortKey.set(sortKey);
    }

    @Override
    protected DObjectRef instantiateMember(Element me) throws Throwable {
        return new DObjectRef(me);
    }
}

package unimelb.daris.client.model.object;

import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlWriter;
import unimelb.daris.client.gui.app.DarisExplorerApp;
import unimelb.daris.client.model.CiteableIdUtils;
import unimelb.mf.client.gui.object.ObjectRef;

public class DObjectRef extends ObjectRef<DObject> {

    private String _cid;
    private String _assetId;
    private String _name;
    private String _description;
    private long _nbc = -1;

    public DObjectRef(Element oe) throws Throwable {
        super(DarisExplorerApp.get());
        _cid = oe.value("@cid");
        _assetId = oe.value("@id");
        _name = oe.value("@name");
        _nbc = oe.longValue("@nbc", -1);
    }

    public String cid() {
        return _cid;
    }

    public String assetId() {
        return _assetId;
    }

    public String name() {
        return _name;
    }

    public String description() {
        return _description;
    }

    public long numberOfChildren() {
        return _nbc;
    }

    public boolean hasChildren() {
        return _nbc > 0;
    }

    public boolean mayHaveChildren() {
        return _nbc != 0;
    }

    public boolean isProject() {
        return CiteableIdUtils.isProjectCID(_cid);
    }

    public boolean isSubject() {
        return CiteableIdUtils.isSubjectCID(_cid);
    }

    public boolean isExMethod() {
        return CiteableIdUtils.isExMethodCID(_cid);
    }

    public boolean isStudy() {
        return CiteableIdUtils.isStudyCID(_cid);
    }

    public boolean isDataSet() {
        return CiteableIdUtils.isDataSetCID(_cid);
    }

    @Override
    protected String serviceName() {
        return "om.pssd.object.describe";
    }

    @Override
    protected void updateServiceArgs(XmlWriter w) throws Throwable {
        w.add("id", _cid);
        w.add("isleaf", true);
    }

    @Override
    protected DObject instantiate(Element re) throws Throwable {
        XmlDoc.Element oe = re.element("object");
        if (oe != null) {
            _assetId = oe.value("id/@asset");
            _nbc = oe.longValue("number-of-children");
            return DObject.create(oe);
        }
        return null;
    }

    public DObject.Type type() {
        if (this.referent() != null) {
            return this.referent().type();
        }
        if (_cid == null) {
            return null;
        }
        if (isProject()) {
            return DObject.Type.PROJECT;
        } else if (isSubject()) {
            return DObject.Type.SUBJECT;
        } else if (isExMethod()) {
            return DObject.Type.EX_METHOD;
        } else if (isStudy()) {
            return DObject.Type.STUDY;
        } else if (isDataSet()) {
            return DObject.Type.DATASET;
        } else {
            return null;
        }
    }

    public String typeAndId() {
        DObject.Type type = type();
        if (type == null) {
            return null;
        }
        return type.toString() + " " + _cid;
    }

}

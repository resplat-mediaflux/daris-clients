package unimelb.daris.client.model.object;

import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import unimelb.daris.client.model.dataset.Dataset;
import unimelb.daris.client.model.exmethod.ExMethod;
import unimelb.daris.client.model.method.Method;
import unimelb.daris.client.model.project.Project;
import unimelb.daris.client.model.study.Study;
import unimelb.daris.client.model.subject.Subject;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetUtils;

public abstract class DObject {



    public static enum Type {
        PROJECT("project"), SUBJECT("subject"), EX_METHOD("ex-method"), STUDY("study"), DATASET("dataset"),
        METHOD("method");

        private String _value;

        Type(String value) {
            _value = value;
        }

        public final String value() {
            return _value;
        }

        public final String toString() {
            return _value;
        }

        public static Type fromString(String s) {
            if (s != null) {
                Type[] vs = values();
                for (Type v : vs) {
                    if (s.equalsIgnoreCase(v.value())) {
                        return v;
                    }
                }
            }
            return null;
        }
    }

    private XmlDoc.Element _oe;
    private String _cid;
    private String _assetId;
    private DObject.Type _type;
    private String _namespace;
    private String _name;
    private String _description;
    private int _version;
    private XmlDoc.Element _meta;
    private XmlDoc.Element _content;


    protected DObject(XmlDoc.Element oe) throws Throwable {
        _oe = oe;
        _cid = oe.value("id");
        _assetId = oe.value("id/@asset");
        _type = DObject.Type.fromString(oe.value("@type"));
        _version = oe.intValue("@version", 0);
        _namespace = oe.value("namespace");
        _name = oe.value("name");
        _description = oe.value("description");
        _meta = oe.element("meta");
        _content = oe.element("content");
    }

    public XmlDoc.Element xml() {
        return _oe;
    }

    public String cid() {
        return _cid;
    }

    public String assetId() {
        return _assetId;
    }

    public DObject.Type type() {
        return _type;
    }

    public String namespace() {
        return _namespace;
    }

    public String name() {
        return _name;
    }

    public String description() {
        return _description;
    }

    public int version() {
        return _version;
    }

    public XmlDoc.Element metadata() {
        return _meta;
    }

    public XmlDoc.Element content() {
        return _content;
    }

    public abstract Type objectType();

    public static DObject create(Element oe) throws Throwable {
        Type objType = Type.fromString(oe.value("@type"));
        if (objType == null) {
            throw new Exception("Failed to instantiate object. In validate object/@type: " + oe.value("@type"));
        }
        switch (objType) {
            case PROJECT:
                return new Project(oe);
            case SUBJECT:
                return new Subject(oe);
            case EX_METHOD:
                return new ExMethod(oe);
            case STUDY:
                return new Study(oe);
            case DATASET:
                return new Dataset(oe);
            case METHOD:
                return new Method(oe);
        }
        throw new Exception("Failed to instantiate object from XML: " + oe.toString());
    }

    public static boolean exists(MFSession session, String cid) throws Throwable {
        return AssetUtils.assetExistsByCid(session, cid);
    }

    public String typeAndId() {
        return String.format("%s %s", type().toString(), cid());
    }
}

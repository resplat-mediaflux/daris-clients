package unimelb.daris.client.model.exmethod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import arc.xml.XmlStringWriter;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import unimelb.daris.client.model.object.DObject;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetUtils;

public class ExMethod extends DObject {

    public ExMethod(Element oe) throws Throwable {
        super(oe);
        // TODO Auto-generated constructor stub
    }

    @Override
    public final Type objectType() {
        return Type.EX_METHOD;
    }

    public static List<String> getStudySteps(MFSession session, String exMethodCID) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", exMethodCID);
        Collection<String> steps = session.execute("om.pssd.ex-method.study.step.find", w.document())
                .values("ex-method/step");
        if (steps == null || steps.isEmpty()) {
            return null;
        }
        return new ArrayList<String>(steps);
    }

    public static String findFirst(MFSession session, String subjectCID) throws Throwable {

        XmlDoc.Element subjectAE = AssetUtils.getAssetMetaByCid(session, subjectCID);
        String methodCID = subjectAE.value("meta/daris:pssd-subject/method");
        if (methodCID == null) {
            throw new Exception("no method set on subject " + subjectCID);
        }

        XmlStringWriter w = new XmlStringWriter();
        w.add("where", "cid in '" + subjectCID + "' and xpath(daris:pssd-ex-method/method/id)='" + methodCID + "'");
        w.add("action", "get-cid");
        w.add("size", 1);
        XmlDoc.Element re = session.execute("asset.query", w.document());

        String cid = re.value("cid");
        if (cid == null) {
            throw new Exception("no instantiated ex-method for subject " + subjectCID);
        }
        return cid;
    }

}

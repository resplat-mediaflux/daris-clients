package unimelb.daris.client.model;

public enum DataUse {

	specific, unspecified, extended;

	public static DataUse fromString(String s) {
		if (s != null) {
			DataUse[] vs = values();
			for (DataUse v : vs) {
				if (v.name().equalsIgnoreCase(s)) {
					return v;
				}
			}
		}
		return null;
	}

}

package unimelb.daris.client.model.project;

import java.util.List;

import arc.xml.XmlDoc.Element;
import unimelb.daris.client.model.method.MethodRef;
import unimelb.daris.client.model.object.DObject;

public class Project extends DObject {

    private List<MethodRef> _methods;

    public Project(Element oe) throws Throwable {
        super(oe);
        // TODO Auto-generated constructor stub
    }

    public int numberOfMethods() {
        return _methods == null ? 0 : _methods.size();
    }

    @Override
    public final Type objectType() {
        return Type.PROJECT;
    }

}

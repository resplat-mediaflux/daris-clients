package unimelb.daris.client.model.study;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import arc.utils.DateTime;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlStringWriter;
import arc.xml.XmlWriter;
import unimelb.daris.client.model.exmethod.ExMethod;
import unimelb.daris.client.model.object.DObject;
import unimelb.daris.client.siemens.upload.SiemensRawPETCTFileInfo;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetUtils;
import unimelb.utils.StringUtils;

public class Study extends DObject {

    public static final String SIEMENS_RAW_MR_STUDY_DOC_TYPE = "daris:siemens-raw-mr-study";
    public static final String SIEMENS_RAW_PETCT_STUDY_DOC_TYPE = "daris:siemens-raw-petct-study";

    public static final String SIEMENS_RAW_MR_STUDY_MIME_TYPE = "siemens-raw-mr/study";
    public static final String SIEMENS_RAW_PETCT_STUDY_MIME_TYPE = "siemens-raw-petct/study";

    public static final String STUDY_TYPE_MRI = "Magnetic Resonance Imaging";
    public static final String STUDY_TYPE_PETCT = "Positron Emission Tomography/Computed Tomography";
    public static final String STUDY_TYPE_PET = "Positron Emission Tomography";
    public static final String STUDY_TYPE_CT = "Computed Tomography";
    public static final String STUDY_TYPE_QA = "Quality Assurance";
    public static final String STUDY_TYPE_UNSPECIFIED = "Unspecified";

    public Study(Element oe) throws Throwable {
        super(oe);
        // TODO Auto-generated constructor stub
    }

    @Override
    public final Type objectType() {
        return Type.STUDY;
    }

    public static String create(MFSession session, String step, String exMethodCID, String name, String description)
            throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("pid", exMethodCID);
        w.add("step", step);
        w.add("fillin", true);
        if (name != null) {
            w.add("name", name);
        }
        if (description != null) {
            w.add("description", description);
        }
        return session.execute("om.pssd.study.create", w.document()).value("id");
    }

    public static int count(MFSession session, String parentCID, String name) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("action", "count");
        w.add("where", constructQuery(parentCID, name));
        return session.execute("asset.query", w.document()).intValue("value");
    }

    private static String constructQuery(String parentCID, String name) {
        StringBuilder sb = new StringBuilder();
        sb.append("cid starts with '" + parentCID + "' and model='om.pssd.study'");
        if (name != null) {
            sb.append(" and xpath(daris:pssd-object/name)='" + name + "'");
        }
        return sb.toString();
    }

    public static String findFirst(MFSession session, String parentCID, String name) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("action", "get-cid");
        w.add("size", 1);
        w.add("where", constructQuery(parentCID, name));
        return session.execute("asset.query", w.document()).value("cid");
    }

    public static void destroy(MFSession session, String studyCID) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("cid", studyCID);
        session.execute("daris.study.destroy", studyCID);
    }

    public static boolean isStudy(MFSession session, String cid) throws Throwable {
        XmlDoc.Element ae = AssetUtils.getAssetMetaByCid(session, cid);
        return "study".equals(ae.value("meta/daris:pssd-object/type"));
    }

    public static void update(MFSession session, String studyCID, String name, String description) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", studyCID);
        if (name != null) {
            w.add("name", name);
        }
        if (description != null) {
            w.add("description", description);
        }
        session.execute("om.pssd.study.update", w.document());
    }

    public static String findSiemensRawMRStudy(MFSession session, String subjectCID, Date studyDate) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        StringBuilder where = new StringBuilder();
        where.append("cid starts with '").append(subjectCID).append("'");
        where.append(" and model='om.pssd.study'");
        where.append(" and xpath(").append(Study.SIEMENS_RAW_MR_STUDY_DOC_TYPE).append("/date)='")
                .append(new SimpleDateFormat(DateTime.DATE_FORMAT).format(studyDate)).append("'");
        w.add("where", where.toString());
        w.add("action", "get-cid");
        w.add("size", 2);
        XmlDoc.Element r = session.execute("asset.query", w.document());
        String cid = r.value("cid");
        if (cid != null && r.count("cid") > 1) {
            throw new Exception("Multiple Siemens raw MR study found.");
        }
        return cid;
    }

    private static void addSiemensRawMRMetadata(XmlWriter w, Collection<String> frameOfReferences, Date studyDate)
            throws Throwable {
        w.push("meta");
        w.push(SIEMENS_RAW_MR_STUDY_DOC_TYPE);
        for (String for_ : frameOfReferences) {
            w.add("frame-of-reference", for_);
        }
        w.addDateOnly("date", studyDate);
        w.push("ingest");
        w.add("date", "now");
        w.pop();
        w.pop();
        w.pop();
    }

    public static String createSiemensRawMRStudy(MFSession session, String subjectCID, String frameOfReference,
            Date studyDate) throws Throwable {

        String exMethodCID = ExMethod.findFirst(session, subjectCID);

        String step = null;
        String[] studyTypes = new String[] { STUDY_TYPE_MRI, STUDY_TYPE_QA, STUDY_TYPE_UNSPECIFIED };
        for (String studyType : studyTypes) {
            XmlStringWriter w = new XmlStringWriter();
            w.add("id", exMethodCID);
            w.add("type", studyType);
            XmlDoc.Element re = session.execute("om.pssd.ex-method.study.step.find", w.document());
            step = re.value("ex-method/step");
            if (step != null) {
                break;
            }
        }
        if (step == null) {
            throw new Exception("No step found in ex-method " + exMethodCID);
        }

        XmlStringWriter w = new XmlStringWriter();
        w.add("pid", exMethodCID);
        w.add("step", step);
        w.add("name", "Raw Siemens MR");
        w.add("description", "Raw Siemens data");
        List<String> fors = new ArrayList<>(1);
        fors.add(frameOfReference);
        addSiemensRawMRMetadata(w, fors, studyDate);

        return session.execute("om.pssd.study.create", w.document()).value("id");
    }

    public static void updateSiemensRawMRStudy(MFSession session, String studyCID, String frameOfReference,
            Date studyDate) throws Throwable {

        XmlDoc.Element ae = AssetUtils.getAssetMetaByCid(session, studyCID);
        // String studyType = ae.value("meta/daris:pssd-study/type");
        String exMethodCID = ae.value("meta/daris:pssd-study/method");
        String exMethodStep = ae.value("meta/daris:pssd-study/method/@step");
        Set<String> fors = new LinkedHashSet<String>();
        fors.add(frameOfReference);
        if (ae.elementExists("meta/" + SIEMENS_RAW_MR_STUDY_DOC_TYPE + "/frame-of-reference")) {
            fors.addAll(ae.values("meta/" + SIEMENS_RAW_MR_STUDY_DOC_TYPE + "/frame-of-reference"));
        }

        XmlStringWriter w = new XmlStringWriter();
        w.add("id", studyCID);
        w.push("method");
        w.add("id", exMethodCID);
        w.add("step", exMethodStep);
        w.pop();

        w.add("name", "Raw Siemens MR");
        w.add("description", "Raw Siemens data");
        addSiemensRawMRMetadata(w, fors, studyDate);
        session.execute("om.pssd.study.update", w.document());
    }

    public static String findSiemensRawPETCTStudy(MFSession session, String subjectCID, SiemensRawPETCTFileInfo info)
            throws Throwable {

        Collection<String> cids = null;

        // search by accession number
        if (info.accessionNumber() != null) {
            cids = findSiemensRawPETCTStudyByAccessionNumber(session, subjectCID, info.accessionNumber(),
                    info.studyDate());
            if (cids != null && !cids.isEmpty()) {
                if (cids.size() == 1) {
                    return cids.iterator().next();
                } else {
                    throw new Exception("Multiple Siemens raw PETCT study (accession_number='" + info.accessionNumber()
                            + "') found: " + StringUtils.join(cids, ", "));
                }
            }
        }

        // search by study date
        if (info.studyDate() != null) {
            cids = findSiemensRawPETCTStudyByDate(session, subjectCID, info.studyDate());
            if (cids != null && !cids.isEmpty()) {
                if (cids.size() == 1) {
                    return cids.iterator().next();
                } else {
                    String dateStr = new SimpleDateFormat(DateTime.DATE_FORMAT).format(info.studyDate());
                    throw new Exception("Multiple Siemens raw PETCT study (date='" + dateStr + "') found: "
                            + StringUtils.join(cids, ", "));
                }
            }
        }
        return null;
    }

    public static Collection<String> findSiemensRawPETCTStudyByDate(MFSession session, String parentCID, Date date)
            throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        StringBuilder where = new StringBuilder();
        where.append("cid starts with '").append(parentCID).append("'");
        where.append(" and model='om.pssd.study'");
        where.append(" and xpath(").append(Study.SIEMENS_RAW_PETCT_STUDY_DOC_TYPE).append("/date)='")
                .append(new SimpleDateFormat(DateTime.DATE_FORMAT).format(date)).append("'");
        w.add("where", where.toString());
        w.add("action", "get-cid");
        w.add("size", "infinity");
        XmlDoc.Element r = session.execute("asset.query", w.document());
        Collection<String> cids = r.values("cid");
        return cids;
    }

    public static Collection<String> findSiemensRawPETCTStudyByAccessionNumber(MFSession session, String parentCID,
            String accessionNumber, Date date) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        StringBuilder where = new StringBuilder();
        where.append("cid starts with '").append(parentCID).append("'");
        where.append(" and model='om.pssd.study'");
        where.append(" and xpath(").append(Study.SIEMENS_RAW_PETCT_STUDY_DOC_TYPE).append("/accession-number)='")
                .append(accessionNumber).append("'");
        if (date != null) {
            where.append(" and xpath(").append(Study.SIEMENS_RAW_PETCT_STUDY_DOC_TYPE).append("/date)='")
                    .append(new SimpleDateFormat(DateTime.DATE_FORMAT).format(date)).append("'");
        }
        w.add("where", where.toString());
        w.add("action", "get-cid");
        w.add("size", "infinity");
        XmlDoc.Element r = session.execute("asset.query", w.document());
        Collection<String> cids = r.values("cid");
        return cids;
    }

    private static void addSiemensRawPETCTMetadata(XmlWriter w, SiemensRawPETCTFileInfo info) throws Throwable {
        w.push("meta");
        w.push(SIEMENS_RAW_PETCT_STUDY_DOC_TYPE);
        if (info.studyInstanceUID() != null) {
            w.add("uid", info.studyInstanceUID());
        }
        if (info.studyDescription() != null) {
            w.add("description", info.studyDescription());
        }
        if (info.studyDate() != null) {
            w.addDateOnly("date", info.studyDate());
        }
        if (info.accessionNumber() != null) {
            w.add("accession-number", info.accessionNumber());
        }
        w.push("ingest");
        w.add("date", "now");
        w.pop();
        w.pop();
        w.pop();
    }

    public static String createSiemensRawPETCTStudy(MFSession session, String subjectCID, SiemensRawPETCTFileInfo info)
            throws Throwable {

        String exMethodCID = ExMethod.findFirst(session, subjectCID);

        String step = null;
        String modality = info.modality();
        String[] studyTypes = new String[] { "PT".equalsIgnoreCase(modality) ? STUDY_TYPE_PET : STUDY_TYPE_CT,
                STUDY_TYPE_PETCT, STUDY_TYPE_QA, STUDY_TYPE_UNSPECIFIED };
        for (String studyType : studyTypes) {
            XmlStringWriter w = new XmlStringWriter();
            w.add("id", exMethodCID);
            w.add("type", studyType);
            XmlDoc.Element re = session.execute("om.pssd.ex-method.study.step.find", w.document());
            step = re.value("ex-method/step");
            if (step != null) {
                break;
            }
        }
        if (step == null) {
            throw new Exception("No step found in ex-method " + exMethodCID);
        }

        XmlStringWriter w = new XmlStringWriter();
        w.add("pid", exMethodCID);
        w.add("step", step);
        w.add("name", "Raw Siemens PETCT");
        w.add("description", "Raw Siemens data");
        addSiemensRawPETCTMetadata(w, info);

        return session.execute("om.pssd.study.create", w.document()).value("id");
    }

    public static String createSiemensRawPETCTStudy(MFSession session, SiemensRawPETCTFileInfo info,
            String patientAssetId) throws Throwable {

        XmlDoc.Element patientAE = AssetUtils.getAssetMetaById(session, patientAssetId);
        String patientAssetNS = patientAE.value("namespace");

        XmlStringWriter w = new XmlStringWriter();
        w.push("related");
        w.add("to", new String[] { "relationship", "had-by" }, patientAssetId);
        w.pop();
        w.add("namespace", patientAssetNS);
        w.add("type", SIEMENS_RAW_PETCT_STUDY_MIME_TYPE);
        addSiemensRawPETCTMetadata(w, info);

        return session.execute("asset.create", w.document()).value("id");
    }

    public static void updateSiemensRawPETCTStudy(MFSession session, String studyCID, SiemensRawPETCTFileInfo info)
            throws Throwable {

        XmlDoc.Element ae = AssetUtils.getAssetMetaByCid(session, studyCID);
        // String studyType = ae.value("meta/daris:pssd-study/type");
        String exMethodCID = ae.value("meta/daris:pssd-study/method");
        String exMethodStep = ae.value("meta/daris:pssd-study/method/@step");

        XmlStringWriter w = new XmlStringWriter();
        w.add("id", studyCID);

        w.push("method");
        w.add("id", exMethodCID);
        w.add("step", exMethodStep);
        w.pop();
        w.add("name", "Raw Siemens PETCT");
        w.add("description", "Raw Siemens data");
        addSiemensRawPETCTMetadata(w, info);

        session.execute("om.pssd.study.update", w.document());
    }

}

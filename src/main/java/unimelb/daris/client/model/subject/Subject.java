package unimelb.daris.client.model.subject;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlStringWriter;
import unimelb.daris.client.model.DataUse;
import unimelb.daris.client.model.object.DObject;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetUtils;

public class Subject extends DObject {

	public static final Logger logger = LoggerFactory.getLogger(Subject.class);

	public Subject(Element oe) throws Throwable {
		super(oe);
		// TODO Auto-generated constructor stub
	}

	@Override
	public final Type objectType() {
		return Type.SUBJECT;
	}

	public static String findFirst(MFSession session, String projectCID, String subjectName) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("where", constructQuery(session, projectCID, subjectName));
		w.add("action", "get-cid");
		w.add("size", 1);
		return session.execute("asset.query", w.document()).value("cid");
	}

	public static int count(MFSession session, String projectCID, String subjectName) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("where", constructQuery(session, projectCID, subjectName));
		w.add("action", "count");
		return session.execute("asset.query", w.document()).intValue("value");
	}

	private static String constructQuery(MFSession session, String projectCID, String subjectName) throws Throwable {
		StringBuilder sb = new StringBuilder();
		sb.append("cid in '").append(projectCID).append("'");
		if (subjectName != null) {
			sb.append(" and xpath(daris:pssd-object/name)='").append(subjectName).append("'");
		}
		return sb.toString();
	}

	public static String create(MFSession session, String projectCID, String methodCID, DataUse dataUse, String name,
			String description, boolean virtual) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("pid", projectCID);
		w.add("method", methodCID);
		if (virtual) {
			w.add("virtual", virtual);
		}
		if (name != null) {
			w.add("name", name);
		}
		if (description != null) {
			w.add("description", description);
		}
		if (dataUse != null) {
			w.add("data-use", dataUse);
		}
		return session.execute("om.pssd.subject.create", w.document()).value("id");
	}

	public static void update(MFSession session, String subjectCID, String name, String description) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("id", subjectCID);
		if (name != null) {
			w.add("name", name);
		}
		if (description != null) {
			w.add("description", description);
		}
		session.execute("om.pssd.subject.update", w.document());
	}

	public static void destroy(MFSession session, String cid) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("cid", cid);
		session.execute("daris.subject.destroy", w.document());
	}

	public static boolean isSubject(MFSession session, String cid) throws Throwable {
		XmlDoc.Element ae = AssetUtils.getAssetMetaByCid(session, cid);
		return "subject".equals(ae.value("meta/daris:pssd-object/type"));
	}

	public static String getFirstExMethod(MFSession session, String subjectCID) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("action", "get-cid");
		w.add("where", "cid in '" + subjectCID + "' and model='om.pssd.ex-method'");
		w.add("size", 1);
		return session.execute("asset.query", w.document()).value("cid");
	}

	public static int countExMethods(MFSession session, String subjectCID) throws Throwable {
		XmlStringWriter w = new XmlStringWriter();
		w.add("action", "count");
		w.add("where", "cid in '" + subjectCID + "' and model='om.pssd.ex-method'");
		return session.execute("asset.query", w.document()).intValue("value");
	}

	static Collection<String> findDicomSubject(MFSession session, String parentCID, String patientID,
			String patientFirstName, String patientLastName, Date patientBirthDate, String patientSex)
			throws Throwable {
		if (patientID == null && patientLastName == null && patientFirstName == null) {
			throw new Exception("Inadequate query condition: neither PatientID nor PatientName is specified.");
		}
		/*
		 * construct query
		 */
		// @formatter:off
		StringBuilder where = new StringBuilder();
		where.append("cid starts with '").append(parentCID).append("'");
		if (patientID != null) {
			where.append(" and (xpath(mf-dicom-patient/id)=ignore-case('").append(patientID).append("') or ");
			where.append("xpath(mf-dicom-patient-encrypted/id)=ignore-case('").append(patientID).append("'))");
		}
		if (patientFirstName != null) {
			where.append(" and (xpath(mf-dicom-patient/name[@type='first'])=ignore-case('").append(patientFirstName).append("') or ");
			where.append("xpath(mf-dicom-patient-encrypted/name[@type='first'])=ignore-case('").append(patientFirstName).append("'))");
		}
		if (patientLastName != null) {
			where.append(" and (xpath(mf-dicom-patient/name[@type='last'])=ignore-case('").append(patientLastName).append("') or ");
			where.append("xpath(mf-dicom-patient-encrypted/name[@type='last'])=ignore-case('").append(patientLastName).append("'))");
		}
		if (patientBirthDate != null) {
			String dateStr = new SimpleDateFormat("dd-MMM-yyyy").format(patientBirthDate);
			where.append(" and (xpath(mf-dicom-patient/dob)='").append(dateStr).append("' or ");
			where.append("xpath(mf-dicom-patient-encrypted/dob)='").append(dateStr).append("')");
		}
		if (patientSex != null) {
			where.append(" and (xpath(mf-dicom-patient/sex)=ignore-case('").append(patientSex).append("') or ");
			where.append("xpath(mf-dicom-patient-encrypted/sex)=ignore-case('").append(patientSex).append("'))");
		}
		// @formatter:on

		logger.debug("Query for subject: " + where.toString());
		XmlStringWriter w = new XmlStringWriter();
		w.add("where", where.toString());
		w.add("action", "get-cid");
		w.add("size", 100);
		return session.execute("asset.query", w.document()).values("cid");
	}

	public static String find(MFSession session, String parentCID, String patientID, String patientFirstName,
			String patientLastName, Date patientBirthDate, String patientSex) throws Throwable {
		if (patientID == null && patientLastName == null && patientFirstName == null) {
			throw new Exception("Inadequate query condition: neither PatientID nor PatientName is specified.");
		}
		Collection<String> cids = null;
		// patientID is the highest priority
		if (patientID != null) {
			cids = findDicomSubject(session, parentCID, patientID, null, null, patientBirthDate, patientSex);
			if (cids == null || cids.isEmpty()) {
				return null;
			} else if (cids.size() == 1) {
				return cids.iterator().next();
			}
		}

		if (patientFirstName != null || patientLastName != null) {
			cids = findDicomSubject(session, parentCID, patientID, patientFirstName, patientLastName, patientBirthDate,
					patientSex);
		}

		if (cids == null || cids.isEmpty()) {
			return null;
		} else if (cids.size() == 1) {
			return cids.iterator().next();
		} else {
			throw new Exception("Multiple subjects found.");
		}
	}

	static String findDicomPatient(MFSession session, String projectCID, String patientID, String patientFirstName,
			String patientLastName, Date patientBirthDate, String patientSex) throws Throwable {
		/*
		 * construct query
		 */
		// @formatter:off
        StringBuilder where = new StringBuilder();
		if(projectCID!=null){
			where.append("cid in '" + projectCID + "' and ");
		}
        where.append("(mf-dicom-patient has value or mf-dicom-patient-encrypted has value)");
        if (patientID != null) {
            where.append(" and (xpath(mf-dicom-patient/id)=ignore-case('").append(patientID).append("') or ");
            where.append("xpath(mf-dicom-patient-encrypted/id)=ignore-case('").append(patientID).append("'))");
        }
        if (patientFirstName != null) {
            where.append(" and (xpath(mf-dicom-patient/name[@type='first'])=ignore-case('").append(patientFirstName).append("') or ");
            where.append("xpath(mf-dicom-patient-encrypted/name[@type='first'])=ignore-case('").append(patientFirstName).append("'))");
        }
        if (patientLastName != null) {
            where.append(" and (xpath(mf-dicom-patient/name[@type='last'])=ignore-case('").append(patientFirstName).append("') or ");
            where.append("xpath(mf-dicom-patient-encrypted/name[@type='last'])=ignore-case('").append(patientFirstName).append("'))");
        }
        if (patientBirthDate != null) {
            String dateStr = new SimpleDateFormat("dd-MMM-yyyy").format(patientBirthDate);
            where.append(" and (xpath(mf-dicom-patient/dob)='").append(dateStr).append("' or ");
            where.append("xpath(mf-dicom-patient-encrypted/dob)='").append(dateStr).append("')");
        }
        if (patientSex != null) {
            where.append(" and (xpath(mf-dicom-patient/sex)=ignore-case('").append(patientFirstName).append("') or ");
            where.append("xpath(mf-dicom-patient-encrypted/sex)=ignore-case('").append(patientFirstName).append("'))");
        }
        // @formatter:on

		logger.debug("query for patient: " + where.toString());
		XmlStringWriter w = new XmlStringWriter();
		w.add("where", where.toString());
		w.add("where", "not(model='om.pssd.subject')");
		w.add("action", "get-id");
		w.add("size", 2);
		XmlDoc.Element r = session.execute("asset.query", w.document());
		String id = r.value("id");
		if (id != null && r.count("id") > 1) {
			throw new Exception("Multiple patients found.");
		}
		return id;
	}

}

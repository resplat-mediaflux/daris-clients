package unimelb.daris.client.model.dataset;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.GZIPOutputStream;

import arc.archive.ArchiveOutput;
import arc.archive.ArchiveRegistry;
import arc.mf.client.ServerClient;
import arc.mf.client.archive.Archive;
import arc.streams.StreamCopy.AbortCheck;
import arc.xml.XmlDoc;
import arc.xml.XmlDoc.Element;
import arc.xml.XmlStringWriter;
import arc.xml.XmlWriter;
import unimelb.daris.client.model.CiteableIdUtils;
import unimelb.daris.client.model.method.MethodRef;
import unimelb.daris.client.model.object.DObject;
import unimelb.daris.client.siemens.upload.SiemensRawMRFileInfo;
import unimelb.daris.client.siemens.upload.SiemensRawPETCTFileInfo;
import unimelb.io.CollectionTransferProgressListener;
import unimelb.io.TransferProgressListener;
import unimelb.mf.archive.MFArchive;
import unimelb.mf.client.session.MFGeneratedInput;
import unimelb.mf.client.session.MFSession;
import unimelb.mf.client.util.AssetNamespaceUtils;
import unimelb.mf.client.util.AssetUtils;
import unimelb.utils.FileNameUtils;
import unimelb.utils.PathUtils;

public class Dataset extends DObject {

    public static final String SIEMENS_RAW_MR_SERIES_DOC_TYPE = "daris:siemens-raw-mr-series";
    public static final String SIEMENS_RAW_PETCT_SERIES_DOC_TYPE = "daris:siemens-raw-petct-series";

    public static final String SIEMENS_RAW_MR_SERIES_MIME_TYPE = "siemens-raw-mr/series";
    public static final String SIEMENS_RAW_PETCT_SERIES_MIME_TYPE = "siemens-raw-petct/series";

    public static enum SourceType {
        PRIMARY, DERIVATION;

        public final String toString() {
            return name().toLowerCase();
        }

        public static SourceType fromString(String s) {
            if (PRIMARY.toString().equalsIgnoreCase(s)) {
                return PRIMARY;
            } else if (DERIVATION.toString().equalsIgnoreCase(s)) {
                return DERIVATION;
            } else {
                return null;
            }
        }
    }

    private SourceType _sourceType;
    private MethodRef _method;
    private String _step;

    public Dataset(Element oe) throws Throwable {
        super(oe);
        _sourceType = SourceType.fromString(oe.value("source/type"));
    }

    public final SourceType sourceType() {
        return _sourceType;
    }

    @Override
    public final Type objectType() {
        return Type.DATASET;
    }

    public MethodRef method() {
        return _method;
    }

    public String step() {
        return _step;
    }

    public static final MFArchive.Type DEFAULT_ARCHIVE_TYPE = MFArchive.Type.AAR;
    public static final int DEFAULT_COMPRESS_LEVEL = 6;

    public static int count(MFSession session, String studyCID, String name, String fileName) throws Throwable {
        StringBuilder sb = new StringBuilder();
        sb.append("cid in '" + studyCID + "' and model='om.pssd.dataset'");
        if (name != null) {
            sb.append(" and xpath(daris:pssd-object/name)='").append(name).append("'");
        }
        XmlStringWriter w = new XmlStringWriter();
        w.add("where", sb.toString());
        w.add("action", "count");
        return session.execute("asset.query", w.document()).intValue("value");
    }

    public static String create(MFSession session, String studyCID, boolean primary, String name, String description,
            String type, MFArchive.Type atype, boolean compress, CollectionTransferProgressListener pl,
            unimelb.io.AbortCheck ac, Map<String, String> inputCids, Path... inputs) throws Throwable {

        if (primary) {
            return createPrimaryDataset(session, studyCID, name, description, type, atype, compress, pl, ac, inputs);
        } else {
            return createDerivedDataset(session, studyCID, name, description, type, atype, compress, pl, ac, inputCids,
                    inputs);
        }
    }

    private static String createPrimaryDataset(MFSession session, String studyCID, String datasetName,
            String datasetDescription, String type, MFArchive.Type archiveType, boolean compress,
            CollectionTransferProgressListener pl, unimelb.io.AbortCheck ac, Path... inputs) throws Throwable {

        XmlStringWriter w = new XmlStringWriter();
        addDatasetMetadata(session, w, studyCID, datasetName, datasetDescription, type, archiveType, compress, inputs);
        w.push("subject");
        w.add("id", CiteableIdUtils.getParentCID(studyCID, 2));
        w.pop();

        ServerClient.Input sci = createServiceInput(archiveType, compress, pl, ac, inputs);
        sci.setStore("asset:" + AssetUtils.getAssetStoreByCid(session, studyCID));
        return session.execute("om.pssd.dataset.primary.create", w.document(), sci).value("id");
    }

    private static String createDerivedDataset(MFSession session, String studyCID, String datasetName,
            String datasetDescription, String type, MFArchive.Type archiveType, boolean compress,
            CollectionTransferProgressListener pl, unimelb.io.AbortCheck ac, Map<String, String> inputCids,
            Path... inputs) throws Throwable {

        XmlStringWriter w = new XmlStringWriter();
        addDatasetMetadata(session, w, studyCID, datasetName, datasetDescription, type, archiveType, compress, inputs);

        if (inputCids != null && !inputCids.isEmpty()) {
            Set<String> cids = inputCids.keySet();
            for (String inputCid : cids) {
                String inputVid = inputCids.get(inputCid);
                w.add("input", new String[] { "vid", inputVid }, inputCid);
            }
        }

        ServerClient.Input sci = createServiceInput(archiveType, compress, pl, ac, inputs);
        sci.setStore("asset:" + AssetUtils.getAssetStoreByCid(session, studyCID));
        return session.execute("om.pssd.dataset.derivation.create", w.document(), sci).value("id");
    }

    private static void addDatasetMetadata(MFSession session, XmlWriter w, String studyCID, String datasetName,
            String datasetDescription, String type, MFArchive.Type atype, boolean gzip, Path... inputs)
            throws Throwable {

        String fileName = fileNameFor(studyCID, datasetName, datasetDescription, type, atype, gzip, inputs);

        if (datasetName == null) {
            datasetName = fileName;
        }

        w.add("pid", studyCID);
        if (datasetName != null) {
            w.add("name", datasetName);
        } else {
            w.add("name", fileName);
        }
        if (datasetDescription != null) {
            w.add("description", datasetDescription);
        }
        if (type != null) {
            w.add("type", type);
        }
        w.add("fillin", true);
        if (fileName != null) {
            w.add("filename", fileName);
        }

        XmlDoc.Element studyAE = AssetUtils.getAssetMetaByCid(session, studyCID);
        w.push("method");
        w.add("id", studyAE.value("meta/daris:pssd-study/method"));
        w.add("step", studyAE.value("meta/daris:pssd-study/method/@step"));
        w.pop();
        if (inputs.length == 1) {
            w.push("meta");
            w.push("mf-note");
            w.add("note", "source: " + inputs[0].toFile().getAbsolutePath());
            w.pop();
        }
        w.pop();

    }

    private static String fileNameFor(String studyCID, String datasetName, String datasetDescription, String type,
            MFArchive.Type atype, boolean gzip, Path... inputs) {
        Path file1 = null;
        Path dir1 = null;

        for (Path input : inputs) {
            if (!Files.exists(input)) {
                throw new IllegalArgumentException("'" + input + "' does not exist");
            }
            if (Files.isDirectory(input)) {
                if (dir1 == null) {
                    dir1 = input;
                }
            } else {
                if (file1 == null) {
                    file1 = input;
                }
            }
            if (dir1 != null && file1 != null) {
                break;
            }
        }

        String fileName;

        if (inputs.length == 1) {
            // single file or directory
            if (file1 != null) {
                // single file
                if (gzip) {
                    fileName = file1.toFile().getName() + ".gz";
                } else {
                    fileName = file1.toFile().getName();
                }
            } else {
                // single directory
                if (atype == null) {
                    atype = MFArchive.Type.AAR;
                }
                fileName = dir1.toFile().getName() + "." + atype.extension;
            }
        } else {
            // multiple files or directories
            if (atype == null) {
                atype = MFArchive.Type.AAR;
            }
            if (datasetName != null) {
                // use the specified name as file name
                if (!datasetName.toLowerCase().endsWith("." + atype.extension)) {
                    fileName = datasetName + "." + atype.extension;
                } else {
                    fileName = datasetName;
                }
            } else {
                // use first directory or first file name as file name.
                if (dir1 != null) {
                    fileName = dir1.toFile().getName() + "." + atype.extension;
                } else {
                    fileName = FileNameUtils.removeFileExtension(file1.toFile().getName()) + "." + atype.extension;
                }
            }
        }
        return fileName;
    }

    private static ServerClient.Input createServiceInput(MFArchive.Type archiveType, boolean compress,
            CollectionTransferProgressListener pl, unimelb.io.AbortCheck ac, Path... inputs) throws Throwable {
        String type = null;
        String ext = null;
        String source = inputs.length == 1 ? inputs[0].toFile().getAbsolutePath() : null;

        if (inputs.length == 1 && !Files.isDirectory(inputs[0])) {
            if (compress) {
                type = "application/gzip";
                ext = "gz";
            } else {
                type = null;
                ext = FileNameUtils.getFileExtension(inputs[0]);
            }
        } else {
            if (archiveType == null) {
                archiveType = DEFAULT_ARCHIVE_TYPE;
            }
            type = archiveType.mimeType;
            ext = archiveType.extension;
        }

        final String cType = type;
        return new ServerClient.GeneratedInput(cType, ext, source, -1) {

            @Override
            protected void copyTo(OutputStream out, arc.streams.StreamCopy.AbortCheck ac1) throws Throwable {
                unimelb.io.AbortCheck abortCheck = ac1 == null ? ac : new unimelb.io.AbortCheck() {

                    @Override
                    public boolean aborted() {
                        if (ac1.hasBeenAborted()) {
                            return true;
                        }
                        if (ac != null && ac.aborted()) {
                            return true;
                        }
                        return false;
                    }
                };
                if (cType != null && MFArchive.Type.isArchiveType(cType)) {
                    Archive.declareSupportForAllTypes();
                    ArchiveOutput ao = ArchiveRegistry.createOutput(out, cType, compress ? DEFAULT_COMPRESS_LEVEL : 0,
                            null);
                    try {
                        for (Path input : inputs) {
                            MFArchive.add(ao, input.getParent(), input, pl, abortCheck);
                        }
                    } finally {
                        ao.close();
                    }
                } else {
                    if (compress) {
                        try (GZIPOutputStream gos = new GZIPOutputStream(out)) {
                            unimelb.io.StreamCopy.copy(inputs[0], gos, pl, abortCheck);
                        }
                    } else {
                        unimelb.io.StreamCopy.copy(inputs[0], out, pl, abortCheck);
                    }
                }
            }
        };
    }

    public static String findSiemensRawMRSeries(MFSession session, String parentCID, Path file) throws Throwable {

        String fileName = file.toFile().getName();

        StringBuilder where = new StringBuilder();
        if (parentCID != null) {
            where.append("cid starts with '").append(parentCID).append("' and ");
        }
        where.append("model='om.pssd.dataset'");
        where.append(" and xpath(daris:pssd-filename/original)='").append(fileName).append("'");
        where.append(" and ").append(SIEMENS_RAW_MR_SERIES_DOC_TYPE).append(" has value");

        XmlStringWriter w = new XmlStringWriter();
        w.add("where", where.toString());
        w.add("action", "get-cid");
        w.add("pdist", "0");
        w.add("size", 2);
        XmlDoc.Element re = session.execute("asset.query", w.document());
        String cid = re.value("cid");
        if (cid != null && re.count("cid") > 1) {
            throw new Exception("multiple Siemens Raw datasets with name '" + fileName + "'found in '" + parentCID
                    + "'. This is most likely an error and needs to be remedied.");
        }
        return cid;
    }

    private static void addSiemensRawMRMetadata(XmlWriter w, SiemensRawMRFileInfo fileInfo, Date expireDate)
            throws Throwable {
        w.push(SIEMENS_RAW_MR_SERIES_DOC_TYPE);

        w.addDateOnly("date", fileInfo.scanDate());
        w.add("modality", "MR");
        w.add("description", "Siemens RAW MR file");
        if (expireDate != null) {
            w.add("date-expire", expireDate);
        }

        w.push("ingest");
        w.add("date", "now");
        w.pop();

        w.pop();
    }

    private static String createOrUpdateSiemensRawMRSeries(MFSession session, String studyCID, String datasetCID,
            Path file, SiemensRawMRFileInfo fileInfo, Date expireDate, boolean checksum, TransferProgressListener pl)
            throws Throwable {
        final String fileName = file.getFileName().toString();
        final long fileLength = Files.size(file);
        if (fileLength == 0) {
            throw new IOException("File length is 0. File: '" + file + "'");
        }

        XmlStringWriter w = new XmlStringWriter();
        if (datasetCID != null) {
            w.add("id", datasetCID);
        } else {
            w.add("pid", studyCID);
        }

        w.add("name", fileName); // Original filename
        w.add("description", "Raw Siemens DataSet");
        w.add("filename", fileName); // Original filename

        // mime type
        w.add("type", SIEMENS_RAW_MR_SERIES_MIME_TYPE);

        // meta
        w.push("meta");
        addSiemensRawMRMetadata(w, fileInfo, expireDate);
        w.pop();

        try (InputStream is = Files.newInputStream(file);
                BufferedInputStream bis = new BufferedInputStream(is);
                InputStream in = checksum ? new CheckedInputStream(bis, new CRC32()) : bis) {
            ServerClient.Input input = new ServerClient.GeneratedInput(null, file.toAbsolutePath().toString(),
                    fileLength) {

                @Override
                protected void copyTo(OutputStream out, AbortCheck ac) throws Throwable {
                    unimelb.io.StreamCopy.copy(in, fileLength, out, pl, new unimelb.io.AbortCheck() {

                        @Override
                        public boolean aborted() {
                            if (ac != null) {
                                return ac.hasBeenAborted();
                            }
                            return false;
                        }
                    });
                }
            };
            String service = datasetCID != null ? "om.pssd.dataset.primary.update" : "om.pssd.dataset.primary.create";
            XmlDoc.Element re = session.execute(service, w.document(), input);
            String cid = datasetCID != null ? datasetCID : re.value("id");
            if (checksum) {
                long fileCRC32 = ((CheckedInputStream) in).getChecksum().getValue();
                long assetCRC32 = -1L;

                XmlDoc.Element ae = AssetUtils.getAssetMetaByCid(session, cid);
                if (ae.elementExists("content/csum")) {
                    assetCRC32 = ae.longValue("content/csum[@base='10']");
                }
                if (fileCRC32 != assetCRC32) {
                    throw new IOException("Checksum mismatch. File: '" + file + "' has CRC32 checksum: " + fileCRC32
                            + " but asset " + cid + " has CRC32 checksum: " + assetCRC32);
                }
            }
            return cid;
        }
    }

    public static String createSiemensRawMRSeries(MFSession session, String studyCID, Path file,
            SiemensRawMRFileInfo fileInfo, Date expireDate, boolean checksum, TransferProgressListener pl)
            throws Throwable {
        return createOrUpdateSiemensRawMRSeries(session, studyCID, null, file, fileInfo, expireDate, checksum, pl);
    }

    public static String updateSiemensRawMRSeries(MFSession session, String datasetCID, Path file,
            SiemensRawMRFileInfo fileInfo, Date expireDate, boolean checksum, TransferProgressListener pl)
            throws Throwable {
        return createOrUpdateSiemensRawMRSeries(session, null, datasetCID, file, fileInfo, expireDate, checksum, pl);
    }

    public static String findSiemensRawPETCTSeries(MFSession session, String parentCID, Path file) throws Throwable {
        String fileName = file.getFileName().toString();

        StringBuilder where = new StringBuilder();
        if (parentCID != null) {
            where.append("cid starts with '").append(parentCID).append("' and ");
        }
        where.append("model='om.pssd.dataset'");
        where.append(" and xpath(daris:pssd-filename/original)='").append(fileName).append("'");
        where.append(" and ").append(SIEMENS_RAW_PETCT_SERIES_DOC_TYPE).append(" has value");

        XmlStringWriter w = new XmlStringWriter();
        w.add("where", where.toString());
        w.add("action", "get-cid");
        w.add("pdist", "0");
        w.add("size", 2);
        XmlDoc.Element re = session.execute("asset.query", w.document());
        String cid = re.value("cid");
        if (cid != null && re.count("cid") > 1) {
            throw new Exception("multiple Siemens raw datasets with name '" + fileName + "'found in '" + parentCID
                    + "'. This is most likely an error and needs to be remedied.");
        }
        return cid;
    }

    private static void addSiemensRawPETCTMetadata(XmlWriter w, SiemensRawPETCTFileInfo fileInfo, Date expireDate)
            throws Throwable {
        w.push(SIEMENS_RAW_PETCT_SERIES_DOC_TYPE);
        if (fileInfo.seriesInstanceUID() != null) {
            w.add("uid", fileInfo.seriesInstanceUID());
        }
        if (fileInfo.protocolName() != null) {
            w.add("protocol", fileInfo.protocolName());
        }
        if (fileInfo.acquisitionDate() != null) {
            w.add("date", fileInfo.acquisitionDate());
        }
        if (fileInfo.modality() != null) {
            w.add("modality", fileInfo.modality());
        }
        if (fileInfo.seriesDescription() != null) {
            w.add("description", fileInfo.seriesDescription());
        }
        if (fileInfo.seriesNumber() != null) {
            w.add("series_number", fileInfo.modality());
        }
        if (fileInfo.imageType() != null && fileInfo.imageType().length >= 3) {
            w.add("type", fileInfo.imageType()[2]);
        }
        if (fileInfo.instanceNumber() != null) {
            w.add("instance", fileInfo.instanceNumber());
        }
        if (fileInfo.exportDate() != null) {
            w.add("date-export", fileInfo.exportDate());
        }
        if (expireDate != null) {
            w.add("date-expire", expireDate);
        }
        w.pop();
    }

    private static String createOrUpdateSiemensRawPETCTSeries(MFSession session, String studyCID, String datasetCID,
            SiemensRawPETCTFileInfo fileInfo, Date expireDate, boolean checksum, TransferProgressListener pl)
            throws Throwable {
        Path file = fileInfo.file();
        String fileName = file.toFile().getName();
        String fileExt = PathUtils.getFileExtension(fileName);
        long fileSize = Files.size(file);
        if (fileSize == 0) {
            throw new IOException("File length is 0. File: '" + file + "'");
        }
        String assetNS = AssetUtils.getAssetMetaByCid(session, datasetCID != null ? datasetCID : studyCID)
                .value("namespace");
        String assetStore = AssetNamespaceUtils.getStore(session, assetNS);

        XmlStringWriter w = new XmlStringWriter();
        if (datasetCID != null) {
            // update
            w.add("id", datasetCID);
        } else {
            // create
            w.add("pid", studyCID);
        }

        w.add("name", fileName); // Original filename
        w.add("description", "Siemens raw PET/CT data");
        w.add("filename", fileName); // Original filename

        // mime type
        w.add("type", SIEMENS_RAW_PETCT_SERIES_MIME_TYPE);

        // meta
        w.push("meta");
        addSiemensRawPETCTMetadata(w, fileInfo, expireDate);
        w.pop();

        try (InputStream is = Files.newInputStream(file);
                BufferedInputStream bis = new BufferedInputStream(is);
                InputStream in = checksum ? new CheckedInputStream(bis, new CRC32()) : bis) {

            ServerClient.Input input = new MFGeneratedInput(null, fileExt, file.toAbsolutePath().toString(), fileSize,
                    assetStore == null ? null : ("asset:" + assetStore)) {

                @Override
                protected void consume(OutputStream out, AbortCheck ac) throws Throwable {
                    unimelb.io.StreamCopy.copy(in, fileSize, out, pl, new unimelb.io.AbortCheck() {

                        @Override
                        public boolean aborted() {
                            if (ac != null) {
                                return ac.hasBeenAborted();
                            }
                            return false;
                        }
                    });
                }
            };

            String service = datasetCID != null ? "om.pssd.dataset.primary.update" : "om.pssd.dataset.primary.create";
            XmlDoc.Element re = session.execute(service, w.document(), input);
            String cid = datasetCID != null ? datasetCID : re.value("id");
            XmlDoc.Element ae = AssetUtils.getAssetMetaByCid(session, cid);
            long assetCSize = ae.longValue("content/size");
            if (assetCSize != fileSize) {
                throw new IOException("File size mismatch. File: '" + file + "' size=" + fileSize + " but asset " + cid
                        + " content size=" + assetCSize);
            }
            if (checksum) {
                long fileCRC32 = ((CheckedInputStream) in).getChecksum().getValue();
                long assetCRC32 = -1L;

                if (ae.elementExists("content/csum")) {
                    assetCRC32 = ae.longValue("content/csum[@base='10']");
                }
                if (fileCRC32 != assetCRC32) {
                    throw new IOException("Checksum mismatch. File: '" + file + "' has CRC32 checksum: " + fileCRC32
                            + " but asset " + cid + " has CRC32 checksum: " + assetCRC32);
                }
            }
            return cid;
        }
    }

    public static String createSiemensRawPETCTSeries(MFSession session, String studyCID,
            SiemensRawPETCTFileInfo fileInfo, Date expireDate, boolean checksum, TransferProgressListener pl)
            throws Throwable {
        return createOrUpdateSiemensRawPETCTSeries(session, studyCID, null, fileInfo, expireDate, checksum, pl);
    }

    public static String updateSiemensRawPETCTSeries(MFSession session, String datasetCID,
            SiemensRawPETCTFileInfo fileInfo, Date expireDate, boolean checksum, TransferProgressListener pl)
            throws Throwable {
        return createOrUpdateSiemensRawPETCTSeries(session, null, datasetCID, fileInfo, expireDate, checksum, pl);
    }

}

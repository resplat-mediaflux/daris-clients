package unimelb.daris.client.model.dataset;

import java.util.Collection;

import arc.xml.XmlDoc;
import arc.xml.XmlStringWriter;
import unimelb.mf.client.session.MFSession;

public class DicomDataset {

    public static XmlDoc.Element getDicomMetaData(MFSession session, String assetId) throws Throwable {
        XmlStringWriter w = new XmlStringWriter();
        w.add("id", assetId);
        return session.execute("dicom.metadata.get", w.document());
    }

    public static String getDicomElementValue(XmlDoc.Element dicomMetaData, String tag) throws Throwable {
        Collection<String> values = dicomMetaData.values(String.format("de[@tag='%s']/value", tag));
        if (values == null || values.isEmpty()) {
            return null;
        }
        if (values.size() == 1) {
            return values.iterator().next();
        } else {
            return String.join(",", values);
        }
    }

}

package unimelb.daris.dicom.data;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Objects;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;

public class DicomFileInfo implements Comparable<DicomFileInfo> {
    public final Path path;

    public final long length;
    public final long datasetOffset;
    public final String mediaStorageSOPClassUID;
    public final String mediaStorageSOPInstanceUID;
    public final String transferSyntaxUID;
    public final String studyInstanceUID;
    public final String seriesInstanceUID;
    public final int seriesNumber;
    public final String sopInstanceUID;
    public final int instanceNumber;
    public final LocalDateTime instanceCreationTime;

    DicomFileInfo(Path path, long datasetOffset, Attributes fileMetaInformation, Attributes dataset) throws Exception {
        this.path = path;
        this.length = Files.size(this.path);
        this.datasetOffset = datasetOffset;
        this.mediaStorageSOPClassUID = fileMetaInformation.getString(Tag.MediaStorageSOPClassUID);
        this.mediaStorageSOPInstanceUID = fileMetaInformation.getString(Tag.MediaStorageSOPInstanceUID);
        this.transferSyntaxUID = fileMetaInformation.getString(Tag.TransferSyntaxUID);
        this.studyInstanceUID = dataset.getString(Tag.StudyInstanceUID);
        this.seriesInstanceUID = dataset.getString(Tag.SeriesInstanceUID);
        this.seriesNumber = dataset.getInt(Tag.SeriesNumber, 0);
        this.sopInstanceUID = dataset.getString(Tag.SOPInstanceUID);
        this.instanceNumber = dataset.getInt(Tag.InstanceNumber, 0);
        this.instanceCreationTime = parseInstanceCreationTime(dataset);
    }

    @Override
    public int compareTo(DicomFileInfo fi) {
        if (fi == null) {
            return 1;
        }
        if (this == fi) {
            return 0;
        }

        int r = this.studyInstanceUID.compareTo(fi.studyInstanceUID);
        if (r != 0) {
            // different studies
            return r;
        }

        r = this.seriesInstanceUID.compareTo(fi.seriesInstanceUID);
        if (r != 0) {
            // different series
            int r1 = Integer.compare(this.seriesNumber, fi.seriesNumber);
            if (r1 != 0) {
                return r1;
            } else {
                return r;
            }
        }

        r = Integer.compare(this.instanceNumber, fi.instanceNumber);
        if (r != 0) {
            // different instance numbers
            return r;
        }

        // instance numbers same - check instanceCreationTime
        if (this.instanceCreationTime != null && fi.instanceCreationTime != null) {
            r = this.instanceCreationTime.compareTo(fi.instanceCreationTime);
        }
        if (r != 0) {
            return r;
        }

        return this.sopInstanceUID.compareTo(fi.sopInstanceUID);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DicomFileInfo) {
            DicomFileInfo dfi = (DicomFileInfo) o;
            return this.sopInstanceUID.equals(dfi.sopInstanceUID)
                    && Objects.equals(this.instanceCreationTime, dfi.instanceCreationTime);
        }
        return false;
    }

    @Override
    public int hashCode() {
        String s = this.seriesInstanceUID;
        if (this.instanceCreationTime != null) {
            s += " " + this.instanceCreationTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        }
        return s.hashCode();
    }

    private static LocalDateTime parseInstanceCreationTime(Attributes attrs) {

        // https://dicom.nema.org/medical/dicom/current/output/chtml/part05/sect_6.2.html

        // Date (VR: DA) YYYYMMDD (yyyyMMdd)
        String date = attrs.getString(Tag.InstanceCreationDate);
        if (date != null && date.length() != 8) {
            throw new DateTimeParseException("Failed to parse date from: '" + date + "'", date, 0);
        }

        // Time (VR: TM) HHMMSS.FFFFFF&ZZXX (HHmmss.SSSSSSZ)
        String time = attrs.getString(Tag.InstanceCreationTime);
        if (time == null || time.isEmpty()) {
            return null;
        }

        // parse TimeZone part e.g. 123011.123000+0730
        String timeZone = null;
        int tzIdx = time.indexOf('+');
        if (tzIdx == -1) {
            tzIdx = time.indexOf('-');
        }
        if (tzIdx != -1) {
            timeZone = time.substring(tzIdx);
            timeZone = padWithZero(timeZone, 5);
            if (timeZone.length() != 5) {
                throw new DateTimeParseException("Failed to parse time from: '" + time + "'", time, time.indexOf('.'));
            }
            time = time.substring(0, tzIdx);
        }

        if (time.indexOf('.') != -1) {
            // has fraction part
            String[] parts = time.split("\\.");
            if (parts.length != 2) {
                throw new DateTimeParseException("Failed to parse time from: '" + time + "'", time, time.indexOf('.'));
            }
            String hhmmss = parts[0];
            hhmmss = padWithZero(hhmmss, 6);
            String ffffff = parts[1];
            ffffff = padWithZero(ffffff, 6);
            if (hhmmss.length() != 6 || ffffff.length() != 6) {
                throw new DateTimeParseException("Failed to parse time from: '" + time + "'", time,
                        time.indexOf('.') + 1);
            }
            time = hhmmss + "." + ffffff;
        } else {
            // has no fraction part
            if (time.length() > 6 || time.length() % 2 != 0) {
                throw new DateTimeParseException("Failed to parse time from: '" + time + "'", time, 0);
            } else if (time.length() < 6) {
                time = padWithZero(time, 6);
            }
            time += ".000000";
        }
        if (timeZone != null) {
            time += timeZone;
        }

        String dateTime;
        String dateTimeFormatterPattern;
        if (date == null) {
            dateTime = time;
            dateTimeFormatterPattern = "HHmmss.SSSSSS";
        } else {
            dateTime = date + time;
            dateTimeFormatterPattern = "yyyyMMddHHmmss.SSSSSS";
        }
        if (timeZone != null) {
            dateTimeFormatterPattern += "Z";
        }
        return LocalDateTime.parse(dateTime, DateTimeFormatter.ofPattern(dateTimeFormatterPattern));
    }

    private static String padWithZero(String s, int totalLength) {
        StringBuilder sb = new StringBuilder(s == null ? "" : s);
        if (sb.length() >= totalLength) {
            return sb.toString();
        }
        while (sb.length() < totalLength) {
            sb.append("0");
        }
        return sb.toString();
    }
}
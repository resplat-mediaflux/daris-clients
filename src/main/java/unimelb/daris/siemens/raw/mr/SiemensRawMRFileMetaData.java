package unimelb.daris.siemens.raw.mr;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import unimelb.daris.siemens.raw.mr.SiemensRawMRFile.Version;
import unimelb.io.BinaryInputStream;
import unimelb.io.SizedInputStream;
import unimelb.io.StreamUtils;
import unimelb.utils.StringUtils;

/**
 * NOTE: This is an incomplete parser. It only parses the primitive param types.
 * 
 * @author wliu5
 *
 */
public class SiemensRawMRFileMetaData {

	private static final Logger logger = LoggerFactory.getLogger(SiemensRawMRFileMetaData.class);

	public static interface Node {

		String type();

		String name();

	}

	public static interface ElementHandler {
		void processElement(String groupName, Element element);
	}

	public static class Element implements Node {

		String type;
		String name;
		List<Attribute> attributes;
		List<String> values;
		List<Element> elements;

		public final String type() {
			return this.type;
		}

		public final String name() {
			return this.name;
		}

		public final boolean hasName() {
			return this.name != null && !this.name.isEmpty();
		}

		public final List<Attribute> attributes() {
			return this.attributes == null ? null : Collections.unmodifiableList(this.attributes);
		}

		public final boolean hasAttribute() {
			return this.attributes != null && !this.attributes.isEmpty();
		}

		public final String value() {
			return (this.values == null || this.values.isEmpty()) ? null : this.values.get(0);
		}

		public final List<String> values() {
			return this.values == null ? null : Collections.unmodifiableList(this.values);
		}

		final void addValue(String value) {
			if (this.values == null) {
				this.values = new ArrayList<String>();
			}
			this.values.add(value);
		}

		public final boolean hasValue() {
			return this.values != null && !this.values.isEmpty();
		}

		public final List<Element> element() {
			return this.elements == null ? null : Collections.unmodifiableList(this.elements);
		}

		public final boolean hasElement() {
			return this.elements != null && !this.elements.isEmpty();
		}

	}

	public static class Attribute implements Node {

		String type;
		String name;
		String value;

		@Override
		public final String type() {
			return this.type;
		}

		@Override
		public final String name() {
			return this.name;
		}

		public final String value() {
			return this.value;
		}
	}

	private String fileName;
	private Version version;
	private Integer mid;
	private Integer fid;
	private String protocolName;
	private Map<String, List<Element>> groupElements;

	public final List<Element> getGroup(String groupName) {
		if (this.groupElements != null) {
			List<Element> es = this.groupElements.get(groupName);
			return es == null ? null : Collections.unmodifiableList(es);
		}
		return null;
	}

	public final Set<String> getGroupNames() {
		return this.groupElements == null ? null : this.groupElements.keySet();
	}

	public final Element getElement(String group, String type, String name) {

		if (type == null && name == null) {
			throw new IllegalArgumentException("either element type or name must be supplied.");
		}

		if (this.groupElements != null && !this.groupElements.isEmpty()) {
			if (group != null) {
				List<Element> elements = this.groupElements.get(group);
				if (elements != null) {
					for (Element e : elements) {
						if ((type == null || type.equals(e.type())) && (name == null || name.equals(e.name()))) {
							return e;
						}
					}
				}
			} else {
				Collection<List<Element>> ess = this.groupElements.values();
				for (List<Element> es : ess) {
					for (Element e : es) {
						if ((type == null || type.equals(e.type())) && (name == null || name.equals(e.name()))) {
							return e;
						}
					}
				}
			}
		}
		return null;
	}

	private void addElement(String group, Element e) {
		if (this.groupElements == null) {
			this.groupElements = new LinkedHashMap<String, List<Element>>();
		}
		List<Element> es = this.groupElements.get(group);
		if (es == null) {
			es = new ArrayList<Element>();
			this.groupElements.put(group, es);
		}
		es.add(e);
	}

	public String fileName() {
		return this.fileName;
	}

	public Version version() {
		return this.version;
	}

	public int mid() {
		return this.mid;
	}

	public int fid() {
		return this.fid;
	}

	public String protocolNameFromFileName() {
		return this.protocolName;
	}

	public String patientName() {
		Element e = getElement("Config", "ParamString", "tPatientName");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String patientID() {
		Element e = getElement("Config", "ParamString", "PatientID");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String patientBirthDate() {
		Element e = getElement("Config", "ParamString", "PatientBirthDay");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String patientSex() {
		Element e = getElement("Config", "ParamLong", "PatientSex");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String studyDescription() {
		Element e = getElement("Config", "ParamString", "tStudyDescription");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String patientPosition() {
		Element e = getElement("Config", "ParamString", "PatientPosition");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String frameOfReference() {
		Element e = getElement("Config", "ParamString", "FrameOfReference");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String softwareVersions() {
		Element e = getElement("Dicom", "ParamString", "SoftwareVersions");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String manufacturer() {
		Element e = getElement("Dicom", "ParamString", "Manufacturer");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String manufacturerModelName() {
		Element e = getElement("Dicom", "ParamString", "ManufacturersModelName");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String institutionAddress() {
		Element e = getElement("Dicom", "ParamString", "InstitutionAddress");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String deviceSeriesNumber() {
		Element e = getElement("Dicom", "ParamString", "DeviceSerialNumber");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String institutionName() {
		Element e = getElement("Dicom", "ParamString", "InstitutionName");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String modality() {
		Element e = getElement("Dicom", "ParamString", "Modality");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String protocolName() {
		Element e = getElement("Dicom", "ParamString", "tProtocolName");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String magneticFieldStrength() {
		Element e = getElement("Dicom", "ParamDouble", "flMagneticFieldStrength");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String transmitCoilName() {
		Element e = getElement("Dicom", "ParamString", "TransmittingCoil");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String flipAngle() {
		Element e = getElement("Dicom", "ParamDouble", "adFlipAngleDegree");
		if (e != null) {
			return e.value();
		}
		return null;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("FileName: ").append(this.fileName).append(System.lineSeparator());
		sb.append("FileVersion: ").append(this.version).append(System.lineSeparator());
		sb.append("MID: ").append(this.mid).append(System.lineSeparator());
		sb.append("FID: ").append(this.fid).append(System.lineSeparator());
		String v = this.protocolName();
		if (v != null) {
			sb.append("ProtocolName: ").append(v).append(System.lineSeparator());
		}
		v = this.modality();
		if (v != null) {
			sb.append("Modality: ").append(v).append(System.lineSeparator());
		}
		v = this.patientName();
		if (v != null) {
			sb.append("PatientName: ").append(v).append(System.lineSeparator());
		}
		v = this.patientID();
		if (v != null) {
			sb.append("PatientID: ").append(v).append(System.lineSeparator());
		}
		v = this.patientSex();
		if (v != null) {
			sb.append("PatientSex: ").append(v).append(System.lineSeparator());
		}
		v = this.patientBirthDate();
		if (v != null) {
			sb.append("PatientBirthDate: ").append(v).append(System.lineSeparator());
		}
		v = this.studyDescription();
		if (v != null) {
			sb.append("StudyDescription: ").append(v).append(System.lineSeparator());
		}
		v = this.patientPosition();
		if (v != null) {
			sb.append("PatientPosition: ").append(v).append(System.lineSeparator());
		}
		v = this.frameOfReference();
		if (v != null) {
			sb.append("FrameOfReference: ").append(v).append(System.lineSeparator());
		}
		v = this.manufacturer();
		if (v != null) {
			sb.append("Manufacturer: ").append(v).append(System.lineSeparator());
		}
		v = this.manufacturerModelName();
		if (v != null) {
			sb.append("ManufacturerModelName: ").append(v).append(System.lineSeparator());
		}
		v = this.deviceSeriesNumber();
		if (v != null) {
			sb.append("DeviceSeriesNumber: ").append(v).append(System.lineSeparator());
		}
		v = this.institutionName();
		if (v != null) {
			sb.append("InstitutionName: ").append(v).append(System.lineSeparator());
		}
		v = this.institutionAddress();
		if (v != null) {
			sb.append("InstitutionAddress: ").append(v).append(System.lineSeparator());
		}
		v = this.magneticFieldStrength();
		if (v != null) {
			sb.append("MagneticFieldStrength: ").append(v).append(System.lineSeparator());
		}
		v = this.transmitCoilName();
		if (v != null) {
			sb.append("TransmitCoilName: ").append(v).append(System.lineSeparator());
		}
		v = this.flipAngle();
		if (v != null) {
			sb.append("FlipAngle: ").append(v).append(System.lineSeparator());
		}
		return sb.toString();
	}

	public static SiemensRawMRFileMetaData parse(Path f, String... groups) throws Throwable {

		try (InputStream in = new BufferedInputStream(Files.newInputStream(f))) {
			return parse(in, f.getFileName().toString(), groups);
		}
	}

	private static SiemensRawMRFileMetaData parse(InputStream in, String fileName, String... groups)
			throws IOException {

		SiemensRawMRFileMetaData md = new SiemensRawMRFileMetaData();
		md.fileName = fileName;

		long vb15HeaderOffset = 0;

		byte[] buf = new byte[32];
		in.mark(SiemensRawMRFile.VB19_EXT_HEADER_LENGTH);
		StreamUtils.readFully(in, buf);
		try {
			int mid = ByteBuffer.wrap(buf, 8, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
			int fid = ByteBuffer.wrap(buf, 12, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
			long offset = ByteBuffer.wrap(buf, 16, 8).order(ByteOrder.LITTLE_ENDIAN).getLong();
			if (fid >= 0 && mid >= 0 && offset == SiemensRawMRFile.VB19_EXT_HEADER_LENGTH) {
				// is VB19
				md.version = Version.VB19;
				md.mid = mid;
				md.fid = fid;
				// offset: 0x20 (32)
				StreamUtils.skip(in, 64);
				// offset: 0x60 (96)
				md.protocolName = StreamUtils.readString(in);
				vb15HeaderOffset = offset;
			} else if (buf[8] == (byte) 'C' && buf[9] == (byte) 'o' && buf[10] == (byte) 'n' && buf[11] == (byte) 'f'
					&& buf[12] == (byte) 'i' && buf[13] == (byte) 'g') {
				// is VB15
				md.version = Version.VB15;
				md.mid = SiemensRawMRFile.getMIDFromFileName(fileName);
				md.fid = SiemensRawMRFile.getFIDFromFileName(fileName);
				md.protocolName = SiemensRawMRFile.getSequenceNameFromFileName(fileName);
			}
		} finally {
			in.reset();
		}

		if (md.version == null) {
			throw new IOException("Failed to parse meas VB version from file: " + fileName);
		}

		parse(in, vb15HeaderOffset, (group, element) -> md.addElement(group, element), groups);
		return md;
	}

	public static void parse(InputStream in, long vb15HeaderOffset, ElementHandler eh, String... groups) throws IOException {

		Set<String> gs = null;
		if (groups != null && groups.length > 0) {
			gs = new HashSet<String>(groups.length);
			for (String group : groups) {
				gs.add(group.toLowerCase());
			}
		}

		try (BinaryInputStream bis = new BinaryInputStream(in, ByteOrder.LITTLE_ENDIAN, false)) {

			if (vb15HeaderOffset > 0) {
				bis.skipFully(vb15HeaderOffset);
			}

			long offset = bis.bytesCount();
			logger.trace(String.format("%08XH: groups.begin", offset));

			int groupsLength = bis.readInt();
			logger.trace(String.format("%08XH: groups.length: %d", offset, groupsLength));

			offset = bis.bytesCount();
			int groupsCount = bis.readInt();
			logger.trace(String.format("%08XH: groups.count: %d", offset, groupsCount));

			for (int i = 0; i < groupsCount; i++) {
				offset = bis.bytesCount();
				String groupName = StreamUtils.readString(bis);
				logger.trace(String.format("%08XH: group.name: %s", offset, groupName));

				offset = bis.bytesCount();
				int groupLength = bis.readInt();
				logger.trace(String.format("%08XH: group.length: %s", offset, groupLength));
				if (gs == null || gs.contains(groupName.toLowerCase())) {
					parseGroup(bis, groupName, groupLength, eh);
				} else {
					bis.skipFully(groupLength);
				}
			}
			offset = bis.bytesCount();
			logger.trace(String.format("%08XH: groups.end", offset));
		}
	}

	private static void parseGroup(BinaryInputStream bis, String groupName, int groupLength, ElementHandler eh)
			throws IOException {
		try (SizedInputStream sis = new SizedInputStream(bis, groupLength, false);
				InputStreamReader isr = new InputStreamReader(sis);
				BufferedReader br = new BufferedReader(isr)) {
			String line;
			while ((line = br.readLine()) != null) {
				if (line.startsWith("### ASCCONV BEGIN")) {
					do {
						line = br.readLine();
					} while (line != null && !line.startsWith("### ASCCONV END ###"));
				}
				Element e = new Element();
				if (line.matches(".*<(.*?)>.*")) {
					Pattern p = Pattern.compile("<(.*?)>");
					Matcher m = p.matcher(line);
					if (m.find()) {
						String[] tag = parseTag(m.group(1));
						e.type = tag[0];
						e.name = tag[1];
					}
					if (line.matches(".*<(.*?)>.*\\{(.*?)\\}.*")) {
						p = Pattern.compile("\\{(.*?)\\}");
						m = p.matcher(line);
						if (m.find()) {
							parseElement(e, m.group(1));
						}
						if (e.type().startsWith("Param")) {
							logger.trace("matching line: " + line);
							eh.processElement(groupName, e);
						}
					} else if (line.matches(".*<(.*?)>\\ *")) {
						// <ParamArray."B">
						// TODO not implemented
					} else if (line.matches(".*<(.*?)>\\ *([^\\{\\ ]+)\\ *")) {
						// <Class> "MeasContext@MrParc"
						// TODO not implemented
					}
				}
			}
		}
	}

	private static void parseElement(Element e, String s) {
		s = s.trim();
		while (!s.isEmpty()) {
			int idx;
			if (s.startsWith("\"")) {
				idx = s.indexOf("\"", 1);
				e.addValue(s.substring(1, idx));
				s = s.substring(idx + 1).trim();
			} else if (s.startsWith("<")) {
				idx = s.indexOf(">", 1);
				String[] tag = parseTag(s.substring(1, idx));
				Attribute a = new Attribute();
				a.type = tag[0];
				a.name = tag[1];
				s = s.substring(idx + 1).trim();
				if (s.startsWith("\"")) {
					idx = s.indexOf("\"", 1);
					a.value = s.substring(1, idx);
					s = s.substring(idx + 1).trim();
				} else {
					idx = s.indexOf(' ');
					if (idx == -1) {
						a.value = s;
						s = "";
					} else {
						a.value = s.substring(0, idx);
						s = s.substring(idx + 1).trim();
					}
				}
			} else {
				idx = s.indexOf(' ');
				if (idx == -1) {
					e.addValue(s);
					s = "";
				} else {
					e.addValue(s.substring(0, idx));
					s = s.substring(idx + 1).trim();
				}
			}
		}
	}

	private static String[] parseTag(String tag) {
		int idx = tag.indexOf('.');
		if (idx == -1) {
			return new String[] { tag, null };
		}
		String type = tag.substring(0, idx);
		String name = StringUtils.trim(tag.substring(idx + 1), '"');
		return new String[] { type, name };
	}

	public static void main(String[] args) throws Throwable {

		// @formatter:off
//        Log4j2Utils.setLogLevel(SiemensRawMRFileMetaData.class, Level.TRACE);
//        SiemensRawMRFileMetaData md = parse(Paths.get("/Users/wliu5/Downloads/Samples/meas_MID00014_FID00893_AdjDico.dat"),
//                "Config", "Dicom");
//        System.out.println(md);
        // @formatter:on
	}
}

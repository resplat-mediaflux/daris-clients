package unimelb.daris.siemens.raw.mr;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Set;

import unimelb.io.BinaryInputStream;
import unimelb.io.CountedInputStream;
import unimelb.io.SizedInputStream;
import unimelb.io.StreamUtils;
import unimelb.utils.PathUtils;

public class SiemensRawMRFile {

	public static final String FILE_NAME_PATTERN_VB15 = "^meas_MID\\d+.*_FID\\d+.dat$";
	public static final String FILE_NAME_PATTERN_VB19 = "^meas_MID\\d+_FID\\d+_.*.dat$";

	public static final int VB19_EXT_HEADER_LENGTH = 0x2800;

	public static final String GROUP_CONFIG = "Config";
	public static final String GROUP_DICOM = "Dicom";
	public static final String GROUP_MEAS = "Meas";
	public static final String GROUP_MEASYAPS = "MeasYaps";
	public static final String GROUP_PHOENIX = "Phoenix";
	public static final String GROUP_SPICE = "Spice";

	public enum Version {

		VB15(SiemensRawMRFile.FILE_NAME_PATTERN_VB15), VB19(SiemensRawMRFile.FILE_NAME_PATTERN_VB19);

		public final String fileNamePattern;

		Version(String pattern) {
			this.fileNamePattern = pattern;
		}

		public static Version fromFileName(String fileName) {
			if (fileName != null) {
				Version[] vs = values();
				for (Version v : vs) {
					if (fileName.matches(v.fileNamePattern)) {
						return v;
					}
				}
			}
			return null;
		}

	}

	public static Version getVersionFromFileName(String path) {
		if (path != null) {
			String fileName = PathUtils.getFileName(path);
			return Version.fromFileName(fileName);
		}
		return null;
	}

	public static Version getVersionFromFileName(Path path) {
		if (path != null) {
			String fileName = path.getFileName().toString();
			return Version.fromFileName(fileName);
		}
		return null;
	}

	public static Version getVersionFromFileName(File file) {
		return getVersionFromFileName(file.toPath());
	}

	public static Integer getFIDFromFileName(String fileName) {
		return getIDFromFileName("FID", fileName);
	}

	public static Integer getMIDFromFileName(String fileName) {
		return getIDFromFileName("MID", fileName);
	}

	private static Integer getIDFromFileName(String prefix, String fileName) {
		int idx = fileName.indexOf(prefix);
		if (idx != -1) {
			idx += prefix.length();
			char c = fileName.charAt(idx);
			StringBuilder sb = new StringBuilder();
			while (Character.isDigit(c)) {
				sb.append(c);
				idx += 1;
				c = fileName.charAt(idx);
			}
			if (sb.length() > 0) {
				return Integer.parseInt(sb.toString());
			}
		}
		return null;
	}

	public static String getSequenceNameFromFileName(String path) {
		if (path != null) {
			String fileName = PathUtils.getFileName(path);
			if (fileName.matches(FILE_NAME_PATTERN_VB15)) {
				// meas_MID27_FLASH_TEST_noGrad_mainNucOnly_23Na_DevOn_OnRes_FID7544.dat
				return fileName.replaceAll("^meas_MID\\d+_", "").replaceAll("_FID\\d+.dat$", "");
			} else if (fileName.matches(FILE_NAME_PATTERN_VB19)) {
				// meas_MID00023_FID00904_ep2d_pasl.dat
				return fileName.replaceAll("^meas_MID\\d+_FID\\d+_", "").replaceAll(".dat$", "");
			}
		}
		return null;
	}

	public static Version getVersionFromMetaData(InputStream in, String fileName) throws IOException {
		Integer fidFromFileName = getFIDFromFileName(fileName);
		Integer midFromFileName = getMIDFromFileName(fileName);

		byte[] buf = new byte[32];
		in.mark(32);
		StreamUtils.readFully(in, buf);
		try {
			// is VB19?
			int mid = ByteBuffer.wrap(buf, 8, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
			int fid = ByteBuffer.wrap(buf, 12, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
			long offset = ByteBuffer.wrap(buf, 16, 8).order(ByteOrder.LITTLE_ENDIAN).getLong();
			if (fid >= 0 && mid >= 0 && offset == VB19_EXT_HEADER_LENGTH) {
				if ((fidFromFileName == null || fid == fidFromFileName)
						&& (midFromFileName == null || mid == midFromFileName)) {
					return Version.VB19;
				}
			}

			// is VB15?
			if (buf[8] == (byte) 'C' && buf[9] == (byte) 'o' && buf[10] == (byte) 'n' && buf[11] == (byte) 'f'
					&& buf[12] == (byte) 'i' && buf[13] == (byte) 'g') {
				return Version.VB15;
			}

			throw new IOException("Failed to parse meas VB version from file: " + fileName);
		} finally {
			in.reset();
		}
	}

	public static Version getVersion(Path file, boolean readMetaData) throws IOException {
		String fileName = file.getFileName().toString();
		Version version = Version.fromFileName(fileName);
		Integer fidFromFileName = getFIDFromFileName(fileName);
		Integer midFromFileName = getMIDFromFileName(fileName);
		if (version != null && fidFromFileName != null && midFromFileName != null && !readMetaData) {
			return version;
		}
		try (InputStream in = Files.newInputStream(file);
				BufferedInputStream bis = new BufferedInputStream(in);
				DataInputStream dis = new DataInputStream(bis)) {
			return getVersionFromMetaData(dis, fileName);
		}
	}

	public static boolean isFileNameValid(Path path) {
		return getVersionFromFileName(path) != null;
	}

	public static boolean isFileNameValid(File path) {
		return getVersionFromFileName(path) != null;
	}

	public static void dump(Path f, Path of, boolean metadata, String... groups) throws IOException {
		try (OutputStream os = Files.newOutputStream(of);
				OutputStreamWriter osw = new OutputStreamWriter(os);
				BufferedWriter bw = new BufferedWriter(osw)) {
			dump(f, bw, metadata, groups);
		}
	}

	public static void dump(Path f, OutputStream out, boolean metadata, String... groups) throws IOException {
		long fileSize = Files.size(f);
		try (InputStream in = Files.newInputStream(f);
				BufferedInputStream bis = new BufferedInputStream(in);
				CountedInputStream cis = new CountedInputStream(bis)) {
			try (OutputStreamWriter osw = new OutputStreamWriter(out) {
				public void close() throws IOException {
					// do not propagate close OutputStream
					flush();
				}
			}; BufferedWriter bw = new BufferedWriter(osw)) {
				dump(cis, f.getFileName().toString(), bw, metadata, groups);
				writeln(String.format("[%08XH] data.length=%d", cis.bytesCount(), fileSize - cis.bytesCount()), bw);
			}
		}
	}

	public static void dump(Path f, Writer w, boolean metadata, String... groups) throws IOException {
		long fileSize = Files.size(f);
		try (InputStream in = Files.newInputStream(f);
				BufferedInputStream bis = new BufferedInputStream(in);
				CountedInputStream cis = new CountedInputStream(bis)) {
			dump(cis, f.getFileName().toString(), w, metadata, groups);
			writeln(String.format("[%08XH] data.length=%d", cis.bytesCount(), fileSize - cis.bytesCount()), w);
		}
	}

	private static void dump(InputStream in, String fileName, Writer w, boolean metadata, String... groups)
			throws IOException {

		Version version = null;
		Integer mid = null;
		Integer fid = null;
		String protocolName = null;
		long vb15HeaderOffset = 0;
		byte[] buf = new byte[32];
		in.mark(VB19_EXT_HEADER_LENGTH);
		StreamUtils.readFully(in, buf);
		try {
			mid = ByteBuffer.wrap(buf, 8, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
			fid = ByteBuffer.wrap(buf, 12, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
			long offset = ByteBuffer.wrap(buf, 16, 8).order(ByteOrder.LITTLE_ENDIAN).getLong();
			if (fid != null && fid >= 0 && mid != null && mid >= 0 && offset == VB19_EXT_HEADER_LENGTH) {
				// is VB19
				version = Version.VB19;
				vb15HeaderOffset = offset;
				// offset: 0x20 (32)
				StreamUtils.skip(in, 64);
				// offset: 0x60 (96)
				protocolName = StreamUtils.readString(in);
				writeln(String.format("[%08XH] meas.version=%s", 0, version), w);
				writeln(String.format("[%08XH] meas.mid=%d", 8, mid), w);
				writeln(String.format("[%08XH] meas.fid=%d", 12, fid), w);
				writeln(String.format("[%08XH] meas.vb15.offset=%04XH", 16, vb15HeaderOffset), w);
				writeln(String.format("[%08XH] meas.protocol.name=%s", 96, protocolName), w);
			} else if (buf[8] == (byte) 'C' && buf[9] == (byte) 'o' && buf[10] == (byte) 'n' && buf[11] == (byte) 'f'
					&& buf[12] == (byte) 'i' && buf[13] == (byte) 'g') {
				// is VB15
				version = Version.VB15;
				mid = getMIDFromFileName(fileName);
				fid = getFIDFromFileName(fileName);
				protocolName = getSequenceNameFromFileName(fileName);
				writeln(String.format("meas.version=%s", version), w);
				writeln(String.format("meas.mid=%d", mid), w);
				writeln(String.format("meas.fid=%d", fid), w);
				writeln(String.format("meas.protocol.name=%s", protocolName), w);
			}
		} finally {
			in.reset();
		}

		if (version == null) {
			throw new IOException("Failed to parse meas VB version from file: " + fileName);
		}

		Set<String> gs = new LinkedHashSet<String>();
		if (groups != null) {
			for (String group : groups) {
				gs.add(group.toLowerCase());
			}
		}
		try (BinaryInputStream bis = new BinaryInputStream(in, ByteOrder.LITTLE_ENDIAN, false)) {

			if (vb15HeaderOffset > 0) {
				// version == Version.VB19
				bis.skipFully(vb15HeaderOffset);
			}

			long offset = bis.bytesCount();

			int groupsLength = bis.readInt();

			offset = bis.bytesCount();
			int nbGroups = bis.readInt();

			writeln(String.format("[%08XH] groups.length=%d, groups.count=%d", offset, groupsLength, nbGroups), w);

			for (int i = 0; i < nbGroups; i++) {
				offset = bis.bytesCount();
				String groupName = StreamUtils.readString(bis);

				offset = bis.bytesCount();
				int groupLength = bis.readInt();

				writeln(String.format("[%08XH] group.name=%s, group.length=%d", offset, groupName, groupLength), w);

				if (metadata && (gs.isEmpty() || gs.contains(groupName.toLowerCase()))) {
					try (SizedInputStream sis = new SizedInputStream(bis, groupLength, false);
							InputStreamReader isr = new InputStreamReader(sis);
							BufferedReader br = new BufferedReader(isr)) {
						String line;
						while ((line = br.readLine()) != null) {
							writeln(line, w);
						}
					}
				} else {
					bis.skipFully(groupLength);
				}
			}
			offset = bis.bytesCount();
		}
	}

	private static void writeln(String line, Writer w) throws IOException {
		writeln(line, w, true, true);
	}

	private static void writeln(String line, Writer w, boolean lineBreak, boolean flush) throws IOException {
		w.write(line);
		if (lineBreak) {
			w.write(System.lineSeparator());
		}
		if (flush) {
			w.flush();
		}
	}

	public static void main(String[] args) throws IOException {
//		System.out.println();
//		Path rf1 = Paths.get("/Users/wliu5/Downloads/Samples/meas_MID00014_FID00893_AdjDico.dat");
//		dump(rf1, System.out, false);

//		System.out.println();
//		Path rf2 = Paths.get("/Users/wliu5/Downloads/Samples/meas_MID36_EdLineFullKlineXSLAC146V_FID33313.dat");
//		dump(rf2, System.out, false);

//		System.out.println(getSequenceNameFromFileName("meas_MID00014_FID00893_AdjDico.dat"));
//		System.out.println(getSequenceNameFromFileName("meas_MID36_EdLineFullKlineXSLAC146V_FID33313.dat"));
	}

}

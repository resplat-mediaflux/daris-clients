package unimelb.daris.siemens.raw.pet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.util.AbstractMap.SimpleEntry;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.io.DicomInputStream;

public class RawPETCTFile {

    public static final String FILE_EXT_PTR = "ptr";

    public static final String FILE_EXT_PTD = "ptd";

    public static final int DICOM_PREAMBLE_LENGTH = 132;

    public static final int DATA_TAG = 0x7fe11010;

    public static final int MAX_DICOM_REGION_SIZE = 1000000;

    public static final int MIN_DICOM_REGION_SIZE = 10000;

    private static class Buffer {

        private byte[] _b = new byte[DICOM_PREAMBLE_LENGTH];
        private int _i = 0;

        public void initialize(RandomAccessFile raf) throws IOException {
            raf.readFully(_b);
            _i = _b.length;
        }

        public int readByte(RandomAccessFile raf) throws IOException {
            int r = raf.read();
            if (r != -1) {
                put((byte) r);
            }
            return r;
        }

        public void put(byte b) {
            if (_i == _b.length) {
                shift();
            }
            _b[_i] = b;
            _i++;
        }

        private void shift() {
            for (int i = 0; i < _b.length - 1; i++) {
                _b[i] = _b[i + 1];
            }
            _i--;
        }

        public boolean isPreamble() {
            if (!"DICM".equals(new String(_b, 128, 4))) {
                return false;
            }
            for (int i = 0; i < 128; i++) {
                if (_b[i] != 0) {
                    return false;
                }
            }
            return true;
        }

    }

    public static final long getDicomOffset(Path f) throws IOException {
        return getDicomOffset(f.toFile());
    }

    public static final long getDicomOffset(File f) throws IOException {
        long fileSize = f.length();
        if (fileSize < DICOM_PREAMBLE_LENGTH) {
            return -1;
        }
        long regionSize = MIN_DICOM_REGION_SIZE;
        while (regionSize <= MAX_DICOM_REGION_SIZE) {
            long startOffset = regionSize > fileSize ? 0 : fileSize - regionSize;
            long dicomOffset = getDicomOffset(f, startOffset);
            if (dicomOffset == -1) {
                regionSize = regionSize * 10;
            } else {
                return dicomOffset;
            }
        }
        return -1;
    }

    private static final long getDicomOffset(File f, long offset) throws IOException {
        Buffer buffer = new Buffer();
        try (RandomAccessFile raf = new RandomAccessFile(f, "r")) {
            if (offset > 0) {
                raf.seek(offset);
            }
            buffer.initialize(raf);
            while (!buffer.isPreamble()) {
                int r = buffer.readByte(raf);
                if (r == -1) {
                    return -1;
                }
            }
            return raf.getFilePointer() - DICOM_PREAMBLE_LENGTH;
        }
    }

    public static final Attributes getDicomAttributes(Path f) throws IOException {
        return getDicomAttributes(f.toFile());
    }

    public static final Attributes getDicomAttributes(File f) throws IOException {
        SimpleEntry<Long, Attributes> entry = readDicomAttributes(f);
        return entry == null ? null : entry.getValue();
    }

    public static final SimpleEntry<Long, Attributes> readDicomAttributes(Path f) throws IOException {
        return readDicomAttributes(f.toFile());
    }

    public static final SimpleEntry<Long, Attributes> readDicomAttributes(File f) throws IOException {
        long dicomOffset = getDicomOffset(f);
        if (dicomOffset < 0) {
            return null;
        }
        try (FileInputStream fis = new FileInputStream(f)) {
            fis.getChannel().position(dicomOffset);
            try (BufferedInputStream bis = new BufferedInputStream(fis);
                    DicomInputStream dis = new DicomInputStream(bis)) {
                Attributes attrs = dis.readFileMetaInformation();
                dis.readAttributes(attrs, -1, DATA_TAG);
                return new SimpleEntry<Long, Attributes>(dicomOffset, attrs);
            }
        }
    }

    public static boolean hasDicomAttributes(Path f) {
        return hasDicomAttributes(f.toFile());
    }

    public static boolean hasDicomAttributes(File f) {
        try {
            long dicomOffset = getDicomOffset(f);
            return dicomOffset >= 0;
        } catch (Throwable e) {
            return false;
        }
    }

    public static boolean hasValidFileExtension(String fileName) {
        if (fileName != null) {
            fileName = fileName.toLowerCase();
            return fileName.endsWith("." + FILE_EXT_PTD) || fileName.endsWith("." + FILE_EXT_PTR);
        }
        return false;
    }

    public static void main(String[] args) throws IOException {
        // @formatter:off
        // Path f1 = Paths.get("/tmp/1.ptd");
        // Path f2 = Paths.get("/tmp/2.ptd");
        // Attributes attrs1 = getDicomAttributes(f1);
        // System.out.println(attrs1.getString(Tag.StudyDate));
        // Attributes attrs2 = getDicomAttributes(f2);
        // System.out.println(attrs2.getString(Tag.StudyDate));

        // long t1a = System.currentTimeMillis();
        // long offset1 = getDicomOffset(f1.toFile());
        // long t1b = System.currentTimeMillis();
        // long t1 = t1b-t1a;
        // System.out.printf("%s: %08x %dms%n", f1, offset1, t1);

        // long t2a = System.currentTimeMillis();
        // long offset2 = getDicomOffset(f2.toFile());
        // long t2b = System.currentTimeMillis();
        // long t2 = t2b-t2a;
        // System.out.printf("%s: %08x %dms%n", f2, offset2, t2);

        // @formatter:on
    }

}

package unimelb.daris.siemens.cli;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import org.dcm4che3.io.DicomInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import unimelb.daris.dicom.data.DicomDump;
import unimelb.daris.siemens.raw.pet.RawPETCTFile;
import unimelb.logging.Logback;

@Command(name = "siemens-raw-petct-dicom-dump", abbreviateSynopsis = true, usageHelpWidth = 120, synopsisHeading = "\nUSAGE:\n  ", descriptionHeading = "\nDESCRIPTION:\n  ", description = "Dumps the meta data header of Siemens raw MR file.", parameterListHeading = "\nPARAMETERS:\n", optionListHeading = "\nOPTIONS:\n", sortOptions = false, version = SiemensRawMRDumpCommand.VERSION, separator = " ")
public class SiemensRawPETCTDumpCommand implements Callable<Integer> {

    static final Logger logger = LoggerFactory.getLogger(SiemensRawPETCTDumpCommand.class);

    public static final String VERSION = "0.0.1";

    @Option(names = { "-h", "--help" }, usageHelp = true, description = "output usage information")
    private boolean printHelp;

    @Option(names = { "-V", "--version" }, versionHelp = true, description = "output version information")
    private boolean printVersion;

    @Option(names = { "-v", "--verbose" }, defaultValue = "false", description = "verbose mode")
    private boolean verbose;

    @Parameters(description = "The Siemens raw PET CT file.", index = "0", arity = "1", paramLabel = "RAW_PET_CT_FILE")
    private Path rawPTFile;

    @Override
    public Integer call() throws Exception {

        Logback.initRootLogger(this.verbose);

        if (!Files.exists(this.rawPTFile)) {
            throw new IllegalArgumentException("File: '" + this.rawPTFile + "' does not exist.");
        }

        if (Files.isDirectory(this.rawPTFile)) {
            throw new IllegalArgumentException("File: '" + this.rawPTFile + "' is a directory.");
        }

        try {
            long dicomOffset = RawPETCTFile.getDicomOffset(this.rawPTFile);
            if (dicomOffset < 0) {
                throw new IOException("Could not locate DICOM attributes from file: '" + this.rawPTFile + "'.");
            }
            try (FileInputStream fis = new FileInputStream(this.rawPTFile.toFile())) {
                if (dicomOffset > 0) {
                    fis.getChannel().position(dicomOffset);
                }
                try (BufferedInputStream bis = new BufferedInputStream(fis);
                        DicomInputStream dis = new DicomInputStream(bis)) {
                    new DicomDump().dump(dicomOffset, dis, RawPETCTFile.DATA_TAG);
                }
            }
        } catch (Throwable e) {
            if (e instanceof Exception) {
                throw (Exception) e;
            } else {
                throw new Exception(e);
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        System.exit(new CommandLine(new SiemensRawPETCTDumpCommand()).execute(args));
    }

}

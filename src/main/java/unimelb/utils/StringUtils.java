package unimelb.utils;

import java.util.Collection;

public class StringUtils {

	public static String trimLeft(String str, String prefix, boolean repeat) {
		if (prefix == null || prefix.isEmpty() || str == null || str.isEmpty()) {
			return str;
		}
		String r = str;
		if (repeat) {
			while (r.startsWith(prefix)) {
				r = r.substring(prefix.length());
			}
		} else {
			if (r.startsWith(prefix)) {
				r = r.substring(prefix.length());
			}
		}
		return r;
	}

	public static String trimRight(String str, String suffix, boolean repeat) {
		if (suffix == null || suffix.isEmpty() || str == null || str.isEmpty()) {
			return str;
		}
		String r = str;
		if (repeat) {
			while (r.endsWith(suffix)) {
				r = r.substring(0, r.length() - suffix.length());
			}
		} else {
			if (r.endsWith(suffix)) {
				r = r.substring(0, r.length() - suffix.length());
			}
		}
		return r;
	}

	public static String trimRight(String str, String suffix) {
		return trimRight(str, suffix, true);
	}

	public static String trimRight(String str, char c) {
		return trimRight(str, String.valueOf(c));
	}

	public static String trimLeft(String str, String prefix) {
		return trimLeft(str, prefix, true);
	}

	public static String trimLeft(String str, char c) {
		return trimLeft(str, String.valueOf(c));
	}

	public static String trim(String str, String c) {
		if (str != null & !str.isEmpty()) {
			return trimLeft(trimRight(str, c), c);
		}
		return str;
	}

	public static String trim(String str, char c) {
		return trim(str, String.valueOf(c));
	}

	public static String repeat(char c, int n) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n; i++) {
			sb.append(c);
		}
		return sb.toString();
	}

	public static String join(Collection<String> strings, String separator) {
		if (strings != null && !strings.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			for (String s : strings) {
				if (first) {
					first = false;
				} else {
					sb.append(separator);
				}
				sb.append(s);
			}
			return sb.toString();
		}
		return null;
	}

	public static void main(String[] args) {
	}

}

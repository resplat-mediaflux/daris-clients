package unimelb.utils;

public class AbortedException extends RuntimeException {

	private static final long serialVersionUID = -1526248700934153114L;

	public AbortedException(String msg) {
		super(msg);
	}

	public AbortedException() {
	}

}

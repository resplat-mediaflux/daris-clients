package unimelb.utils;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathUtils {

	public static final char SLASH_CHAR = '/';
	public static final char BACKSLASH_CHAR = '\\';
	public static final String SLASH = "/";
	public static final String BACKSLASH = "\\";

	public static String toSystemIndependentPath(String path) {
		if (path != null && !path.isEmpty()) {
			String p = path.replace('\\', '/').replaceAll("/{2,}", "/");
			return StringUtils.trimRight(p, "/");
		}
		return path;
	}

	public static String toSystemIndependentPath(Path path) {
		return toSystemIndependentPath(path.toString());
	}

	public static String toSystemIndependentPath(File f) {
		return toSystemIndependentPath(f.getPath());
	}

	public static String getParentPath(String path) {
		if (path != null && !path.isEmpty()) {
			return null;
		}
		Path parent = Paths.get(path).getParent();
		if (parent != null) {
			return parent.toString();
		}
		return null;
	}

	public static String getFileName(String path) {
		if (path == null || path.isEmpty()) {
			return path;
		}
		path = path.trim();
		String sep;
		if (path.indexOf(SLASH) != -1) {
			sep = SLASH;
			path = StringUtils.trimRight(path, SLASH);
		} else if (path.indexOf(BACKSLASH) != -1) {
			sep = BACKSLASH;
			path = StringUtils.trimRight(path, BACKSLASH);
		} else {
			return path;
		}
		int idx = path.lastIndexOf(sep);
		if (idx != -1 && !path.equals(sep)) {
			return path.substring(idx + 1);
		} else {
			return null;
		}
	}

	public static String getFileExtension(String fileName) {
		if (fileName != null) {
			int idx = fileName.lastIndexOf('.');
			if (idx > 0) {
				return fileName.substring(idx + 1);
			}
		}
		return null;
	}

	public static void main(String[] args) {
	}

}

package unimelb.utils;

public interface Action {

    void execute();

}

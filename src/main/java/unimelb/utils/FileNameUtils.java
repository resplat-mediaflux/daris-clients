package unimelb.utils;

import java.io.File;
import java.nio.file.Path;

public class FileNameUtils {

	public static String getFileExtension(Path f) {
		return getFileExtension(f.toFile());
	}

	public static String getFileExtension(File f) {
		return getFileExtension(f.getName());
	}

	public static String getFileExtension(String fileName) {
		if (fileName != null) {
			int idx = fileName.lastIndexOf('.');
			if (idx > 0) {
				String ext = fileName.substring(idx + 1);
				if (ext != null && !ext.isEmpty() && ext.indexOf('/') == -1 && ext.indexOf('\\') == -1) {
					return ext.trim();
				}
			}
		}
		return null;
	}

	public static String removeFileExtension(String fileName) {
		if (fileName != null) {
			int idx = fileName.lastIndexOf('.');
			if (idx > 0) {
				String ext = fileName.substring(idx + 1);
				if (ext != null && !ext.isEmpty() && ext.indexOf('/') == -1 && ext.indexOf('\\') == -1) {
					return fileName.substring(0, idx);
				}
			}
		}
		return null;
	}

	public static String tidySafeFileName(String name, char separator, int maxLength) {
		name = name.trim().replaceAll("\\ {2,}+", String.valueOf(separator));
		name = name.replaceAll("[\\/:*?\"<>|]", String.valueOf(separator));
		name = name.replaceAll("\\ {2,}+", String.valueOf(separator)).trim();
		if (maxLength > 0 && name.length() > maxLength) {
			return name.substring(0, maxLength).trim();
		} else {
			return name;
		}
	}

	public static String tidySafeFileName(String name, int maxLength) {
		return tidySafeFileName(name, ' ', maxLength);
	}

	public static String tidySafeFileName(String name) {
		return tidySafeFileName(name, ' ', -1);
	}

	public static String tidySafeFileName(String name, char separator) {
		return tidySafeFileName(name, separator, -1);
	}
}

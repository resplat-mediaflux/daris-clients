package unimelb.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import unimelb.utils.AbortedException;

public class StreamCopy {

	public static final int DEFAULT_BUFFER_SIZE = 8192;

	public static void copy(InputStream in, OutputStream out, TransferProgressListener pl, AbortCheck ac)
			throws IOException {
		copy(in, -1, out, pl, ac);
	}

	public static void copy(InputStream in, long length, OutputStream out, TransferProgressListener pl, AbortCheck ac)
			throws IOException {

		if (length == 0) {
			// zero-sized
			return;
		}

		byte[] b = new byte[DEFAULT_BUFFER_SIZE];
		int n;
		if (length < 0) {
			// unsized
			while ((n = in.read(b)) >= 0) {
				if (ac != null && ac.aborted()) {
					throw new AbortedException();
				}
				out.write(b, 0, n);
				if (pl != null) {
					pl.incTransferredBytes(n);
				}
			}
		} else {
			// sized
			long remaining = length;
			while (remaining > 0 && (n = remaining < b.length ? in.read(b, 0, (int) remaining) : in.read(b)) >= 0) {
				remaining -= n;
				if (ac != null && ac.aborted()) {
					throw new AbortedException();
				}
				out.write(b, 0, n);
				if (pl != null) {
					pl.incTransferredBytes(n);
				}
			}
		}
	}

	public static void copy(File f, OutputStream out, TransferProgressListener pl, AbortCheck ac) throws IOException {
		try (FileInputStream fis = new FileInputStream(f); BufferedInputStream bis = new BufferedInputStream(fis)) {
			copy(bis, out, pl, ac);
		}
	}

	public static void copy(Path f, OutputStream out, TransferProgressListener pl, AbortCheck ac) throws IOException {
		try (InputStream is = Files.newInputStream(f); BufferedInputStream bis = new BufferedInputStream(is)) {
			copy(bis, out, pl, ac);
		}
	}

	public static void copy(InputStream in, Path f, TransferProgressListener pl, AbortCheck ac) throws IOException {
		copy(in, -1, f, pl, ac);
	}

	public static void copy(InputStream in, long length, Path f, TransferProgressListener pl, AbortCheck ac)
			throws IOException {
		try (OutputStream os = Files.newOutputStream(f); BufferedOutputStream bos = new BufferedOutputStream(os)) {
			copy(in, length, bos, pl, ac);
		}
	}

	public static void copy(InputStream in, File f, TransferProgressListener pl, AbortCheck ac) throws IOException {
		copy(in, -1, f, pl, ac);
	}

	public static void copy(InputStream in, long length, File f, TransferProgressListener pl, AbortCheck ac)
			throws IOException {
		try (FileOutputStream fos = new FileOutputStream(f); BufferedOutputStream bos = new BufferedOutputStream(fos)) {
			copy(in, length, bos, pl, ac);
		}
	}

}

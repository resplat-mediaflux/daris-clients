package unimelb.io;

public interface TransferProgressListener {

	void incTransferredBytes(long nbBytes);
}

package unimelb.io;

import java.util.concurrent.atomic.AtomicLong;

public abstract class AbstractCollectionTransferProgressListener implements CollectionTransferProgressListener {

	private AtomicLong _totalFiles;
	private AtomicLong _totalBytes;
	private AtomicLong _transferredFiles;
	private AtomicLong _transferredBytes;

	public AbstractCollectionTransferProgressListener(long totalFiles, long totalBytes) {
		_totalFiles = new AtomicLong(totalFiles);
		_totalBytes = new AtomicLong(totalBytes);
		_transferredFiles = new AtomicLong(0);
		_transferredBytes = new AtomicLong(0);
	}

	public AbstractCollectionTransferProgressListener() {
		this(0, 0);
	}

	@Override
	public void beginCollectionTransfer(long totalFiles, long totalBytes) {
		_totalFiles.set(totalFiles);
		_totalBytes.set(totalBytes);
	}

	@Override
	public void incTotalFiles(long nbFiles) {
		_totalFiles.getAndAdd(nbFiles);
	}

	@Override
	public void incTotalBytes(long nbBytes) {
		_totalBytes.getAndAdd(nbBytes);
	}

	@Override
	public void incTransferredFiles(long nbFiles) {
		_transferredFiles.getAndAdd(nbFiles);
	}

	@Override
	public void incTransferredBytes(long nbBytes) {
		_transferredBytes.getAndAdd(nbBytes);
	}

	public long transferredFiles() {
		return _transferredFiles.get();
	}

	public long transferredBytes() {
		return _transferredBytes.get();
	}

	public long totalFiles() {
		return _totalFiles.get();
	}

	public long totalBytes() {
		return _totalBytes.get();
	}

}

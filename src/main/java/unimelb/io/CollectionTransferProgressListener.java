package unimelb.io;

public interface CollectionTransferProgressListener extends TransferProgressListener {

	void beginCollectionTransfer(long totalFiles, long totalBytes);

	void incTotalFiles(long nbFiles);

	void incTotalBytes(long nbBytes);

	void incTransferredFiles(long nbFiles);

	void inform(String message);

	void endCollectionTransfer();
}

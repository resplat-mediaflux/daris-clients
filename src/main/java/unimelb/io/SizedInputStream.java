package unimelb.io;

import java.io.IOException;
import java.io.InputStream;

public class SizedInputStream extends CountedInputStream {

	private final long _size;
	private final boolean _propagateClose;

	public SizedInputStream(InputStream in, long size, boolean propagateClose) {
		super(in);
		_size = size;
		_propagateClose = propagateClose;
	}

	public SizedInputStream(InputStream in, long size) {
		this(in, size, false);
	}

	@Override
	public int read() throws IOException {
		if (_size >= 0 && bytesCount() >= _size) {
			return -1;
		}
		return super.read();
	}

	@Override
	public int read(byte b[]) throws IOException {
		return read(b, 0, b.length);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (_size >= 0 && bytesCount() >= _size) {
			return -1;
		}
		if (_size > 0 && bytesCount() + len > _size) {
			len = (int) (_size - bytesCount());
		}
		return super.read(b, off, len);
	}

	@Override
	public long skip(long n) throws IOException {
		if (_size >= 0 && bytesCount() >= _size) {
			return 0;
		}
		if (_size > 0 && bytesCount() + n > _size) {
			n = _size - bytesCount();
		}
		return super.skip(n);
	}

	@Override
	public int available() throws IOException {
		if (remaining() == 0) {
			return 0;
		}
		int a = in.available();
		if (_size >= 0 && a > _size) {
			return (int) _size;
		} else {
			return a;
		}
	}

	public long remaining() {
		if (_size < 0) {
			return -1;
		}
		if (_size == 0) {
			return 0;
		}
		// _size > 0
		if (bytesCount() >= _size) {
			return 0;
		} else {
			return _size - bytesCount();
		}
	}

	@Override
	public void close() throws IOException {
		if (_propagateClose) {
			in.close();
		}
	}
}
package unimelb.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

public class StreamUtils {

	public static final int BUFFER_SIZE = 8192;

	public static void readFully(InputStream in, byte[] b, int off, int len) throws IOException {
		if (len < 0) {
			throw new IndexOutOfBoundsException();
		}
		int n = 0;
		while (n < len) {
			int count = in.read(b, off + n, len - n);
			if (count < 0)
				throw new EOFException();
			n += count;
		}
	}

	public static void readFully(InputStream in, byte[] b) throws IOException {
		readFully(in, b, 0, b.length);
	}

	public static void skip(InputStream in, long length) throws IOException {
		byte[] buffer = new byte[BUFFER_SIZE];
		long remaining = length;
		long totalRead = 0;
		int read = 0;
		while (remaining > 0
				&& (read = in.read(buffer, 0, buffer.length > remaining ? (int) remaining : buffer.length)) != -1) {
			remaining -= read;
			totalRead += read;
		}
		if (remaining > 0) {
			throw new EOFException(
					"Skipped " + totalRead + "/" + length + " bytes. " + remaining + " bytes remaining.");
		}
	}

	public static long exhaust(InputStream in) throws IOException {
		long total = 0;
		long read;
		byte[] buf = new byte[BUFFER_SIZE];
		while ((read = in.read(buf)) != -1) {
			total += read;
		}
		return total;
	}
	
	public static String readString(InputStream in) throws IOException {
		StringBuilder sb = new StringBuilder();
		byte b = (byte) in.read();
		while (b != 0) {
			sb.append((char) b);
			b = (byte) in.read();
		}
		return sb.toString().trim();
	}

}

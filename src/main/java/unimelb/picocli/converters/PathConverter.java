package unimelb.picocli.converters;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import picocli.CommandLine;

public class PathConverter implements CommandLine.ITypeConverter<Path> {

    private final boolean _mustExist;
    private final boolean _mustNotExist;
    private final boolean _mustBeDirectory;
    private final boolean _mustBeRegularFile;
    private final boolean _toAbsolute;

    public PathConverter() {
        this(false, false, false, false, false);
    }

    public PathConverter(boolean mustExist, boolean mustNotExist, boolean mustBeDirectory, boolean mustBeRegularFile,
            boolean toAbsolute) {
        _mustBeDirectory = mustBeDirectory;
        _mustBeRegularFile = mustBeRegularFile;
        if (mustBeDirectory || mustBeRegularFile) {
            _mustExist = true;
            _mustNotExist = false;
        } else {
            _mustExist = mustExist;
            _mustNotExist = mustNotExist;
        }
        _toAbsolute = toAbsolute;
    }

    @Override
    public Path convert(String value) throws Exception {
        Path path = Paths.get(value);
        boolean exists = Files.exists(path);
        boolean isDir = Files.isDirectory(path);
        boolean isRegularFile = Files.isRegularFile(path);
        String type = isDir ? "directory" : "file";
        if (_mustExist && !exists) {
            throw new IllegalArgumentException(type + ": '" + value + "' does not exist.");
        }
        if (_mustNotExist && exists) {
            throw new IllegalArgumentException(type + ": '" + value + "' already exists.");
        }
        if (_mustBeDirectory && !isDir) {
            throw new IllegalArgumentException("'" + value + "' is not a directory.");
        }
        if (_mustBeRegularFile && !isRegularFile) {
            throw new IllegalArgumentException("'" + value + "' is not a regular file.");
        }
        return _toAbsolute ? path.toAbsolutePath() : path;
    }

    public static class MustExist extends PathConverter {
        public MustExist() {
            super(true, false, false, false, false);
        }
    }

    public static class MustNotExist extends PathConverter {
        public MustNotExist() {
            super(false, true, false, false, false);
        }
    }

    public static class MustBeDirectory extends PathConverter {
        public MustBeDirectory() {
            super(true, false, true, false, false);
        }
    }

    public static class MustBeRegularFile extends PathConverter {
        public MustBeRegularFile() {
            super(true, false, false, true, false);
        }
    }

}

@echo off

setlocal

pushd "%~dp0..\"
set "DARIS_CLIENTS_HOME=%cd%"
popd

set "LIB=%DARIS_CLIENTS_HOME%\lib"
set "JRE=%DARIS_CLIENTS_HOME%\jre"

if exist "%JRE%\" (
    set "JAVA=%JRE%\bin\java"
) else (
    set JAVA=java
)

"%JAVA%" -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:+UseStringDeduplication -Xmx200m -cp "%LIB%;%LIB%/*" unimelb.daris.client.cli.StudyCreateCommand %*

endlocal

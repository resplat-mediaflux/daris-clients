#!/bin/bash
#
# This is a wrapper script to maintain backward compatibility to the old mbcpetct-raw-upload client.
#
SRC="/home/meduser/PET-CT/Raw"
CONFIG="$HOME/.Arcitecta/petct-raw-upload.cfg"
LOGDIR="/mnt/stor-q/508-MBIU/PET-CT/Raw_Archive_Upload_Logs"
PID=""
DELETE=1
CHECKSUM=1
EXPIRE=0
NB_WORKERS=2
VERBOSE=0

UPLOAD=$(which siemens-raw-petct-upload)
DARIS_CLIENTS_HOME="$HOME/.local/daris-clients"
[[ -z $UPLOAD ]] && UPLOAD="${DARIS_CLIENTS_HOME}/bin/unix/siemens-raw-petct-upload"

function print_usage() {
    echo ""
    echo "Usage:"
    echo "  $(basename $0) [options..]"
    echo ""
    echo "Options:"
    echo "  -help          Displays this help."
    echo "  -path          The path for the directory holding the data or a single file. Default is ${SRC}"
    echo "  -id            Specifies the PSSD data model (DaRIS) should be used and that this is the citeable ID that the Study should be associated with. Can be depth 2 (the repository), 3 (a Project) or 4 (a Subject)."
    echo "  -no-log        Disables writing any log file."
    echo "  -logpath       Specify the directory for log files to be written in. Default is ${LOGDIR}"
    echo "  -no-chksum     Disables check sum validation of uploaded file"
    echo "  -no-delete     Disables the deletion of the input file after check sum validation"
    echo "  -expire        Specifies that meta-data is to be attached to the file with an expiry data of 1 year after acquisition"
    echo ""
}

while [[ $# -gt 0 ]]; do
    arg="$1"
    case $arg in
    -id)
        PID="$2"
        shift
        shift
        ;;
    -path)
        SRC="$2"
        shift
        shift
        ;;
    -logpath)
        LOGDIR="$2"
        shift
        shift
        ;;
    -no-log)
        LOGDIR=""
        shift
        ;;
    -no-checksum)
        CHECKSUM=0
        shift
        ;;
    -no-delete)
        DELETE=0
        shift
        ;;
    -expire)
        EXPIRE=1
        shift
        ;;
    -verbose | --verbose)
        VERBOSE=1
        shift
        ;;
    -h | -help | --help)
        print_usage
        exit 0
        ;;
    -no-decrypt | -sleep)
        shift # ignore
        ;;
    *)
        echo "Error: Unexpected argument: $1"
        print_usage
        exit 1
        ;;
    esac
done

# input file or directory
if [[ -z "${SRC}" ]]; then
    echo "Error: missing input directory or file."
    print_usage
    exit 1
else
    if [[ ! -e ${SRC} ]]; then
        echo "Error: input file/directory '${SRC}' does not exist"
        exit 1
    fi
fi

COMMAND="${UPLOAD}"
# --nb-workers
if [[ ${NB_WORKERS} -gt 1 ]]; then
    COMMAND="${COMMAND} --nb-workers ${NB_WORKERS}"
fi

# --verbose
if [[ ${VERBOSE} -ne 0 ]]; then
    COMMAND="${COMMAND} --verbose"
fi

# --config <file>
if [[ ! -z "${CONFIG}" ]]; then
    if [[ -f ${CONFIG} ]]; then
        COMMAND="${COMMAND} --config ${CONFIG}"
    else
        echo "Error: config file '${CONFIG}' does not exist"
        exit 1
    fi
fi

# --id <cid>
[[ -n "${PID}" ]] && COMMAND="${COMMAND} --id ${PID}"

# --log-dir <dir>
if [[ ! -z "${LOGDIR}" ]]; then
    if [[ -d ${LOGDIR} ]]; then
        COMMAND="${COMMAND} --log-dir ${LOGDIR}"
    else
        echo "Error: log directory '${LOGDIR}' does not exist"
        exit 1
    fi
fi

# --delete
if [[ ${DELETE} -eq 1 ]]; then
    COMMAND="${COMMAND} --delete"
else
    COMMAND="${COMMAND} --no-delete"
fi

# --checksum
if [[ ${CHECKSUM} -eq 0 ]]; then
    COMMAND="${COMMAND} --no-checksum"
else
    COMMAND="${COMMAND} --checksum"
fi

# --expire
if [[ ${EXPIRE} -eq 1 ]]; then
    COMMAND="${COMMAND} --expire"
else
    COMMAND="${COMMAND} --no-expire"
fi

COMMAND="${COMMAND} ${SRC}"
echo ""
echo "${COMMAND}"
echo ""

${COMMAND}

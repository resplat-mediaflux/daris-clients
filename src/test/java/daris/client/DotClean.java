package daris.client;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;

public class DotClean {

	public static final String DOT_UNDERSCORE = "._";
	public static final String DOT_DS_STORE = ".DS_Store";
	public static final long DOT_UNDERSCORE_FILE_SIZE = 4096;
	public static boolean debug = false;

	public static void clean(Path dir) throws Throwable {
		clean(dir, false);
	}

	public static void clean(Path dir, boolean followSymlinks) throws Throwable {
		Files.walkFileTree(dir,
				followSymlinks ? EnumSet.of(FileVisitOption.FOLLOW_LINKS) : EnumSet.noneOf(FileVisitOption.class),
				Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
						try {
							// delete .DS_Store file, or ._* file if the file size is 4096
							String fileName = file.getFileName().toString();
							if (fileName.equals(DOT_DS_STORE) || (fileName.startsWith(DOT_UNDERSCORE)
									&& Files.size(file) == DOT_UNDERSCORE_FILE_SIZE)) {
								System.out.println("Deleting file: " + file);
								Files.deleteIfExists(file);
								return FileVisitResult.CONTINUE;
							}
							if (debug) {
								System.out.println("Skipped file: " + file);
							}
						} catch (Throwable e) {
							e.printStackTrace();
						}
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult visitFileFailed(Path file, IOException ioe) {
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult postVisitDirectory(Path dir, IOException ioe) {
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
						return super.preVisitDirectory(dir, attrs);
					}
				});
	}

	public static void main(String[] args) throws Throwable {
//		Path dir = Paths.get(args[0]);
		Path dir = Paths.get("/tmp/test1");
		boolean followSymlinks = false;
		debug = true;
		clean(dir, followSymlinks);
	}

}

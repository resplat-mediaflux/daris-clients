# daris-clients
A set of command line utilities to manipulate data in DaRIS system.

---
## I. Utilities (developed by RCS@UniMelb)

### 1. daris-download
A command line utility to download data from DaRIS. 

  * [Usage](https://gitlab.unimelb.edu.au/resplat-mediaflux/daris-clients/-/blob/master/docs/daris-download.md)

### 2. daris-subject-create
A command line utility to create a subject object in a DaRIS project.

  * [Usage](https://gitlab.unimelb.edu.au/resplat-mediaflux/daris-clients/-/blob/master/docs/daris-subject-create.md)

### 3. daris-subject-update
A command line utility to update meta data for a specific subject.

  * [Usage](https://gitlab.unimelb.edu.au/resplat-mediaflux/daris-clients/-/blob/master/docs/daris-subject-update.md)

### 4. daris-study-create
A command line utility to create a study object.

  * [Usage](https://gitlab.unimelb.edu.au/resplat-mediaflux/daris-clients/-/blob/master/docs/daris-study-create.md)

### 5. daris-study-update
A command line utility to update a study object.

  * [Usage](https://gitlab.unimelb.edu.au/resplat-mediaflux/daris-clients/-/blob/master/docs/daris-study-update.md)

### 6. daris-dataset-create
A command line utility to create a dataset object by uploading local file(s) and setting meta data.

  * [Usage](https://gitlab.unimelb.edu.au/resplat-mediaflux/daris-clients/-/blob/master/docs/daris-dataset-create.md)

---
## II. Configuration

Strictly, you don't need to do much in the way of preparation, but creating a configuration file makes this process a lot quicker and easier.

You can put your conf file anywhere you want, the default location is
  * On Linux or Mac OS,  **$HOME/.Arcitecta/mflux.cfg** 
  * On Windows, **%userprofile%\.Arcitecta\mflux.cfg**

If you put it there, the utilitities will find it. If you don't, you can use the flag `--mf.config <path to config file>` to point to your config.

The most basic config should look like this:

    host=daris.researchsoftware.unimelb.edu.au
    port=443
    transport=https
    domain=YOUR_DOMAIN
    user=YOUR_USERNAME

If you have got a auth token for the client, the confi should be like below:

    host=daris.researchsoftware.unimelb.edu.au
    port=443
    transport=https
    token=XXX_TOKEN_XXX





